//PACKAGE
package be.utils;

//IMPORTS
import be.seguridad.Seguridad;
import java.io.FileInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

//CLASS
public class Config {
    
    //CONSTS
    public static String SEPARATOR              = "\t";
    public static String NEWLINE                = "\n";
    
    //PROPERTIES
    private Properties  properties;
    private String      currentUser;
    private String      currentDirectory;
    
    //STATIC INSTANCE
    private static Config config;
    
    //CONSTRUCTORS
    private Config () {
        init();
    }
    
    //INIT
    private void init() {        
	properties = new Properties();
	InputStream input = null;
	try {                        
            input = new FileInputStream(this.getClass().getClassLoader().getResource("").getPath()+"/config.properties");
            // load a properties file
            properties.load(input);
            String encryptedFields = _$("BE_ENCRYPTED_FIELDS");
            if (encryptedFields != null) {
                String[] fields = encryptedFields.split(",");               
                String value;
                for (String field : fields) {                   
                    field = field.trim();
                    value = _$(field);
                    if (value != null) {                        
                        value = Seguridad.decrypt(value);
                        _$$(field, value);                        
                    }
                }
            }
        } catch (Exception e) {
            //Util.log(e.getMessage(), "Config", "init");
	} finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    //Util.log(e.getMessage(), "Config", "init");
                }
            }
	}        
    }
    
    //BASIC METHODS
    private static Config _instance() {
        if (config==null) config = new Config();
        return config;
    }
    
    //ADVANCED METHODS
    private void _$$(String param, String value) {
        properties.setProperty(param, value);
    }
    
    private String _$(String param) {
        return properties.containsKey(param) ? (String) properties.get(param) : null;
    }
    
    private String _date() {
        Date date = new Date();            
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        return format.format(date);        
    }

    private String _dateTime() {
        Date date = new Date();            
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss");
        return format.format(date);        
    }
    
    private void _user(String user) {
        currentUser = user;
    }
    
    private String _user() {
        if (currentUser==null) currentUser = System.getProperty("user.name");
        return currentUser;
    }
    
    private String _directory() {
        return currentDirectory;
    }
      
    //STATICS METHODS
    public static String $(String label) {
        return _instance()._$(label);
    }    
    
    public static String date() {
        return _instance()._date();
    }
    
    public static String dateTime() {
        return _instance()._dateTime();
    }
    
    public static String user() {
        return _instance()._user();
    }
    
    public static void user(String user) {
        _instance()._user(user);
    }
    
    public static String directory() {        
        return _instance()._directory();
    }      
}
