//PACKAGE
package be.utils;

//IMPORTS

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;


//CLASS
public class Etiqueta {
    //CONSTS
    
    //PROPERTIES
    private Properties properties;
    //
    private static Etiqueta labels;
    
    //CONSTRUCTORS
    private Etiqueta () {
        init();
    }
    //INIT
    private void init() {
	properties = new Properties();
	InputStream input = null;
	try {                        
            input = new FileInputStream(this.getClass().getClassLoader().getResource("").getPath()+"/Etiqueta.properties");
            // load a properties file
            properties.load(input);                        
        } catch (Exception e) {
            Util.log(this, e);
	} finally {
            if (input != null) {
                try {
                    input.close();
                } catch (Exception e) {
                    Util.log(this, e);
                }
            }
	}        
    }
    //BASIC METHODS
    private static Etiqueta _instance() {
        if (labels==null) labels = new Etiqueta();
        return labels;
    }
    //ADVANCED METHODS
    private String _$(String label) {
        return properties.containsKey(label) ? (String) properties.get(label) : (String) properties.get("BE_VPOS_LABEL_NOT_FOUND") + ": " + label;
    }
    //STATICS METHODS
    public static String $(String label) {
        return _instance()._$(label);
    }
    public static String ERROR() {
        return _instance()._$("BE_ERROR");
    }
    public static String SUCCESS() {
        return _instance()._$("BE_SUCCESS");
    }
}
