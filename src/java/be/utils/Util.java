//PACKAGE
package be.utils;

//IMPORTS
import be.entidades.Usuario;
import java.io.File;
import java.io.PrintWriter;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.spec.RSAPublicKeySpec;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.SecureRandom;
import java.text.NumberFormat;
import java.util.Locale;
import org.owasp.esapi.ESAPI;

//CLASS
public class Util {

    //CONSTS
    // Maxim: Copied from UUID implementation :)
    private static volatile SecureRandom numberGenerator = null;
    private static final long MSB = 0x8000000000000000L;

    //PROPERTIES
    private Logger log;
    private PrintWriter writer;

    //INSTANCE
    private static Util util;

    //CONSTRUCTORS
    private Util() {
        init();
    }

    //INIT
    private void init() {
        log = Logger.getAnonymousLogger();
        String filename = Config.$("BE_LOG_FILENAME") + "-" + Config.dateTime() + ".log";
        try {
            SimpleFormatter format = new SimpleFormatter();
            FileHandler file = new FileHandler(filename);
            file.setFormatter(format);
            log.addHandler(file);

            File archivoLog = new File(filename + "1");
            writer = new PrintWriter(archivoLog);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //BASIC METHODS
    private static Util instancia() {
        if (util == null) {
            util = new Util();
        }
        return util;
    }

    //ADVANCED METHODS
    private void _log(Object instance, Object message) {
        try {
            String              timestamp      = _getCurrentDateTime();
            StringBuilder       tag            = new StringBuilder();
            StringBuilder       trace          = new StringBuilder();            
            String              msg            = "";
            try {
                msg = ((Exception) message).getLocalizedMessage() + " - " + message.toString();
                
                ((Exception) message).printStackTrace();
                
            } catch(Exception e) {
                msg =  message.toString();
            }
            StackTraceElement[] stack          = Thread.currentThread().getStackTrace();
            StackTraceElement   currentElement = null;
            String              classname      = null;
            String              method         = null;
            String              linenumber     = null;

            for(int i=3; i<stack.length; i++) {
                currentElement = stack[i];
                classname      = currentElement.getClassName();
                method         = currentElement.getMethodName();
                linenumber     = Integer.toString(currentElement.getLineNumber()); 
                if (i>1) trace.append(" -> ");           
                trace.append("(");
                trace.append(classname);
                trace.append(".");
                trace.append(method);
                trace.append("[");
                trace.append(linenumber);
                trace.append("])");
                if (classname.equals("javax.servlet.http.HttpServlet")) i = stack.length;            
            }

            currentElement = stack[3];
            classname      = currentElement.getClassName();
            method         = currentElement.getMethodName();
            linenumber     = Integer.toString(currentElement.getLineNumber());

            if (classname != null && !classname.equals("")) {                   
                tag.append("Clase..: ");
                tag.append(classname);
                if (method != null && !method.equals("")) {
                    tag.append("]\n[Metodo.: ");
                    tag.append(method);
                    tag.append("]\n[Linea..: ");
                    tag.append(linenumber);                
                }        
            }
            String record = "\n\nLOG:\n[Tiempo.: " + timestamp + "]\n[" + tag + "]\n[Mensaje: " + msg + "]\n[Traza..: " + trace + "]\n\n";            
            log.info(record);
            System.out.println(record);
        } catch(Exception e) {            
            log.info(e.toString());            
            System.out.println(e.toString());
        }
    }

    private String _getLongDate(Date date) {
        DateFormat format = new SimpleDateFormat("E, dd/MM/yyyy");
        String response = format.format(date);
        return response;
    }

    private String _getDate(Date date) {
        DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String response = format.format(date);
        return response;
    }

    private String _getTime(Date date) {
        DateFormat format = new SimpleDateFormat("HH:mm:ss a");
        String response = format.format(date);
        return response;
    }

    private String _getDateTime(Date date) {
        return _getDate(date) + " " + _getTime(date);
    }
    
    private String _getCurrentDate() {
        //Calendar.getInstance().getTime();
        return _getDate(new Date());
    }

    private String _getCurrentTime() {
        return _getTime(new Date());
    }

    private String _getCurrentDateTime() {
        Date date = new Date();
        return _getDateTime(date);
    }

    private String _getCurrentLongDate() {
        Date date = new Date();
        return _getLongDate(date);
    }

    private String _getYear(Date date) {
        DateFormat format = new SimpleDateFormat("yyyy");
        String response = format.format(date);
        return response;
    }

    private String _getMonth(Date date) {
        DateFormat format = new SimpleDateFormat("MM");
        String response = format.format(date);
        return response;
    }

    private String _getDay(Date date) {
        DateFormat format = new SimpleDateFormat("dd");
        String response = format.format(date);
        return response;
    }

    private String _getCurrentYear() {
        Date date = new Date();
        return _getYear(date);
    }

    private String _getCurrentMonth() {
        Date date = new Date();
        return _getMonth(date);
    }

    private String _getCurrentDay() {
        Date date = new Date();
        return _getDay(date);
    }

    //STATICS METHODS
    public static void log(Object instance, String message) {
        instancia()._log(instance, message);
    }

    public static void log(Object instance, Exception e) {
        instancia()._log(instance, e);
    }

    public static String getLongCurrentDate() {
        return instancia()._getCurrentLongDate();
    }

    public static String getCurrentDate() {
        return instancia()._getCurrentDate();
    }

    public static String getCurrentTime() {
        return instancia()._getCurrentTime();
    }

    public static String getCurrentDateTime() {
        return instancia()._getCurrentDateTime();
    }

    public static String getDate(Date date) {
        return instancia()._getDate(date);
    }

    public static String getTime(Date date) {
        return instancia()._getTime(date);
    }

    public static String getDateTime(Date date) {
        return instancia()._getDateTime(date);
    }

    public static String getDate(String date) {        
        String result = null;
        try { 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            result = instancia()._getDate(dateFormat.parse(date));
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result;
    }

    public static String getTime(String date) {
        String result = null;
        try { 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            result = instancia()._getTime(dateFormat.parse(date));
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result;
    }

    public static String getDateTime(String date) {
        String result = null;
        try { 
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            result = instancia()._getDateTime(dateFormat.parse(date));
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result;
    }    
    
    public static String getYear(Date date) {
        return instancia()._getYear(date);
    }

    public static String getMonth(Date date) {
        return instancia()._getMonth(date);
    }

    public static String getDay(Date date) {
        return instancia()._getDay(date);
    }

    public static String getCurrentYear() {
        return instancia()._getCurrentYear();
    }

    public static String getCurrentMonth() {
        return instancia()._getCurrentMonth();
    }

    public static String getCurrentDay() {
        return instancia()._getCurrentDay();
    }

    public static String checkNull(Object object) {
        return checkNull(object, false);
    }

    public static String checkNull(Object object, boolean show) {
        return object == null || object.equals("") ? (show ? "null" : "") : object.toString();
    }

    public static String ruta(HttpServletRequest request, String url) {
        ServletContext context = request.getServletContext();
        return context.getRealPath(url);
    }

    public static void asignarUsuarioActual(HttpServletRequest request, Usuario user) {
        request.getSession().setAttribute("CURRENT_USER", user);
    }

    public static Usuario obtenerUsuarioActual(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return (Usuario) session.getAttribute("CURRENT_USER");
    }

    public static void logout(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.setAttribute("CURRENT_USER", null);
        session.invalidate();
        session = request.getSession(true);
    }

    public static String obtenerAccion(HttpServletRequest request) {
        return (String) request.getParameter("a");
    }

    public static KeyPair obtenerLlaves(HttpServletRequest request) {
        return (KeyPair) request.getSession().getAttribute("KEYS");
    }

    public static void asignarLlaves(HttpServletRequest request, KeyPair keys) {
        request.getSession().setAttribute("KEYS", keys);
    }

    public static String[] obtenerParLlaves(HttpServletRequest request) {
        String[] result = null;
        try {
            KeyPair keys = obtenerLlaves(request);
            if (keys == null) {
                KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
                keyPairGenerator.initialize(1024);
                keys = keyPairGenerator.generateKeyPair();
                asignarLlaves(request, keys);
            }

            KeyFactory factory = KeyFactory.getInstance("RSA");
            RSAPublicKeySpec publicKeySpec = factory.getKeySpec(keys.getPublic(), RSAPublicKeySpec.class);
            String publicKeyMod = publicKeySpec.getModulus().toString(16);
            String publicKeyExp = publicKeySpec.getPublicExponent().toString(16);

            result = new String[2];
            result[0] = publicKeyMod;
            result[1] = publicKeyExp;

        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result;
    }

    public static String serializar(Properties properties) {
        StringBuilder result = null;
        if (properties != null) {
            result = new StringBuilder();
            String value;
            String separator = "";
            result.append("{");
            for (String field : properties.stringPropertyNames()) {
                value = properties.getProperty(field);
                if (value != null) {
                    result.append(separator);
                    result.append("\"");
                    result.append(field);
                    result.append("\"");
                    result.append(":");
                    value = value.startsWith("{") || value.startsWith("[") ? value : ("\"" + value.replaceAll("\"", "´") + "\"");
                    result.append(value);
                    separator = ",";
                }
            }
            result.append("}");
        }
        return result == null ? null : result.toString();
    }

    public static String unir(Properties properties) {
        return unir(properties, null);
    }

    public static String unir(Properties properties, String enclosure) {
        StringBuilder result = null;
        if (properties != null) {
            result = new StringBuilder();
            String value;
            String separator = "";
            enclosure = enclosure == null ? "" : enclosure;
            for (String field : properties.stringPropertyNames()) {
                value = properties.getProperty(field);
                result.append(separator);
                result.append(field);
                result.append("=");
                result.append(enclosure);
                result.append(value == null ? "" : value);
                result.append(enclosure);
                separator = ",";
            }
        }
        return result == null ? null : result.toString();
    }

    public static String parseSQLParams(String sql, Properties params) {
        if (sql != null && params != null) {
            String value;
            for (String param : params.stringPropertyNames()) {
                value = params.getProperty(param);
                sql = sql.replace(param, value);
            }
            return sql;
        }
        return null;
    }

    public static String generarID() {
        SecureRandom ng = new SecureRandom();
        return Long.toHexString(MSB | ng.nextLong()) + Long.toHexString(MSB | ng.nextLong());
    }

    public static String toHexString(byte[] bytes) {
        StringBuilder hexString = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }

        return hexString.toString();
    }

    public static String unir(Object[] array) {
        return unir(array, null);
    }

    public static String unir(Object[] array, String enclosure) {
        StringBuilder result = null;
        try {
            if (array != null) {
                String separator = "";
                result = new StringBuilder();
                enclosure = enclosure == null ? "" : enclosure;
                for (Object item : array) {
                    result.append(separator);
                    result.append(enclosure);
                    result.append(item == null ? "" : item.toString());                   
                    result.append(enclosure);
                    separator = ",";
                }
            }
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result == null ? null : result.toString();
    }

    public static Properties obtenerPropiedades(HttpServletRequest request) {
        Properties result = null;
        if (request != null) {
            try {
                result = new Properties();
                Enumeration params = request.getParameterNames();
                String value;
                String param;
                while (params.hasMoreElements()) {
                    param = (String) params.nextElement();
                    value = request.getParameter(param).trim();
                    result.setProperty(param, value);
                }
            } catch (Exception e) {
                Util.log(new Util(), e);
            }
        }
        return result;
    }

    public static void mostrarError(PrintWriter out, String message) {
        Properties properties = new Properties();
        message = message == null ? "" : message;
        properties.setProperty("status", "false");
        properties.setProperty("message", message);
        mostrar(out, properties);
    }

    public static void mostrar(PrintWriter out, Properties properties) {
        out.print(Util.serializar(properties));
    }

    public static void asignarParametroSesion(HttpServletRequest request, String param, Object value) {
        request.getSession().setAttribute(param, value);
    }

    public static Object obtenerParametroSesion(HttpServletRequest request, String param) {
        return request.getSession().getAttribute(param);
    }

    public static String obtenerCodigoColaborador(HttpServletRequest request) {
        try {
            Usuario currentUser = Util.obtenerUsuarioActual(request);
            String colaborador = request.getParameter("colaborador");
            colaborador = colaborador == null || colaborador.trim().equals("") ? currentUser.id : colaborador;
            if (!Character.isDigit(colaborador.charAt(0))) {
                colaborador = colaborador.substring(1);
            }
            return colaborador;
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return null;
    }
    
    public static String obtenerDireccionIP(HttpServletRequest request) {
        try {                
            return request.getRemoteAddr();
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return null;        
    }
    
    public static String capitalize(String text) {
        if (text == null || text.isEmpty()) {
            return "";
        } else {
            text = text.trim().toLowerCase();        
            return Character.toString(text.charAt(0)).toUpperCase()+text.substring(1);        
        }
    }
    
    public static String checkParam(String param) {
        try {
            param = param == null ?  "" : param.toString();
            param = ESAPI.encoder().encodeForHTMLAttribute(param);
            param = String.join("/", param.split("&#x2f;"));
            return param;
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return null;
    }
    
    public static String currencyFormat(double number) {
        NumberFormat format = NumberFormat.getInstance(Locale.UK);    
        return format.format(number);
    }
    
    public static String currencyFormat(String number) {
        NumberFormat format = NumberFormat.getInstance(Locale.UK);    
        return format.format(number);
    }    
    
    public static long getCurrentTimestamp() {
        return System.currentTimeMillis();
    }
    
    public static long getSessionTimeout() {
        try {
            return Long.parseLong(Config.$("BE_SESSION_TIMEOUT"))*1000;
        } catch(Exception e) {
            return 180 * 1000;
        }
    }

    public static String getCurrentDateTimeDB() {
        String result = null;
        try { 
            //DateFormat format = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            result = format.format(new Date());                        
        } catch (Exception e) {
            Util.log(new Util(), e);
        }
        return result;
    }   
    public static String userZeros(String valor) {
         
         try {
           int num = Integer.parseInt(valor.toString());
            return String.valueOf(num);
        } catch(Exception e) {
            return valor;
        }
         
         
      }  
}
