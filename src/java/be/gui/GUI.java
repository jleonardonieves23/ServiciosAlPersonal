//PACKAGE
package be.gui;

//IMPORTS

//CLASS
public class GUI {
    //CONSTS
    
    //PROPERTIES
    
    //
    private static GUI gui;
    
    //CONSTRUCTORS
    private GUI () {
        init();
    }
    //INIT
    private void init() {
        
    }
    //BASIC METHODS
    private static GUI _instance() {
        if (gui==null) gui = new GUI();
        return gui;
    }
    //ADVANCED METHODS
    private String _getMenuDeco() {    
        return "<span class=\"glyphicon glyphicon-record\"></span>";
    }
    
    private String _getMenuLogoutDeco() {    
        return "<span class=\"glyphicon glyphicon-off\"></span>";
    }    
    
    private String _getButtonBackDeco() {            
        return "<span class=\"glyphicon glyphicon-chevron-left\"></span>";
    } 
    
    //STATICS METHODS
    public static String getMenuDeco() {
        return _instance()._getMenuDeco();
    }
    
    public static String getMenuLogoutDeco() {
        return _instance()._getMenuLogoutDeco();
    }
    
    public static String getButtonBackDeco() {
        return _instance()._getButtonBackDeco();
    }    
}
