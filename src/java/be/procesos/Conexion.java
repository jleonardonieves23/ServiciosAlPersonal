package be.procesos;

import be.seguridad.Seguridad;
import javax.naming.*;
import java.util.Hashtable;
import org.apache.tomcat.dbcp.dbcp2.*;
import be.utils.Util;

public class Conexion extends BasicDataSourceFactory {

    @Override
    public Object getObjectInstance(Object obj, Name name, Context nameCtx, Hashtable environment) throws Exception {        
        try {
            System.out.println("be.procesos.Conexion: " + name);
            org.apache.naming.ResourceRef ref = (org.apache.naming.ResourceRef) obj;
            String pwd = (String) ref.get("password").getContent();            
            pwd = Seguridad.decrypt(pwd);    
            StringRefAddr param = new StringRefAddr("password", pwd);
            for(int i=0; i<ref.size(); i++) {
                if (ref.get(i).getType().equals("password")) {
                    ref.remove(i);
                }
            }
            ref.add(param);
            Object o = super.getObjectInstance(ref, name, nameCtx, environment);
            System.out.println("be.procesos.Conexion: " + name + " -> " + (o != null));
            BasicDataSource ds = (BasicDataSource) o;
            return ds;
        } catch(Exception e) {
            Util.log(this, e);
            System.out.println("be.procesos.Conexion (error): " + e);                       
            return null;
        }
    }
}
