//PACKAGE
package be.procesos;

//IMPORTS
import java.sql.*;
import javax.sql.*;
import javax.naming.*;
import be.utils.*;

//CLASS
public class Datos {
    //CONSTS
    
    //PROPERTIES    
    private String     context;
    private Connection connection;
                
    //CONSTRUCTORS
    public Datos() {
        init(null);
    }

    public Datos(String jndi) {
        init(jndi);
    }
    
    //INIT
    private void init(String jndi) {
        jndi      = jndi == null ? Config.$("BE_JNDI") : jndi;
        this.context    = "java:/comp/env/" + jndi;
        this.connection = jndi();        
    }
    
    //BASIC METHODS

    //ADVANCED METHODS
    private Connection jndi() {
        String error = Etiqueta.$("BE_ERROR_DATABASE_CONNECTION");
        Connection result = null;
        try {
            Context initialContext = new InitialContext();
            if (initialContext == null) {
                Util.log(this, error);
                 System.out.println("Datos.conectar (error): " + error);
            }
            DataSource datasource = (DataSource) initialContext.lookup(this.context);
            if (datasource != null) {
                result = datasource.getConnection();
                 System.out.println("Datos.conectar: " + context + " -> " + (result != null));
            } else {
                Util.log(this, error);
                System.out.println("Datos.conectar (error): " + error);
            }
        } catch (Exception e) {
            Util.log(this, e);
            System.out.println("Datos.conectar (error): " + error);
        }
        return result;
    }
    
    public void conectar()  {
        conectar(null);
    }
            
    public void conectar(String id) {
        cerrar();
        this.context    = id == null ? this.context : "java:/comp/env/" + id;
        this.connection = jndi();         
    }
    
    public void cerrar() {
        if (this.connection != null) {
            try {
                if (this.connection!=null) {
                    this.connection.close();
                    System.out.println("Datos.cerrar: " + context);
                }
            } catch (SQLException e) {
                Util.log(this, e);                
                System.out.println("Datos.cerrar (error): " + e);
            }
        }
    }
    
    public Object ejecutar(String sql) {
        Object result = null;
        if (this.connection != null) {
            try {
                Statement statement = this.connection.createStatement();                
                System.out.println("Datos.ejecutar: " + sql);
                ResultSet resultSet = statement.executeQuery(sql);
                if (resultSet == null) {
                    result = null;
                } else {                    
                    result = resultSet;
                } 
            } catch(Exception e) {
                Util.log(this, e);
                System.out.println("Datos.ejecutar (error): " + e);
                return false;
            }
        }        
        return result;
    }
    
    public boolean procesar(String sql) {        
        boolean result = false;
        if (this.connection != null) {
            try {                
                Statement statement = this.connection.createStatement(); 
                System.out.println("Datos.procesar: " + sql);
                statement.execute(sql);
                result = true;
            } catch(Exception e) {
                Util.log(this, e);
                System.out.println("Datos.procesar (error): " + e);
                return false;
            }
        }        
        return result;
    }
    
    public boolean cargar() {
        boolean result = true;
        return result;
    }
}
