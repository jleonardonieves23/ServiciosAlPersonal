package be.servlets;

import be.entidades.Usuario;
import be.utils.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpSession;

@WebServlet(name = "ejecutar", urlPatterns = {"/ejecutar"})
@MultipartConfig

public class ejecutar extends HttpServlet {

    private final String MSG_BAD_PARAMETERS = "Los parámetros enviados son incorrectos";
    private final String MSG_SESSION_EXPIRE = "La sesión ha expirado. Por favor recarge la página"; 
    private final String MSG_ERROR_ACCESS   = "Error de acceso"; 
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {  
            HttpSession session = request.getSession(); 
            if ((Util.getCurrentTimestamp() - session.getLastAccessedTime() > Util.getSessionTimeout())) {
                Util.mostrarError(out, MSG_SESSION_EXPIRE + "(1)");
            } else {
                Usuario usuario = Util.obtenerUsuarioActual(request);
                if (usuario != null && usuario.id != null) {
                    String direccion_ip = Util.obtenerDireccionIP(request);
                    Thread.currentThread().setName(usuario.id + "@" + direccion_ip);
                }
                String action = Util.checkParam(request.getParameter("a"));
                action = action == null ? ""  : action;
                String[] parts = action.split("\\p{Punct}");
                if (parts == null || parts.length != 2) {
                    Util.mostrarError(out, MSG_BAD_PARAMETERS);
                } else /*if(usuario == null || usuario.tieneAcceso(action)) */{
                    try {
                        String classname = parts[0];
                        String methodname = parts[1];
                        Class<?> currentClass = Class.forName("be.servlets.mod." + classname);
                        Object object = currentClass.newInstance();
                        java.lang.reflect.Method method = currentClass.getMethod(methodname, HttpServletRequest.class, HttpServletResponse.class, PrintWriter.class, HttpSession.class);
                        method.invoke(object, request, response, out, session);
                    } catch (Exception e) {
                        Util.log(this, e);
                        Util.mostrarError(out, MSG_BAD_PARAMETERS);    
                    }
                } 
            }        
        } catch (Exception e) {            
            Util.log(this, e);
        }
    }                                                                
         
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
