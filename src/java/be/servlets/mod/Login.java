package be.servlets.mod;

import be.entidades.Usuario;
import be.seguridad.Seguridad;
import be.utils.Etiqueta;
import be.utils.Util;
import java.io.PrintWriter;
import java.security.KeyPair;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Login {
   
    private final String MSG_SESSION_EXPIRE = "La sesión ha expirado. Por favor recarge la página"; 
        
    public void procesar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {      
        try {
            String status  = "false";
            String message = "";
            String data    = "";            
            KeyPair keys    = Util.obtenerLlaves(request);
            String login    = request.getParameter("login");
            String password = request.getParameter("password");
            password        = Seguridad.decrypt(password);                   
            //password        = Seguridad.decryptRSA(keys, password);                                                               
            if (password == null) {
                status  = "false";
                message = Etiqueta.$("BE_LOGIN_REFUSED") + ": " + Seguridad.getMessage();                    
            } else {
                if (Seguridad.login(login, password)) {
                    Usuario usuario = Seguridad.getCurrentUser();
                    String id = "";
                    if (usuario != null && usuario.codigo != null) {
                        id = usuario.codigo.trim().substring(1);
                        usuario.recargar(id);                
                        usuario.id = id;                        
                        String direccion_ip = Util.obtenerDireccionIP(request);
                        Thread.currentThread().setName(usuario.id + "@" + direccion_ip);
                        Util.asignarUsuarioActual(request, usuario);
                        usuario.imagen(request);
                        data = Util.serializar(usuario.obtenerPropiedades()); 
                        usuario.guardar();
                    }  
                    status = "true";
                    message = Etiqueta.$("BE_LOGIN_SUCCESS");
                    Seguridad.log("login", message, null, id);
                } else {
                    status = "false";
                    message = Etiqueta.$("BE_LOGIN_REFUSED") + ". " + Seguridad.getMessage();
                }
            }
            
            Properties properties = new Properties();
            properties.setProperty("status", status);
            properties.setProperty("message", message);            
            properties.setProperty("data", data);
            Util.mostrar(out, properties);        
        } catch(Exception e) {
            e.printStackTrace();
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
               
    }       
    
    public void actualizar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {      
        try {
       
        } catch(Exception e) {
            e.printStackTrace();
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
               
    }           
}