package be.servlets.mod;

import be.entidades.Entidades;
import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Config;
import be.utils.Util;
import be.utils.Etiqueta;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Impuestos {

    public void obtenerPeriodos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        out.print("{");
        String status = "false";
        Datos datos = null;
        try {
            datos = new Datos("jdbc/portal");
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            String colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());

            String sql;
            Object results;
            ResultSet data;

            //DATOS GENERALES
            sql = "SELECT * FROM ARI_Periodos WHERE status = 1 ORDER BY ANO DESC, MES DESC";
            results = datos.ejecutar(sql);

            out.print("\"periodos\":[");

            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                String id;
                String ano;
                String mes;
                String fecha_cierre;
                int i = 0;
                while (data != null && !data.isClosed() && !data.isAfterLast() && data.next()) {
                    id = Util.checkNull(data.getString("id"));
                    ano = Util.checkNull(data.getString("ano"));
                    mes = Util.checkNull(data.getString("mes"));
                    fecha_cierre = Util.checkNull(data.getString("fecha_cierre"));

                    if (i++ > 0) {
                        out.print(",");
                    }
                    out.print("{");
                    out.print("\"id\":\"" + id + "\"");
                    out.print(",");
                    out.print("\"ano\":\"" + ano + "\"");
                    out.print(",");
                    out.print("\"mes\":\"" + mes + "\"");
                    out.print(",");
                    out.print("\"fecha_cierre\":\"" + fecha_cierre + "\"");
                    out.print("}");
                }
            }

            out.print("],");

            status = "true";

            datos.cerrar();
        } catch (Exception e) {
            datos.cerrar();
            Util.log(this, e);
            status = "false";
        }finally {
            datos.cerrar();
        }
        out.print("\"status\":\"" + status + "\"");
        out.print("}");
    }

    public void obtenerDatosARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        out.print("{");
        Properties params;            
        Object results;
        ResultSet data;
        String status = "false";
        Datos datos = null;
        String sql = "";
        String colaborador = "";        
        
        String REMUNERACION_ESTIMADA = "";
        String COD_COMPANIA = "0";
        String CARNET = "0";
        String CEDULA = "0";
        String NOMBRE = "0";
        String CLAMIN = "0";
        String FECHA_INGRESO = "0";
        String ANIOS = "0";
        String MES = "0";
        String MESING = "0";
        String DIAING = "0";
        String DIARIO = "0";
        String PAQUETE = "0";
        String SALARIO = "0";
        String REMUNARACION_PERCIBIDAS = "0";
        String BONO_FUNCIONARIO = "0";
        String BONO_VACACIONAL = "0";
        String REMUNERACION_VARIABLE = "0";
        String TOTAL_REM_ACUM = "0";
        String TOTAL_RET_ACUM = "0";
        String DIAS_VACACIONES = "0";   
        String NOMBRE_COMPAÑIA = "0";
        String DEGRAVAMEN_UNICO = "0";
        String REBAJA_PERSONAL = "0";
        String REBAJA_CONYUGUE = "0";
        String REBAJA_FAMILIAR = "0";
        String UNIDAD_TRIBUTARIA = "0";   
                    
        try {
            datos = new Datos("jdbc/rrhh");
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());
            String ano = Util.checkNull(request.getParameter("ano"));
            String mes = Util.checkNull(request.getParameter("mes"));

            //QUERY UNICO PARA TODOS LOS DATOS DEL ARI
            //sql = "SELECT * FROM (SELECT A.CIA_CODCIA AS COD_COMPANIA, RTRIM(LTRIM(A.FICTRA)) AS CARNET, RTRIM(LTRIM(A.CEDULA)) AS CEDULA, A.APELL1 || ' ' || A.APELL2 || ' ' || A.NOMBR1 AS NOMBRE, A.CLAMIN AS CLAMIN, A.FECING AS FECHA_INGRESO, EXTRACT(YEAR FROM SYSDATE) - EXTRACT(YEAR FROM A.FECING) AS ANIOS, EXTRACT(MONTH FROM SYSDATE) AS MES, EXTRACT(MONTH FROM A.FECING) AS MESING, (A.SUELD1 + A.SUELD2 + A.SUELD3) AS DIARIO, (((A.SUELD1 + A.SUELD2 + A.SUELD3) * 30)*12) + (( DECODE(A.SUELD1, NULL, 0, A.SUELD1) + DECODE(A.SUELD2, NULL, 0, A.SUELD2) + DECODE(A.SUELD3, NULL, 0, A.SUELD3) ) * 155) As PAQUETE, ((DECODE(A.SUELD1, NULL, 0, SUELD1)+ DECODE(A.SUELD2, NULL, 0, A.SUELD2) + DECODE(A.SUELD3, NULL, 0, A.SUELD3) ) * 30) AS SALARIO FROM NMM001 A WHERE A.FICTRA = '<colaborador>' AND A.FECRET IS NULL AND A.TNOM_TIPNOM <> '9999') A LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS REMUNARACION_PERCIBIDAS FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO = '1' AND FPRO_ANOCAL = '<anio>') B ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS BONO_FUNCIONARIO FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO = '60' AND FPRO_ANOCAL = '<anio>' AND MESCAL < '7') C ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS BONO_VACACIONAL FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO IN (200,9030,9032,9033,9034) AND FPRO_ANOCAL = '<anio>') D ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS REMUNERACION_VARIABLE FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO IN (9,40,41,42,43,44,45,46,62,63,64,65,66,67,68,80,81,82,83,85,100,101,107,108,109,110,128,140,141,182,202,203,223) AND FPRO_ANOCAL = '<anio>') E ON 1 = 1 LEFT JOIN (SELECT ( DECODE(A.REMP01, NULL, 0, A.REMP01) + DECODE(A.REMP02, NULL, 0, A.REMP02) + DECODE(A.REMP03, NULL, 0, A.REMP03) + DECODE(A.REMP04, NULL, 0, A.REMP04) + DECODE(A.REMP05, NULL, 0, A.REMP05) + DECODE(A.REMP06, NULL, 0, A.REMP06) + DECODE(A.REMP07, NULL, 0, A.REMP07) + DECODE(A.REMP08, NULL, 0, A.REMP08) + DECODE(A.REMP09, NULL, 0, A.REMP09) + DECODE(A.REMP10, NULL, 0, A.REMP10) + DECODE(A.REMP11, NULL, 0, A.REMP11) + DECODE(A.REMP12, NULL, 0, A.REMP12) ) AS TOTAL_REM_ACUM, ( DECODE(A.RETE01, NULL, 0, A.RETE01) + DECODE(A.RETE02, NULL, 0, A.RETE02) + DECODE(A.RETE03, NULL, 0, A.RETE03) + DECODE(A.RETE04, NULL, 0, A.RETE04) + DECODE(A.RETE05, NULL, 0, A.RETE05) + DECODE(A.RETE06, NULL, 0, A.RETE06) + DECODE(A.RETE07, NULL, 0, A.RETE07) + DECODE(A.RETE08, NULL, 0, A.RETE08) + DECODE(A.RETE09, NULL, 0, A.RETE09) + DECODE(A.RETE10, NULL, 0, A.RETE10) + DECODE(A.RETE11, NULL, 0, A.RETE11) + DECODE(A.RETE12, NULL, 0, A.RETE12)) AS TOTAL_RET_ACUM FROM NMM014 A WHERE TRAB_FICTRA = '<colaborador>' AND ANOIMP = '<anio>') F ON 1 = 1 LEFT JOIN ( SELECT DECODE(sum(totales) - sum(disfrutados) , NULL, 0, sum(totales) - sum(disfrutados) ) as DIAS_VACACIONES FROM (select extract(year from feccon) as periodo, SUM(DECODE(TIPREG, 1, CANVAC)) as totales, 0 as disfrutados from NMM011 where trab_fictra = '<colaborador>' and cto_codcto = 9030 AND TIPREG = 1 group by extract(year from feccon) UNION ALL select extract(year from feccon) as periodo, 0 as totales, sum(decode(TIPREG, 3, CANVAC)) as disfrutados from NMM011 where trab_fictra = '<colaborador>' and cto_codcto = 9030 AND TIPREG = 3 group by extract(year from feccon)) A WHERE periodo = '<anio>') G ON 1 = 1 LEFT JOIN (SELECT NOMCI1 AS NOMBRE_COMPAÑIA, DESUNI AS DEGRAVAMEN_UNICO, REBPER AS REBAJA_PERSONAL, REBCON As REBAJA_CONYUGUE, REBFAM As REBAJA_FAMILIAR, UNITRI As UNIDAD_TRIBUTARIA FROM NMT001 WHERE CODCIA = '01') H ON 1 = 1";
            sql = "SELECT * FROM (SELECT A.CIA_CODCIA AS COD_COMPANIA, RTRIM(LTRIM(A.FICTRA)) AS CARNET, RTRIM(LTRIM(A.CEDULA)) AS CEDULA, A.APELL1 || ' ' || A.APELL2 || ' ' || A.NOMBR1 AS NOMBRE, A.CLAMIN AS CLAMIN, A.FECING AS FECHA_INGRESO, <anio> - EXTRACT(YEAR FROM A.FECING) AS ANIOS, EXTRACT(MONTH FROM SYSDATE) AS MES, EXTRACT(MONTH FROM A.FECING) AS MESING, EXTRACT(DAY FROM A.FECING) AS DIAING, (A.SUELD1 + A.SUELD2 + A.SUELD3) AS DIARIO, (((A.SUELD1 + A.SUELD2 + A.SUELD3) * 30)*12) + (( DECODE(A.SUELD1, NULL, 0, A.SUELD1) + DECODE(A.SUELD2, NULL, 0, A.SUELD2) + DECODE(A.SUELD3, NULL, 0, A.SUELD3) ) * 155) As PAQUETE, ((DECODE(A.SUELD1, NULL, 0, SUELD1)+ DECODE(A.SUELD2, NULL, 0, A.SUELD2) + DECODE(A.SUELD3, NULL, 0, A.SUELD3) ) * 30) AS SALARIO FROM NMM001 A WHERE A.FICTRA = '<colaborador>' AND A.FECRET IS NULL AND A.TNOM_TIPNOM <> '9999') A LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS REMUNARACION_PERCIBIDAS FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO = '1' AND FPRO_ANOCAL = <anio>) B ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS BONO_FUNCIONARIO FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO = '60' AND FPRO_ANOCAL = <anio> AND MESCAL < '7') C ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS BONO_VACACIONAL FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO IN (200,9030,9032,9033,9034) AND FPRO_ANOCAL = <anio>) D ON 1 = 1 LEFT JOIN (SELECT DECODE(SUM(MONCTO), NULL, 0, SUM(MONCTO)) AS REMUNERACION_VARIABLE FROM INFOCENT.NMM024_T_ARI WHERE TRAB_FICTRA = '<colaborador>' AND CTO_CODCTO IN (9,40,41,42,43,44,45,46,62,63,64,65,66,67,68,80,81,82,83,85,100,101,107,108,109,110,128,140,141,182,202,203,223) AND FPRO_ANOCAL = <anio>) E ON 1 = 1 LEFT JOIN (SELECT ( DECODE(A.REMP01, NULL, 0, A.REMP01) + DECODE(A.REMP02, NULL, 0, A.REMP02) + DECODE(A.REMP03, NULL, 0, A.REMP03) + DECODE(A.REMP04, NULL, 0, A.REMP04) + DECODE(A.REMP05, NULL, 0, A.REMP05) + DECODE(A.REMP06, NULL, 0, A.REMP06) + DECODE(A.REMP07, NULL, 0, A.REMP07) + DECODE(A.REMP08, NULL, 0, A.REMP08) + DECODE(A.REMP09, NULL, 0, A.REMP09) + DECODE(A.REMP10, NULL, 0, A.REMP10) + DECODE(A.REMP11, NULL, 0, A.REMP11) + DECODE(A.REMP12, NULL, 0, A.REMP12) ) AS TOTAL_REM_ACUM, ( DECODE(A.RETE01, NULL, 0, A.RETE01) + DECODE(A.RETE02, NULL, 0, A.RETE02) + DECODE(A.RETE03, NULL, 0, A.RETE03) + DECODE(A.RETE04, NULL, 0, A.RETE04) + DECODE(A.RETE05, NULL, 0, A.RETE05) + DECODE(A.RETE06, NULL, 0, A.RETE06) + DECODE(A.RETE07, NULL, 0, A.RETE07) + DECODE(A.RETE08, NULL, 0, A.RETE08) + DECODE(A.RETE09, NULL, 0, A.RETE09) + DECODE(A.RETE10, NULL, 0, A.RETE10) + DECODE(A.RETE11, NULL, 0, A.RETE11) + DECODE(A.RETE12, NULL, 0, A.RETE12)) AS TOTAL_RET_ACUM FROM NMM014 A WHERE TRAB_FICTRA = '<colaborador>' AND ANOIMP = <anio>) F ON 1 = 1 LEFT JOIN ( SELECT DECODE(sum(totales) - sum(disfrutados) , NULL, 0, sum(totales) - sum(disfrutados) ) as DIAS_VACACIONES FROM (select extract(year from feccon) as periodo, SUM(DECODE(TIPREG, 1, CANVAC)) as totales, 0 as disfrutados from NMM011 where trab_fictra = '<colaborador>' and cto_codcto = 9030 AND TIPREG = 1 group by extract(year from feccon) UNION ALL select extract(year from feccon) as periodo, 0 as totales, sum(decode(TIPREG, 3, CANVAC)) as disfrutados from NMM011 where trab_fictra = '<colaborador>' and cto_codcto = 9030 AND TIPREG = 3 group by extract(year from feccon)) A WHERE periodo = <anio>) G ON 1 = 1 LEFT JOIN (SELECT NOMCI1 AS NOMBRE_COMPAÑIA, DESUNI AS DEGRAVAMEN_UNICO, REBPER AS REBAJA_PERSONAL, REBCON As REBAJA_CONYUGUE, REBFAM As REBAJA_FAMILIAR, UNITRI As UNIDAD_TRIBUTARIA FROM NMT001 WHERE CODCIA = '01') H ON 1 = 1";
            
            params = new Properties();
            params.setProperty("<anio>", ano);
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            results = datos.ejecutar(sql);
                                
            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                if (data != null && !data.isClosed() && !data.isAfterLast() && data.next()) {                
                    
                    REMUNERACION_ESTIMADA = Util.checkNull(data.getString("PAQUETE"));
                    COD_COMPANIA = Util.checkNull(data.getString("COD_COMPANIA"));
                    CARNET = Util.checkNull(data.getString("CARNET"));
                    CEDULA = Util.checkNull(data.getString("CEDULA"));
                    NOMBRE = Util.checkNull(data.getString("NOMBRE"));
                    CLAMIN = Util.checkNull(data.getString("CLAMIN"));
                    FECHA_INGRESO = Util.checkNull(data.getString("FECHA_INGRESO"));
                    ANIOS = Util.checkNull(data.getString("ANIOS"));
                    MES = Util.checkNull(data.getString("MES"));
                    MESING = Util.checkNull(data.getString("MESING"));
                    DIAING = Util.checkNull(data.getString("DIAING"));
                    DIARIO = Util.checkNull(data.getString("DIARIO"));
                    PAQUETE = Util.checkNull(data.getString("PAQUETE"));
                    SALARIO = Util.checkNull(data.getString("SALARIO"));
                    REMUNARACION_PERCIBIDAS = Util.checkNull(data.getString("REMUNARACION_PERCIBIDAS"));
                    BONO_FUNCIONARIO = Util.checkNull(data.getString("BONO_FUNCIONARIO"));
                    BONO_VACACIONAL = Util.checkNull(data.getString("BONO_VACACIONAL"));
                    REMUNERACION_VARIABLE = Util.checkNull(data.getString("REMUNERACION_VARIABLE"));
                    TOTAL_REM_ACUM = Util.checkNull(data.getString("TOTAL_REM_ACUM"));
                    TOTAL_RET_ACUM = Util.checkNull(data.getString("TOTAL_RET_ACUM"));
                    DIAS_VACACIONES = Util.checkNull(data.getString("DIAS_VACACIONES"));
                    NOMBRE_COMPAÑIA = Util.checkNull(data.getString("NOMBRE_COMPAÑIA"));
                    DEGRAVAMEN_UNICO = Util.checkNull(data.getString("DEGRAVAMEN_UNICO"));
                    REBAJA_PERSONAL = Util.checkNull(data.getString("REBAJA_PERSONAL"));
                    REBAJA_CONYUGUE = Util.checkNull(data.getString("REBAJA_CONYUGUE"));
                    REBAJA_FAMILIAR = Util.checkNull(data.getString("REBAJA_FAMILIAR"));
                    UNIDAD_TRIBUTARIA = Util.checkNull(data.getString("UNIDAD_TRIBUTARIA"));
                }
            }
            
            int mesact = Integer.parseInt(MES);
            int diaing = Integer.parseInt(DIAING);
            int mesing = Integer.parseInt(MESING);
            double salario = Double.parseDouble(SALARIO);
            double acum = Double.parseDouble(REMUNARACION_PERCIBIDAS);
            double dias = 0;
            double Vac = Double.parseDouble(BONO_VACACIONAL);
            double Func = Double.parseDouble(BONO_FUNCIONARIO);
            int anio = Integer.parseInt(ANIOS);
            int clamin = Integer.parseInt(CLAMIN);
            int mesfal;
            int mesanio = 13;
            double pend1;
            double totalGen;        
            double benefic;
            double tiempo;
            double bonovac;
            double vacac;
            double utilid;
            double Func2;
            double bfuntotal;
            double pack;

            mesfal = mesanio - mesact;
            tiempo = mesanio - mesing - (diaing == 1 ? 0 : 1);
            pend1 = salario * mesfal;
            totalGen = acum + pend1;
            
            String indice = anio > 20 ? "21" : Integer.toString(anio);
            dias = Double.parseDouble((String) Config.$("BE_BONO_VACACIONAL_" + indice));

            bonovac = ( dias / 30.0d) * salario;
            utilid  = (155.0d / 30.0d) * salario;

            //'**** Verificación de Pago de Vacaciones ****

            if (Vac == 0) {
                vacac = bonovac;
            } else {
                vacac = Vac;
            }	

            //'**** Verificación de Pago de Bono Funcionario ****

            Func2 = salario / 2;
            bfuntotal = Func2 + (Func == 0  ? Func2 : Func);

            //'**** Cálculo del Paquete ****

            if (anio > 0) {
                if (clamin == 1) {
                    benefic = ((utilid) + (vacac) + (bfuntotal));
                } else {
                    benefic = ((utilid) + (vacac));
                }
            } else {
                if (clamin == 1) {
                    benefic = ((((tiempo * 155) / 12)) * (salario / 30)) + (bfuntotal);
                } else {
                    benefic = (((tiempo * 155) / 12) * (salario / 30));
                }
            }

            pack = totalGen + benefic;    
            
            PAQUETE = Double.toString(pack);

            out.print("\"TOTAL_REM_ACUM\":\"" + TOTAL_REM_ACUM + "\"");
            out.print(",");
            out.print("\"TOTAL_RET_ACUM\":\"" + TOTAL_RET_ACUM + "\"");
            out.print(","); 
            out.print("\"MES\":\"" + MES + "\"");
            out.print(",");
            out.print("\"MESING\":\"" + MESING + "\"");
            out.print(",");
            out.print("\"ANIOS\":\"" + ANIOS + "\"");
            out.print(",");
            out.print("\"DIARIO\":\"" + DIARIO + "\"");
            out.print(",");
            out.print("\"TIPO_COLABORADOR\":\"" + CLAMIN + "\"");
            out.print(",");
            out.print("\"SALARIO\":\"" + SALARIO + "\"");
            out.print(",");                
            out.print("\"PAQUETE\":\"" + PAQUETE + "\"");
            out.print(",");            
            out.print("\"REMUNERACION_ESTIMADA\":\"" + PAQUETE + "\"");            
            out.print(",");
            out.print("\"DEGRAVAMEN_UNICO\":\"" + DEGRAVAMEN_UNICO + "\"");
            out.print(",");
            out.print("\"REBAJA_PERSONAL\":\"" + REBAJA_PERSONAL + "\"");
            out.print(",");
            out.print("\"REBAJA_CONYUGUE\":\"" + REBAJA_CONYUGUE + "\"");
            out.print(",");
            out.print("\"REBAJA_FAMILIAR\":\"" + REBAJA_FAMILIAR + "\"");
            out.print(",");
            out.print("\"UNIDAD_TRIBUTARIA\":\"" + UNIDAD_TRIBUTARIA + "\"");
            out.print(",");
            out.print("\"REMUNERACIONES\":\"" + REMUNARACION_PERCIBIDAS + "\"");
            out.print(",");
            out.print("\"BONO_FUNCIONARIO\":\"" + BONO_FUNCIONARIO + "\"");
            out.print(",");
            out.print("\"VACACIONES\":\"" + BONO_VACACIONAL + "\"");
            out.print(",");
            out.print("\"BONO_ADICIONAL\":\"" + REMUNERACION_VARIABLE + "\"");
            out.print(",");
            out.print("\"APENOM\":\"" + NOMBRE + "\"");
            out.print(",");
            out.print("\"CODIGO\":\"" + CARNET + "\"");
            out.print(",");
            out.print("\"CEDULA\":\"" + CEDULA + "\"");
            out.print(",");
            
            //TARIFAS
            sql = "SELECT REMDES, REMHAS, TASIM1, VALSUS FROM NMT006 WHERE CIA_CODCIA ='01'";
            sql = Util.parseSQLParams(sql, params);
            results = datos.ejecutar(sql);
            
            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                boolean head = false;
                data = (ResultSet) results;
                int i = 0;
                while (results != null && ResultSet.class.isInstance(results) && data.next()) {
                    if (!head) {
                        head = true;
                        out.print("\"TARIFAS\":[");
                    }
                    String REMDES = Util.checkNull(data.getString("REMDES"));
                    String REMHAS = Util.checkNull(data.getString("REMHAS"));
                    String TASIM1 = Util.checkNull(data.getString("TASIM1"));
                    String VALSUS = Util.checkNull(data.getString("VALSUS"));
                     if (i++ > 0) {
                            out.print(",");
                        }
                    out.print("{");
                    out.print("\"desde\":\"" + REMDES + "\"");
                    out.print(",");
                    out.print("\"hasta\":\"" + REMHAS + "\"");
                    out.print(",");
                    out.print("\"tarifa\":\"" + TASIM1 + "\"");
                    out.print(",");
                    out.print("\"sustraendo\":\"" + VALSUS + "\"");
                    out.print("}");
                }
                out.print("],");
            }
            
            status = "true";

            datos.cerrar();
            
            be.entidades.ARI ari = new be.entidades.ARI();
            //ari.cargar(ari._ENTITY_NAME, "ano=" + ano + " and mes=" + mes, null);
            ari.cargar(ari._ENTITY_NAME, "colaborador = '" + colaborador.trim() + "' and ano=" + ano + " and mes=" + mes, null);

            out.print("\"ari\":{");
            out.print("\"id\":\"" + ari.id + "\",");
            out.print("\"colaborador\":\"" + ari.colaborador + "\",");
            out.print("\"fecha\":\"" + ari.fecha + "\",");
            out.print("\"status\":\"" + ari.status + "\",");
            out.print("\"impuesto_retenido\":\"" + ari.impuesto_retenido + "\",");
            out.print("\"remuneraciones_recibidas\":\"" + ari.remuneraciones_recibidas + "\",");
            out.print("\"ut\":\"" + ari.ut + "\",");
            out.print("\"ut_rebajas\":\"" + ari.ut_rebajas + "\",");
            out.print("\"remuneracion_estimada\":\"" + ari.remuneracion_estimada + "\",");
            out.print("\"desgravamen_unico\":\"" + ari.desgravamen_unico + "\",");
            out.print("\"remuneraciones1\":\"" + ari.remuneraciones1 + "\",");
            out.print("\"remuneraciones2\":\"" + ari.remuneraciones2 + "\",");
            out.print("\"remuneraciones3\":\"" + ari.remuneraciones3 + "\",");
            out.print("\"desgravamen1\":\"" + ari.desgravamen1 + "\",");
            out.print("\"desgravamen2\":\"" + ari.desgravamen2 + "\",");
            out.print("\"desgravamen3\":\"" + ari.desgravamen3 + "\",");
            out.print("\"desgravamen4\":\"" + ari.desgravamen4 + "\",");
            out.print("\"unico\":\"" + ari.unico + "\",");
            out.print("\"rebaja1_cantidad\":\"" + ari.rebaja1_cantidad + "\",");
            out.print("\"rebaja2_cantidad\":\"" + ari.rebaja2_cantidad + "\",");
            out.print("\"rebaja3_cantidad\":\"" + ari.rebaja3_cantidad + "\",");
            out.print("\"porcentaje_impuesto\":\"" + ari.porcentaje_impuesto + "\",");
            out.print("\"total_variacion\":\"" + ari.total_variacion + "\",");
            out.print("\"remuneraciones1_texto\":\"" + ari.remuneraciones1_texto + "\",");
            out.print("\"remuneraciones2_texto\":\"" + ari.remuneraciones2_texto + "\",");
            out.print("\"remuneraciones3_texto\":\"" + ari.remuneraciones3_texto + "\",");
            out.print("\"ano\":\"" + ari.ano + "\",");
            out.print("\"mes\":\"" + ari.mes + "\"");
            out.print("}");
            out.print(",");

        } catch (Exception e) {
            datos.cerrar();
            Util.log(this, e);
            status = "false";
        } finally {
            datos.cerrar();
        }
        out.print("\"status\":\"" + status + "\"");
        out.print("}");
        
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".ARI.consultado";        
        String resultado = status.equals("true") ? "Consultado" : "Error de consulta";
        String key = colaborador.toString().trim();            
        Seguridad.log(accion, resultado, sql, key);
    }

    public void guardarDatosARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        guardarARI(request, response, out, session, "1");
    }

    public void declararARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        guardarARI(request, response, out, session, "2");
    }

    private void guardarARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session, String accion) {
        out.print("{");
        String status = "false";
        String message = Etiqueta.$("BE_ERROR");
        be.entidades.ARI datos = null;
        try {
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            String colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());

            datos = new be.entidades.ARI();

            datos.id = Util.checkParam(request.getParameter("id"));
            datos.status = accion;
            datos.colaborador = colaborador.trim();
            datos.fecha = Util.getCurrentDateTimeDB();
            datos.impuesto_retenido = Util.checkParam(request.getParameter("impuesto_retenido"));
            datos.remuneraciones_recibidas = Util.checkParam(request.getParameter("remuneraciones_recibidas"));
            datos.ut = Util.checkParam(request.getParameter("ut"));
            datos.ut_rebajas = Util.checkParam(request.getParameter("ut_rebajas"));
            datos.remuneracion_estimada = Util.checkParam(request.getParameter("remuneracion_estimada"));
            datos.desgravamen_unico = Util.checkParam(request.getParameter("desgravamen_unico"));
            datos.remuneraciones1 = Util.checkParam(request.getParameter("remuneraciones1"));
            datos.remuneraciones2 = Util.checkParam(request.getParameter("remuneraciones2"));
            datos.remuneraciones3 = Util.checkParam(request.getParameter("remuneraciones3"));
            datos.desgravamen1 = Util.checkParam(request.getParameter("desgravamen1"));
            datos.desgravamen2 = Util.checkParam(request.getParameter("desgravamen2"));
            datos.desgravamen3 = Util.checkParam(request.getParameter("desgravamen3"));
            datos.desgravamen4 = Util.checkParam(request.getParameter("desgravamen4"));
            datos.unico = Util.checkParam(request.getParameter("unico"));
            datos.rebaja1_cantidad = Util.checkParam(request.getParameter("rebaja1_cantidad"));
            datos.rebaja2_cantidad = Util.checkParam(request.getParameter("rebaja2_cantidad"));
            datos.rebaja3_cantidad = Util.checkParam(request.getParameter("rebaja3_cantidad"));
            datos.porcentaje_impuesto = Util.checkParam(request.getParameter("porcentaje_impuesto"));
            datos.total_variacion = Util.checkParam(request.getParameter("total_variacion"));
            datos.remuneraciones1_texto = Util.checkParam(request.getParameter("remuneraciones1_texto"));
            datos.remuneraciones2_texto = Util.checkParam(request.getParameter("remuneraciones2_texto"));
            datos.remuneraciones3_texto = Util.checkParam(request.getParameter("remuneraciones3_texto"));
            datos.ano = Util.checkParam(request.getParameter("ano"));
            datos.mes = Util.checkParam(request.getParameter("mes"));

            boolean results = datos.guardar();

            if (results) {
                status = "true";
                message = Etiqueta.$("BE_SUCCESS");
            }

        } catch (Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print("\"status\":\"" + status + "\",");
        out.print("\"message\":\"" + message + "\"");
        out.print("}");
    }

    public void anioRegistroARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        out.print("{");
        String status = "false";
        Datos datos = null;
        try {
            datos = new Datos("jdbc/rrhh");

            Properties params;
            String sql;
            Object results;
            ResultSet data;

            sql = "SELECT MIN (ano) AS ANIOHASTA FROM ARI";
            params = new Properties();
            sql = Util.parseSQLParams(sql, params);
            results = datos.ejecutar(sql);

            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                if (data != null && !data.isClosed() && !data.isAfterLast() && data.next()) {                
                    String ANIODESDE = Util.checkNull(data.getString("ANIODESDE"));
                    out.print("\"ANIODESDE\":\"" + ANIODESDE + "\"");
                    out.print(",");
                }
            }
            Properties params2;
            String sql2;
            Object results2;
            ResultSet data2;

            sql2 = "SELECT MAX (ano) AS ANIOHASTA FROM ARI";
            params2 = new Properties();
            sql2 = Util.parseSQLParams(sql2, params2);
            results2 = datos.ejecutar(sql2);

            if (results2 != null && ResultSet.class.isInstance(results2)) {
                data2 = (ResultSet) results2;
                data2.next();
                String ANIOHASTA = Util.checkNull(data2.getString("ANIOHASTA"));
                data2.close();
                out.print("\"ANIOHASTA\":\"" + ANIOHASTA + "\"");
                out.print(",");
            }

            status = "true";

            datos.cerrar();
        } catch (Exception e) {
            datos.cerrar();
            Util.log(this, e);
            status = "false";
        } finally {
            datos.cerrar();
        }
        out.print("\"status\":\"" + status + "\"");
        out.print("}");
    }

    public void consultarCategoria(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        out.print("{");
        String status = "false";
        Datos adicional = null;
        try {
            String colaborador = request.getParameter("colaborador");
            adicional = new Datos("jdbc/rrhh");
            Properties params;
            String sql;
            Object results;
            ResultSet data;

            sql = "SELECT CLAMIN, TNOM_TIPNOM, FICTRA, APELL1, APELL2, NOMBR1, NOMBR2  from NMM001 WHERE FICTRA = '<colaborador>'";
            params = new Properties();
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            results = adicional.ejecutar(sql);

            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                if (data != null && !data.isClosed() && !data.isAfterLast() && data.next()) {                
                    String CATEGORIA = Util.checkNull(data.getString("TNOM_TIPNOM"));
                    String NOMBR1 = Util.checkNull(data.getString("NOMBR1"));
                    String NOMBR2 = Util.checkNull(data.getString("NOMBR2"));
                    String APELL1 = Util.checkNull(data.getString("APELL1"));
                    String APELL2 = Util.checkNull(data.getString("APELL2"));

                    out.print("\"CATEGORIA\":\"" + CATEGORIA + "\"");
                    out.print(",");
                    out.print("\"APENOM\":\"" + APELL1 + " " + APELL2 + " " + NOMBR1 + " " + NOMBR2 + "\"");
                    out.print(",");
                }
            }

            status = "true";

            adicional.cerrar();
        } catch (Exception e) {
            adicional.cerrar();
            Util.log(this, e);
        } finally {
            adicional.cerrar();
        }
        out.print("\"status\":\"" + status + "\"");
        out.print("}");
    }

    public void listadoConsolidadoARI(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        String status = "false";
        String sql = "";
        String colaborador = "";
        Entidades datos = null;
        try {
            //Datos datos = new Datos("jdbc/rrhh");
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());            
            String mes = request.getParameter("mes");
            String anio = request.getParameter("anio");
            String estatus = request.getParameter("status");

            be.entidades.ARIExtendido entidad = new be.entidades.ARIExtendido();
            datos = new Entidades(entidad);

            datos.cargar("ano= '" + anio + "' and mes='" + mes + "' and status='" + estatus + "'");

            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }
        } catch (Exception e) {
            Util.log(this, e);
        }
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";        
        String resultado = status.equals("true") ? "Consultado" : "Error de consulta";
        String key = colaborador.toString().trim();         
        Seguridad.log(accion, resultado, datos.getSql(), key);  
    }

    public void obtenerDatosARC(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        out.print("{");
        String status = "false";
        String sql = "";
        String colaborador = "";
        Datos datos = null;
        try {
            datos = new Datos("jdbc/rrhh");
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());
            
            String anio = request.getParameter("anio");
            
             if (anio == null || anio.isEmpty()) {
                    anio = Util.getCurrentYear();
            }
 
            Properties params;
            
            Object results;
            ResultSet data;

            sql = "SELECT A.FICTRA AS CODIGO,E.ANOIMP AS ANIO, A.APELL1 AS APE1, A.APELL2 AS APE2, A.NOMBR1 AS NOM1, A.NOMBR2 AS NOM2, A.RIFTRA AS RIF,A.NACTRA AS NACIONALIDAD, A.CEDULA AS CEDULA,A.TIPDOC, B.NOMCI1 AS NOMBRE_BANCO, "
                    + " B.RIFCI1 AS RIF_BANCO, B.NROCON AS NRO_CONTRIBUYENTE, B.NOMFUN AS NOMBRE_FUNCIONARIO, B.CEDFUN AS CEDULA_FUNCIONARIO, "
                    + " B.DIRCI1 AS DIRECCION_AGENTE, B.DIRCI2, C.DESCIU AS CIUDAD, B.CODPOS AS CODIGO_POSTAL, D.DESEDO AS ESTADO, B.TLFCI1 AS TELEFONO, "
                    + " E.FECCRE AS PERIODO_DESDE, E.FECACT AS PERIODO_HASTA , E.REMP01 AS REM_ENE, E.REMP02 AS REM_FEB, E.REMP03 AS REM_MAR, E.REMP04 AS REM_ABR, E.REMP05 AS REM_MAY, E.REMP06 AS REM_JUN, "
                    + " E.REMP07 AS REM_JUL, E.REMP08 AS REM_AGO, E.REMP09 AS REM_SEP, E.REMP10 AS REM_OCT, E.REMP11 AS REM_NOV, E.REMP12 AS REM_DIC, "
                    + " E.RETE01 AS RET_ENE, E.RETE02 AS RET_FEB, E.RETE03 AS RET_MAR, E.RETE04 AS RET_ABR, E.RETE05 AS RET_MAY, E.RETE06 AS RET_JUN, "
                    + " E.RETE07 AS RET_JUL, E.RETE08 AS RET_AGO, E.RETE09 AS RET_SEP, E.RETE10 AS RET_OCT, E.RETE11 AS RET_NOV, E.RETE12 AS RET_DIC, "
                    + " E.MESDE1 AS MESVAR1, E.REMES1 AS REL_ENE, E.DESGE1 AS REL_ENED, E.CONYU1 AS REL_ENEC, E.CARFA1 AS REL_ENEF, E.PORIM1 AS REL_ENEP, E.MESDE2 AS MESVAR2, E.REMES2 AS REL_VAR2, "
                    + " E.DESGE2 AS REL_VAR2D, E.CONYU2 AS REL_VAR2C, E.CARFA2 AS REL_VAR2F, E.PORIM2 AS REL_VAR2P, E.MESDE3 AS MESVAR3, E.REMES3 AS REL_JUN, E.DESGE3 AS REL_JUND, "
                    + " E.CONYU3 AS REL_JUNC, E.CARFA3 AS REL_JUNF, E.PORIM3 AS REL_JUNP, E.MESDE4 AS MESVAR4, E.REMES4 AS REL_VAR4, E.DESGE4 AS REL_VAR4D, E.CONYU4 AS REL_VAR4C, "
                    + " E.CARFA4 AS REL_VAR4F, E.PORIM4 AS REL_VAR4P, E.MESDE5 AS MESVAR5, E.REMES5 AS REL_VAR5, E.DESGE5 AS REL_VAR5D, E.CONYU5 AS REL_VAR5C, E.CARFA5 AS REL_VAR5F, "
                    + " E.PORIM5 AS REL_VAR5P FROM NMM001 A , NMT001 B , NMT040 C, NMT041 D, NMM014 E "
                    + " WHERE(A.CIA_CODCIA = B.CODCIA And B.CDAD_CODCIU = C.CODCIU And B.EDO_CODEDO = D.CODEDO And B.CODCIA = E.CIA_CODCIA And A.FICTRA = E.TRAB_FICTRA) "
                    + " AND E.TRAB_FICTRA = '<colaborador>' AND E.ANOIMP = ('<anio>'-1)";
            params = new Properties();
            params.setProperty("<anio>", anio);
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            results = datos.ejecutar(sql);
                       
            if (results != null && ResultSet.class.isInstance(results)) {
                data = (ResultSet) results;
                if (data != null && !data.isClosed() && !data.isAfterLast() && data.next()) {                
                    String ANIO = Util.checkNull(data.getString("ANIO"));
                    String NOM = Util.checkNull(data.getString("NOM1"));
                    String SNOM = Util.checkNull(data.getString("NOM2"));
                    String APE = Util.checkNull(data.getString("APE1"));
                    String SAPE = Util.checkNull(data.getString("APE2"));
                    String CODIGO = Util.checkNull(data.getString("CODIGO"));
                    String NACIONALIDAD = Util.checkNull(data.getString("NACIONALIDAD"));
                    String CEDULA = Util.checkNull(data.getString("CEDULA"));
                    String RIF = Util.checkNull(data.getString("RIF"));
                    String NOMBRE_BANCO = Util.checkNull(data.getString("NOMBRE_BANCO"));
                    String RIF_BANCO = Util.checkNull(data.getString("RIF_BANCO"));
                    String NRO_CONTRIBUYENTE = Util.checkNull(data.getString("NRO_CONTRIBUYENTE"));
                    String NOMBRE_FUNCIONARIO = Util.checkNull(data.getString("NOMBRE_FUNCIONARIO"));
                    String CEDULA_FUNCIONARIO = Util.checkNull(data.getString("CEDULA_FUNCIONARIO"));
                    String DIRECCION_AGENTE = Util.checkNull(data.getString("DIRECCION_AGENTE"));
                    String CIUDAD = Util.checkNull(data.getString("CIUDAD"));
                    String CODIGO_POSTAL = Util.checkNull(data.getString("CODIGO_POSTAL"));
                    String ESTADO = Util.checkNull(data.getString("ESTADO"));
                    String TELEFONO = Util.checkNull(data.getString("TELEFONO"));
                    String REM_ENE = Util.checkNull(data.getString("REM_ENE"));
                    String REM_FEB = Util.checkNull(data.getString("REM_FEB"));
                    String REM_MAR = Util.checkNull(data.getString("REM_MAR"));
                    String REM_ABR = Util.checkNull(data.getString("REM_ABR"));
                    String REM_MAY = Util.checkNull(data.getString("REM_MAY"));
                    String REM_JUN = Util.checkNull(data.getString("REM_JUN"));
                    String REM_JUL = Util.checkNull(data.getString("REM_JUL"));
                    String REM_AGO = Util.checkNull(data.getString("REM_AGO"));
                    String REM_SEP = Util.checkNull(data.getString("REM_SEP"));
                    String REM_OCT = Util.checkNull(data.getString("REM_OCT"));
                    String REM_NOV = Util.checkNull(data.getString("REM_NOV"));
                    String REM_DIC = Util.checkNull(data.getString("REM_DIC"));
                    String RET_ENE = Util.checkNull(data.getString("RET_ENE"));
                    String RET_FEB = Util.checkNull(data.getString("RET_FEB"));
                    String RET_MAR = Util.checkNull(data.getString("RET_MAR"));
                    String RET_ABR = Util.checkNull(data.getString("RET_ABR"));
                    String RET_MAY = Util.checkNull(data.getString("RET_MAY"));
                    String RET_JUN = Util.checkNull(data.getString("RET_JUN"));
                    String RET_JUL = Util.checkNull(data.getString("RET_JUL"));
                    String RET_AGO = Util.checkNull(data.getString("RET_AGO"));
                    String RET_SEP = Util.checkNull(data.getString("RET_SEP"));
                    String RET_OCT = Util.checkNull(data.getString("RET_OCT"));
                    String RET_NOV = Util.checkNull(data.getString("RET_NOV"));
                    String RET_DIC = Util.checkNull(data.getString("RET_DIC"));
                    /*SERIE 1*/
                    String MESVAR1 = Util.checkNull(data.getString("MESVAR1"));
                    String REL_ENE = Util.checkNull(data.getString("REL_ENE"));
                    String REL_ENED = Util.checkNull(data.getString("REL_ENED"));
                    String REL_ENEC = Util.checkNull(data.getString("REL_ENEC"));
                    String REL_ENEF = Util.checkNull(data.getString("REL_ENEF"));
                    String REL_ENEP = Util.checkNull(data.getString("REL_ENEP"));
                    /*SERIE 2*/
                    String MESVAR2 = Util.checkNull(data.getString("MESVAR2"));
                    String REL_VAR2 = Util.checkNull(data.getString("REL_VAR2"));
                    String REL_VAR2D = Util.checkNull(data.getString("REL_VAR2D"));
                    String REL_VAR2C = Util.checkNull(data.getString("REL_VAR2C"));
                    String REL_VAR2F = Util.checkNull(data.getString("REL_VAR2F"));
                    String REL_VAR2P = Util.checkNull(data.getString("REL_VAR2P"));
                    /*SERIE 3*/
                    String MESVAR3 = Util.checkNull(data.getString("MESVAR3"));
                    String REL_JUN = Util.checkNull(data.getString("REL_JUN"));
                    String REL_JUND = Util.checkNull(data.getString("REL_JUND"));
                    String REL_JUNC = Util.checkNull(data.getString("REL_JUNC"));
                    String REL_JUNF = Util.checkNull(data.getString("REL_JUNF"));
                    String REL_JUNP = Util.checkNull(data.getString("REL_JUNP"));
                    /*SERIE 4*/
                    String MESVAR4 = Util.checkNull(data.getString("MESVAR4"));
                    String REL_VAR4 = Util.checkNull(data.getString("REL_VAR4"));
                    String REL_VAR4D = Util.checkNull(data.getString("REL_VAR4D"));
                    String REL_VAR4C = Util.checkNull(data.getString("REL_VAR4C"));
                    String REL_VAR4F = Util.checkNull(data.getString("REL_VAR4F"));
                    String REL_VAR4P = Util.checkNull(data.getString("REL_VAR4P"));
                    /*SERIE 5*/
                    String MESVAR5 = Util.checkNull(data.getString("MESVAR5"));
                    String REL_VAR5 = Util.checkNull(data.getString("REL_VAR5"));
                    String REL_VAR5D = Util.checkNull(data.getString("REL_VAR5D"));
                    String REL_VAR5C = Util.checkNull(data.getString("REL_VAR5C"));
                    String REL_VAR5F = Util.checkNull(data.getString("REL_VAR5F"));
                    String REL_VAR5P = Util.checkNull(data.getString("REL_VAR5P"));

                    out.print("\"ANIO\":\"" + ANIO + "\"");
                    out.print(",");
                    out.print("\"APENOM\":\"" + APE + " " + SAPE + " " + NOM + " " + SNOM + "\"");
                    out.print(",");
                    out.print("\"CODIGO\":\"" + CODIGO + "\"");
                    out.print(",");
                    out.print("\"NACIONALIDAD\":\"" + NACIONALIDAD + "\"");
                    out.print(",");
                    out.print("\"CEDULA\":\"" + CEDULA + "\"");
                    out.print(",");
                    out.print("\"RIF\":\"" + RIF + "\"");
                    out.print(",");
                    out.print("\"NOMBRE_BANCO\":\"" + NOMBRE_BANCO + "\"");
                    out.print(",");
                    out.print("\"RIF_BANCO\":\"" + RIF_BANCO + "\"");
                    out.print(",");
                    out.print("\"NRO_CONTRIBUYENTE\":\"" + NRO_CONTRIBUYENTE + "\"");
                    out.print(",");
                    out.print("\"NOMBRE_FUNCIONARIO\":\"" + NOMBRE_FUNCIONARIO + "\"");
                    out.print(",");
                    out.print("\"CEDULA_FUNCIONARIO\":\"" + CEDULA_FUNCIONARIO + "\"");
                    out.print(",");
                    out.print("\"DIRECCION_AGENTE\":\"" + DIRECCION_AGENTE + "\"");
                    out.print(",");
                    out.print("\"CIUDAD\":\"" + CIUDAD + "\"");
                    out.print(",");
                    out.print("\"CODIGO_POSTAL\":\"" + CODIGO_POSTAL + "\"");
                    out.print(",");
                    out.print("\"ESTADO\":\"" + ESTADO + "\"");
                    out.print(",");
                    out.print("\"TELEFONO\":\"" + TELEFONO + "\"");
                    out.print(",");
                    out.print("\"REM_ENE\":\"" + REM_ENE + "\"");
                    out.print(",");
                    out.print("\"REM_FEB\":\"" + REM_FEB + "\"");
                    out.print(",");
                    out.print("\"REM_MAR\":\"" + REM_MAR + "\"");
                    out.print(",");
                    out.print("\"REM_ABR\":\"" + REM_ABR + "\"");
                    out.print(",");
                    out.print("\"REM_MAY\":\"" + REM_MAY + "\"");
                    out.print(",");
                    out.print("\"REM_JUN\":\"" + REM_JUN + "\"");
                    out.print(",");
                    out.print("\"REM_JUL\":\"" + REM_JUL + "\"");
                    out.print(",");
                    out.print("\"REM_AGO\":\"" + REM_AGO + "\"");
                    out.print(",");
                    out.print("\"REM_SEP\":\"" + REM_SEP + "\"");
                    out.print(",");
                    out.print("\"REM_OCT\":\"" + REM_OCT + "\"");
                    out.print(",");
                    out.print("\"REM_NOV\":\"" + REM_NOV + "\"");
                    out.print(",");
                    out.print("\"REM_DIC\":\"" + REM_DIC + "\"");
                    out.print(",");
                    /*Serie 1*/
                    out.print("\"MESVAR1\":\"" + MESVAR1 + "\"");
                    out.print(",");
                    out.print("\"REL_ENE\":\"" + REL_ENE + "\"");
                    out.print(",");
                    out.print("\"REL_ENED\":\"" + REL_ENED + "\"");
                    out.print(",");
                    out.print("\"REL_ENEC\":\"" + REL_ENEC + "\"");
                    out.print(",");
                    out.print("\"REL_ENEF\":\"" + REL_ENEF + "\"");
                    out.print(",");
                    out.print("\"REL_ENEP\":\"" + REL_ENEP + "\"");
                    out.print(",");
                    /*Serie 2*/
                    out.print("\"MESVAR2\":\"" + MESVAR2 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR2\":\"" + REL_VAR2 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR2D\":\"" + REL_VAR2D + "\"");
                    out.print(",");
                    out.print("\"REL_VAR2C\":\"" + REL_VAR2C + "\"");
                    out.print(",");
                    out.print("\"REL_VAR2F\":\"" + REL_VAR2F + "\"");
                    out.print(",");
                    out.print("\"REL_VAR2P\":\"" + REL_VAR2P + "\"");
                    out.print(",");
                    /*Serie 3*/
                    out.print("\"MESVAR3\":\"" + MESVAR3 + "\"");
                    out.print(",");
                    out.print("\"REL_JUN\":\"" + REL_JUN + "\"");
                    out.print(",");
                    out.print("\"REL_JUND\":\"" + REL_JUND + "\"");
                    out.print(",");
                    out.print("\"REL_JUNC\":\"" + REL_JUNC + "\"");
                    out.print(",");
                    out.print("\"REL_JUNF\":\"" + REL_JUNF + "\"");
                    out.print(",");
                    out.print("\"REL_JUNP\":\"" + REL_JUNP + "\"");
                    out.print(",");
                    /*Serie 4*/
                    out.print("\"MESVAR4\":\"" + MESVAR4 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR4\":\"" + REL_VAR4 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR4D\":\"" + REL_VAR4D + "\"");
                    out.print(",");
                    out.print("\"REL_VAR4C\":\"" + REL_VAR4C + "\"");
                    out.print(",");
                    out.print("\"REL_VAR4F\":\"" + REL_VAR4F + "\"");
                    out.print(",");
                    out.print("\"REL_VAR4P\":\"" + REL_VAR4P + "\"");
                    out.print(",");
                    /*Serie 5*/
                    out.print("\"MESVAR5\":\"" + MESVAR5 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR5\":\"" + REL_VAR5 + "\"");
                    out.print(",");
                    out.print("\"REL_VAR5D\":\"" + REL_VAR5D + "\"");
                    out.print(",");
                    out.print("\"REL_VAR5C\":\"" + REL_VAR5C + "\"");
                    out.print(",");
                    out.print("\"REL_VAR5F\":\"" + REL_VAR5F + "\"");
                    out.print(",");
                    out.print("\"REL_VAR5P\":\"" + REL_VAR5P + "\"");
                    out.print(",");
                    /*RETENCIONES*/
                    out.print("\"RET_ENE\":\"" + RET_ENE + "\"");
                    out.print(",");
                    out.print("\"RET_FEB\":\"" + RET_FEB + "\"");
                    out.print(",");
                    out.print("\"RET_MAR\":\"" + RET_MAR + "\"");
                    out.print(",");
                    out.print("\"RET_ABR\":\"" + RET_ABR + "\"");
                    out.print(",");
                    out.print("\"RET_MAY\":\"" + RET_MAY + "\"");
                    out.print(",");
                    out.print("\"RET_JUN\":\"" + RET_JUN + "\"");
                    out.print(",");
                    out.print("\"RET_JUL\":\"" + RET_JUL + "\"");
                    out.print(",");
                    out.print("\"RET_AGO\":\"" + RET_AGO + "\"");
                    out.print(",");
                    out.print("\"RET_SEP\":\"" + RET_SEP + "\"");
                    out.print(",");
                    out.print("\"RET_OCT\":\"" + RET_OCT + "\"");
                    out.print(",");
                    out.print("\"RET_NOV\":\"" + RET_NOV + "\"");
                    out.print(",");
                    out.print("\"RET_DIC\":\"" + RET_DIC + "\"");
                    out.print(",");

                    status = "true";
                }
            }

            
            datos.cerrar();
        } catch (Exception e) {
            datos.cerrar();
            Util.log(this, e);
            status = "false";
        }finally {
            datos.cerrar();
        }               
        out.print("\"status\":\"" + status + "\"");
        out.print("}");
        
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";        
        String resultado = status.equals("true") ? "Consultado" : "Error de consulta";
        String key = colaborador.toString().trim();             
        Seguridad.log(accion, resultado, sql, key);        
    }      
}
