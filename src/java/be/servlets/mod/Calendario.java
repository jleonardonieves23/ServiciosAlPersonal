package be.servlets.mod;

import be.entidades.Dia;
import be.entidades.Entidades;
import be.entidades.Usuario;
import be.utils.Util;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Calendario {
    
    public void calendarios(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {  
        try {
            String period = request.getParameter("period");
            String calendario = request.getParameter("calendario");
            String birthday = request.getParameter("birthday");
            String fecha = "";  
            String hoy = "";
            String mes = "";
            boolean esHoy = period != null && period.trim().toLowerCase().equals("hoy");
            boolean esMes = period != null && period.trim().toLowerCase().equals("mes");
            boolean esBirthdayOnly = birthday != null && birthday.trim().toLowerCase().equals("only");
            boolean esBirthdayAll = birthday != null && birthday.trim().toLowerCase().equals("all");
            
            if (esHoy) {
                hoy = Util.getCurrentDate();
                fecha = Util.getCurrentDay() + "/" + Util.getCurrentMonth() + "/%";
            }
            
            if (esMes) {                
                mes = "%/" + Util.getCurrentMonth() + "/" + Util.getCurrentYear();
                fecha = "%/" + Util.getCurrentMonth() + "/%";
            }            
            
            Dia dia = new Dia();
            Entidades datos = new Entidades(dia);
            String filtro = "SUBSTRING(fecha, 7, 4) = year(getdate())";
                        
            filtro = esHoy ? "fecha = '" + hoy + "'" : filtro;
            filtro = esMes ? "fecha like '" + mes + "'" : filtro;            
            filtro = filtro != null && !filtro.isEmpty() && calendario != null ? " and " : filtro;
            filtro = calendario != null ? filtro + " calendario = '" + calendario + "'" : filtro;
            
            if (!esBirthdayOnly && !esBirthdayAll) {
                datos.cargar(filtro, "fecha");
            }
            
            Usuario user = new Usuario();
            Entidades datos2 = new Entidades(user);
            
            if (birthday != null && !birthday.isEmpty()) {
                fecha = esBirthdayAll ? "%" : fecha;
                datos2.cargar(fecha != null && !fecha.isEmpty() ? "fecha_nacimiento like '" + fecha + "'" : null, "isnull(substring(fecha_nacimiento, 4, 2), ''), isnull(substring(fecha_nacimiento, 1, 2), '')");
                String nombre;
                String year = Util.getCurrentYear();
                String[] parts;
                if (!datos2.vacio()) {
                    for(be.entidades.Entidad e : datos2.elementos()) {
                        user = (Usuario) e;
                        if (user.fecha_nacimiento != null && !user.fecha_nacimiento.isEmpty()) {
                            parts = user.fecha_nacimiento.split("/");
                            if (parts.length > 2) {
                                fecha = parts[0] + "/" + parts[1] + "/" + year;
                                nombre = user.obtenerNombreCompleto();
                                dia = new Dia();
                                dia.fecha = fecha;
                                dia.descripcion = nombre.toLowerCase();
                                dia.calendario = "Cumpleaños";
                                dia.id = user.id;
                                datos.agregar(dia);   
                            }
                        }
                    }
                }  
            }
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }            
        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }            
    }   
    
    public void calendario(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String calendar = request.getParameter("calendar");
            be.entidades.Calendario calendario = new be.entidades.Calendario();
            Entidades datos = new Entidades(calendario);
            datos.cargar(calendar, "fecha", true);                                                                                    
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }       
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }    
    }
    
}
