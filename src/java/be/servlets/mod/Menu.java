package be.servlets.mod;

import be.entidades.Entidad;
import be.entidades.Entidades;
import be.entidades.Usuario;
import be.utils.Util;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Menu {
        
    public void obtener(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {  
        try {
            out.print("[");
            String context = request.getParameter("context");
            String contexto = (context == null || context.isEmpty()) ? (" contexto is null or contexto = '' ") : (" contexto = '" + context + "' ");
            Usuario usuario = Util.obtenerUsuarioActual(request);
            be.entidades.Menu menu = new be.entidades.Menu();
            Entidades datos = new Entidades(menu);
            contexto += " AND activo = 1 ";
            datos.cargar(contexto, "posicion");
            if (!datos.vacio()) {
                String separator = "";                     
                String permiso;
                for(Entidad item : datos.elementos()) {
                    permiso = item.obtener("permiso");
                    if (permiso != null && usuario.tienePermisos(permiso) && item.obtener("accion") != null && !item.obtener("accion").isEmpty()) {
                        out.print(separator);                                
                        out.print(item.serializar());
                        separator = ",";
                    }                     
                }
            }            
            out.print("]");
        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }            
    }  
    
    public void obtenerLista(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {  
                try {             
            
            String grupo = request.getParameter("grupo");
            
            be.entidades.Menu entidad = new be.entidades.Menu();
            Entidades datos = new Entidades(entidad);
            
            if (grupo.equals("0")){                
                datos.cargar();   
            }
            else
            {
                datos.cargar("permiso ='" + grupo  + "'");   
            }
            
            
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }             
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }  
}
