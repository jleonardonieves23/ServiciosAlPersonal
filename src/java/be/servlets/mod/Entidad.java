package be.servlets.mod;

import be.entidades.Entidades;
import be.procesos.Datos;
import be.utils.Util;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

public class Entidad {

    public void obtener(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String filter = request.getParameter("filter");
            String order = request.getParameter("order");
            String entidadName = request.getParameter("entidad");   
            
            if (entidadName == null || entidadName.isEmpty()) {
                entidadName = (String) request.getAttribute("entidad");
            }
            if (entidadName != null && !entidadName.isEmpty()) {
                String packageName = be.entidades.Entidad.class.getPackage().getName();
                String className = packageName +  "." + entidadName;            
                be.entidades.Entidad entidad = (be.entidades.Entidad) Class.forName(className).newInstance();
                Entidades datos = new Entidades(entidad);
                datos.cargar(filter, order, true);
                if (datos.vacio()) {
                    out.print("[" + entidad.serializar() + "]");
                } else {
                    out.print(datos.serializar());
                }
            } else {
                out.print("[]");
            }
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }               
    }   

    public void guardar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String entidadName = request.getParameter("entidad");            
            String packageName = be.entidades.Entidad.class.getPackage().getName();
            String className = packageName +  "." + entidadName;
            be.entidades.Entidad entidad = (be.entidades.Entidad) Class.forName(className).newInstance();
            String id = request.getParameter("id");
            String status = "false";
            String message = "";
            Properties params = Util.obtenerPropiedades(request);
            entidad.cargar(params);
            if (entidad.guardar()) {
                status = "true";
            } else {
                message = "Hubo un error en la operación";
            }
            id = entidad.obtenerID();
            Properties properties = new Properties();
            properties.setProperty("status", status);
            properties.setProperty("id", id);
            properties.setProperty("message", message);
            Util.mostrar(out, properties);        
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }     

    public void borrar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String entidadName = request.getParameter("entidad");            
            String packageName = be.entidades.Entidad.class.getPackage().getName();
            String className = packageName +  "." + entidadName;
            be.entidades.Entidad entidad = (be.entidades.Entidad) Class.forName(className).newInstance();
            String id = request.getParameter("id");
            String status = "false";
            String message = "";
            entidad.cargar(id);
            if (entidad.borrar()) {
                status = "true";
            } else {
                message = "Hubo un error en la operación";
            }
            Properties properties = new Properties();
            properties.setProperty("status", status);
            properties.setProperty("id", id);
            properties.setProperty("message", message);
            Util.mostrar(out, properties);        
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    } 
    
    public void cargarMasivo(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {  
        try {
            String status = "false";
            String message = "";
            String entidadName = request.getParameter("entidad");
            if (entidadName != null && !entidadName.isEmpty()) {
                String packageName = be.entidades.Entidad.class.getPackage().getName();
                String className = packageName +  "." + entidadName;            
                be.entidades.Entidad entidad = (be.entidades.Entidad) Class.forName(className).newInstance();            
                String table = entidad._TABLE_NAME;
                Part file = request.getPart("file");
                if (file != null) {     
                    InputStream inputStream = file.getInputStream();
                    InputStreamReader inputStreamReader =  new InputStreamReader(inputStream, Charset.forName("UTF-8"));
                    BufferedReader in = new BufferedReader(inputStreamReader);
                    StringBuilder s = new StringBuilder();
                    String line;
                    while ((line = in.readLine()) != null) {
                        s.append(line);
                        s.append("\n");
                    }                    
                                       
                    String result = s.toString();
                    String[] data = result.trim().replace("\r", "").split("\n");
                    line = data[0];
                    String[] cells;
                    String fields;
                    
                    //!!!!!!!!!!!!!!!
                    //ESTA COCHINADA DE SQL ES PORQUE SQL SERVER 2005 NO ACEPTA INSERT CON MULTIPLES ROWS
                    //!!!!!!!!!!!!!!!
                    String sql = "";
                    if (line!=null && !line.isEmpty()) {
                        cells = line.trim().replace("\r", "").split("\t");
                        fields = "(id," + Util.unir(cells) + ")";
                        s = new StringBuilder();
                        for(int i=1; i<data.length; i++) {
                            cells = data[i].trim().replace("\r", "").split("\t");    
                            if (cells.length > 0) {
                                s.append("INSERT INTO ");
                                s.append(table);                                
                                s.append(" ");
                                s.append(fields);
                                s.append(" VALUES ");
                                s.append("('");
                                s.append(entidad.generarID());
                                s.append("',");
                                s.append(Util.unir(cells, "'"));
                                s.append(");");
                            }
                        }
                        sql = s.toString();                       
                        Datos datos = new Datos(entidad._CONNECTION);
                        if (datos.procesar(sql)) {
                            status = "true";
                            message = "Operación Exitosa";
                        } else {
                            status = "false";
                            message = "Hubo un error en la operación";
                        }   
                        datos.cerrar();
                    }                                
                }             
            } else {
                message = "No se identificó la entidad";
            }
            Properties result = new Properties();
            result.setProperty("message", message);
            result.setProperty("status", "true");
            Util.mostrar(out, result);
        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }            
    }           
}
