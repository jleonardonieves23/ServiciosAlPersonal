package be.servlets.mod;

import be.entidades.ActiveDirectory;
import be.entidades.Entidades;
import be.seguridad.Seguridad;
import be.utils.Util;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;
import javax.imageio.ImageIO;
import javax.naming.directory.DirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

public class Usuario {

    public void obtener(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            be.entidades.Usuario usuario = new be.entidades.Usuario();
            Entidades datos = new Entidades(usuario);
            datos.cargar();
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }       
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }    
    }
         
    public void buscar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {        
        out.print("{"); 
        String status = "true";
        String separator = "";                   
        try {            
            String search = request.getParameter("search");
            if (search == null || search.trim().equals("")) {
                status = "false";
            } else {               
                DirContext activeDirectoryContext = (DirContext) session.getAttribute("ACTIVE_DIRECTORY_CONTEXT");
                if (activeDirectoryContext == null) {
                    activeDirectoryContext = Seguridad.getContext();
                    session.setAttribute("ACTIVE_DIRECTORY_CONTEXT", activeDirectoryContext);
                }
                ActiveDirectory ad = new ActiveDirectory(activeDirectoryContext);
                be.entidades.Usuario usuario = ad.getUser(search);   
                if (usuario == null) {
                    status = "false";
                } else 
                {
                    String id = search.substring(1);
                    usuario.recargar("id", id);
                    usuario.imagen(request);     
                    out.print("\"usuario\":");
                    out.print(usuario.serializar());
                    separator = ",";                    
                }
                
            }                
        } catch(Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print(separator);
        out.print("\"status\":\"" + status + "\"");
        out.print("}"); 
    }

    public void guardarPerfil(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {        
        guardar(request, response, out, session, true);
    }    
    
    public void guardar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {        
        guardar(request, response, out, session, false);
    }

    public void guardar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session, boolean updateSession)  {        
        out.print("{"); 
        String status = "true";
        String separator = "";                
        try {
                           
            Properties params = Util.obtenerPropiedades(request);
            be.entidades.Usuario usuario = new be.entidades.Usuario();
            if(usuario.cargar(params) && usuario.guardar()) {
                String id = usuario.id;
                usuario.imagen(request);
                if (updateSession) {
                    usuario = Util.obtenerUsuarioActual(request);
                    usuario.cargar(usuario.obtenerPropiedadesTabla());
                    Util.asignarUsuarioActual(request, usuario);
                }

                Part imagen = request.getPart("file");
                if (imagen != null) {     
                    InputStream inputStream = imagen.getInputStream();                                 
                    BufferedImage originalImage = ImageIO.read(inputStream);
                    int IMG_WIDTH = 150;
                    int IMG_HEIGHT = 150;
                    BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, BufferedImage.TYPE_4BYTE_ABGR);
                    Graphics2D g = resizedImage.createGraphics();
                    g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
                    g.dispose();      
                    String path = Util.ruta(request, "/pic");
                    String filename = path + "\\" + id + ".png";                
                    File file = new File(filename);
                    ImageIO.write(resizedImage, "PNG", file);    
                }    
            } else {
               status = "false"; 
            }
                        
        } catch(Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print(separator);
        out.print("\"status\":\"" + status + "\"");
        out.print("}"); 
    }
    
    public void listar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {              
            DirContext activeDirectoryContext = Seguridad.getContext();
            ActiveDirectory ad = new ActiveDirectory(activeDirectoryContext);
            out.write(ad.getAllUsers());
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }    
    }    
           
}
