package be.servlets.mod;

import be.utils.Util;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Banner {
    
    public void obtener(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            ArrayList list = new ArrayList();
            File folder = new File(Util.ruta(request, "/mod/banners/img"));
            for (final File fileEntry : folder.listFiles()) {
                 if (fileEntry.isFile()) {
                     list.add(fileEntry.getName());
                 }
             }    
            Properties properties = new Properties();
            properties.setProperty("status", "true");
            properties.setProperty("images", "[" + Util.unir(list.toArray(), "\"") + "]");
            Util.mostrar(out, properties);            
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }    
    
}
