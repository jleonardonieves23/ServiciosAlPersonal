package be.servlets.mod;

import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Util;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Seguros {
  
    public void consultarInfoPoliza(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String colaborador = "";
        String sql = "";
        boolean statuss = true;
        String status = "false";    
        String message = "";        
        try {
            colaborador = Util.obtenerCodigoColaborador(request);
            
            //INFORMACION DE LA POLIZA
            Datos datos = new Datos("jdbc/rrhh");            
            sql = "SELECT 0 AS TIPO, A.APELL1 || ' ' || A.APELL2 || ' ' || A.NOMBR1 || ' ' || A.NOMBR2 AS NOMBRE, LTRIM(A.CEDULA) AS CEDULA, '' AS PARENTEZCO, DECODE(A.SEXTRA, '1', 'MASCULINO', '2', 'FEMENINO', ' ') AS GENERO, A.FECNAC AS FECHA_NACIMIENTO, D.NOMRAM AS CONCEPTO, C.FECINC AS FECHA_INCLUSION, C.FECDES AS FECHA_EXCLSION, C.RMO_CODRAM AS COD_CONCEPTO FROM NMM001 A, RHM011 C, RHT003 D WHERE A.CIA_CODCIA = C.CIA_CODCIA AND A.SUBTIP = C.TRAB_SUBTIP AND A.FICTRA = C.TRAB_FICTRA AND C.CIA_CODCIA = D.CIA_CODCIA AND C.CSG_CIASEG = D.CSG_CIASEG AND C.RMO_CODRAM = D.CODRAM AND (LTRIM(A.FICTRA) = '<colaborador>') AND (LTRIM(C.CSG_CIASEG) = '01') AND (D.VENPOL IS NULL) AND (A.FECRET IS NULL) AND (C.FECDES IS NULL) UNION ALL SELECT 1 AS TIPO, D.NOMPAR AS NOMBRE, LTRIM(D.CEDPAR) AS CEDULA, E.DESPAR AS PARENTEZCO, DECODE(D.SEXPAR, '1', 'MASCULINO', '2', 'FEMENINO', ' ') AS GENERO, D.FENAPA AS FECHA_NACIMIENTO, F.NOMRAM AS CONCEPTO, C.FECINC AS FECHA_INCLUSION, C.FECDES AS FECHA_EXCLSION, C.RMO_CODRAM AS COD_CONCEPTO FROM RHM011 A, NMM001 B, RHM002 C, RHM001 D, RHT006 E, RHT003 F WHERE (A.TRAB_FICTRA = B.FICTRA) AND (A.CIA_CODCIA = B.CIA_CODCIA) AND (A.TRAB_FICTRA = C.TRAB_FICTRA) AND (A.RMO_CODRAM = C.RMO_CODRAM) AND (A.CIA_CODCIA = C.CIA_CODCIA) AND (C.TRAB_FICTRA = D.TRAB_FICTRA) AND (C.FML_CONPAR = D.CONPAR) AND (C.CIA_CODCIA = D.CIA_CODCIA) AND (D.PRT_CODPAR = E.CODPAR) AND (B.FECRET IS NULL) AND (C.FECDES IS NULL) AND (LTRIM(A.TRAB_FICTRA) = '<colaborador>') AND (LTRIM(A.CSG_CIASEG) = '01') AND (C.RMO_CODRAM = F.CODRAM) AND (A.CSG_CIASEG = F.CSG_CIASEG) AND (F.VENPOL IS NULL) AND (A.CIA_CODCIA = F.CIA_CODCIA) ORDER BY TIPO, NOMBRE";
            Properties params = new Properties();                
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            Object results = datos.ejecutar(sql);            
                        
            if (results != null && ResultSet.class.isInstance(results)) {
                ResultSet data          = (ResultSet) results;
                String separator        = "";
                String tipo             = "";
                String nombre           = "";
                String cedula           = "";
                String parentezco       = "";
                String genero           = "";
                String fecha_nacimiento = "";
                String concepto         = "";
                String fecha_inclusion  = "";
                String fecha_exclsion   = "";
                String cod_concepto     = "";
                out.print("\"rows\":");                       
                out.print("["); 
                while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                    tipo             = Util.checkNull(data.getString("tipo"));
                    nombre           = Util.checkNull(data.getString("nombre"));
                    cedula           = Util.checkNull(data.getString("cedula"));
                    parentezco       = Util.checkNull(data.getString("parentezco"));
                    genero           = Util.checkNull(data.getString("genero"));
                    fecha_nacimiento = Util.checkNull(data.getString("fecha_nacimiento"));
                    concepto         = Util.checkNull(data.getString("concepto"));
                    fecha_inclusion  = Util.checkNull(data.getString("fecha_inclusion"));
                    fecha_exclsion   = Util.checkNull(data.getString("fecha_exclsion"));
                    cod_concepto     = Util.checkNull(data.getString("cod_concepto"));

                    out.print(separator);                                                
                    out.print("{");
                    out.print("\"tipo\":\""+tipo+"\"");                         
                    out.print(",");                        
                    out.print("\"nombre\":\""+nombre+"\"");                         
                    out.print(",");                        
                    out.print("\"cedula\":\""+cedula+"\"");                         
                    out.print(",");                        
                    out.print("\"parentezco\":\""+parentezco+"\"");                         
                    out.print(",");                        
                    out.print("\"genero\":\""+genero+"\"");                         
                    out.print(",");                                            
                    out.print("\"fecha_nacimiento\":\""+fecha_nacimiento+"\"");                         
                    out.print(",");                        
                    out.print("\"concepto\":\""+concepto+"\"");                         
                    out.print(",");                        
                    out.print("\"fecha_inclusion\":\""+fecha_inclusion+"\"");                         
                    out.print(",");                        
                    out.print("\"fecha_exclusion\":\""+fecha_exclsion+"\"");                         
                    out.print(",");        
                    out.print("\"cod_concepto\":\""+cod_concepto+"\"");   
                     
                    out.print("}");        
                    separator = ",";                        
                }    
                out.print("]");
                out.print(",");            
                status = "true";
            }
            
            /*
            //INFORMACION DE CONCEPTOS DE LA POLIZA            
            sql = "SELECT CODRAM AS CODIGO, NOMRAM AS NOMBRE FROM RHT003 WHERE LTRIM(CIA_CODCIA) = '01' AND LTRIM(CSG_CIASEG) = '01' AND VENPOL IS NULL ORDER BY CODRAM";            
            results = datos.ejecutar(sql);                                    
            if (results != null && ResultSet.class.isInstance(results)) {
                ResultSet data          = (ResultSet) results;
                String separator        = "";
                out.print("\"conceptos\":");                       
                out.print("["); 
                while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                    out.print(separator);                                                
                    out.print("{");
                    out.print("\"codigo\":\""+Util.checkNull(data.getString("codigo"))+"\"");                         
                    out.print(",");                        
                    out.print("\"nombre\":\""+Util.checkNull(data.getString("nombre"))+"\"");                         
                    out.print("}");        
                    separator = ",";                        
                }    
                out.print("]");
                out.print(",");                                                   
                status = "true";
            }   
            */
            datos.cerrar();                             
            
            //INFORMACION DESCRIPTIVA
            datos = new Datos("jdbc/portal");            
            sql = "SELECT texto FROM Info where id='seguro_info'";
            
            results = datos.ejecutar(sql);
            String description = "";
            
            if (results != null && ResultSet.class.isInstance(results)) {
                ResultSet data     = (ResultSet) results;   
                if (data.next()) {
                    description = Util.checkNull(data.getString("texto"));   
                    description = description == null ? "" : description;
                }
            }
            out.print("\"description\":\""+description+"\"");                         
            out.print(",");  
            datos.cerrar();
                        
        } catch (Exception e) {
            message = e.getMessage();
            Util.log(this, e);
            status = "false";
        }
        out.print("\"message\":\"" + message + "\""); 
        out.print(","); 
        out.print("\"status\":\"" + status + "\"");            
        out.print("}"); 
        
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";
        String resultado = statuss ? "Consultado" : "Error de consulta";
        String key = colaborador;        
        Seguridad.log(accion, resultado, sql, key);
    }      
}
