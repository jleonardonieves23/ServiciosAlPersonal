package be.servlets.mod;

import be.entidades.ActiveDirectory;
import be.entidades.Entidades;
import be.procesos.Datos;
import be.utils.Util;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class Listado {
          
    public void bivalente(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {      
        try {
            String status  = "true";
            String message = "";
            String data    = "{\"items\":[{\"name\":\"Si\",\"value\":\"1\"},{\"name\":\"No\",\"value\":\"0\"}]}";      

            Properties properties = new Properties();
            properties.setProperty("status", status);
            properties.setProperty("message", message);            
            properties.setProperty("data", data);
            Util.mostrar(out, properties);        
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
               
    }  
    
    public void gruposDirectorioActivo(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {      
        try {
            String status  = "false";
            String message = "";
            String data    = "{\"items\":[{\"name\":\"Banco Exterior\",\"value\":\"Banco Exterior\"}";    
            
            ActiveDirectory ad = new ActiveDirectory();
            ArrayList<String> listado = ad.getAllGroups();
            
            for(String item:listado) {
                data += ",{\"name\":\"" + item +  "\",\"value\":\"" + item +  "\"}";
            }
            data += "]}";
            Properties properties = new Properties();
            properties.setProperty("status", status);
            properties.setProperty("message", message);            
            properties.setProperty("data", data);
            Util.mostrar(out, properties);        
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
               
    }     
}
