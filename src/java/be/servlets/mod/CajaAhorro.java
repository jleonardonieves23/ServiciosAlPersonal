package be.servlets.mod;

import be.entidades.Entidades;
import be.procesos.Datos;
import be.utils.Util;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class CajaAhorro {
  
    public void consultar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String status  = "false";    
        String message = "";        
        try {
            String colaborador = Util.obtenerCodigoColaborador(request);            
            be.entidades.CajaAhorro data = new be.entidades.CajaAhorro();            
            colaborador = String.format("%08d", Integer.parseInt(colaborador));
            data.cargar(colaborador);            
            out.print("\"data\":");                       
            out.print(data.serializar());
            out.print(","); 

            be.entidades.CajaAhorroMovimiento movimiento = new be.entidades.CajaAhorroMovimiento();
            Entidades entidades = new Entidades(movimiento);
            entidades.cargar("Codigo='"+colaborador+"'", "FECHA_MOV DESC");
            String movimientos = entidades.serializar();            
            out.print("\"rows\":");                       
            out.print(movimientos.isEmpty() ? "[]" : movimientos);
            out.print(",");             

            status = "true";                             
        } catch (Exception e) {
            message = e.getMessage();
            Util.log(this, e);
            status = "false";
        }
        out.print("\"message\":\"" + message + "\""); 
        out.print(","); 
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");  
    }      
}
