package be.servlets.mod;

import be.entidades.ActiveDirectory;
import be.entidades.Usuario;
import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Util;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.naming.directory.DirContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Comedor {

    public void buscar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {        
        out.print("{"); 
        String sql = "";
        String status = "true";
        String code  = "";
        try {

            Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);            
            if (usuario!=null) {
                code  = Util.obtenerCodigoColaborador(request);
                be.entidades.UsuarioSPI colaborador = new be.entidades.UsuarioSPI();
                colaborador.cargar(String.format("%10s", code));

                String month = request.getParameter("month");
                String year  = request.getParameter("year");                
                month        = month == null || month.equals("") ? Util.getCurrentMonth() : month;
                year         = year  == null || year.equals("")  ? Util.getCurrentYear()  : year;
                int m1       = Integer.parseInt(month);
                int y1       = Integer.parseInt(year);
                int m2       = m1 > 1 ? m1-1 : 12;
                int y2       = m1 > 1 ? y1   : y1-1;
                                
                String startDate = y2 + "-" + m2 + "-16";
                String endDate   = y1 + "-" + m1 + "-15";
                
                Datos datos = new Datos("jdbc/profit"); 
                try {
                    sql = "SET LANGUAGE us_english; SELECT fecha = factura_fecha_emision, hora = cast(DATEPART(HOUR, factura_fecha_completa) as varchar) + ':' + cast(DATEPART(MINUTE, factura_fecha_completa) as varchar), codigo = cliente_codigo, cliente = cliente_nombre, rif = cliente_rif, factura = factura_numero, monto_neto = factura_monto_neto, articulo = articulo_nombre, cantidad = factura_detalle_cantidad, monto_articulo = factura_detalle_precio_venta, turno = turno_descripcion FROM ( SELECT articulo_codigo = co_art , articulo_nombre = art_des FROM art ) AS articulo , ( SELECT cliente_codigo = co_cli, cliente_nombre = cli_des, cliente_rif = rif FROM clientes ) AS clientes , ( SELECT factura_numero = fact_num, factura_nombre = nombre, factura_fecha_completa = fe_us_el, factura_rif = rif, factura_numero_control = num_control, factura_status = status, factura_comentario = comentario, factura_descripcion = descrip, factura_saldo = saldo, factura_fecha_emision = fec_emis, factura_fecha_vencimiento = fec_venc, factura_cliente_codigo = co_cli, factura_venderdor_codigo = co_ven, factura_monto_neto = tot_neto, factura_turno_codigo = num_turno FROM factura ) AS factura , ( SELECT factura_detalle_factura_numero = fact_num, factura_detalle_articulo_codigo = co_art, factura_detalle_cantidad = total_art, factura_detalle_precio_venta = prec_vta, factura_detalle_monto_neto = reng_neto FROM fact_reng ) AS factura_detalle, ( SELECT turno_codigo = num_turno, turno_descripcion = des_turno FROM turnosic, turnos WHERE turnosic.co_turno = turnos.co_turno ) AS turno WHERE factura_cliente_codigo = cliente_codigo AND factura_detalle_factura_numero = factura_numero AND factura_detalle_articulo_codigo = articulo_codigo AND factura_turno_codigo = turno_codigo AND cliente_codigo like '<code>' AND factura_fecha_emision <= '<endDate>' AND factura_fecha_emision >= '<startDate>' ORDER BY factura_fecha_emision, factura_cliente_codigo, factura_numero ";

                    Properties params = new Properties();                
                    params.setProperty("<code>", code);
                    params.setProperty("<startDate>", startDate);
                    params.setProperty("<endDate>", endDate);
                    sql = Util.parseSQLParams(sql, params);

                    Object results = datos.ejecutar(sql);

                    if (results != null && ResultSet.class.isInstance(results)) {
                        ResultSet data = (ResultSet) results;
                        String separator = "";
                        out.print("\"colaborador\":"); 
                        out.print("\"" + colaborador.primer_nombre + " " + colaborador.segundo_nombre + " " + colaborador.primer_apellido +"\"");                       
                        out.print(",");
                        out.print("\"rows\":");                       
                        out.print("[");

                        float total = 0;  
                        while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                            out.print(separator);                        
                            out.print("{");
                            out.print("\"fecha\":\"" + Util.checkNull(data.getString("fecha")) + "\"");            
                            out.print(",");
                            out.print("\"hora\":\"" + Util.checkNull(data.getString("hora")) + "\"");            
                            out.print(",");
                            out.print("\"factura\":\"" + Util.checkNull(data.getString("factura")) + "\"");    
                            out.print(",");
                            out.print("\"monto_neto\":\"" + Util.checkNull(data.getString("monto_neto")) + "\""); 
                            out.print(",");
                            out.print("\"articulo\":\"" + Util.checkNull(data.getString("articulo")) + "\""); 
                            out.print(",");
                            out.print("\"cantidad\":\"" + Util.checkNull(data.getString("cantidad")) + "\""); 
                            out.print(",");
                            out.print("\"monto_articulo\":\"" + Util.checkNull(data.getString("monto_articulo")) + "\"");                                              
                            out.print(",");
                            out.print("\"fecha\":\"" + Util.checkNull(data.getString("fecha")) + "\"");                                                                                              
                            out.print(",");
                            out.print("\"turno\":\"" + Util.checkNull(data.getString("turno")) + "\"");                                                                      
                            out.print("}");                        
                            separator = ",";
                            total +=  Float.parseFloat(Util.checkNull(data.getString("cantidad"))) * Float.parseFloat(data.getString("monto_articulo"));
                            //data.next();
                        }
                        out.print("]");
                        out.print(",");
                        out.print("\"total\":\"" + total + "\"");                    
                        out.print(",");
                    }
                } catch(Exception ex) {
                    Util.log(this, ex);
                    status = "false";
                }
                datos.cerrar();                                               
            }
        } catch (Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";        
        String resultado = status.equals("true") ? "Consultado" : "Error de consulta";
        String key = code;          
        Seguridad.log(accion, resultado, sql, key);           
    }

    public void listar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {        
        out.print("{");            
        String sql = "";
        String code  = "";
        String traza = "";
        String status = "true";            
        try {
            Datos datos = new Datos("jdbc/profit");              
            datos.procesar("SET LANGUAGE us_english");
            
            Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
            if (usuario!=null) {
                code   = request.getParameter("colaborador");
                String month  = request.getParameter("month");
                String year   = request.getParameter("year");    
                String report = request.getParameter("report");
                code          = code  == null || code.equals("")  ? "%"                    : code;
                month         = month == null || month.equals("") ? Util.getCurrentMonth() : month;
                year          = year  == null || year.equals("")  ? Util.getCurrentYear()  : year;
                int m1        = Integer.parseInt(month);
                int y1        = Integer.parseInt(year);
                int m2        = m1 > 1 ? m1-1 : 12;
                int y2        = m1 > 1 ? y1   : y1-1;
                
                DirContext activeDirectoryContext = (DirContext) session.getAttribute("ACTIVE_DIRECTORY_CONTEXT");
                if (activeDirectoryContext == null) {                    
                    activeDirectoryContext = Seguridad.getContext();
                    session.setAttribute("ACTIVE_DIRECTORY_CONTEXT", activeDirectoryContext);
                }
                ActiveDirectory ad = new ActiveDirectory(activeDirectoryContext);
                Usuario user = ad.getUser(code);                
                
                if (user == null) {
                    user          = new Usuario();
                    user.id       = code;
                    user.nombre   = "";
                    user.apellido = "";
                }
                
                
                String startDate = y2 + "-" + m2 + "-16";
                String endDate   = y1 + "-" + m1 + "-15";

                String columns                = ""; 
                sql                           = "";                 
                String fecha                  = "";
                String ultima_fecha           = "";
                float monto_colaborador       = 0;
                float monto_banco             = 0;
                float cantidad                = 0;
                float total_monto_colaborador = 0;
                float total_monto_banco       = 0;  
                float total_cantidad          = 0;
                
                if (report.equals("colaboradores")) {
                    columns = "[{\"title\": \"C&oacute;digo\", \"name\": \"codigo\"}, {\"title\": \"Colaborador\", \"name\": \"colaborador\"}, {\"title\": \"Monto\", \"name\": \"monto_neto\"} ]";
                    sql = "SET LANGUAGE us_english; SELECT codigo = cliente_codigo, cliente = cliente_nombre, monto_neto = sum(factura_monto_neto) FROM ( SELECT cliente_codigo = co_cli, cliente_nombre = cli_des, cliente_rif = rif FROM clientes ) AS clientes , ( SELECT factura_numero = fact_num, factura_nombre = nombre, factura_fecha_completa = fe_us_el, factura_rif = rif, factura_numero_control = num_control, factura_status = status, factura_comentario = comentario, factura_descripcion = descrip, factura_saldo = saldo, factura_fecha_emision = fec_emis, factura_fecha_vencimiento = fec_venc, factura_cliente_codigo = co_cli, factura_venderdor_codigo = co_ven, factura_monto_neto = tot_neto, factura_turno_codigo = num_turno FROM factura ) AS factura WHERE factura_cliente_codigo = cliente_codigo AND cliente_codigo like '<code>' AND factura_fecha_emision <= '<endDate>' AND factura_fecha_emision >= '<startDate>' and  cliente_codigo <> 'VIS' GROUP BY cliente_codigo, cliente_nombre, cliente_rif  ORDER BY  cliente_codigo ";
                }

                if (report.equals("niveles")) {
                    columns = "[{\"title\": \"Fecha\", \"name\": \"fecha\"}, {\"title\": \"Nivel\", \"name\": \"nivel\"}, {\"title\": \"Cantidad\", \"name\": \"cantidad\"}, {\"title\": \"Monto Colaborador\", \"name\": \"monto_colaborador\"}, {\"title\": \"Monto Banco\", \"name\": \"monto_banco\"} ]";
                    //sql = " SELECT fecha = right('00'+cast(DATEPART(DAY, factura_fecha_emision) as varchar),2) + '-' + right('00'+cast(DATEPART(MONTH, factura_fecha_emision) as varchar),2) + '-' + cast(DATEPART(YEAR, factura_fecha_emision) as varchar), nivel = cliente_nivel, cantidad = count(*), monto_colaborador = sum(factura_monto_neto), monto_banco = (count(*) * (select prec_vta5 from art where co_art = '012')) - sum(factura_monto_neto) FROM ( SELECT articulo_codigo = co_art , articulo_nombre = art_des FROM art ) AS articulo , ( SELECT cliente_codigo = co_cli, cliente_nivel = tipo FROM clientes ) AS clientes , ( SELECT factura_numero = fact_num, factura_nombre = nombre, factura_fecha_completa = fe_us_el, factura_rif = rif, factura_numero_control = num_control, factura_status = status, factura_comentario = comentario, factura_descripcion = descrip, factura_saldo = saldo, factura_fecha_emision = fec_emis, factura_fecha_vencimiento = fec_venc, factura_cliente_codigo = co_cli, factura_venderdor_codigo = co_ven, factura_monto_neto = tot_neto, factura_turno_codigo = num_turno FROM factura ) AS factura , ( SELECT factura_detalle_factura_numero = fact_num, factura_detalle_articulo_codigo = co_art, factura_detalle_cantidad = total_art, factura_detalle_precio_venta = prec_vta, factura_detalle_monto_neto = reng_neto FROM fact_reng ) AS factura_detalle, ( SELECT turno_codigo = num_turno, turno_descripcion = des_turno FROM turnosic, turnos WHERE turnosic.co_turno = turnos.co_turno ) AS turno WHERE factura_cliente_codigo = cliente_codigo AND factura_detalle_factura_numero = factura_numero AND factura_detalle_articulo_codigo = articulo_codigo AND articulo_codigo = '012' AND turno_descripcion = 'ALMUERZO' AND factura_turno_codigo = turno_codigo AND cliente_codigo like '<code>' AND factura_fecha_emision < '<endDate>' AND factura_fecha_emision >= '<startDate>' GROUP BY right('00'+cast(DATEPART(DAY, factura_fecha_emision) as varchar),2) + '-' + right('00'+cast(DATEPART(MONTH, factura_fecha_emision) as varchar),2) + '-' + cast(DATEPART(YEAR, factura_fecha_emision) as varchar), cliente_nivel ORDER BY  1 ,2  ";
                    sql = "SET LANGUAGE us_english; SELECT fecha = factura_fecha_emision, nivel = cliente_nivel, cantidad = count(*), monto_colaborador = sum(factura_monto_neto), monto_banco = (count(*) * (select prec_vta5 from art where co_art = '012')) - sum(factura_monto_neto) FROM ( SELECT articulo_codigo = co_art , articulo_nombre = art_des FROM art ) AS articulo , ( SELECT cliente_codigo = co_cli, cliente_nivel = tipo FROM clientes ) AS clientes , ( SELECT factura_numero = fact_num, factura_nombre = nombre, factura_fecha_completa = fe_us_el, factura_rif = rif, factura_numero_control = num_control, factura_status = status, factura_comentario = comentario, factura_descripcion = descrip, factura_saldo = saldo, factura_fecha_emision = fec_emis, factura_fecha_vencimiento = fec_venc, factura_cliente_codigo = co_cli, factura_venderdor_codigo = co_ven, factura_monto_neto = tot_neto, factura_turno_codigo = num_turno FROM factura ) AS factura , ( SELECT factura_detalle_factura_numero = fact_num, factura_detalle_articulo_codigo = co_art, factura_detalle_cantidad = total_art, factura_detalle_precio_venta = prec_vta, factura_detalle_monto_neto = reng_neto FROM fact_reng ) AS factura_detalle, ( SELECT turno_codigo = num_turno, turno_descripcion = des_turno FROM turnosic, turnos WHERE turnosic.co_turno = turnos.co_turno ) AS turno WHERE factura_cliente_codigo = cliente_codigo AND factura_detalle_factura_numero = factura_numero AND factura_detalle_articulo_codigo = articulo_codigo AND articulo_codigo = '012' AND turno_descripcion = 'ALMUERZO' AND factura_turno_codigo = turno_codigo AND cliente_codigo like '<code>' AND factura_fecha_emision < '<endDate>' AND factura_fecha_emision >= '<startDate>' and  cliente_codigo <> 'VIS' GROUP BY factura_fecha_emision, cliente_nivel ORDER BY  1 ,2  ";
                    m2        = m1 < 12 ? m1+1 : 1;
                    y2        = m1 < 12 ? y1   : y1+1;                    
                    startDate = y1 + "-" + m1 + "-01";
                    endDate   = y2 + "-" + m2 + "-01";
                }
                traza = sql;
                Properties params = new Properties();                
                params.setProperty("<code>", code);
                params.setProperty("<startDate>", startDate);
                params.setProperty("<endDate>", endDate);
                sql = Util.parseSQLParams(sql, params);
                               
                Object results = datos.ejecutar(sql);
                                
                if (results != null && ResultSet.class.isInstance(results)) {
                    ResultSet data = (ResultSet) results;
                    String separator = "";
                    
                    out.print("\"rows\":");                       
                    out.print("[");                                                            
                    while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                        out.print(separator);                                                
                        out.print("{");
                        if (report.equals("colaboradores")) {
                            out.print("\"id\":\"" + Util.checkNull(data.getString("codigo")) + "\"");            
                            out.print(",");
                            out.print("\"codigo\":\""+Util.checkNull(data.getString("codigo"))+"\"");                         
                            out.print(",");                        
                            out.print("\"colaborador\":\""+Util.checkNull(data.getString("cliente"))+"\"");                         
                            out.print(",");                        
                            out.print("\"monto_neto\":\"" + Util.checkNull(data.getString("monto_neto")) + "\"");                                                                                        
                        }
                        if (report.equals("niveles")) {
                            fecha             = Util.checkNull(data.getString("fecha"));
                            cantidad          = Float.parseFloat(Util.checkNull(data.getString("cantidad")));
                            monto_colaborador = Float.parseFloat(Util.checkNull(data.getString("monto_colaborador")));
                            monto_banco       = Float.parseFloat(Util.checkNull(data.getString("monto_banco")));
                            if (ultima_fecha.isEmpty()) {
                                ultima_fecha = fecha;
                                out.print("\"id\":\"fecha\"");            
                                out.print(",");
                                out.print("\"fecha\":\""+fecha+"\"");                         
                                out.print(",");                                                    
                                out.print("\"nivel\":\"\"");                         
                                out.print(",");                        
                                out.print("\"cantidad\":\"\"");                         
                                out.print(",");                                  
                                out.print("\"monto_colaborador\":\"\"");                         
                                out.print(",");                        
                                out.print("\"monto_banco\":\"\"");
                                out.print("}");        
                                separator = ","; 
                                out.print(separator); 
                                out.print("{");
                            }  
                            if (fecha.equals(ultima_fecha)) {
                                out.print("\"id\":\"" + Util.checkNull(data.getString("nivel")) + "\"");            
                                out.print(",");
                                out.print("\"fecha\":\"\"");                         
                                out.print(",");                                                    
                                out.print("\"nivel\":\""+Util.checkNull(data.getString("nivel"))+"\"");                         
                                out.print(",");  
                                out.print("\"cantidad\":\""+cantidad+"\"");                         
                                out.print(",");                                  
                                out.print("\"monto_colaborador\":\""+monto_colaborador+"\"");                         
                                out.print(",");                        
                                out.print("\"monto_banco\":\"" + monto_banco + "\"");            
                                total_monto_colaborador += monto_colaborador;
                                total_monto_banco       += monto_banco;
                                total_cantidad          += cantidad;
                            } else {
                                out.print("\"id\":\"total\"");            
                                out.print(",");
                                out.print("\"fecha\":\"Total\"");                         
                                out.print(",");                                                    
                                out.print("\"nivel\":\"\"");                         
                                out.print(",");  
                                out.print("\"cantidad\":\"" + Float.toString(total_cantidad) + "\"");                         
                                out.print(",");                                  
                                out.print("\"monto_colaborador\":\"" + Float.toString(total_monto_colaborador) + "\"");                         
                                out.print(",");                        
                                out.print("\"monto_banco\":\"" + Float.toString(total_monto_banco) + "\"");
                                out.print("}");
                                out.print(separator);                                                
                                ultima_fecha            = fecha; 
                                total_monto_colaborador = 0;
                                total_monto_banco       = 0; 
                                total_cantidad          = 0;
                                out.print("{"); 
                                out.print("\"id\":\"fecha\"");            
                                out.print(",");
                                out.print("\"fecha\":\""+fecha+"\"");                         
                                out.print(","); 
                                out.print("\"cantidad\":\"\"");                         
                                out.print(",");                                  
                                out.print("\"nivel\":\"\"");                         
                                out.print(",");                        
                                out.print("\"monto_colaborador\":\"\"");                         
                                out.print(",");                        
                                out.print("\"monto_banco\":\"\"");                                       
                                out.print("}");
                                out.print(separator);                                                
                                out.print("{");                                 
                                out.print("\"id\":\"" + Util.checkNull(data.getString("nivel")) + "\"");            
                                out.print(",");
                                out.print("\"fecha\":\"\"");                         
                                out.print(",");                                                    
                                out.print("\"nivel\":\""+Util.checkNull(data.getString("nivel"))+"\"");                         
                                out.print(",");  
                                out.print("\"cantidad\":\""+cantidad+"\"");                         
                                out.print(",");                                  
                                out.print("\"monto_colaborador\":\""+monto_colaborador+"\"");                         
                                out.print(",");                        
                                out.print("\"monto_banco\":\"" + monto_banco + "\"");            
                                total_monto_colaborador += monto_colaborador;
                                total_monto_banco       += monto_banco;  
                                total_cantidad          += cantidad;
                            }                                                                        
                        }   
                        out.print("}");        
                        separator = ",";
                        //data.next();
                    }
                    if (report.equals("niveles")) {
                        out.print(separator);
                        out.print("{");
                        out.print("\"id\":\"total\"");            
                        out.print(",");
                        out.print("\"fecha\":\"Total\"");                         
                        out.print(",");                                                    
                        out.print("\"nivel\":\"\"");                         
                        out.print(",");  
                        out.print("\"cantidad\":\"" + Float.toString(total_cantidad) + "\"");                         
                        out.print(",");                                  
                        out.print("\"monto_colaborador\":\"" + Float.toString(total_monto_colaborador) + "\"");                         
                        out.print(",");                        
                        out.print("\"monto_banco\":\"" + Float.toString(total_monto_banco) + "\"");
                        out.print("}");                        
                    }
                    out.print("]");
                    out.print(",");
                    out.print("\"columns\":" + columns);                    
                    out.print(",");
                }   
                datos.cerrar();                
            }
        } catch (Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";        
        String resultado = status.equals("true") ? "Consultado" : "Error de consulta";
        String key = code; 
        Seguridad.log(accion, resultado, traza, key);        
    }

}
