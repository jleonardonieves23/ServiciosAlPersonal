package be.servlets.mod;

import be.entidades.Entidades;
import be.entidades.VacacionesPeriodo;
import be.entidades.VacacionesSolicitud;
import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Util;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Vacaciones {
    
       public void periodos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String colaborador = request.getParameter("colaborador");
            VacacionesPeriodo entidad = new VacacionesPeriodo();
            Entidades datos = new Entidades(entidad);
            datos.cargar("colaborador like '%" + colaborador + "'");
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }             
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }     
    
    public void planificacion(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            String colaborador = request.getParameter("colaborador");
            VacacionesSolicitud entidad = new VacacionesSolicitud();
            Entidades datos = new Entidades(entidad);
            datos.cargar("(colaborador = '" + colaborador + "' or colaborador like '%" + colaborador + "') and status in ('Pendiente', 'Aprobado')");
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }             
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }  

    public void guardarPlanificacion(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        try {             
            VacacionesSolicitud entidad = new VacacionesSolicitud();
            
            entidad.colaborador = request.getParameter("colaborador");
            entidad.fecha_comienzo = request.getParameter("fecha_comienzo");
            entidad.fecha_fin = request.getParameter("fecha_fin");
            entidad.fecha_reintegro = request.getParameter("fecha_reintegro");
            entidad.bono_vacacional_solicitado = request.getParameter("bono");
            entidad.dias_disfrute = request.getParameter("dias_disfrute");
            entidad.status = "Pendiente";
            Properties properties = new Properties();
            String message = "";
            String status = "false";
            
            if (entidad.guardar()) {                
                message = "Proceso Exitoso";
                status = "true";
            } else {
                message = "Error al guardar los datos";
                status = "false";
            }             
            properties.setProperty("status", status);                        
            properties.setProperty("message", message);                        
            Util.mostrar(out, properties);
        } catch(Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }  

    public void consultarDiasPendientes(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{"); 
        String colaborador = "";
        String sql = "";
        boolean statuss = true;
        String status = "false";            
        try {
            Datos datos = new Datos("jdbc/rrhh");            
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;            
            colaborador = String.format("%1$10s", colaborador.trim());
            sql = "SELECT periodo, sum(totales) as totales, sum(disfrutados) as disfrutados, sum(totales) -  sum(disfrutados) as pendientes  FROM (select extract(year from feccon) as periodo, SUM(DECODE(TIPREG, 1, CANVAC)) as totales, 0 as disfrutados from NMM011 where trab_fictra = '<colaborador>'  and cto_codcto = 9030 AND TIPREG = 1 group by extract(year from feccon) UNION ALL select extract(year from feccon) as periodo, 0 as totales, sum(decode(TIPREG, 3, CANVAC)) as disfrutados from NMM011 where trab_fictra = '<colaborador>'  and cto_codcto = 9030 AND TIPREG = 3 group by extract(year from feccon)) A group by periodo";
            Properties params = new Properties();                
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            Object results = datos.ejecutar(sql);
            
            if (results != null && ResultSet.class.isInstance(results)) {
                ResultSet data     = (ResultSet) results;
                String separator   = "";
                String periodo     = "";
                String totales     = "";
                String disfrutados = "";
                String pendientes  = "";
                out.print("\"rows\":");                       
                out.print("["); 
                while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                    periodo     = Util.checkNull(data.getString("periodo"));
                    totales     = Util.checkNull(data.getString("totales"));
                    disfrutados = Util.checkNull(data.getString("disfrutados"));
                    pendientes  = Util.checkNull(data.getString("pendientes"));
                    out.print(separator);                                                
                    out.print("{");
                    out.print("\"periodo\":\""+periodo+"\"");                         
                    out.print(",");                        
                    out.print("\"totales\":\""+totales+"\"");                         
                    out.print(",");
                    out.print("\"disfrutados\":\""+disfrutados+"\"");                         
                    out.print(",");
                    out.print("\"pendientes\":\""+pendientes+"\"");                                                                                                                     
                    out.print("}");        
                    separator = ",";                        
                }    
                out.print("]");
                out.print(",");            
                datos.cerrar();                            
                status = "true";
            }
        } catch (Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");
        
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";
        String resultado = statuss ? "Consultado" : "Error de consulta";
        String key = colaborador;
        Seguridad.log(accion, resultado, sql, key);
    } 
    
    public void consultarDiasPendientesDetallado(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String colaborador = "";
        String sql = "";
        boolean statuss = true;
        String status = "false";            
        try {
            Datos datos = new Datos("jdbc/rrhh");            
            be.entidades.Usuario usuario = (be.entidades.Usuario) Util.obtenerUsuarioActual(request);
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = colaborador == null || colaborador.isEmpty() ? usuario.id : colaborador;
            colaborador = String.format("%1$10s", colaborador.trim());
            sql = "select TRAB_FICTRA as colaborador, FECCON AS fecha, TIPREG AS concepto, canvac as dias, fecsal as salida, fecreg as regreso from NMM011 where trab_fictra = '<colaborador>' and CTO_CODCTO = '9030' order by FECCON";
            Properties params = new Properties();                
            params.setProperty("<colaborador>", colaborador);
            sql = Util.parseSQLParams(sql, params);
            Object results = datos.ejecutar(sql);
            
            if (results != null && ResultSet.class.isInstance(results)) {
                ResultSet data     = (ResultSet) results;
                String separator   = "";
                String fecha       = "";
                String salida      = "";
                String regreso     = "";
                String dias        = "";
                String concepto    = "";
                out.print("\"rows\":");                       
                out.print("["); 
                while (data != null && !data.isClosed() && !data.isAfterLast() && data.next())  {
                    fecha       = Util.checkNull(data.getString("fecha"));
                    salida      = Util.checkNull(data.getString("salida"));
                    regreso     = Util.checkNull(data.getString("regreso"));
                    concepto    = Util.checkNull(data.getString("concepto"));
                    dias        = Util.checkNull(data.getString("dias"));                    
                    out.print(separator);                                                
                    out.print("{");
                    out.print("\"fecha\":\""+fecha+"\"");                         
                    out.print(",");  
                    out.print("\"salida\":\""+salida+"\"");                         
                    out.print(",");  
                    out.print("\"regreso\":\""+regreso+"\"");                         
                    out.print(",");  
                    out.print("\"dias\":\""+dias+"\"");                         
                    out.print(",");
                    out.print("\"concepto\":\""+concepto+"\"");                                                                                                                                              
                    out.print("}");        
                    separator = ",";                        
                }    
                out.print("]");
                out.print(",");            
                datos.cerrar();                            
                status = "true";
            }
        } catch (Exception e) {
            Util.log(this, e);
            status = "false";
        }
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");  
        
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";
        String resultado = statuss ? "Consultado" : "Error de consulta";
        String key = colaborador;        
        Seguridad.log(accion, resultado, sql, key);
    }  
    
}
