package be.servlets.mod;

import be.entidades.Entidades;
import be.seguridad.Seguridad;
import be.utils.Util;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ReciboPago {

    public void obtenerRecibos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        String colaborador = "";
        String sql = "";
        boolean status = true;
        try {
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = String.format("%1$10s", colaborador);
            be.entidades.ReciboPago entity = new be.entidades.ReciboPago();
            Entidades datos = new Entidades(entity);
            datos.setAuditable(true);
            String orden = "FECMOV DESC";
            String filtro = "";
            filtro += " TRAB_FICTRA = '" + colaborador + "'";
            status = datos.cargar(filtro, orden);
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }
            sql = datos.getSql();
        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";
        String resultado = status ? "Consultado" : "Error de consulta";
        String key = colaborador;        
        Seguridad.log(accion, resultado, sql, key);
    }

    public void obtenerDetallesRecibos(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        String colaborador = "";
        String sql = "";
        boolean status = true;

        try {
            String FECMOV = request.getParameter("FECMOV");
            String FPRO_ANOCAL = request.getParameter("FPRO_ANOCAL");
            String FPRO_NUMPER = request.getParameter("FPRO_NUMPER");
            String PROC_TIPPRO = request.getParameter("PROC_TIPPRO");
            String TNOM_TIPNOM = request.getParameter("TNOM_TIPNOM");

            out.print("{\"detalles\":");

            //DETALLES
            colaborador = Util.obtenerCodigoColaborador(request);
            colaborador = String.format("%1$10s", colaborador);
            be.entidades.ReciboPagoDetalle entity = new be.entidades.ReciboPagoDetalle();
            Entidades datos = new Entidades(entity);
            datos.setAuditable(true);
            String filtro = "";
            filtro += " a.TRAB_FICTRA = '" + colaborador + "' ";
            filtro += " and a.FPRO_ANOCAL = '" + FPRO_ANOCAL + "'";
            filtro += " and a.FPRO_NUMPER = '" + FPRO_NUMPER + "'";
            filtro += " and a.PROC_TIPPRO = '" + PROC_TIPPRO + "'";
            //filtro += " and SUBPRO = '" + SUBPRO + "'";
            filtro += " and a.TNOM_TIPNOM = '" + TNOM_TIPNOM + "'";
            String orden = " FUNCTO asc, CODIGO asc  ";
            status = datos.cargar(filtro, orden);
            if (datos.vacio()) {
                out.print("[]");
            } else {
                out.print(datos.serializar());
            }

            //SUELDO
            String ano = FECMOV.substring(0, 4);
            String mes = FECMOV.substring(5, 7);
            be.entidades.Sueldo sueldo = new be.entidades.Sueldo();
            String periodo = Integer.toString((Integer.parseInt(ano) * 100) + Integer.parseInt(mes));
            filtro = "";
            orden = "periodo desc";
            filtro += " colaborador = '" + colaborador + "' ";
            filtro += " and periodo <= " + periodo;
            filtro += " and rownum = 1";
            sueldo.cargar("Sueldo", filtro, orden);
            sueldo.SUELDO = sueldo.SUELDO == null ? "" : sueldo.SUELDO;
            out.print(",\"sueldo\":\"" + sueldo.SUELDO + "\"}");
            sql = datos.getSql();

        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
        String classname = this.getClass().getCanonicalName();
        String accion = classname + ".consultado";
        String resultado = status ? "Consultado" : "Error de consulta";
        String key = colaborador;        
        Seguridad.log(accion, resultado, sql, key);

    }

    public void obtenerSueldoColaborador(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session) {
        try {
            be.entidades.Sueldo entity = new be.entidades.Sueldo();
            String ano = request.getParameter("FPRO_ANOCAL");
            String mes = request.getParameter("FPRO_NUMPER");
            String periodo = Integer.toString((Integer.parseInt(ano) * 100) + Integer.parseInt(mes));
            String colaborador = Util.obtenerCodigoColaborador(request);
            String filtro = "";
            String orden = "periodo desc";
            filtro += " colaborador = '" + colaborador + "' ";
            filtro += " and periodo <= " + periodo;
            filtro += " and rownum = 1";
            entity.cargar(filtro, orden);
            out.print(entity.serializar());
        } catch (Exception e) {
            Util.log(this, e);
            Util.mostrarError(out, e.getMessage());
        }
    }
}
