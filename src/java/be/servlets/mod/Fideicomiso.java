package be.servlets.mod;

import be.utils.Util;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Fideicomiso {
  
    public void consultar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String status  = "false";    
        String message = "No hay datos para mostrar";        
        try {
            String colaborador = Util.obtenerCodigoColaborador(request);            
            colaborador = String.format("%10s", colaborador);
            be.entidades.UsuarioSPI usuario = new be.entidades.UsuarioSPI();
            usuario.cargar("UsuarioSPI", "codigo='"+colaborador.trim()+"' or cedula='" + colaborador.trim() + "'", "codigo");
            if (usuario.cedula != null && !usuario.cedula.trim().isEmpty()) {
                be.entidades.Fideicomiso data = new be.entidades.Fideicomiso();                            
                data.cargar(usuario.cedula.trim());  
                if (data.cedula != null && !data.cedula.trim().isEmpty()) {
                    out.print("\"nombre\":"); 
                    out.print("\"" + usuario.primer_nombre + " " + usuario.segundo_nombre + " " + usuario.primer_apellido +"\"");                       
                    out.print(","); 
                    out.print("\"data\":");                       
                    out.print(data.serializar());
                    out.print(","); 
                    status  = "true"; 
                    message = ""; 
                }
            }
        } catch (Exception e) {
            message = e.getMessage();
            Util.log(this, e);
            status = "false";
        }
        out.print("\"message\":\"" + message + "\""); 
        out.print(","); 
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");  
    }      
}
