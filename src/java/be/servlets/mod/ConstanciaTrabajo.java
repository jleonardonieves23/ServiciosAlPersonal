package be.servlets.mod;

import be.entidades.Entidades;
import be.procesos.Datos;
import be.utils.Etiqueta;
import be.utils.Util;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.Properties;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class ConstanciaTrabajo {
  
    public void consultar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String status  = "false";    
        String message = "";        
        try {
            String colaborador = Util.obtenerCodigoColaborador(request);            
            
            be.entidades.ConstanciaTrabajo constancia = new be.entidades.ConstanciaTrabajo();
            Entidades entidades = new Entidades(constancia);
            entidades.cargar("codusuario='"+colaborador+"'", "fecha_solicitud DESC");
            String constancias = entidades.serializar();            
            out.print("\"rows\":");                       
            out.print(constancias.isEmpty() ? "[]" : constancias);
            out.print(",");             

            status = "true";                             
        } catch (Exception e) {
            message = e.getMessage();
            Util.log(this, e);
            status = "false";
        }
        out.print("\"message\":\"" + message + "\""); 
        out.print(","); 
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");  
    }      
    
    public void solicitar(HttpServletRequest request, HttpServletResponse response, PrintWriter out, HttpSession session)  {
        out.print("{");            
        String status  = "false";    
        String message = Etiqueta.$("BE_ERROR");
        try {
            String colaborador = Util.obtenerCodigoColaborador(request);            
            String tipo        = Util.checkParam(request.getParameter("tipo"));
            String sueldo      = Util.checkParam(request.getParameter("sueldo"));
            String detalle     = Util.checkParam(request.getParameter("detalle"));
                        
            if (!tipo.isEmpty()) {
                be.entidades.ConstanciaTrabajo constancia = new be.entidades.ConstanciaTrabajo();
                detalle = detalle.replaceAll("&#x20;", " ");
                if (tipo.equals("6")) {
                    if (sueldo.equals("1")) {
                        detalle = "1A-"+detalle;
                    }
                    if (sueldo.equals("2")) {
                        detalle = "2S-"+detalle;
                    }
                    if (sueldo.equals("3")) {
                        detalle = "3M-"+detalle;
                    }
                }
                constancia.codusuario = colaborador;
                constancia.tipodoc    = tipo;
                constancia.detalle    = detalle;
                constancia.cantd_doc  = "1";
                constancia.fecha_solicitud = Util.getCurrentYear() + "-" + Util.getCurrentMonth() + "-" + Util.getCurrentDay() + " " + Util.getCurrentTime();
                //constancia.fecha_solicitud = "' + CONVERT(datetime, '" +  Util.getCurrentYear() + "-" + Util.getCurrentMonth() + "-" + Util.getCurrentDay() + " " + Util.getCurrentTime() + "', 120)) + '";
                
                if (constancia.guardar()) {                
                    status =  "true";
                    message = Etiqueta.$("BE_SUCCESS");
                }
            }
        } catch (Exception e) {
            message = e.getMessage();
            Util.log(this, e);
            status = "false";
        }
        out.print("\"message\":\"" + message + "\""); 
        out.print(","); 
        out.print("\"status\":\"" + status + "\"");            
        out.print("}");         
    }    
}

   
 
