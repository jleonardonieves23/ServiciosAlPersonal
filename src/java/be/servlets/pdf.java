package be.servlets;

import be.utils.Config;
import be.utils.QRCreator;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.servlet.ServletContext;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;

@WebServlet(name = "pdf", urlPatterns = {"/pdf"})
@MultipartConfig

public class pdf extends HttpServlet {

    static String PATH = "";
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try  {
            String FileName = request.getParameter("filename");         
            response.setHeader("Content-Disposition","inline; filename=\""+FileName+".pdf\"");        
            response.setContentType("application/pdf; name=\""+FileName+".pdf\""); 
        
            ServletContext context = request.getServletContext();
            String path = context.getRealPath("/");            
            String data = request.getParameter("data");            
            String qr   = request.getParameter("qr");
            PATH = path + "\\img\\";
            //doc = doc.replaceAll(" src='", "src='" + path);
            //doc = doc.replaceAll(" src='", "src=\"" + path);
            
            ByteArrayInputStream html = new ByteArrayInputStream(data.getBytes());
                                 
            FileInputStream css = new FileInputStream(path + "/lib/be/print.css");
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, response.getOutputStream());
            HeaderFooter event = new HeaderFooter();
            writer.setPageEvent(event);           
            document.open();           
            XMLWorkerHelper.getInstance().parseXHtml(writer, document, html, css);    
            
            if (qr != null && !qr.equals("")) {
                QRCreator qrcreator = new QRCreator();            
                BufferedImage bufferedImage = qrcreator.createQR(qr, 300);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bufferedImage, "png", baos);
                byte[] bytes = baos.toByteArray();

                Phrase phrase = new Phrase();  
                Image image = Image.getInstance(bytes);
                image.scalePercent(3000);
                Chunk chunk5 = new Chunk(image, 0, 0);
                phrase.add(chunk5);
                
                PdfContentByte cb = writer.getDirectContent(); 
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                    phrase,
                    document.left() + 30,
                    document.bottom() + 30, 0);             
            }
            
            document.close();                                    
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    class HeaderFooter extends PdfPageEventHelper {
        BaseColor blue = new BaseColor(27831);
        BaseColor grey = new BaseColor(90, 90, 90);
        Font headerFont = new Font(Font.FontFamily.UNDEFINED, 21, Font.BOLD, blue);
        Font footerFont = new Font(Font.FontFamily.UNDEFINED, 8, Font.NORMAL, grey);
        Font firmaFont = new Font(Font.FontFamily.UNDEFINED, 8, Font.NORMAL);
 
        public void onEndPage(PdfWriter writer, Document document) {
            try {
                PdfContentByte cb = writer.getDirectContent();
                
                Phrase header = new Phrase("", headerFont);
                
                //Image logo = Image.getInstance(PATH + "logo-mini.jpg");                
                Image logo = Image.getInstance(PATH + "LogoExterior.png");                
                logo.scalePercent(1000);
                Chunk chunk = new Chunk(logo, 0, 0);                
                header.add(chunk);

                Phrase header2 = new Phrase("", headerFont);
                Image logo2 = Image.getInstance(PATH + "grupo_if.gif");                
                logo2.scalePercent(1000);
                Chunk chunk2 = new Chunk(logo2, 0, 0);
                header2.add(chunk2);
                /* 
                Phrase firma = new Phrase("", firmaFont);
                Image logo3 = Image.getInstance(PATH + "firma.png");                
                logo3.scalePercent(3000);
                Chunk chunk3 = new Chunk(logo3, 0, 0);
                firma.add(chunk3);
                
                Phrase firma2 = new Phrase("Gerente del Departamento Conexión Humana", firmaFont);
                
                
                Phrase sello = new Phrase("", firmaFont);
                Image logo4 = Image.getInstance(PATH + "LogoA.bmp");                
                logo4.scalePercent(3000);
                Chunk chunk4 = new Chunk(logo4, 0, 0);
                sello.add(chunk4);
                */
                Phrase footer = new Phrase("Banco Exterior, J-00002950-4. - (0212) 508.5000 - Av. Urdaneta, con Esquina Urapal a Río, Edif. Banco Exterior, La Candelaria, Caracas, Venezuela.", footerFont);
                
                ColumnText.showTextAligned(cb, Element.ALIGN_LEFT,
                        header,
                        document.left(),
                        document.top()-20, 0);
                
                ColumnText.showTextAligned(cb, Element.ALIGN_RIGHT,
                        header2,
                        document.right(),
                        document.top()-20, 0);                

                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        footer,
                        (document.right() - document.left()) / 2 + document.leftMargin(),
                        document.bottom() - 10, 0);
                /*
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        firma2,
                        (document.right() - document.left()) / 2 + document.leftMargin(),
                        document.bottom() + 50, 0);
                
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        firma,
                        (document.right() - document.left()) / 2 + document.leftMargin(),
                        document.bottom() + 80, 0);                
                
                ColumnText.showTextAligned(cb, Element.ALIGN_CENTER,
                        sello,
                        (document.right() - document.left()) / 2 + document.leftMargin() + 120,
                        document.bottom() + 80, 0);  
                */
            } catch(Exception e) {
                e.printStackTrace();
            }
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>         
}
