//PACKAGE
package be.seguridad;

//IMPORTS
import be.entidades.ActiveDirectory;
import be.entidades.Auditoria;
import be.entidades.Usuario;
import java.text.MessageFormat;
import java.util.*;
import javax.naming.*;
import javax.naming.directory.Attribute;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import be.utils.*;
import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import javax.crypto.Cipher;

//CLASS
public class Seguridad {
    
    //CONSTS
    private static final String  DISTINGUISHED_NAME         = "distinguishedName";
    private static final String  CN                         = "cn";
    private static final String  MEMBER                     = "member";
    private static final String  MEMBER_OF                  = "memberOf";
    private static final String  SEARCH_BY_SAM_ACCOUNT_NAME = "(SAMAccountName={0})";
    private static final String  SEARCH_GROUP_BY_GROUP_CN   = "(&(objectCategory=group)(cn={0}))";
    private static final String  DEFAULT_ENCODING           = "UTF-8";
    
    private static final String  LDAP_CODE                  = "SAMACCOUNTNAME"; 
    private static final String  LDAP_USERNAME              = "DISPLAYNAME"; 
    private static final String  LDAP_NAME                  = "GIVENNAME"; 
    private static final String  LDAP_LASTNAME              = "SN"; 
    private static final String  LDAP_EMAIL                 = "MAIL"; 
    private static final String  LDAP_PHONE                 = "TELEPHONENUMBER"; 
    private static final String  LDAP_DEPARTMENT            = "DEPARTMENT"; 
    private static final String  LDAP_DIVISION              = "DIVISION"; 
    private static final String  LDAP_TITLE                 = "TITLE"; 
    private static final String  LDAP_LOCATION              = "ST"; 
    private static final String  LDAP_STATUS                = "userAccountControl";
    private static final String  LDAP_ACCOUNT_ACTIVE        = "Cuenta activa";
    
    private static final String ADS_UF_ACCOUNTDISABLE_MSG   = "The user account is disabled.";
    private static final String ADS_UF_LOCKOUT_MSG          = "The account is currently locked out.";
    private static final String ADS_UF_PASSWORD_EXPIRED_MSG = "The user password has expired.";
         
    private static final int ADS_UF_ACCOUNTDISABLE          = 0x00000002; 
    private static final int ADS_UF_LOCKOUT                 = 0x00000010; 
    private static final int ADS_UF_PASSWORD_EXPIRED        = 0x00800000;
    
    private static String ERROR_MESSAGE                     = "";
    
    
    //PROPERTIES
    private Usuario currentUser;
    private DirContext ctx;
    
    //STATIC INSTANCE
    private static Seguridad security;
        
    //CONSTRUCTORS
    private Seguridad () {
        init();
    }
    
    //INIT
    private void init() {          
    }  
    
    //BASIC METHODS
    private static Seguridad _instance() {      
        if (security==null) security = new Seguridad();
        return security;
    }
    
    //ADVANCED METHODS        
    private String getCN(String cnName) {
        if (cnName != null && cnName.toUpperCase().startsWith("CN=")) {
            cnName = cnName.substring(3);
        }
        int position = cnName.indexOf(',');
        if (position == -1) {
            return cnName;
        } else {
            return cnName.substring(0, position);
        }
    }

    private boolean isSame(String target, String candidate) {
        if (target != null && target.equalsIgnoreCase(candidate)) {
            return true;
        }
        return false;
    }

    private boolean userMemberOf(DirContext ctx, String searchBase, HashMap processedUserGroups, HashMap unProcessedUserGroups, String groupCN, String groupDistinguishedName) throws NamingException {
        HashMap newUnProcessedGroups = new HashMap();
        for (Iterator entry = unProcessedUserGroups.keySet().iterator(); entry.hasNext();) {
            String unprocessedGroupDistinguishedName = (String) entry.next();
            String unprocessedGroupCN = (String) unProcessedUserGroups.get(unprocessedGroupDistinguishedName);
            if (processedUserGroups.get(unprocessedGroupDistinguishedName) != null) {
                continue;
            }
            if (isSame(groupCN, unprocessedGroupCN) && isSame(groupDistinguishedName, unprocessedGroupDistinguishedName)) {
                return true;
            }
        }

        for (Iterator entry = unProcessedUserGroups.keySet().iterator(); entry.hasNext();) {
            String unprocessedGroupDistinguishedName = (String) entry.next();
            String unprocessedGroupCN = (String) unProcessedUserGroups.get(unprocessedGroupDistinguishedName);
            processedUserGroups.put(unprocessedGroupDistinguishedName, unprocessedGroupCN);
            // Fetch Groups in unprocessedGroupCN and put them in newUnProcessedGroups
            NamingEnumeration ns = executeSearch(ctx, SearchControls.SUBTREE_SCOPE, searchBase,
                    MessageFormat.format(SEARCH_GROUP_BY_GROUP_CN, new Object[]{unprocessedGroupCN}),
                    new String[]{CN, DISTINGUISHED_NAME, MEMBER_OF});

            // Loop through the search results
            while (ns.hasMoreElements()) {
                SearchResult sr = (SearchResult) ns.next();

                // Make sure we're looking at correct distinguishedName, because we're querying by CN
                String userDistinguishedName = sr.getAttributes().get(DISTINGUISHED_NAME).get().toString();
                if (!isSame(unprocessedGroupDistinguishedName, userDistinguishedName)) {
                    continue;
                }

                // Look for and process memberOf
                Attribute memberOf = sr.getAttributes().get(MEMBER_OF);
                if (memberOf != null) {
                    for (Enumeration e1 = memberOf.getAll(); e1.hasMoreElements();) {
                        String unprocessedChildGroupDN = e1.nextElement().toString();
                        String unprocessedChildGroupCN = getCN(unprocessedChildGroupDN);
                        newUnProcessedGroups.put(unprocessedChildGroupDN, unprocessedChildGroupCN);
                    }
                }
            }
        }
        if (newUnProcessedGroups.size() == 0) {
            return false;
        }

        //  process unProcessedUserGroups
        return userMemberOf(ctx, searchBase, processedUserGroups, newUnProcessedGroups, groupCN, groupDistinguishedName);
    }

    private NamingEnumeration executeSearch(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        // Create the search controls
        SearchControls searchCtls = new SearchControls();

        // Specify the attributes to return
        if (attributes != null) {
            searchCtls.setReturningAttributes(attributes);
        }

        // Specify the search scope
        searchCtls.setSearchScope(searchScope);

        // Search for objects using the filter
        NamingEnumeration result = ctx.search(searchBase, searchFilter, searchCtls);
        return result;
    }

    private SearchResult executeSearchSingleResult(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        NamingEnumeration result = executeSearch(ctx, searchScope, searchBase, searchFilter, attributes);

        SearchResult sr = null;
        // Loop through the search results
        while (result.hasMoreElements()) {
            sr = (SearchResult) result.next();
            break;
        }
        return sr;
    }

    private boolean executeLogin(String username, String group, DirContext ctx) {
        String DC                                       = Config.$("BE_LDAP_DC").trim();
        String OU                                       = Config.$("BE_LDAP_OU").trim();
        String defaultSearchBase                        = DC;
        String groupDistinguishedName                   = OU + "," + DC; 
        //String groupDistinguishedName                   = "CN=" + group + "," + OU + "," + DC; 
        try {            
            // userName is SAMAccountName
            SearchResult    sr                          = executeSearchSingleResult(ctx, SearchControls.SUBTREE_SCOPE, defaultSearchBase, MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME, new Object[]{username}), new String[]{DISTINGUISHED_NAME, CN, MEMBER_OF, LDAP_CODE,LDAP_USERNAME,LDAP_NAME,LDAP_LASTNAME,LDAP_EMAIL,LDAP_PHONE,LDAP_DEPARTMENT,LDAP_DIVISION,LDAP_TITLE,LDAP_LOCATION});
            String          groupCN                     = getCN(groupDistinguishedName);
            HashMap         processedUserGroups         = new HashMap();
            HashMap         unProcessedUserGroups       = new HashMap();            
            String          unprocessedGroupDN          = "";
            String          unprocessedGroupCN          = "";                    
            // Look for and process memberOf            
            Attribute       memberOf                    = sr.getAttributes().get(MEMBER_OF);
            if (memberOf != null) {
                ActiveDirectory ad = new ActiveDirectory(ctx);
                currentUser = ad.getUser(username);      
                currentUser.status = LDAP_ACCOUNT_ACTIVE;
                String message = Etiqueta.$("BE_LOGIN_SUCCESS") + ": "  + username + " , " + group;
                ERROR_MESSAGE  = message;
                Util.log(this, message);
                return true;            
            }
        } catch (Exception e) {
            ERROR_MESSAGE  = e.getMessage();
            Util.log(this, e);
        } 
        return false;
    } 
    
    private String clear(String param) {
        String[] values = param.split(":");
        return values[values.length-1];
    }
    
    private String _getMessage() {
        return ERROR_MESSAGE;
    }
    
    private String _encrypt(String text) {
        BASE64Encoder enc = new BASE64Encoder();
        return enc.encode(text.getBytes());
    }

    private String _decrypt(String text) {
         BASE64Decoder dec = new BASE64Decoder();
         String result = null;
         try {
             result = new String(dec.decodeBuffer(text));
         } catch(Exception e) {
             ERROR_MESSAGE  = e.getMessage();                                      
             Util.log(this, e);
         }
         return result; 
    }
   
    private boolean createContext() {
        return createContext(false);
    }
    private boolean createContext(boolean timeout) {
        try {
            String domain   = Config.$("BE_LDAP_DOMAIN").trim();            
            String username = Config.$("BE_LDAP_USER").trim();
            String password = Config.$("BE_LDAP_PASSWORD").trim();
            return createContext(domain, username, password, timeout);
        } catch(Exception e) {
            ERROR_MESSAGE = e.getMessage();            
            Util.log(this, e);
            return false;
        }
    }
    
    
    private boolean createContext(int timeout) {
        try {
            String domain   = Config.$("BE_LDAP_DOMAIN").trim();
            String username = Config.$("BE_LDAP_USER").trim();
            String password = Config.$("BE_LDAP_PASSWORD").trim();
            return createContext(domain, username, password, timeout);
        } catch(Exception e) {
            ERROR_MESSAGE = e.getMessage();            
            Util.log(this, e);
            return false;
        }
    }
    
    private boolean createContext(String domain, String username, String password) {
        return createContext(domain, username, password, true);
    }
    
    private boolean createContext(String domain, String username, String password, boolean timeout) {
        Hashtable  env = new Hashtable();                        
        
        env.put(Context.INITIAL_CONTEXT_FACTORY,    "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL,               "ldap://" + Config.$("BE_LDAP_SERVER"));
        env.put(Context.SECURITY_AUTHENTICATION,    "simple");
        env.put(Context.SECURITY_PRINCIPAL,         domain + "\\" + username);
        env.put(Context.SECURITY_CREDENTIALS,       password); 
        if (timeout){
            env.put("com.sun.jndi.ldap.read.timeout",   Config.$("BE_LDAP_TIMEOUT"));
        }
        
        try {
            ctx = new InitialDirContext(env);                    
            return true;
        } catch(Exception e) {
            processConextException(e);
            Util.log(this, e);
            return false;
        }
    }
    
    private boolean createContext(String domain, String username, String password, int timeout) {
        Hashtable  env = new Hashtable();                        
        
        env.put(Context.INITIAL_CONTEXT_FACTORY,    "com.sun.jndi.ldap.LdapCtxFactory");
        env.put(Context.PROVIDER_URL,               "ldap://" + Config.$("BE_LDAP_SERVER"));
        env.put(Context.SECURITY_AUTHENTICATION,    "simple");
        env.put(Context.SECURITY_PRINCIPAL,         domain + "\\" + username);
        env.put(Context.SECURITY_CREDENTIALS,       password); 
        if (timeout > 0){
            env.put("com.sun.jndi.ldap.read.timeout",  Integer.toString(timeout));
        }
        
        try {
            ctx = new InitialDirContext(env);                    
            return true;
        } catch(Exception e) {
            processConextException(e);
            Util.log(this, e);
            return false;
        }
    }    
    
    private boolean _login(String domain, String username, String password) {
        ERROR_MESSAGE     = "";
        boolean result    = false;
        boolean torre     = true;
        String domainname = Util.checkNull(Config.$("BE_LDAP_DOMAIN_NAME")).trim();      
        String servers    = Util.checkNull(Config.$("BE_LDAP_SERVER")).trim();              
        String server     = null;      
        Hashtable env     = new Hashtable();                        
        String[] servers_list = null;
        
        servers_list = servers.split(",");
        int i = 0;
        env.put(Context.INITIAL_CONTEXT_FACTORY,    "com.sun.jndi.ldap.LdapCtxFactory");        
        env.put(Context.SECURITY_AUTHENTICATION,    "simple");
        env.put(Context.SECURITY_PRINCIPAL,         domain + "\\" + username);
        env.put(Context.SECURITY_CREDENTIALS,       password); 
        env.put("com.sun.jndi.ldap.read.timeout",   Config.$("BE_LDAP_TIMEOUT"));
        env.put("com.sun.jndi.ldap.connect.timeout", Config.$("BE_LDAP_TIMEOUT"));

        boolean valid = false;
        ctx = null;
        while(!valid && i < servers_list.length) {
            server = servers_list[i].trim();
            i++;
            env.put(Context.PROVIDER_URL, "ldap://" + server);
            try {
                ctx    = new InitialDirContext(env);            
                result = true;
                valid = true;
            } catch(Exception e) {  
                valid = false;
                e.printStackTrace();
            }                
        }

        if (ctx == null) {
            domain = domain.substring(0, domain.length()-1) + "2";
            String oficinas = (domainname.substring(0, 2).trim().toLowerCase().equals("qa")) ? "qaoficinas" : "oficinas";
            
            env = new Hashtable();                        

            env.put(Context.INITIAL_CONTEXT_FACTORY,    "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL,               "ldap://" + oficinas + "." + domainname);
            env.put(Context.SECURITY_AUTHENTICATION,    "simple");
            env.put(Context.SECURITY_PRINCIPAL,         domain + "\\" + username);
            env.put(Context.SECURITY_CREDENTIALS,       password); 
            env.put("com.sun.jndi.ldap.read.timeout",   Config.$("BE_LDAP_TIMEOUT"));

            try {
                ctx = new InitialDirContext(env);                            
                if (ctx == null) {
                    result = false;
                } else {                       
                    result = true;
                    torre  = false;                     
                }
            } catch(Exception e) {                           
                processConextException(e);
                result = false;
            }

        } else {            
            torre = true;            
        }  
        
        if  (ctx != null) {            
            String oficinas               = (domainname.substring(0, 2).trim().toLowerCase().equals("qa")) ? "qaoficinas" : "oficinas";
            String[] parts                = domainname.split("\\.");
            String DC                     = String.join(",DC=", parts);
            DC                            = torre ? ("DC=" + DC) : ("DC=" + oficinas + ",DC=" + DC);            
            String defaultSearchBase      = DC;            
            
            SearchControls searchCtls = new SearchControls();            
            searchCtls.setSearchScope(SearchControls.SUBTREE_SCOPE);            
            String searchFilter = MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME, new Object[]{username});
            NamingEnumeration found = null;
            try {
                found = ctx.search(defaultSearchBase, searchFilter, searchCtls);
                result = true;
            } catch (Exception e) {
                processConextException(e);
                result = false;                
            }
            if (result) {
                SearchResult sr  = null;
                if (found != null) {    
                    while (found.hasMoreElements()) {
                        try {
                            sr = (SearchResult) found.next();
                            result = true;
                        } catch (Exception e) {
                            processConextException(e);
                            result = false;                            
                        }                    
                        break;
                    }    
                    if (sr != null) {
                        ActiveDirectory ad = new ActiveDirectory(ctx);
                        Usuario user = new Usuario();                        
                        ad.populateUser(sr, user);  
                        currentUser = user;      
                        currentUser.status = LDAP_ACCOUNT_ACTIVE;
                        String message = Etiqueta.$("BE_LOGIN_SUCCESS") + ": "  + username;
                        ERROR_MESSAGE  = message;
                        Util.log(this, message); 
                        ad.getAllGroups();
                    } else {
                       currentUser = null; 
                    }
                }
                /*
                try {
                    ctx.close();
                } catch(Exception e) {
                    Util.log(this, e);
                }
*/
            }
        }
        return result;
    }
    
    private void processConextException(Exception e) {
        String message  = e.getMessage();
        Hashtable LDAP_MESSAGES = new Hashtable();
        LDAP_MESSAGES.put("525","La cuenta NO existe.");
        LDAP_MESSAGES.put("52e","la clave es incorrecta.");
        LDAP_MESSAGES.put("52f","La cuenta está restriginda para el acceso.");
        LDAP_MESSAGES.put("530","La cuenta no puede conectarse en este periodo de tiempo.");
        LDAP_MESSAGES.put("531","La cuenta está restringida para ingresar en la estación de trabajo.");
        LDAP_MESSAGES.put("532","La clave ha EXIPARDO.");
        LDAP_MESSAGES.put("533","La cuenta está BLOQUEADA.");
        LDAP_MESSAGES.put("568","ERROR_TOO_MANY_CONTEXT_IDS: During a logon attempt, the user's security context accumulated too many security Identifiers. (ie Group-AD) ");
        LDAP_MESSAGES.put("701","La cuenta ha EXPIRADO.");
        LDAP_MESSAGES.put("773","Debe cambiar la clave.");
        LDAP_MESSAGES.put("775","La cuenta está BLOQUEADA.");
        String[] parts = message.split(",");
        String[] tokens;
        //ERROR_MESSAGE = e.getMessage();
        for(String part : parts) {
            tokens = part.trim().split(" ");
            if (tokens[0].equals("data")) {
                ERROR_MESSAGE = (String) LDAP_MESSAGES.get(tokens[1]);                    
            }
        }        
        Util.log(this, e);
    }
    
    private DirContext _getContext() {
        createContext();
        return ctx;
    }
    
    private DirContext _getContext(int timeout) {
        createContext(timeout);
        return ctx;
    }
    
    private boolean _login(String username, String password) {
        String domain = Config.$("BE_LDAP_DOMAIN");
        return _login(domain, username, password);
    }
    
    private void _log(String accion, String resultado, String traza, String objeto) {
        if (accion != null) {
            Auditoria auditoria = new Auditoria();  
            String info = Thread.currentThread().getName();
            String[] parts = info.split("@");
            String usuario = parts.length > 0 ? parts[0] : "";
            String direccion_ip = parts.length > 1 ? parts[1] : "";            
            if (traza != null) {
                String[] partes = traza.split("'");
                traza = "";
                for (int i=0; i<partes.length; i++) {
                    traza += partes[i] + "´";
                }
            }           
            accion = accion.trim();
            accion = accion.replace("be.entidades.", "");
            auditoria.usuario      = usuario;
            auditoria.fecha        = "' + cast(convert(datetime, '"+Util.getCurrentDateTimeDB()+"', 101) as varchar) + '";
            auditoria.accion       = accion;
            auditoria.resultado    = resultado;
            auditoria.traza        = traza;                
            auditoria.direccion_ip = direccion_ip;
            auditoria.objeto       = objeto.trim();
            auditoria.guardar(); 
            Util.log(this, "(Accion: " + accion + "), (Resultado: " + resultado + "), (Traza: " +  traza + "), (IP: " +  direccion_ip + "), (Objeto: " +  objeto + ")");
        }
    }
    
    //STATICS METHODS    
    public static boolean login(String domain, String username, String password) {
        return _instance()._login(domain, username, password);
    }

    public static boolean login(String username, String password) {
        return _instance()._login(username, password);
    }
    
    public static String encrypt(String text) {
        return _instance()._encrypt(text);
    }

    public static String decrypt(String text) {
         return _instance()._decrypt(text);
    }
    
    public static Usuario getCurrentUser() {
        return _instance().currentUser;
    }
    
    public static DirContext getContext() {
        return _instance()._getContext();
    }   
    
    public static DirContext getContext(int timeout) {
        return _instance()._getContext(timeout);
    }    
        
    public static void log(String accion, String resultado, String traza, String objeto) {
        _instance()._log(accion, resultado, traza, objeto);
    }
    
    public static KeyPair genetateRSAKeys() {
        KeyPair keys = null;
        try {
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
            keyPairGenerator.initialize(1024); 
            keys = keyPairGenerator.generateKeyPair();            
        } catch (Exception e) {
            ERROR_MESSAGE  = e.getMessage();
            Util.log(new Seguridad(), e);
        }        
        return keys;
    }
    
    public static String decryptRSA(KeyPair keys, String value) {
        String result = null;
        try {            
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");            
            cipher.init(Cipher.DECRYPT_MODE, keys.getPrivate()); 
            BigInteger bigInteger = new BigInteger(value, 16);
            byte[] bytes = bigInteger.toByteArray(); 
            result = new String(cipher.doFinal(bytes, 0, 128), "UTF-8");
        } catch (Exception e) {
            ERROR_MESSAGE  = e.getMessage(); 
            Util.log(new Seguridad(), e);
        }
        return result;
    }    
    
    public static String encryptRSA(KeyPair keys, String value) {
        String result = null;
        try {
            Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");            
            cipher.init(Cipher.ENCRYPT_MODE, keys.getPrivate()); 
            BigInteger bigInteger = new BigInteger(value, 16);
            byte[] bytes = bigInteger.toByteArray(); 
            result = new String(cipher.doFinal(bytes, 0, 128), "UTF-8");
        } catch (Exception e) {
            ERROR_MESSAGE  = e.getMessage();
            Util.log(new Seguridad(), e);
        }
        return result;
    }    
    
    public static String getMessage() {
        return _instance()._getMessage();
    }
}

