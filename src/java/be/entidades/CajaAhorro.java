package be.entidades;

public class CajaAhorro extends Entidad {
    public String codigo;
    public String nombre_apellido;
    public String saldo_aho_emp;
    public String saldo_aho_pat;
    public String saldo_prest_corto;
    public String saldo_prest_med;
    public String cuota_prest_cor;
    public String cuota_prest_med;
    public String saldo_neto;
    public String disp_retiro_par;
    public String disp_prestamo;
    public String fecha_ult_act;
    public String cuota_aporte_emp;
    public String cuota_aporte_pat;
    public String nro_cuenta;
    public String fecha_ult_prest;
    public String monto_ult_prest;
    public String fecha_ult_ret;
    public String monto_ult_ret;
    public String fecha_ult_prest_esp;
    public String monto_ult_prest_esp;
    public String saldo_prest_esp;
    public String cuota_prest_esp;
    public String fecha_ult_aporte;

    
    public CajaAhorro() {
        super("CajaAhorro", "(select * from Saldos_Caja where nombre_apellido is not null) as Saldos_Caja ", "codigo", "nombre_apellido", new String[]{ "codigo","nombre_apellido","saldo_aho_emp","saldo_aho_pat","saldo_prest_corto","saldo_prest_med","cuota_prest_cor","cuota_prest_med","saldo_neto","disp_retiro_par","disp_prestamo","fecha_ult_act","cuota_aporte_emp","cuota_aporte_pat","nro_cuenta","fecha_ult_prest","monto_ult_prest","fecha_ult_ret","monto_ult_ret","fecha_ult_prest_esp","monto_ult_prest_esp","saldo_prest_esp","cuota_prest_esp","fecha_ult_aporte"});
        this._CONNECTION = "jdbc/admin";
        this._AUDITABLE = true;
    }
    
}
