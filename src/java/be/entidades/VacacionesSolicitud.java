package be.entidades;

public class VacacionesSolicitud extends Entidad {
    public String id;
    public String colaborador;
    public String fecha_comienzo;
    public String fecha_fin;
    public String fecha_reintegro;
    public String dias_disfrute;
    public String bono_vacacional_solicitado;
    public String bono_vacacional_pagado;
    public String status;

   
    
    public VacacionesSolicitud() {
        super("VacacionesSolicitud", "vacaciones_solicitudes", "id", new String[]{ "id" , "colaborador" , "fecha_comienzo" , "fecha_fin" , "fecha_reintegro", "dias_disfrute" , "bono_vacacional_solicitado" , "bono_vacacional_pagado" , "status"});
    }
    
}


    