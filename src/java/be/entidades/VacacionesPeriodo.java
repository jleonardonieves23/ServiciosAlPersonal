package be.entidades;

public class VacacionesPeriodo extends Entidad {
    public String id;
    public String colaborador;
    public String periodo;
    public String dias_disfrute;
    public String dias_disfrutados;
    public String bono_pagado;
   
    
    public VacacionesPeriodo() {
        super("VacacionesPeriodo", "vacaciones_periodos", "id", new String[]{ "id", "colaborador", "periodo", "dias_disfrute", "dias_disfrutados", "bono_pagado"});
        this._AUDITABLE = true;
    }
    
}
