package be.entidades;

public class ARI extends Entidad {
    public String id;
    public String colaborador;
    public String fecha;
    public String status;
    public String impuesto_retenido;
    public String remuneraciones_recibidas;
    public String ut;
    public String ut_rebajas;
    public String remuneracion_estimada;
    public String desgravamen_unico;
    public String remuneraciones1;
    public String remuneraciones2;
    public String remuneraciones3;
    public String desgravamen1;
    public String desgravamen2;
    public String desgravamen3;
    public String desgravamen4;
    public String unico;
    public String rebaja1_cantidad;
    public String rebaja2_cantidad;
    public String rebaja3_cantidad;
    public String porcentaje_impuesto;
    public String total_variacion;
    public String remuneraciones1_texto;
    public String remuneraciones2_texto;
    public String remuneraciones3_texto;
    public String ano;
    public String mes;
   
    public ARI() {
        super("ARI", "ARI", "id", "colaborador", new String[]{"id","colaborador","fecha","status","impuesto_retenido","remuneraciones_recibidas","ut","ut_rebajas","remuneracion_estimada","desgravamen_unico","remuneraciones1","remuneraciones2","remuneraciones3","desgravamen1","desgravamen2","desgravamen3","desgravamen4","unico","rebaja1_cantidad","rebaja2_cantidad","rebaja3_cantidad","porcentaje_impuesto","total_variacion","remuneraciones1_texto","remuneraciones2_texto","remuneraciones3_texto","ano","mes"});
        this._AUDITABLE = true;
    }
    
}
