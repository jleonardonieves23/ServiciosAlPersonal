package be.entidades;

public class Unidad extends Entidad {
    public String id;
    public String nombre;
    public String centro_costo;
    public String telefono_local; 	
    public String responsable; 
    public String correo_electronico; 
    public String direccion;
    public String unidad_superior; 
    public String fecha; 
    public String usuario; 
    public String status; 
   
    public Unidad() {
        super("Unidad", "Unidades", "id", "nombre", new String[]{"id", "nombre", "centro_costo", "telefono_local", "responsable", "correo_electronico", "direccion", "unidad_superior", "fecha", "usuario", "status"});
        this._AUDITABLE = true;
    }
    
}
