package be.entidades;

public class ARIPeriodo extends Entidad {
    public String id;
    public String ano;
    public String mes;
    public String fecha_cierre;
    public String status;
   
    public ARIPeriodo() {
        super("ARIPeriodo", "ARI_Periodos", "id", "ano", new String[]{"id","ano","mes","fecha_cierre","status"});
        this._AUDITABLE = true;
    }
    
}
