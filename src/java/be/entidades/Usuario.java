package be.entidades;

import be.utils.Util;
import java.io.File;
import java.text.DateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

public class Usuario extends Entidad {
    public String id;
    public String codigo;
    public String nombre;
    public String apellido;
    public String cargo;
    public String unidad;
    public String rif;
    public String correo_corporativo;
    public String correo_personal;
    public String telefono_oficina;
    public String telefono_personal;
    public String celular_corporativo;
    public String celular_personal;
    public String direccion;
    public String fecha_nacimiento;
    public String fecha_ingreso;
    public String status;
    public String codigo_supervisor;
    public String picture;
    public String permisos;
    
    public Usuario() {
        super("User", "personas", "id", "codigo", new String[]{"id","codigo","nombre","apellido","cargo","unidad","rif","correo_corporativo","correo_personal","telefono_oficina","telefono_personal","celular_corporativo","celular_personal","direccion","fecha_nacimiento","fecha_ingreso","codigo_supervisor"});
        this._AUDITABLE = true;
    }
    
    public boolean tieneAcceso(String action) {
        boolean result = false;
        try {
            Menu menu = new Menu();
           // menu.cargar("accion", action);
            //return tienePermisos(menu.permiso);
            
            be.entidades.Entidades opciones = new be.entidades.Entidades(menu);
            opciones.cargar("accion = '" + action + "' and activo='1'");
            for (Entidad m: opciones.elementos()) {
                result |=  tienePermisos(((Menu)m).permiso);
            }
        } catch (Exception e) {
            Util.log(this, e);
            result = false;
        }
        return result;
    }
         
    public String obtenerNombreCompleto() {
        return (this.nombre != null  ? this.nombre : "") + " " + (this.apellido != null  ? this.apellido : "");
    }
    
    public void imagen(HttpServletRequest request) {   
        String path = Util.ruta(request, "/pic");
        this.picture = "pic/avatar.jpg";
        String pic = id != null ? id.trim() : (codigo != null ? codigo.trim().substring(1) : "");
        String filename = path + "\\" + pic + ".png";                        
        File file = new File(filename);
        if (file.isFile()) {
            this.picture = "pic/" + pic + ".png";
        }        
    }
    
    public boolean tienePermisos(String permission) {
        if (permission==null || permission.isEmpty()) {
            return false;
        } else {
            return this.permisos != null && (permission.equals("Banco Exterior") ||  this.permisos.contains(","+permission+","));
        }
    }
    
    public String generarID() {
        return codigo != null  && !codigo.isEmpty() ? codigo.trim().substring(1) : Util.generarID();
    }
    
    @Override
    public boolean cargar(String field, String id, boolean reload) {
        boolean result = super.cargar(field, id, reload);
        if (result) {
            UsuarioSPI usuario = new UsuarioSPI();
            String codigo = ("          " + ( this.codigo == null || this.codigo.isEmpty() ? id : this.codigo.trim().substring(1)));
            codigo = codigo.substring(codigo.length()-10);            
            usuario.cargar(codigo);            
            //this.id = id;
            //this.codigo = usuario.codigo.trim();
            this.rif =  usuario.cedula != null && !usuario.cedula.isEmpty() ? usuario.cedula.trim() : this.rif;            
            this.nombre =  usuario.primer_nombre != null && !usuario.primer_nombre.isEmpty() ? (Util.capitalize(usuario.primer_nombre) + " " + Util.capitalize(usuario.segundo_nombre)).trim() : this.nombre;
            this.apellido =  usuario.primer_apellido != null && !usuario.primer_apellido.isEmpty() ? (Util.capitalize(usuario.primer_apellido)  + " " + Util.capitalize(usuario.segundo_apellido)).trim() : this.apellido;
            this.fecha_ingreso =  usuario.fecha_ingreso != null && !usuario.fecha_ingreso.isEmpty() ? Util.getDate(usuario.fecha_ingreso) : this.fecha_ingreso;
            this.fecha_nacimiento =  usuario.fecha_nacimiento != null && !usuario.fecha_nacimiento.isEmpty() ? Util.getDate(usuario.fecha_nacimiento) : this.fecha_nacimiento;
                        
        }
        return result;
    }
}
