package be.entidades;

public class CajaAhorroMovimiento extends Entidad {
    public String CODIGO;
    public String FECHA_MOV;
    public String DESCRIPCION;
    public String DEBE; 
    public String HABER;

    
    public CajaAhorroMovimiento() {
        super("CajaAhorroMovimiento", "DETALLE_CAJA", "codigo", "DESCRIPCION", new String[]{ "CODIGO", "FECHA_MOV", "DESCRIPCION", "DEBE", "HABER"});
        this._CONNECTION = "jdbc/admin";
        this._AUDITABLE = true;
    }
    
}
