package be.entidades;

public class ConstanciaTrabajo extends Entidad {
    
    public String codusuario;
    public String tipodoc;
    public String cantd_doc;
    public String fecha_solicitud;
    public String detalle;
    public String IdSolicitud;
    
    public ConstanciaTrabajo() {
        super("[ConstanciaTrabajo]", "ConstanciaTrabajo", "IdSolicitud", "codusuario", new String[]{ "codusuario","tipodoc","cantd_doc","fecha_solicitud","detalle","IdSolicitud"});
        this._CONNECTION = "jdbc/admin";
        this._IS_AUTO_ID = true;
        this._LANGUAGE = true;
    }
    
}
