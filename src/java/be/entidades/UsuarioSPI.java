package be.entidades;

public class UsuarioSPI extends Entidad {
    public String codigo;
    public String cedula;
    public String primer_nombre;
    public String segundo_nombre;
    public String primer_apellido;
    public String segundo_apellido;
    public String fecha_nacimiento;
    public String fecha_ingreso;
    
    private static final String TABLA = "(select trim(fictra) as codigo, ltrim(cedula) as cedula, nombr1 as primer_nombre, nombr2 as segundo_nombre, apell1 as primer_apellido, apell2 as segundo_apellido, fecnac as fecha_nacimiento, fecing as fecha_ingreso from nmm001) datos";
    
    public UsuarioSPI() {             
        super("UsuarioSPI", TABLA, "codigo", new String[]{ "codigo", "cedula", "primer_nombre", "segundo_nombre", "primer_apellido", "segundo_apellido", "fecha_nacimiento", "fecha_ingreso"});
        this._CONNECTION = "jdbc/rrhh";
        //this._AUDITABLE = true;
    }    
}
