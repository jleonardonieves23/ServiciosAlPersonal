package be.entidades;

import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Config;
import be.utils.Util;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Entidad {
    public String  _CONNECTION = Config.$("BE_JNDI_ENTITIES");
    public String  _ENTITY_NAME;
    public String  _TABLE_NAME;
    public String  _FIELD_ID;
    public String  _FIELD_KEY;
    
    
    protected String[] _TABLE_FIELDS;
    protected boolean  _IS_AUTO_ID;
    private   Class    _ENTITY;
    private   Field[]  _FIELDS;
    
    protected boolean  _AUDITABLE = false;
    protected boolean  _LANGUAGE = false;
    
    public Entidad() {
        _ENTITY_NAME  = null;
        _TABLE_NAME   = null;
        _FIELD_ID     = null;
        _TABLE_FIELDS = null;   
        _IS_AUTO_ID   = false;
    }
    
    protected Entidad(String  entityName, String tableName, String fieldId, String[] tableFields) {
        _ENTITY_NAME  = entityName;
        _TABLE_NAME   = tableName;
        _FIELD_ID     = fieldId;
        _ENTITY       = obtenerEntidad();
        _FIELDS       = obtenerCampos();
        _TABLE_FIELDS = tableFields;
        _FIELD_KEY    = fieldId;
    }
    
    protected Entidad(String  entityName, String tableName, String fieldId, String fieldKey, String[] tableFields) {
        _ENTITY_NAME  = entityName;
        _TABLE_NAME   = tableName;
        _FIELD_ID     = fieldId;
        _ENTITY       = obtenerEntidad();
        _FIELDS       = obtenerCampos();
        _TABLE_FIELDS = tableFields;
        _FIELD_KEY    = fieldKey;
    }    
        
    public Class obtenerEntidad() {
        if (_ENTITY == null) {
            _ENTITY = this.getClass();
        }
        return _ENTITY;
    }
    
    public Field[] obtenerCampos() {
        if (_FIELDS == null) {
            _FIELDS = getClass().getFields();
        }
        return _FIELDS;
    }
    
    public Field obtenerCampo(String name) {
        Field field = null;
        try {
            field = getClass().getField(name);
        } catch (Exception e) {
            //Util.log(this, e);
        }
        return field;
    }
    
    public void coneccion(String coneccion) {
        _CONNECTION = coneccion;
    }
    
    public String obtenerID() {
        return obtener(_FIELD_ID);
    }
    
    public String serializar() {
        String result = null;
        try {
            Properties params = new Properties();
            Field[] fields = obtenerCampos();
            String name;
            String value;
            for (Field f : fields) {                
                name = f.getName();
                if (!name.startsWith("_")) {
                    value = f.get(this) == null ? "" : f.get(this).toString();
                    params.setProperty(name, value);
                }
            }        
            result = Util.serializar(params);
        } catch(Exception e) {
            Util.log(this, e);
        }
        return result;
    }

    public String obtener(String field) {
        String result = null;
        try {
            Field f = getClass().getField(field);
            result = f.get(this) == null ? null : f.get(this).toString();
        } catch(Exception e) {
            Util.log(this, e);
        }
        return result;        
    }
    
    public void asignar(String field, String value) {
        if (field != null && !field.isEmpty()) {
            try {            
                Field f = getClass().getField(field);
                f.set(this, value);
            } catch(Exception e) {
                Util.log(this, e);
            }        
        }
    }

    public String obtener(Field field) {
        String result = null;
        try {
            result = field.get(this) == null ? "" : field.get(this).toString();
        } catch(Exception e) {
            Util.log(this, e);
        }
        return result;        
    }
    
    public boolean cargar() {
        boolean result = false;
        try {            
            String id = obtener(_FIELD_ID);
            result = cargar(id);
        } catch(Exception e) {
            Util.log(this, e);
        }
        return result;
    }
    
    public boolean cargar(String id) {      
        return cargar(_FIELD_ID, id);
    }
    public boolean cargar(String field, String id) {
        return cargar(field, id, false);
    }
    public boolean cargar(String field, String id, boolean reload) {
        boolean result = false;
        String traza;
        String objeto = "";
        if( id != null && !id.isEmpty()) {
            try {
                String sql = "SELECT * FROM <TABLE> WHERE <FIELD_ID> = '<ID>'";
                Properties params = new Properties();
                params.setProperty("<TABLE>", _TABLE_NAME);
                params.setProperty("<FIELD_ID>", field);
                params.setProperty("<ID>", id);
                sql = Util.parseSQLParams(sql, params);
                Datos dataprocessor = new Datos(_CONNECTION);
                Object response =  dataprocessor.ejecutar(sql);
                objeto = Config.$("BE_OBJECT_SELECTED").trim() + (id == null ? "" : id); 
                if (response != null && ResultSet.class.isInstance(response)) {
                    ResultSet data = (ResultSet) response;
                    data.next();
                    result = cargar(data, reload);
                    if (result) {
                        String key = this.obtener(this._FIELD_ID);
                        objeto = Config.$("BE_OBJECT_SELECTED").trim() + (key == null ? "" : key); 
                    }
                }
                dataprocessor.cerrar();
                traza = sql;
            
            } catch (Exception e) {
                traza = e.toString();
                Util.log(this, e);
            }  
            String classname = this.getClass().getCanonicalName();
            if (this._AUDITABLE) {
                String accion = classname + ".consultado";        
                String resultado = result ? "Consultado" : "Error de consulta";
                String key = id != null ? id : obtener(_FIELD_ID); 
                //String key = obtener(_FIELD_ID); 
                if (key == null){
                    key = objeto;
                }                
                Seguridad.log(accion, resultado, traza, Util.userZeros(key));
            }      
                
        }
        return result;
    }
    
    public boolean cargar(String entidad, String filtro, String orden) {
        boolean result = false;
        String traza;
        String objeto = "";
        if( filtro != null && !filtro.isEmpty()) {
            try {
                orden = orden == null || orden.isEmpty() ? "" : (" order by " + orden);                
                String sql = "SELECT * FROM <TABLE> WHERE <FILTER> <ORDER>";
                Properties params = new Properties();
                params.setProperty("<TABLE>", _TABLE_NAME);
                params.setProperty("<FILTER>", filtro);
                params.setProperty("<ORDER>", orden);
                sql = Util.parseSQLParams(sql, params);                
                Datos dataprocessor = new Datos(_CONNECTION);
                Object response =  dataprocessor.ejecutar(sql);
                objeto = Config.$("BE_OBJECT_SELECTED").trim() + (filtro == null ? "" : filtro); 
                if (response != null && ResultSet.class.isInstance(response)) {
                    ResultSet data = (ResultSet) response;
                    data.next();
                    result = cargar(data, false);
                    if (result) {
                        String id = this.obtener(this._FIELD_ID);
                        objeto = Config.$("BE_OBJECT_SELECTED").trim() + (id == null ? "" : id); 
                    }
                }
                dataprocessor.cerrar();
                traza = sql;
            
            } catch (Exception e) {
                traza = e.toString();
                Util.log(this, e);
            }  
            String classname = this.getClass().getCanonicalName();
            if (this._AUDITABLE) {
                String accion = classname + ".consultado";        
                String resultado = result ? "Consultado" : "Error de consulta";
                String key = obtener(_FIELD_ID);         
                if (key == null){
                    key = objeto;
                }                
                Seguridad.log(accion, resultado, traza, Util.userZeros(key));
            }      
        }
        return result;
    }
    
    public boolean cargar(ResultSet data) {
        return cargar(data, false);
    }
    
    public boolean cargar(ResultSet data, boolean reload) {
        boolean result = false;
        try {
            Properties params = new Properties();
            Field[] fields = obtenerCampos();
            String name;
            String value;            
            for(Field f : fields) {
                name = f.getName();
                if (!name.startsWith("_")) {
                    value = null;
                    try { value = data.getString(name); } catch(Exception ex) { }
                    if (!reload || (value != null && !value.isEmpty())) {
                        params.setProperty(name, Util.checkNull(value));
                    }
                }
            }
            result = cargar(params, reload);
        } catch(Exception e) {
            Util.log(this, e);
        }                
        return result;
    }
    
    public boolean cargar(Properties data) {
        return cargar(data, false);
    }
    
    public boolean cargar(Properties data, boolean reload) {
        boolean result = false;
        try {
            Field[] fields = obtenerCampos();
            String name;
            String value;            
            for(String s : data.stringPropertyNames()) {
                Field f = obtenerCampo(s);
                if (f != null) {
                    name = f.getName();
                    if (!name.startsWith("_")) {                    
                        value = data.stringPropertyNames().contains(name) ? data.getProperty(name) : null;
                        if (!reload || (value != null && !value.isEmpty())) {
                            f.set(this, value);
                        }
                    }
                }
            }
            result = true;
        } catch(Exception e) {
            Util.log(this, e);
        }                
        return result;
    }
    
    
    public boolean recargar(String id) {      
        return cargar(_FIELD_ID, id, true);
    }
    public boolean recargar(String field, String id) {
        return cargar(field, id, true);
    }
    
    public boolean recargar(ResultSet data) {
        return cargar(data, true);
    }
    
    public boolean recargar(Properties data) {
        return cargar(data, true);
    }    
    
    public Properties obtenerPropiedades() {
        Properties result = null;
        try {
            String v;
            String[] parts;
            String part;
            result = new Properties();
            for(Field f : obtenerCampos()) {
                if (!f.getName().startsWith("_")) {
                    v = obtener(f);
                    parts = v != null ? v.split(":") : new String[1];
                    part = parts[parts.length-1];                
                    result.setProperty(f.getName(), part);
                }
            }        
        } catch (Exception e) {
            Util.log(this, e);
        }               
        return result;
    }   
    
    public Properties obtenerPropiedadesTabla() {
        Properties result = null;
        try {
            String v;
            String[] parts;
            String part;
            result = new Properties();
            for(String s : _TABLE_FIELDS) {
                Field f = obtenerCampo(s);
                if (!f.getName().startsWith("_")) {
                    v = obtener(f);
                    
                    parts = v != null ? v.split(":") : new String[1];
                    part = parts[parts.length-1];                
                    result.setProperty(f.getName(), part);
                    
                    result.setProperty(f.getName(), v);
                }
            }        
        } catch (Exception e) {
            Util.log(this, e);
        }               
        return result;
    }    
   
    public String[] obtenerNombresCampos() {
        _FIELDS = obtenerCampos();
        List<String> list = new ArrayList();
        for(int i=0; i<_FIELDS.length; i++) {
            if (!_FIELDS[i].getName().startsWith("_")) {
                list.add(_FIELDS[i].getName());
            }
        }
        String[] result = new String[list.size()];
        list.toArray(result);        
        return result;
    }

    public String[] obtenerNombresCamposTabla() {
        if (_IS_AUTO_ID) {
            String[] result = new String[_TABLE_FIELDS.length-1];
            int j = 0;
            for(int i=0; i<_TABLE_FIELDS.length; i++) {
                if (!_TABLE_FIELDS[i].equals(_FIELD_ID)) {
                    result[j++] = _TABLE_FIELDS[i];
                }
            }
            return result;
        } else {
            return _TABLE_FIELDS;
        }
    }
    
    public String[] obtenerValores() {
        String[] fields = obtenerNombresCamposTabla();
        String[] result = new String[fields.length];
        for(int i=0; i<fields.length; i++) {
            result[i] = obtener(fields[i]);
        }
        return result;
    }
    
    public boolean guardar() {        
        boolean result = false;
        String traza   = null;
        String id      = null;
        String sql     = null;
        String fields  = null;
        String values  = null;
        String objeto  = "";        
        try {            
            id = obtener(_FIELD_ID);            
            if (id == null || id.isEmpty() || !buscar(id)) {
                id = (id == null || id.isEmpty()) && !_IS_AUTO_ID ? generarID() : id;
                id = _IS_AUTO_ID ? "" : id;
                asignar(_FIELD_ID, id);
                sql = "INSERT INTO <TABLE> (<FIELDS>) VALUES (<VALUES>)";
                fields = Util.unir(obtenerNombresCamposTabla());
                values = Util.unir(obtenerValores(), "'");
                objeto = Config.$("BE_OBJECT_INSERT").trim() + (id == null ? "" : id);            
            } else {
                sql = "UPDATE <TABLE> SET <VALUES> WHERE <FIELD_ID> = '<ID>'";
                fields = "";
                values = Util.unir(obtenerPropiedadesTabla(), "'");
                objeto = Config.$("BE_OBJECT_MODIFIED").trim() + (id == null ? "" : id);            
            }
            Properties params = new Properties();
            params.setProperty("<TABLE>",    _TABLE_NAME);
            params.setProperty("<FIELD_ID>", _FIELD_ID);
            params.setProperty("<ID>",       id);
            params.setProperty("<FIELDS>",   fields);
            params.setProperty("<VALUES>",   values);            
            sql = Util.parseSQLParams(sql,   params);
            if (_LANGUAGE) sql = "SET LANGUAGE us_english; " + sql;
            Datos dataprocessor = new Datos(_CONNECTION);                        
            result = dataprocessor.procesar(sql);
            dataprocessor.cerrar();
            traza = sql;
            
        } catch (Exception e) {
            traza = e.toString();
            Util.log(this, e);
        }  
        String classname = this.getClass().getCanonicalName();
        if (!classname.equals(Auditoria.class.getCanonicalName())) {
            String accion = classname + ".guardar";        
            String resultado = result ? "Guardado" : "Error guardando";
            String key = obtener(_FIELD_KEY);         
            if (key == null){
                key = objeto;
            }            
            Seguridad.log(accion, resultado, traza, Util.userZeros(key));
        }
        return result;
    }
    
    public String generarID() {
        return Util.generarID();
    }
    
    public boolean buscar(String id) {
        boolean result = false;
        try {
            String sql = "";
            sql = "SELECT <FIELD_ID> FROM <TABLE> WHERE <FIELD_ID>='<ID>'";
            Properties params = new Properties();
            params.setProperty("<TABLE>", _TABLE_NAME);
            params.setProperty("<FIELD_ID>", _FIELD_ID);
            params.setProperty("<ID>", id);
            sql = Util.parseSQLParams(sql, params);
            Datos dataprocessor = new Datos(_CONNECTION);
            Object response = dataprocessor.ejecutar(sql);              
            result = response != null && ResultSet.class.isInstance(response) && ((ResultSet) response).next();
            dataprocessor.cerrar();
        } catch (Exception e) {
            Util.log(this, e);
        }           
        return result;
    }
 
    public boolean borrar() {
        boolean result = false;
        String traza   = "";
        String objeto  = "";
        String id = obtener(_FIELD_ID);
        try {
            String sql = "DELETE FROM <TABLE> WHERE <FIELD_ID> = '<ID>'";
            Properties params = new Properties();
            params.setProperty("<TABLE>", _TABLE_NAME);
            params.setProperty("<FIELD_ID>", _FIELD_ID);
            params.setProperty("<ID>", id);
            sql = Util.parseSQLParams(sql, params);
            Datos dataprocessor = new Datos(_CONNECTION);
            result = dataprocessor.procesar(sql);  
            objeto = Config.$("BE_OBJECT_DELETE").trim() + id;            
            dataprocessor.cerrar();
            traza   = sql;
        } catch (Exception e) {
            Util.log(this, e);
            traza = e.toString();
        }     
        String classname = this.getClass().getCanonicalName();
        if (!classname.equals(Auditoria.class.getCanonicalName())) {
            String accion = classname + ".guardar";        
            String resultado = result ? "Borrado" : "Error borrando";
            String key = obtener(_FIELD_KEY);
            if (key == null){
                key = objeto;
            }            
            Seguridad.log(accion, resultado, traza, key);
        }        
        return result;
    }
}
