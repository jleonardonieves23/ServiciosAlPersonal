package be.entidades;

import be.procesos.Datos;
import be.seguridad.Seguridad;
import be.utils.Config;
import be.utils.Util;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Properties;

public class Entidades {

    private ArrayList<Entidad> _LIST;
    private Class _ENTITY;
    public String _FIELD_ID;

    protected boolean _AUDITABLE = false;
    protected String _SQL = "";

    private Entidades() {

    }

    public Entidades(Entidad entidad) {
        _ENTITY = entidad.getClass();
        _LIST = new ArrayList<>();
    }

    public boolean cargar() {
        return cargar(null, null);
    }

    public boolean vacio() {
        return _LIST.isEmpty();
    }

    public boolean cargar(String criteria) {
        return cargar(criteria, null);
    }

    public boolean cargar(String criteria, String order) {
        return cargar(criteria, order, false);
    }

    public boolean cargar(String criteria, String order, boolean searchInAll) {
        boolean result = false;
        String traza;
        String objeto = "";
        try {
            Entidad entidad = (Entidad) _ENTITY.newInstance();
            String table = entidad._TABLE_NAME;
            String connection = entidad._CONNECTION;
            Datos datos = new Datos(connection);
            String sql = "SELECT * FROM <TABLE> <CRITERIA> <ORDER>";            
            if (searchInAll) {
                if (criteria != null && !criteria.isEmpty()) {
                    String separator = "";
                    StringBuilder filter = new StringBuilder();
                    for (String s : entidad.obtenerNombresCampos()) {
                        filter.append(separator);
                        filter.append(s);
                        filter.append(" like '" + criteria + "%'");
                        separator = " or ";
                    }
                    criteria = filter.toString();
                }
            }
            criteria = criteria == null || criteria.isEmpty() ? "" : (" WHERE " + criteria);
            order = order == null || order.isEmpty() ? " ORDER BY 1 " : (" ORDER BY " + order);
            Properties params = new Properties();
            params.setProperty("<TABLE>", table);
            params.setProperty("<CRITERIA>", criteria);
            params.setProperty("<ORDER>", order);
            sql = Util.parseSQLParams(sql, params);
            this.setSql(sql);            
            Object response = datos.ejecutar(sql);            
            if (response != null) {
                ResultSet data = (ResultSet) response;
                while (data.next()) {
                    entidad = (Entidad) _ENTITY.newInstance();
                    entidad.cargar(data);
                    _LIST.add(entidad);
                }
                result = true;               
            }
            datos.cerrar();
            this.setSql(sql);
        } catch (Exception e) {
            Util.log(this, e);
        }
        
        String key = obtener2(_FIELD_ID);         
                if (key == null){
                    key = objeto;
                }

        return result;
    }

    public Entidad[] elementos() {
        Entidad[] array = new Entidad[_LIST.size()];
        _LIST.toArray(array);
        return array;
    }

    public Entidad obtener(int index) {
        return _LIST.get(index);
    }

    public String obtener2(String field) {
        String result = null;
        try {
            if (field != null) {
                Field f = getClass().getField(field);
                result = f.get(this) == null ? null : f.get(this).toString();
            }
        } catch (Exception e) {
            Util.log(this, e);
        }
        return result;
    }

    public Entidad buscar(String key) {
        Entidad entidad;
        Field[] fields;
        String value;
        for (Object object : elementos()) {
            entidad = (Entidad) object;
            fields = entidad.obtenerCampos();
            for (Field f : fields) {
                value = entidad.obtener(f);
                if (value != null && value.equals(key)) {
                    return entidad;
                }
            }
        }
        return null;
    }

    public void agregar(Entidad entidad) {
        _LIST.add(entidad);
    }

    public void remover(int index) {
        _LIST.remove(index);
    }

    public String serializar() {
        StringBuilder result = null;
        if (!vacio()) {
            result = new StringBuilder();
            String separator = "";
            result.append("[");
            for (Entidad item : elementos()) {
                result.append(separator);
                result.append(item.serializar());
                separator = ",";
            }
            result.append("]");
        } else {
            result.append("[]");
        }
        return result == null ? null : result.toString();
    }

    public void setAuditable(boolean valor) {
        this._AUDITABLE = valor;
    }

    public void setSql(String valor) {
        this._SQL = valor;
    }

    public String getSql() {
        return this._SQL;
    }
}
