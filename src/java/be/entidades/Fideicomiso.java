package be.entidades;

public class Fideicomiso extends Entidad {
    public String contrato;
    public String cedula;
    public String fechadesde;
    public String fechahasta;
    public String nombre;
    public String cedulafide;
    public String nombrefide;
    public String saldofondo;
    public String saldopresta;
    public String promdiario;
    public String dispretiro;
    public String saldodisp;
    public String ganabrutas;
    public String gastos;
    public String gananetas;
    public String intporpagar;
    public String intacumulados;
    public String intcobrados;
    public String embargo;
    public String coefgasto;
    public String rendanual;
    public String cierre;

    
    public Fideicomiso() {
        //super("Fideicomiso", "beneficiarios", "substr(cedula,2,length(cedula))", "nombre", new String[]{ "contrato","cedula","fechadesde","fechahasta","nombre","cedulafide","nombrefide","saldofondo","saldopresta","promdiario","dispretiro","saldodisp","ganabrutas","gastos","gananetas","intporpagar","intacumulados","intcobrados","embargo","coefgasto","rendanual"});
        super("Fideicomiso", "(select a.CONTRATO, substr(a.CEDULA,2,length(a.CEDULA)) cedula, a.FECHADESDE, a.FECHAHASTA, a.NOMBRE, a.CEDULAFIDE, a.NOMBREFIDE, a.SALDOFONDO, a.SALDOPRESTA, a.PROMDIARIO, a.DISPRETIRO, a.SALDODISP, a.GANABRUTAS, a.GASTOS, a.GANANETAS, a.INTPORPAGAR, a.INTACUMULADOS, a.INTCOBRADOS, a.EMBARGO, a.COEFGASTO, a.RENDANUAL, b.fl_cierre  CIERRE from beneficiarios a, generales b) beneficiarios", "cedula", "nombre", new String[]{ "contrato","cedula","fechadesde","fechahasta","nombre","cedulafide","nombrefide","saldofondo","saldopresta","promdiario","dispretiro","saldodisp","ganabrutas","gastos","gananetas","intporpagar","intacumulados","intcobrados","embargo","coefgasto","rendanual", "cierre"});
        this._CONNECTION = "jdbc/fideicomiso";
        this._AUDITABLE = true;
    }
    
}
