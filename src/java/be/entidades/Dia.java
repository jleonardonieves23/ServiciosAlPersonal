package be.entidades;

public class Dia extends Entidad {
    public String id;
    public String fecha;
    public String calendario;
    public String descripcion;

    
    public Dia() {
        super("Dia", "Dias", "id", "fecha", new String[]{ "id", "fecha", "calendario", "descripcion"});
    }
    
}
