package be.entidades;

public class Info extends Entidad {
    public String id;
    public String texto;
    public String titulo;
   
    public Info() {
        super("Info", "Info", "id", "titulo", new String[]{"id", "texto", "titulo"});
    }
    
}
