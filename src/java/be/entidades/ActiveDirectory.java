//PACKAGE
package be.entidades;

//IMPORTS
import be.seguridad.Seguridad;
import be.utils.Config;
import be.utils.Util;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

//CLASS
public class ActiveDirectory {
   
    //CONSTS
    private static final String  DISTINGUISHED_NAME              = "distinguishedName";
    private static final String  CN                              = "cn";
    private static final String  MEMBER_OF                       = "memberOf";
    private static final String  SEARCH_BY_SAM_ACCOUNT_NAME      = "(|(SAMAccountName={0}*)(DisplayName=*{0}*)(MAIL=*{0}*)(TITLE=*{0}*)(TELEPHONENUMBER=*{0}*)(DEPARTMENT=*{0}*)(LDAP_DIVISION=*{0}*))";
    private static final String  SEARCH_BY_SAM_ACCOUNT_NAME_BASE = "(|(SAMAccountName={0}*)(DisplayName=*{0}*))";
    private static final String  SEARCH_BY_SAM_ACCOUNT_NAME_ONLY = "(|(SAMAccountName={0}*))";
    
    private static final String  LDAP_CODE                       = "SAMACCOUNTNAME"; 
    private static final String  LDAP_USERNAME                   = "DISPLAYNAME"; 
    private static final String  LDAP_NAME                       = "GIVENNAME"; 
    private static final String  LDAP_LASTNAME                   = "SN"; 
    private static final String  LDAP_EMAIL                      = "MAIL"; 
    private static final String  LDAP_PHONE                      = "TELEPHONENUMBER"; 
    private static final String  LDAP_DEPARTMENT                 = "DEPARTMENT"; 
    private static final String  LDAP_DIVISION                   = "DIVISION"; 
    private static final String  LDAP_TITLE                      = "TITLE"; 
    private static final String  LDAP_LOCATION                   = "ST"; 
    private static final String  LDAP_STATUS                     = "userAccountControl";
    private static final String  LDAP_ACCOUNT_ACTIVE             = "Cuenta activa";
    
    private static final String ADS_UF_ACCOUNTDISABLE_MSG        = "Cuenta desactivada";
    private static final String ADS_UF_LOCKOUT_MSG               = "Cuenta bloqueada";
    private static final String ADS_UF_PASSWORD_EXPIRED_MSG      = "Clave expirada";
         
    private static final int ADS_UF_ACCOUNTDISABLE               = 0x00000002; 
    private static final int ADS_UF_LOCKOUT                      = 0x00000010; 
    private static final int ADS_UF_PASSWORD_EXPIRED             = 0x00800000;

    //PROPERTIES
    private DirContext context;
        
    //CONSTRUCTORS
    public ActiveDirectory() {
        init(null);
    }
    
    public ActiveDirectory(DirContext context) {
        init(context);
    }
    
    //INIT
    private void init(DirContext context) {  
        this.context = context;
    }  
    
    //ADVANCED METHODS        
    private NamingEnumeration executeSearch(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        // Create the search controls
        SearchControls searchCtls = new SearchControls();

        // Specify the attributes to return
        if (attributes != null) {
            searchCtls.setReturningAttributes(attributes);
        }

        // Specify the search scope
        searchCtls.setSearchScope(searchScope);

        // Search for objects using the filter
        NamingEnumeration result = ctx.search(searchBase, searchFilter, searchCtls);
        return result;
    }

    private SearchResult executeSearchSingleResult(DirContext ctx, int searchScope, String searchBase, String searchFilter, String[] attributes) throws NamingException {
        NamingEnumeration result = executeSearch(ctx, searchScope, searchBase, searchFilter, attributes);

        SearchResult sr = null;
        // Loop through the search results
        while (result.hasMoreElements()) {
            sr = (SearchResult) result.next();
            break;
        }
        return sr;
    }

    public void setConext(DirContext context) {
        this.context = context;
    }
    
    public DirContext getContext() {
        return this.context;
    }
    public Usuario getUser(String search) {  
        String defaultSearchBase = Config.$("BE_LDAP_DC").trim();
        return getUser(search, defaultSearchBase);
    }
    public Usuario getUser(String search, String defaultSearchBase) {         
        if (this.context == null) {            
            this.context = Seguridad.getContext();                                
        }
        if (this.context != null) {
            try {                
                SearchResult sr          = executeSearchSingleResult(this.context, SearchControls.SUBTREE_SCOPE, defaultSearchBase, MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME_ONLY, new Object[]{search}), new String[]{DISTINGUISHED_NAME, CN, MEMBER_OF, LDAP_CODE,LDAP_USERNAME,LDAP_NAME,LDAP_LASTNAME,LDAP_EMAIL,LDAP_PHONE,LDAP_DEPARTMENT,LDAP_DIVISION,LDAP_TITLE,LDAP_LOCATION,LDAP_STATUS});
                Usuario user             = new Usuario();                        
                populateUser(sr, user);      
                try {
                    ((Context)sr.getObject()).close();
                } catch(Exception e ) {

                }
                return user;
            } catch (Exception e) {
                Util.log(this, e);
            } 
        }
        return null;
    } 
    
    private String processPermissions(String permissions) {
        if (permissions != null && !permissions.isEmpty()) {
        StringBuilder result = new StringBuilder();
        if (permissions != null && !permissions.isEmpty()) {
            String[] parts = permissions.split(",");
            String separator = ",";
            result.append(separator);            
            for(String p : parts) {
                p = p.trim();
                if (p.startsWith("CN=")) {
                    p = p.substring(3);                    
                    result.append(p);                    
                    result.append(separator);                       
                }
            }
            result.append(separator);            
        }
            return result.toString();
        } else {
            return "";
        }
    }
    
    public void populateUser(SearchResult sr, Usuario user) {
        try {
            if (sr != null && user != null) {
                user.codigo              = clear(Util.checkNull(sr.getAttributes().get(LDAP_CODE))).trim();
                user.nombre              = clear(Util.checkNull(sr.getAttributes().get(LDAP_NAME))).trim();
                user.apellido            = clear(Util.checkNull(sr.getAttributes().get(LDAP_LASTNAME))).trim();
                user.correo_corporativo  = clear(Util.checkNull(sr.getAttributes().get(LDAP_EMAIL))).trim();
                user.telefono_oficina    = clear(Util.checkNull(sr.getAttributes().get(LDAP_PHONE))).trim();
                user.unidad              = clear(Util.checkNull(sr.getAttributes().get(LDAP_DEPARTMENT))).trim();
                user.cargo               = clear(Util.checkNull(sr.getAttributes().get(LDAP_TITLE))).trim();
                user.direccion           = clear(Util.checkNull(sr.getAttributes().get(LDAP_LOCATION))).trim();            
                user.permisos            = processPermissions(clear(Util.checkNull(sr.getAttributes().get(MEMBER_OF))));
                int status               = 0;
                try {
                    status = Integer.parseInt(clear(Util.checkNull(sr.getAttributes().get(LDAP_STATUS)).trim()).trim());
                } catch (Exception ex) {

                }
                user.status              = "";
                /*
                user.status              += (status & ADS_UF_ACCOUNTDISABLE)   ==  ADS_UF_ACCOUNTDISABLE    ? ADS_UF_ACCOUNTDISABLE_MSG   : "";            
                user.status              += (status & ADS_UF_LOCKOUT)          ==  ADS_UF_LOCKOUT           ? ADS_UF_LOCKOUT_MSG          : "";            
                user.status              += (status & ADS_UF_PASSWORD_EXPIRED) ==  ADS_UF_PASSWORD_EXPIRED  ? ADS_UF_PASSWORD_EXPIRED_MSG : "";                            
                */
                user.status              = user.status.equals("") ? LDAP_ACCOUNT_ACTIVE : user.status;
                        
                ((Context)sr.getObject()).close();
                        
            }
        } catch(Exception e) {
            Util.log(this, e);
        }            
    }
    
    public ArrayList<Usuario> getUsers(String search) {    
        if (this.context != null && search != null && !search.isEmpty()) {
            try {
                ArrayList<Usuario> users = new ArrayList<Usuario>();  
                String defaultSearchBase = Config.$("BE_LDAP_DC").trim();                                  
                NamingEnumeration result = executeSearch(this.context, SearchControls.SUBTREE_SCOPE, defaultSearchBase, MessageFormat.format(SEARCH_BY_SAM_ACCOUNT_NAME, new Object[]{search}), new String[]{DISTINGUISHED_NAME, CN, MEMBER_OF, LDAP_CODE,LDAP_USERNAME,LDAP_NAME,LDAP_LASTNAME,LDAP_EMAIL,LDAP_PHONE,LDAP_DEPARTMENT,LDAP_DIVISION,LDAP_TITLE,LDAP_LOCATION,LDAP_STATUS});
                SearchResult sr = null;
                Usuario user;
                String nombre;
                while (result.hasMoreElements()) {
                    sr     = (SearchResult) result.next();
                    user   = new Usuario();                      
                    populateUser(sr, user);
                    nombre = user.nombre;
                    if (!nombre.isEmpty() && !nombre.contains("ADM ") && !nombre.toUpperCase().equals("NOMBRE")) {                    
                        users.add(user);        
                    }
                }      
                try {
                    if (sr != null) {
                        ((Context)sr.getObject()).close();
                    }
                } catch(Exception e ) {

                }                                            
                return users;
            } catch (Exception e) {
                Util.log(this, e);
            } 
        }
        return null;
    }    
    
    public String getAllUsers() { 
        StringBuilder result         = new StringBuilder();
        ArrayList<Usuario> usuarios  = null;
        Usuario usuario              = null;
        String nombre                = "";
        String apellido              = "";
        String codigo                = "";
        String rif                   = "";
        String cargo                 = "";
        String unidad                = "";
        String correo_corporativo    = "";
        String telefono_oficina      = "";
        String direccion             = "";
        String status                = "";
        int    total                 = 0;      
        String[] categorias          = new String[]{"F", "E", "C" , "P"};
        String categoria             = "";
        String sep                   = "";
        for(int j=0; j<categorias.length; j++) {
            categoria = categorias[j];
            for(int k=0; k<7; k++) {
                usuarios = this.getUsers(categoria + k + "*"); 
                if (usuarios != null) {
                    int i   = 0;
                    total   = usuarios.size();
                    while (usuarios != null && i<total) {
                        usuario = usuarios.get(i++);
                        if (usuario != null) {
                            if (usuario.codigo != null && usuario.codigo.trim().length()>1) {
                                nombre             = (usuario.nombre != null && !usuario.nombre.equals("") ?  usuario.nombre : "").trim();
                                apellido           = (usuario.apellido != null && !usuario.apellido.equals("") ?  usuario.apellido : "").trim();
                                codigo             = (usuario.codigo != null && !usuario.codigo.equals("") ?  usuario.codigo : "").trim();
                                rif                = (usuario.rif != null && !usuario.rif.equals("") ?  usuario.rif : "").trim();
                                cargo              = (usuario.cargo != null && !usuario.cargo.equals("") ?  usuario.cargo : "").trim();
                                unidad             = (usuario.unidad != null && !usuario.unidad.equals("") ?  usuario.unidad : "").trim();
                                correo_corporativo = (usuario.correo_corporativo != null && !usuario.correo_corporativo.equals("") ?  usuario.correo_corporativo : "").trim();
                                telefono_oficina   = (usuario.telefono_oficina != null && !usuario.telefono_oficina.equals("") ?  usuario.telefono_oficina : "").trim();
                                direccion          = (usuario.direccion != null && !usuario.direccion.equals("") ?  usuario.direccion : "").trim();
                                status             = (usuario.status != null && !usuario.status.equals("") ?  usuario.status : "").trim();
                                
                                if (!nombre.isEmpty() && !nombre.contains("ADM ") && !nombre.toUpperCase().equals("NOMBRE")) {
                                                                                                           
                                    result.append(sep);   
                                    sep = ",";
                                    result.append("{");
                                    
                                    result.append("\"nombre\":\"");
                                    result.append(nombre);
                                    result.append("\",");
                                    
                                    result.append("\"apellido\":\"");
                                    result.append(apellido);                                            
                                    result.append("\",");
                                    
                                    result.append("\"codigo\":\"");
                                    result.append(codigo);
                                    result.append("\",");
                                    
                                    result.append("\"rif\":\"");
                                    result.append(rif);
                                    result.append("\",");
                                    
                                    result.append("\"cargo\":\"");
                                    result.append(cargo);    
                                    result.append("\",");
                                    
                                    result.append("\"unidad\":\"");
                                    result.append(unidad);
                                    result.append("\",");
                                    
                                    result.append("\"correo_corporativo\":\"");
                                    result.append(correo_corporativo);
                                    result.append("\",");
                                    
                                    result.append("\"telefono_oficina\":\"");
                                    result.append(telefono_oficina);
                                    result.append("\",");
                                    
                                    result.append("\"direccion\":\"");
                                    result.append(direccion);
                                    result.append("\",");
                                    
                                    result.append("\"status\":\"");
                                    result.append(status);
                                    result.append("\"}");   
                                }
                            }
                        }
                    }
                }
            }
        } 
        return "[" + result.toString() + "]";           
    }
    
    public ArrayList<String> getAllGroups() {
        ArrayList<String> list = new   ArrayList<String>();
        
        try {          
            String groupsou   = Config.$("BE_LDAP_GROUPS_OU").trim();
            String domain     = Config.$("BE_LDAP_DOMAIN").trim();
            String username   = Config.$("BE_LDAP_USER").trim();
            String password   = Config.$("BE_LDAP_PASSWORD").trim();            
            String domainname = Util.checkNull(Config.$("BE_LDAP_DOMAIN_NAME"));      
            String server = Util.checkNull(Config.$("BE_LDAP_SERVER")); 
            Hashtable env     = new Hashtable();                        

            env.put(Context.INITIAL_CONTEXT_FACTORY,    "com.sun.jndi.ldap.LdapCtxFactory");
            env.put(Context.PROVIDER_URL,               "ldap://" + server);
            env.put(Context.SECURITY_AUTHENTICATION,    "simple");
            env.put(Context.SECURITY_PRINCIPAL,         domain + "\\" + username);
            env.put(Context.SECURITY_CREDENTIALS,       password); 
            env.put("com.sun.jndi.ldap.read.timeout",   Config.$("BE_LDAP_TIMEOUT"));
            
            DirContext ctx    = new InitialDirContext(env);            

            SearchControls ctls = new SearchControls();
            String[] attrIDs = { "cn" };
            ctls.setReturningAttributes(attrIDs);
            ctls.setSearchScope(SearchControls.SUBTREE_SCOPE);
            String app =  Config.$("BE_LDAP_APP").trim();
            domainname = domainname.replace("ldap://", "");
            String[] parts                = domainname.split("\\.");
            String DC                     = String.join(",DC=", parts);
            DC                            = (groupsou + ",DC=" + DC);          
            
            String search = MessageFormat.format("(cn={0}*)", new Object[]{app});
            NamingEnumeration answer = ctx.search(DC, search, ctls);
            while (answer.hasMore()) {
                SearchResult rslt = (SearchResult) answer.next();
                Attributes attrs = rslt.getAttributes();
                String groups = attrs.get("cn").toString();
                String [] groupname = groups.split(":");
                String userGroup = groupname[1];
                list.add(attrs.get("cn").toString().split(":")[1].trim());
                try {
                    ((Context) rslt.getObject()).close();
                } catch(Exception e ) {

                }                                 
            }
        } catch(Exception e) {
            Util.log(this, e);
        }
        
        return list;
    }
    
    
    private String clear(String param) {
        String[] values = param.split(":");
        return values[values.length-1];
    }
           
}


