package be.entidades;

public class Sueldo extends Entidad {
    public String SUELDO;
    public String COLABORADOR;
    public String ANO;
    public String MES;
    public String PERIODO;
    
    private static final String TABLA = "(select round(suedia*30) as SUELDO, TRAB_FICTRA as COLABORADOR, EXTRACT(YEAR FROM fecaum) as ANO, EXTRACT(MONTH FROM fecaum) as MES, (EXTRACT(YEAR FROM fecaum)*100) + EXTRACT(MONTH FROM fecaum) as PERIODO from nmm004 order by periodo desc) datos";
    
    public Sueldo() {             
        super("Sueldo", TABLA, "COLABORADOR", new String[]{ "SUELDO","COLABORADOR","ANO","MES","PERIODO"});
        this._CONNECTION = "jdbc/rrhh";
    }    
}
