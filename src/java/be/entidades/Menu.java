package be.entidades;

public class Menu extends Entidad {
    public String id;
    public String accion;
    public String titulo;
    public String permiso;        
    public String contexto;
    public String posicion;
    public String activo;
    
    public Menu() {
        super("Menu", "Menu", "id", "titulo", new String[]{ "id", "accion", "titulo", "permiso", "activo", "posicion", "contexto"});
    }
    
}
