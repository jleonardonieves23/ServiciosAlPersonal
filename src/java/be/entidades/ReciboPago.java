package be.entidades;

public class ReciboPago extends Entidad {
    
    public String TRAB_FICTRA;
    public String TNOM_TIPNOM;
    public String PROC_TIPPRO;
    public String FECMOV;
    public String DESDEP;
    public String DESCAR;
    public String FPRO_ANOCAL;
    public String FPRO_NUMPER;
    public String SUBPRO;
    public String TOTAL_ASIG;
    public String TOTAL_DEDUC;
    public String TOTAL_NETO;
    public String DESCRIPCION_RECIBO;
    public String CEDULA;
    public String APELLNOMB;
    public String TEXTORECIBO;
    
            
    public ReciboPago() {     
        super("ReciboPago", "(SELECT ER.*, NT37.LINEA1 || ' ' || NT37.LINEA2 || ' ' || NT37.LINEA3 || ' ' || NT37.LINEA4 as TEXTORECIBO FROM BEMICROOMP.ENCABEZADO_RECIBO ER LEFT JOIN INFOCENT.NMT037 NT37 ON ER.TNOM_TIPNOM = NT37.TNOM_TIPNOM AND ER.PROC_TIPPRO=NT37.PROC_TIPPRO AND ER.FPRO_ANOCAL= NT37.FPRO_ANOCAL AND ER.FPRO_NUMPER= NT37.FPRO_NUMPER AND ER.SUBPRO=NT37.FPRO_SUBPRO) TEXTORECIBO", "TRAB_FICTRA", new String[]{ "TNOM_TIPNOM","TRAB_FICTRA","PROC_TIPPRO","FECMOV","DESDEP","DESCAR","FPRO_ANOCAL","FPRO_NUMPER","SUBPRO","TOTAL_ASIG","TOTAL_DEDUC","TOTAL_NETO","DESCRIPCION_RECIBO","CEDULA","APELLNOMB", "TEXTORECIBO"});
        this._CONNECTION = "jdbc/rrhh";
        this._AUDITABLE = true;
    }
    
}
