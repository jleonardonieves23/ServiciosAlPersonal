package be.entidades;

public class Auditoria extends Entidad {
    public String id;
    public String usuario;
    public String fecha;
    public String accion;
    public String resultado;
    public String traza;
    public String direccion_ip;
    public String objeto;

    public Auditoria() {
        super("Auditoria", "Auditoria", "id", new String[]{ "id", "usuario", "fecha", "accion", "resultado", "traza", "direccion_ip", "objeto"});
    }    
    
}
