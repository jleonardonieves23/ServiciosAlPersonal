//MAIN RUN
$(function() {
    
    jQuery.fn.exists = function(){ return this.length > 0; }
    
    var path = new String(window.location.pathname);
    BE_CURRENT_CONTEXT = path.substring(0, path.length - 1);
    BE_DISPATCHER_URL  = "ejecutar";
    $("#user").fadeOut();
    $("#message").slideUp();
    $("div.content-holder").fadeIn();   
            
    //ZOOM
    /*
    var z = cookie("zoom");
    if (z) {
        $("body").animate({ 'zoom': z }, 0);
    } 
    */
   
    setTimeout(setsize, 100);
           
    $("input").attr("autocomplete", "off");
    
    if ($("#reloj").exists()) {
        $("#reloj").html(BE_TIEMPO_SALIDA);
        comenzarRelojSalida();
    }
    
});    
    
////////////////////////////////////////////////////////////////////////////////

//GLOBAL VARIABLES
var BE_CURRENT_CONTEXT  = "";
var BE_DISPATCHER_URL   = "";
var BE_NO_DATA_FOUND    = "No hay datos para mostrar";
var BE_ERROR            = "Se produjo un error en la operaci&oacute;n";
var BE_SUCCESS          = "Operaci&oacute;n exitosa";
var ENTITY              = "";
var FIELD_ID            = "";
var ORDER_BY            = "";
var FILTER              = "";
var TABLE_OPTIONS       = null;
var BE_FUENTE           = 'Arial';
var BE_FONDO            = 1;
var BE_TIEMPO_SALIDA    = 180;
var BE_TIEMPO           = 0;
var BE_RELOJ            = null;
var BE_RELOJ_MENSAJE    = null;
var BE_RELOJ_SALIDA     = null;
var MILISEGUNDO         = 1000;
////////////////////////////////////////////////////////////////////////////////

//FUNCTIONS

function comenzarRelojSalida() {
    clearTimeout(BE_RELOJ);
    //clearTimeout(BE_RELOJ_MENSAJE);
    //clearTimeout(BE_RELOJ_SALIDA);
    BE_TIEMPO = BE_TIEMPO_SALIDA;
    BE_RELOJ = setTimeout(reloj, MILISEGUNDO);
    execute("Login.actualizar", function(){});
}

function reloj() {
   BE_TIEMPO--; 
   BE_RELOJ = setTimeout(reloj, MILISEGUNDO);
   $("#reloj").html(BE_TIEMPO);
   if (BE_TIEMPO == 30) {
       relojMensaje();
   }
   if (BE_TIEMPO<=0) {
       relojSalida();
   }
}

function relojMensaje() {
    bootbox.confirm(
    {
        message: "Tu sesi&oacute;n est&aacute; cerca de finalizar. &iquest;Desea renovarla?",
        buttons: {
            confirm: {
                label: 'Si',
                className: 'btn-primary'
            },
            cancel: {
                label: 'No',
                className: 'btn-default'
            },
        },
        callback: procesarRepuestaRelojSalida
    });
    
}

function relojSalida() {
    bootbox.hideAll();
    window.location.href= "/ServiciosAlPersonal/?a=logout";
}

function procesarRepuestaRelojSalida(resultado) {
    if (resultado) {
       comenzarRelojSalida(); 
    } else {
        relojSalida();
    }
}
/*
function cookie(field, value) {
    if (typeof(value) == "undefined") {
        return $.cookie(field);
    } else  {
        $.cookie(field, value);
    } 
}

 function setFonts() {
    //FUENTE
    var fuente = cookie("fuente");    
    if (fuente) {
        cambiarFuente(fuente);
    }     
    setTimeout(setFonts, 100);
};
*/

 function setsize() {
     /*
    //FUENTE
    var fuente = cookie("fuente");    
    if (fuente) {
        cambiarFuente(fuente);
    }     
    */    
    if ($("div.content-wrapper").position().left < 60) {
        var x = 60;
        var y = $("div.content-wrapper").position().top;
        $("div.content-wrapper").offset({ top: y, left: x });
        setTimeout(setsize, 100);
    }    
}

function debug(label, data) {                    
    /*
    if (data) {
        console.log(label, serialize(data));
    } else {
        console.log(label);                 
    }
    */
}

function message(message, options) {    
    if (message) {
        var showInConsole      = true;
        var showInMessagePanel = true;
        var showInMessageBox   = false;
        var showInTooltips     = false;
        var target = isNull(options) || isNull(options.target) ? null : options.target;

        if (options != null)  {
           showInConsole      = isNull(options.showInConsole) ? showInConsole : options.showInConsole;
           showInMessagePanel = isNull(options.showInMessagePanel) ? showInMessagePanel : options.showInMessagePanel;
           showInMessageBox   = isNull(options.showInMessageBox) ? showInMessageBox : options.showInMessageBox;
           showInTooltips     = isNull(options.showInTooltips) ? showInTooltips : options.showInTooltips;
        }

        if (showInConsole) {
            debug(message);        
        }
        if (showInMessagePanel && message) {
            $("#message").html(message);
            $("#message").slideDown();        
        }
        if (showInMessageBox) {
            if (options && options.callback) {
                bootbox.alert({message:message, callback:options.callback});            
            } else {
                bootbox.alert(message);            
            }
        }
        if (showInTooltips && target) {        
            $(target).popover({content:message, html: true});
            $(target).popover('show');
            setTimeout(function() { $(target).popover('hide'); }, 1000);
        }      
        if (!showInMessageBox && options && options.callback) {
            options.callback();
        }
    }
}

function message_not_found(options) {
    message(BE_ERROR, options);
}

function message_error(options) {
    message(BE_NO_DATA_FOUND, options);
}

function message_success(options) {
    message(BE_SUCCESS, options);
}

function call(module) {
    window.location.href = "?a=" + module;
}

function clear_message() {
    $("#message").slideUp({complete: function() {  $("#message").html("");}});
}

function execute(action, callback, data) {
    data = isNull(data) ? {} : data
    $.extend(data, {a:action});  
    /************************/
    debug("SEND", data);
    /************************/
    $.ajax({ 
        type: 'POST', 
        url: BE_DISPATCHER_URL, 
        data: data,
        complete: function(data) { 
            /************************/
            debug("RECIEVE", data);            
            /************************/
            try {                
                var txt = data.responseText.split("\n").join("").split("\r").join("");
                var response = txt != "" ? $.parseJSON(txt) : {};
                callback(response);
            } catch (e) {
                message(e);
            }
        } 
    }); 
}

function run(url, callback, data) {
    data = isNull(data) ? {} : data
    $.extend(data, {a:action});  
    /************************/
    debug("SEND", data);
    /************************/
    $.ajax({ 
        type: 'POST', 
        url: BE_DISPATCHER_URL, 
        data: data,
        complete: function(data) { 
            /************************/
            debug("RECIEVE", data);            
            /************************/
            try {
                var response = data.responseText != "" ? $.parseJSON(data.responseText) : {};
                callback(response);
            } catch (e) {
                message(e);
            }
        } 
    }); 
}

function showLoading() {
    var html = "<img src='img/ajax-loader.gif'>";
    $.featherlight(html,{closeIcon: "", closeOnClick: false, closeOnEsc: false});
    var current = $.featherlight.current();
    current.open();
}

function hideLoading() {
    var current = $.featherlight.current();
    current.close();
}

function executeWithFile(callback, form) {    
    var formdata = false;
    if (window.FormData) {
        formdata = new FormData();
        form = $(form);
        var inputs = form.find("input");       
        inputs.each(function( index ) {
            var obj = $(this);
            if (obj.prop("type") != "file") {
                formdata.append(obj.attr("name"), obj.val());
            } else {
                formdata.append(obj.attr("name"), obj[0].files[0]);  
            }
        });               
        data = formdata;
    } else {
        data = form.serialize();
    }    
    /************************/
    debug("SEND WITH FILE", data);
    /************************/
    $.ajax({ 
        type:        'POST', 
        processData: false,
        contentType: false,
        url: BE_DISPATCHER_URL, 
        data: data,
        complete: function(data) { 
            /************************/
            debug("RECIEVE WITH FILE", data);            
            /************************/
            try {
                var response = data.responseText != "" ? $.parseJSON(data.responseText) : {};
                callback(response);
            } catch (e) {
                message(e);
            }
        }         
    }); 
}

function setTitle(title) {    
    $("h2").html(title);
}

function converToNumber(str) {
    return parseFloat(str);
    //return new Number(str);
}

function converToLocaleNumber(str) {
    str = typeof(str) == 'undefined' || str == null ? "" : str;
    str = str.split(".").join("");
    str = str.split(",").join(".");
    //return new Number(str);
    str = str ? str : 0;
    return parseFloat(str);
}

function numberFormat(number) {
    number = typeof(number) == "undefined"  || number == null ? 0 : number;
    number = typeof(number) == "string" ? converToNumber(number) : number;
    var numberFormatOptions = {style: "decimal",  useGrouping: true, minimumFractionDigits: 2, maximumFractionDigits: 2};
    var response = number.toLocaleString("es-ES", numberFormatOptions);
    response = response == "NaN" ? "0,00" : response;
    return response;
}

function currencyFormat(number) {
    number = typeof(number) == "string" ? converToNumber(number) : number;    
    var currencyFormatOptions = {style: "currency", currency: "VEF", useGrouping: true, currencyDisplay: "code" };            
    return number.toLocaleString("es-ES", currencyFormatOptions);    
}

function dateFormat(date) { 
    if (!date || date == "") return "";
    var d = "";
    var m = "";
    var y = "";     
    if (typeof(date) == "string") {
        date = date.split(" ")[0];       
        if (date.indexOf("-") >-1) {
            date = date.split("-");
            d = date[2];
            m = date[1];
            y = date[0];
        } else if (date.indexOf("/") >-1) {
            date = date.split("/");
            d = date[0];
            m = date[1];
            y = date[2];            
        }
        d = parseInt(d);
        m = parseInt(m);
        y = parseInt(y);
        d = d<10 ? ("0" + d) : d;
        m = m<10 ? ("0" + m) : m;        
        return d+"/"+m+"/"+y;
    } else {
        d = date.getDate();
        m = date.getMonth()+1;
        d = d<10 ? ("0" + d) : d;
        m = m<10 ? ("0" + m) : m;
        y = date.getFullYear();
        return d+"/"+m+"/"+y;
    }
    return "";
}

function getToday() {
    return dateFormat(CURRENT_DATE);
}

function clearNull(value) {
    return isNull(value) ? "" : value;
}

function populate(frm, data) {   
    $.each(data, function(key, value){  
    var $ctrl = $('[name='+key+']', frm);  
    switch($ctrl.attr("type"))  
    {  
        case "text" :   
        case "hidden":  
        $ctrl.val(value);   
        break;   
        case "radio" : case "checkbox":   
        $ctrl.each(function(){
           if($(this).attr('value') == value) {  $(this).attr("checked",value); } });   
        break;  
        default:
        $ctrl.val(value); 
    }  
    });  
}

function buildMenu(menu, data) {
    var item;
    for(var i in data) {
        item = data[i];
        menu.append("<div><a href='?a="+item.accion+"'>"+deco+" " + item.titulo + "</a></div>");                
    }    
}

function getOption(options, option) {
    return isNull(options) || isNull(option) ? null : options[option];
}

function loadEntities(options) {
    if (typeof(options) == 'undefined') {
                options = {entidad: ENTITY};
    } else {
        options.entidad = ENTITY
    }
    execute("Entidad.obtener", processGetEntities, options);
}

function processGetEntities(data) {
    var options = $.extend({}, {entidad: ENTITY , editable: true, fieldId: FIELD_ID}, TABLE_OPTIONS);
    var table = createTable(data, options);             
    $(".listdata").html(table);          
    $(".table-filter").prop("placeholder", "Buscar");
}

function createTable(data, options) {       
    var buscador = "<div class='table-filter-wrapper'><input style='float: left; margin: 0;' type='text' class='form-control table-filter' name='table-filter' onchange='filter(this)' placeholer='Buscar' value='"+FILTER+"'><button class='btn btn-primary table-search' style='float: left; margin: 0px 0px 10px 10px;'><span class='glyphicon glyphicon-search'></span></button><button class='btn btn-primary table-show-all' style='float: left; margin: 0px 0px 10px 10px;' onclick='showAll()'><span class='glyphicon glyphicon-list-alt'></span></button></div>";
    var dataNotFound = "<br/><span>" + BE_NO_DATA_FOUND + "</span>";
    if (!isNull(data) || data == "" || data == "[]") {
        var item = data[0];                
        if (!isNull(item)) {   
            var field      = "";
            var value      = "";
            var table      = "";       
            var id         = "";
            var rowdataid  = "";
            var hasColumns = getOption(options, "columns") != null;
            var columns    = hasColumns ? options.columns : item;
            var editable   = getOption(options, "editable");
            var fieldId    = getOption(options, "fieldId");
            var exported   = getOption(options, "export");
            var entity     = clearNull(getOption(options, "entidad"));
            
            table += buscador;
            
            exported = typeof(exported) != "undefined"  ? true : exported;
           
            if (exported) {  
                
                table += "<div class='table-export'>";
                table += "&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-primary' onclick='exportData(this)' >Exportar</button>";
                table += "</div>";
            }

            table += "<div class='table-wrapper' style='height: 37px; overflow-y: scroll; overflow-x: hidden; margin: 0; padding: 0; clear: both;'>";
            table += "<table class='datatable table table-header' placeholder='"+entity+"' style='margin: 0; padding: 0;'>";                    
            table += "<tr>";
            
            if (editable) {                
                table += "<td class='cell-border' width='20'>";
                table += "<span><span class='glyphicon glyphicon-exclamation-sign text-default'></span></span>";  
                table += "</td>";     
            }                        
            for(var f in columns) {
                field = hasColumns ? columns[f].title : f;
                value = hasColumns ? columns[f].name : f;
                if (hasColumns && typeof(columns[f]["select"]) != "undefined") {
                    var action = columns[f]["select"];
                    
                    prepareSelect(action, value);                    
                }
                if (field != fieldId) {
                    table += "<td class='cell-border' placeholder='" + value + "'>";               
                    table += "<input onclick='showTableBy(this)' value='"+field+"' placeholder='" + value + "' readonly='readonly' />";
                    table += "</td>";                    
                }
            }                            
            table += "</tr>";
            table += "</table>";
            table += "</div>";
            table += "<div class='datatable-wrapper' style='height: 400px; overflow-y: scroll; overflow-x: hidden; margin: 0; padding: 0;'>";
            table += "<table class='datatable table' placeholder='"+entity+"' style='margin: 0; padding: 0;'>";            
            
            for(var i in data) {
                item = data[i];
                id = fieldId ? item[fieldId] : "";     
                
                if (!isNull(id) && id != "") {
                    rowdataid = id.split(" ").join("-");
                    table += "<tr class='rowdata"+rowdataid+"' placeholder='" + id + "'>";
                    if (editable) {
                        table += "<td class='cell-border' width='20'>";  
                        table += "<span class='pointer' onclick='deleteEntity(this)'><span class='glyphicon glyphicon-remove text-danger'></span></span>";  
                        table += "</td>";                
                    }                
                    for(var j in columns) {                        
                        field = hasColumns ? columns[j].name : field;
                        if (field != fieldId) {
                            value = item[field];
                            table += "<td class='cell-border'>";                                
                            if (hasColumns && typeof(columns[j]) != "undefined" && typeof(columns[j]["select"]) != "undefined") {
                                table += "<select name='"+field+"' itemprop='"+value+"' class='select-"+field.toLowerCase()+"' onchange='saveEntity(this)' " +  (editable ?  "" : "readonly='readonly' ") +" ></select>";
                            } else {                            
                                table += "<input name='"+field+"' value='"+value+"' placeholder='"+value+"' onkeydown='checkKey()' onchange='saveEntity(this)' " +  (editable ?  "" : "readonly='readonly' ") +" />";                    
                            }
                            table += "</td>";
                        }
                    }   
                }
                table += "</tr>";
            }
            
            if (editable) {
                table += "<tr>";   
                table += "<td class='cell-border'>"; 
                table += "<span class='pointer' onclick='saveEntity(this, true)'><span class='glyphicon glyphicon-plus text-primary'></span></span>";
                table += "</td>";                                
                for(var j in columns) {
                    field = hasColumns ? columns[j].name : field;
                    if (field != fieldId) {
                        table += "<td class='cell-border' >";
                        if (hasColumns && typeof(columns[j]) != "undefined" && typeof(columns[j]["select"]) != "undefined") {
                            table += "<select name='"+field+"' itemprop='"+value+"' class='select-"+field.toLowerCase()+"' onchange='saveEntity(this)' " +  (editable ?  "" : "readonly='readonly' ") +" ></select>";
                        } else {                            
                            table += "<input name='"+field+"' value='' placeholder='' onkeydown='checkKey()' onchange='saveEntity(this)' />";  
                        }
                        table += "</td>";
                    }
                }            
                table += "</tr>";
            }
            table += "</table>";             
            table += "</div>";  
            return table;
        }
        return buscador + dataNotFound;        
    } else {
        return buscador + dataNotFound;
    }
}

function prepareSelect(action, field) {
    execute(action, function(response) { fillOptions(".select-"+field.toLowerCase(), response.data.items); });    
}

function exportData(obj) {
    var SEPARADOR  = ";";
    var NUEVALINEA = "\n";
    var div = $(obj).parents().eq(0);
    var data = "";
    var sep = "";
    var header = div.siblings("div.table-wrapper");    
    var filename = header.find("table").eq(0).attr("placeholder");
    filename = !filename ? "datos" + currentTimestamp() : filename;
    var inputs  = header.find("input");
    inputs.each(function() {       
        data += sep + $(this).val();
        sep = SEPARADOR;       
    });
    data += NUEVALINEA;
    sep = "";      
    var rows  = div.siblings("div.datatable-wrapper").find("tr");    
    rows.each(function() {
        var inputs  = $(this).find("input");
        inputs.each(function() {    
            data += sep + $(this).val();
            sep = SEPARADOR;       
        });
        data += NUEVALINEA;
        sep = "";        
    });    
    data = encodeURIComponent(data);
     
    $('<a></a>')
        .attr('id','downloadFile')
        .attr('href','data:text/csv;charset=utf8,' + data)
        .attr('download',filename+'.csv')
        .appendTo('body');

    $('#downloadFile').ready(function() {
        $('#downloadFile').get(0).click();
    });
}

function checkKey(e) {    
    var code = event.which || event.keyCode;
    if (code == 27) {
        var obj = $(event.target);
        obj.val(obj.attr("placeholder"));        
        $("input[name=table-filter]").focus();
    }    
}

function getEntityFrom(obj) {
    var row = $(obj).parents("tr").first();
    var id = row.attr("placeholder");    
    var fields = row.find("input");
    var entity = {};
    entity["entidad"] = ENTITY;
    entity["id"] = isNull(id) ? "" : id;
    entity["isNew"] = isNull(id) ? true : false;
    for(var i=0; i<fields.length; i++) {
        var field = $(fields[i]);
        if (!isNull(field)) {
            var name = field.attr("name");
            var value = field.val();        
            entity[name] = value;
        }
    }
    var fields = row.find("select");  
    for(var i=0; i<fields.length; i++) {
        var field = $(fields[i]);
        if (!isNull(field)) {
            var name = field.attr("name");
            var value = field.val();        
            entity[name] = value;
        }
    }    
    return entity;    
}

function showAll() {
    $("input[name=table-filter]").val("");
    FILTER = "";
    var entity = getEntityFrom($(".listdata td"));
    $.extend(entity, {order: ORDER_BY, filter: FILTER});        
    execute("Entidad.obtener", processGetEntities, entity);  
}

function filter(obj) {
    FILTER = $(obj).val();    
    var entity = getEntityFrom($(".listdata td"));
    $.extend(entity, {order: ORDER_BY, filter: FILTER});        
    execute("Entidad.obtener", processGetEntities, entity);  
}

function showTableBy(obj) {
    //ORDER_BY = $(obj).html();
    ORDER_BY = $(obj).attr("placeholder");    
    var entity = getEntityFrom($(".listdata td"));
    $.extend(entity, {order: ORDER_BY, filter: FILTER});        
    execute("Entidad.obtener", processGetEntities, entity);
}

function deleteEntity(obj) {
    bootbox.confirm("&iquest;Est&aacute; seguro que desea borrar este registro?", function(result) {
        if (result) {
            var entity = getEntityFrom(obj);                
            execute("Entidad.borrar", processDeleteEntity, entity);
        }
    });     
}

function processDeleteEntity(data) {
    if (!isNull(data)) {
        if (data.status == "true") {
            var id = data.id;
            var rowdataid = id.split(" ").join("-");
            $(".rowdata"+rowdataid).remove();
        } else {
            message(data.message, {showInMessageBox: true});
        }
    } else {
        message_error({showInMessageBox: true});
    }
}

function saveEntity(obj, saveNew) {
    var entity = getEntityFrom(obj);
    var id = entity.id;
    var processor = id && !entity.isNew ? processUpdateEntity : processAddEntity;
    if (id || saveNew) {
        execute("Entidad.guardar", processor, entity);    
    }
}

function processUpdateEntity(data) {
    if (!isNull(data)) {
        if (data.status == "true") {            
            var id = data.id;
            var rowdataid = id.split(" ").join("-");
            var table = $(".datatable");            
            var row  = table.find("tr.rowdata" + rowdataid).first();
            var cell = row.find("td").first();   
            var target = cell.find("span").first();
            message_success({showInMessagePanel: false, showInTooltips: true, target: target});            
        } else {
            message(data.message, {showInMessageBox: true});
        }
    } else {
        message_error({showInMessageBox: true});
    }
}

function processAddEntity(data) {
    if (!isNull(data)) {
        if (data.status == "true") {
            var table = $(".datatable");
            var row  = table.find("tr").last();
            table.append("<tr>"+row.html()+"</tr>");
            var id = data.id;
            var rowdataid = id.split(" ").join("-");
            row.attr("placeholder", id);
            row.addClass("rowdata" + rowdataid);
            var cell = row.find("td").first();            
            cell.html("<span class='pointer' onclick='deleteEntity(this)'><span class='glyphicon glyphicon-remove text-danger'></span></span>");            
            row.find("input[name=id]").val(id);    
            var target = cell.find("span").first();
            message_success({showInMessagePanel: false, showInTooltips: true, target: target});
        } else {
            message(data.message, {showInMessageBox: true});
        }
    } else {
        message_error({showInMessageBox: true});
    }
}

function renderBatchLoad(entity) {
    var div = $(".batchload");
    createBatchLoadWidget(div, entity);
}

function createBatchLoadWidget(obj, entity) {
    if (obj && entity) {
        obj = $(obj);
        var id = obj.attr("id");
        id = isNull(id) ? Math.floor(Math.random()*100) : id;
        id += "__file";
        obj.html("<form enctype='multipart/form-data' onsubmit='return false;' id='frm"+id+"'><input type='hidden' name='a' value='Entidad.cargarMasivo'><input type='hidden' name='entidad' value='"+entity+"'><input type='file' id='"+id+"' name='file' class='hidden-control' onchange='batchLoad(this)'><button class='btn btn-warning' onclick='selectBatchLoadFile(this)' id='btn"+id+"'>Cargar</button></form>");
    }
}

function selectBatchLoadFile(obj) {
    var id = $(obj).attr("id");
    id = id.substring(3);
    var file = $("#" + id);
    file.click();    
}

function batchLoad(obj) {    
    if (obj.files && obj.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var id = "frm" + $(obj).prop("id");
            var form = null;
            var formdata = false;
            for (var i in document.forms) {
                var f = document.forms[i];                
                if (f["id"] == id) {
                    form = f;
                }
            }
            if (form) {            
                if (window.FormData){
                    formdata = new FormData(form);
                }                  
                executeWithFile(processBatchUpload, form);                       
            }
        }
        reader.readAsDataURL(obj.files[0]);
    }        
}

function redirect(url) {
    location.href = url;
}

function reload() {
    location.reload();
}

function processBatchUpload(data) {
    if (data.status == "false") {
        message(data.message);
    } else {
        reload();
    }
}

function fillOptions(select, items) {       
    var options = "";
    for (var i in items) {
        var option = items[i];
        options += "<option value='" + option.value + "'>" + option.name + "</option>";
    }
    $(select).html(options);                    
    $(select).each(function(){
        var select = $(this);
        var value = select.attr("itemprop");
        select.find("option").each(function(){
            if (this.value == value) {                
                $(this).prop("selected", true);
            }
        });
    });    
}

function isNull(obj) {
    return typeof obj == "undefined" || obj == null;
}

function serialize(data, showAll) {    
    var result = data;    
    if (typeof(data) == "object") {
        result = "";
        var sep = "";
        var value = "";
        for(var i in data) {
            if (typeof(data[i]) == "function") {
                if (showAll) value = "function()";
            } else {
                if (typeof(data) == "object") {
                    value =  serialize(data[i]);    
                } else {
                    value =  data[i];    
                }
            }        
            result += sep + i + ":" + value;
            sep = " , ";
        }
    }
    return result;
}

function cambiarFuente(fuente) {
    $("body *").css("font-family", fuente);
    $("body *.glyphicon ").css("font-family", "GLYPHICONS Halflings");
    //cookie("fuente", fuente);
}

function cambiarFondo(fondo) {        
    $("body").css("background-image", "url('img/wallpapers/wallpaper"+fondo+".jpg')");
    //cookie("fondo", fondo);
}

function zoom(z) {
    $("body").animate({ 'zoom': z }, 400);
    //cookie("zoom", z);
}

function escapeHtmlEntities (text) {
    return text.replace(/[\u00A0-\u2666<>\&]/g, function(c) {
        return '&' + (entityTable[c.charCodeAt(0)] || '#' + c.charCodeAt(0)) + ';';
    });
};

function monthName(month) {
    month = parseInt(month)    
    var response = "";
    if (month ==  1) response = "Enero";
    if (month ==  2) response = "Febrero";
    if (month ==  3) response = "Marzo";
    if (month ==  4) response = "Abril";
    if (month ==  5) response = "Mayo";
    if (month ==  6) response = "Junio";
    if (month ==  7) response = "Julio";
    if (month ==  8) response = "Agosto";
    if (month ==  9) response = "Septiembre";
    if (month == 10) response = "Octubre";
    if (month == 11) response = "Noviembre";
    if (month == 12) response = "Diciembre";
    return response
}
function nacionalidad(naci) {
    naci = parseInt(naci)    
    var response = "";
    if (naci ==  1) response = "Venezolano";
    if (naci ==  2) response = "Extranjero";
    return response
}

function json(text) {
    return $.parseJSON(text);
}

function currentTimestamp() {
    return new Date().getTime(); 
}

function enmascararColaborador() {
    $('#colaborador').inputmask('Regex', { regex: "[EFCAPefcap]{0,1}[0-9]{1,5}" });
}

function numberToString(number) {
    return (number+"").split("\.").join("").split(",").join(".");
}

function numeroNegativo(number) {
    return (number=number*-1)
}

function capitalize(text){
    return text.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});    
}

var entityTable = {
    34 : 'quot', 38 : 'amp', 39 : 'apos', 60 : 'lt', 62 : 'gt', 160 : 'nbsp', 161 : 'iexcl', 162 : 'cent', 163 : 'pound', 164 : 'curren', 165 : 'yen', 166 : 'brvbar', 167 : 'sect', 168 : 'uml', 169 : 'copy', 170 : 'ordf', 171 : 'laquo', 172 : 'not', 173 : 'shy', 174 : 'reg', 175 : 'macr', 176 : 'deg', 177 : 'plusmn', 178 : 'sup2', 179 : 'sup3', 180 : 'acute', 181 : 'micro', 182 : 'para', 183 : 'middot', 184 : 'cedil', 185 : 'sup1', 186 : 'ordm', 187 : 'raquo', 188 : 'frac14', 189 : 'frac12', 190 : 'frac34', 191 : 'iquest', 192 : 'Agrave', 193 : 'Aacute', 194 : 'Acirc', 195 : 'Atilde', 196 : 'Auml', 197 : 'Aring', 198 : 'AElig', 199 : 'Ccedil', 200 : 'Egrave', 201 : 'Eacute', 202 : 'Ecirc', 203 : 'Euml', 204 : 'Igrave', 205 : 'Iacute', 206 : 'Icirc', 207 : 'Iuml', 208 : 'ETH', 209 : 'Ntilde', 210 : 'Ograve', 211 : 'Oacute', 212 : 'Ocirc', 213 : 'Otilde', 214 : 'Ouml', 215 : 'times', 216 : 'Oslash', 217 : 'Ugrave', 218 : 'Uacute', 219 : 'Ucirc', 220 : 'Uuml', 221 : 'Yacute', 222 : 'THORN', 223 : 'szlig', 224 : 'agrave', 225 : 'aacute', 226 : 'acirc', 227 : 'atilde', 228 : 'auml', 229 : 'aring', 230 : 'aelig', 231 : 'ccedil', 232 : 'egrave', 233 : 'eacute', 234 : 'ecirc', 235 : 'euml', 236 : 'igrave', 237 : 'iacute', 238 : 'icirc', 239 : 'iuml', 240 : 'eth', 241 : 'ntilde', 242 : 'ograve', 243 : 'oacute', 244 : 'ocirc', 245 : 'otilde', 246 : 'ouml', 247 : 'divide', 248 : 'oslash', 249 : 'ugrave', 250 : 'uacute', 251 : 'ucirc', 252 : 'uuml', 253 : 'yacute', 254 : 'thorn', 255 : 'yuml', 402 : 'fnof', 913 : 'Alpha', 914 : 'Beta', 915 : 'Gamma', 916 : 'Delta', 917 : 'Epsilon', 918 : 'Zeta', 919 : 'Eta', 920 : 'Theta', 921 : 'Iota', 922 : 'Kappa', 923 : 'Lambda', 924 : 'Mu', 925 : 'Nu', 926 : 'Xi', 927 : 'Omicron', 928 : 'Pi', 929 : 'Rho', 931 : 'Sigma', 932 : 'Tau', 933 : 'Upsilon', 934 : 'Phi', 935 : 'Chi', 936 : 'Psi', 937 : 'Omega', 945 : 'alpha', 946 : 'beta', 947 : 'gamma', 948 : 'delta', 949 : 'epsilon', 950 : 'zeta', 951 : 'eta', 952 : 'theta', 953 : 'iota', 954 : 'kappa', 955 : 'lambda', 956 : 'mu', 957 : 'nu', 958 : 'xi', 959 : 'omicron', 960 : 'pi', 961 : 'rho', 962 : 'sigmaf', 963 : 'sigma', 964 : 'tau', 965 : 'upsilon', 966 : 'phi', 967 : 'chi', 968 : 'psi', 969 : 'omega', 977 : 'thetasym', 978 : 'upsih', 982 : 'piv', 8226 : 'bull', 8230 : 'hellip', 8242 : 'prime', 8243 : 'Prime', 8254 : 'oline', 8260 : 'frasl', 8472 : 'weierp', 8465 : 'image', 8476 : 'real', 8482 : 'trade', 8501 : 'alefsym', 8592 : 'larr', 8593 : 'uarr', 8594 : 'rarr', 8595 : 'darr', 8596 : 'harr', 8629 : 'crarr', 8656 : 'lArr', 8657 : 'uArr', 8658 : 'rArr', 8659 : 'dArr', 8660 : 'hArr', 8704 : 'forall', 8706 : 'part', 8707 : 'exist', 8709 : 'empty', 8711 : 'nabla', 8712 : 'isin', 8713 : 'notin', 8715 : 'ni', 8719 : 'prod', 8721 : 'sum', 8722 : 'minus', 8727 : 'lowast', 8730 : 'radic', 8733 : 'prop', 8734 : 'infin', 8736 : 'ang', 8743 : 'and', 8744 : 'or', 8745 : 'cap', 8746 : 'cup', 8747 : 'int', 8756 : 'there4', 8764 : 'sim', 8773 : 'cong', 8776 : 'asymp', 8800 : 'ne', 8801 : 'equiv', 8804 : 'le', 8805 : 'ge', 8834 : 'sub', 8835 : 'sup', 8836 : 'nsub', 8838 : 'sube', 8839 : 'supe', 8853 : 'oplus', 8855 : 'otimes', 8869 : 'perp', 8901 : 'sdot', 8968 : 'lceil', 8969 : 'rceil', 8970 : 'lfloor', 8971 : 'rfloor', 9001 : 'lang', 9002 : 'rang', 9674 : 'loz', 9824 : 'spades', 9827 : 'clubs', 9829 : 'hearts', 9830 : 'diams', 338 : 'OElig', 339 : 'oelig', 352 : 'Scaron', 353 : 'scaron', 376 : 'Yuml', 710 : 'circ', 732 : 'tilde', 8194 : 'ensp', 8195 : 'emsp', 8201 : 'thinsp', 8204 : 'zwnj', 8205 : 'zwj', 8206 : 'lrm', 8207 : 'rlm', 8211 : 'ndash', 8212 : 'mdash', 8216 : 'lsquo', 8217 : 'rsquo', 8218 : 'sbquo', 8220 : 'ldquo', 8221 : 'rdquo', 8222 : 'bdquo', 8224 : 'dagger', 8225 : 'Dagger', 8240 : 'permil', 8249 : 'lsaquo', 8250 : 'rsaquo', 8364 : 'euro'
};

function executeReturn(action, data) {
    data = isNull(data) ? {} : data
    $.extend(data, {a:action});  
    /************************/
    debug("SEND", data);
    /************************/
    $.ajax({ 
        type: 'POST', 
        url: BE_DISPATCHER_URL, 
        data: data,
        complete: function(data) { 
            /************************/
            debug("RECIEVE", data);            
            /************************/
            try {
                var response = data.responseText != "" ? $.parseJSON(data.responseText) : {};
                alert(JSON.stringify(response));
                return response;
                //callback(response);
            } catch (e) {
                message(e);
            }
            return response;
        } 
    }); 
}