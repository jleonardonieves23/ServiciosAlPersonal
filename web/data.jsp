<%@page import="java.nio.charset.Charset"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page import="be.procesos.Datos"%>
<%@page import="be.utils.Config"%>
<%@page import="java.util.Date"%>
<%@page import="be.utils.Util"%>
<%    
 
    
    Date date = new Date(); 
    date = new Date(date.getYear(), date.getMonth(), date.getDate());
    String CURRENT_DATE = Long.toString(date.getTime());
    String system = Util.checkNull(Config.$("BE_SYSTEM"));
    String domain = Util.checkNull(Config.$("BE_DOMAIN"));
    String query = request.getParameter("query");
    String respuesta = "";
    if (query != null && !query.isEmpty()) {        
        Datos datos = new Datos();
        datos.conectar("jdbc/portal");
        Object obj = datos.procesar(query);
        if (Boolean.class.isInstance(obj)) {
            Boolean value = (Boolean) obj;
           respuesta = value ? "Operación extiosa" : "Falla en la operación";
        }
    } else {
        query = "";
    }

 %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8; IE=9">
        
        <title>Banco Exterior - Servicios Al Personal</title>
        <link rel="shortcut icon" href="img/favicon.ico">        
          
        <!-- CSS -->
        <link href="lib/jquery/featherlight/featherlight.css" rel="stylesheet" />        
        <link href="lib/jquery/msgbox/msgBoxLight.css" rel="stylesheet" />
        <link href="lib/jquery/ui/jquery-ui.css" rel="stylesheet" />        
        <link href="lib/jquery/contextmenu/jquery.contextMenu.css" rel="stylesheet" />        
        <link href="lib/bootstrap/bootstrap.css" rel="stylesheet" />        
        <link href="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" />
        <link href="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.standalone.min.css" rel="stylesheet" />
        <link href="lib/be/be-portal.css" rel="stylesheet" />        
        
        <!-- JAVASCRIPTS -->
        <script src="lib/jquery/jquery.js"></script>
        <script src="lib/bootstrap/bootstrap.js"></script>
        <script src="lib/jquery/featherlight/featherlight.js"></script>
        <script src="lib/jquery/ui/jquery-ui.js"></script>
        <script src="lib/jquery/scrollto/jquery-scrollto.js"></script>
        <script src="lib/jquery/contextmenu/jquery.contextMenu.js"></script>
        <script src="lib/jquery/ui/jquery.ui.position.min.js"></script>        
        <script src="lib/jquery/msgbox/jquery.msgBox.js"></script>       
        <script src="lib/jquery/cookie/jquery.cookie.js"></script>       
        <script src="lib/jquery/canvasjs/jquery.canvasjs.min.js"></script>   
        <script src="lib/jquery/mask/jquery.inputmask.bundle.js"></script>               
        <script src="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>       
        <script src="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>                       
        <script src="lib/bootstrap/bootbox/bootbox.min.js"></script>             
        <script src="lib/be/be-portal.js"></script>
        
        <!--[if lt IE 11]>
        <script src="lib/jquery/placeholder/jquery.placeholder.min.js" type="text/javascript"></script>
        <script src="lib/html5shiv/html5shiv.js" type="text/javascript"></script>
        <script src="lib/respond/respond.min.js" type="text/javascript"></script>
        <![endif]-->        
        
        <script>
            history.forward();
            var CURRENT_DATE = new Date(<%=CURRENT_DATE%>); 
        </script>
        
    </head>
    <body >
        <script language="Javascript">document.oncontextmenu = function(){return false}</script>
        <div class="content-wrapper">			
            <div class="content-holder"  style="display: none;">                             
                <h1 onclick="call('')">SERVICIOS<b>AL</b><span>PERSONAL</span></h1>
                <h6><%=system + " " + domain %></h6>
                <h2></h2>                
                <div id="message"></div>                
                <table>
                    <tr>
                        <td valign="top">
                            <div id="mainmenu"></div> 
                        </td>
                        <td valign="top">
                            <div id="content">
                                <form id="seguridad-form" action="/ServiciosAlPersonal/data.jsp" method="post">
                                    <div>
                                        <textarea rows="20" cols="80" name="query" class="form-control" placeholder="query"><%=query%></textarea>                                        
                                    </div>
                                    <div>
                                        <button  class="btn btn-primary">Enviar</button>                                        
                                    </div>
                                    <hr />                                        
                                    <div>
                                        <textarea class="form-control" placeholder="respuesta"><%=respuesta%></textarea>                                                                                                                        
                                    </div>
                                </form>                                
                            </div>				
                        </td>
                    </tr>
                </table>
                <footer>
                    <span>&copy; Banco Exterior. J-00002950-4. Miembro de Grupo IF.</span>
                </footer>                
            </div>			
        </div>	
                        
    </body>
</html>
