<%@page import="be.utils.Config"%>
<%@page import="java.util.Date"%>
<%@page import="be.utils.Util"%>
<%@page import="java.io.File"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="java.util.Enumeration"%>
<!DOCTYPE html>
<%    
    if ((Util.getCurrentTimestamp() - session.getLastAccessedTime()) > Util.getSessionTimeout()) {
        session.setAttribute("CURRENT_USER", null);
    }
            
    String param        = Util.checkParam(request.getParameter("a"));
    String[] parts      = param.split("/");
    String module       = parts[0];
  
    String searchModule = "mod/search/search.jsp";
    String hidden_class = "";
    String home_class   = "";
    String module_class = "";
    
    if (module != null && module.equals("logout")) {
        Util.logout(request);
        //Usuario usuario2 = (Usuario) Util.obtenerUsuarioActual(request);
    }    
    
    Usuario usuario      = (Usuario) Util.obtenerUsuarioActual(request);
    String moduleContent = usuario == null ? "mod/login/login.jsp" : "mod/home/home.jsp";
    
    if (module != null && !module.equals("")) {
        module_class = module;
        if (usuario != null && usuario.tieneAcceso(param)) {
            String submodules = param + "/" + parts[parts.length-1];
            String modulePath = "mod/" + submodules + ".jsp";
            String path = Util.ruta(request, modulePath);
            File file = new File(path);
            moduleContent = file.isFile() ? modulePath : moduleContent;                 
        } else {
            hidden_class = "hidden";
            home_class   = "home";
            module_class = "login";            
        }
    } else {
        module_class = "login";
    }
    
    Date date = new Date(); 
    date = new Date(date.getYear(), date.getMonth(), date.getDate());
    String CURRENT_DATE = Long.toString(date.getTime());
    String system = Util.checkNull(Config.$("BE_SYSTEM"));
    String domain = Util.checkNull(Config.$("BE_DOMAIN"));

 %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8; IE=9">
        
        <title>Banco Exterior - Servicios Al Personal</title>
        <link rel="shortcut icon" href="img/favicon.ico">        
          
        <!-- CSS -->
        <link href="lib/jquery/featherlight/featherlight.css" rel="stylesheet" />        
        <link href="lib/jquery/msgbox/msgBoxLight.css" rel="stylesheet" />
        <link href="lib/jquery/ui/jquery-ui.css" rel="stylesheet" /> 
        <link href="lib/jquery/contextmenu/jquery.contextMenu.css" rel="stylesheet" /> 
        <link href="lib/bootstrap/bootstrap.css" rel="stylesheet" />        
        <link href="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" />
        <link href="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.standalone.min.css" rel="stylesheet" />        
        <link href="lib/be/be-portal.css" rel="stylesheet" />        
                
        <!-- JAVASCRIPTS -->
        <script src="lib/jquery/jquery.js"></script>
        <script src="lib/bootstrap/bootstrap.js"></script>
        <script src="lib/jquery/featherlight/featherlight.js"></script>
        <script src="lib/jquery/ui/jquery-ui.js"></script>
        <script src="lib/jquery/scrollto/jquery-scrollto.js"></script>
        <!-- <script src="lib/jquery/contextmenu/jquery.contextMenu.js"></script> -->
        <script src="lib/jquery/ui/jquery.ui.position.min.js"></script>        
        <script src="lib/jquery/msgbox/jquery.msgBox.js"></script>       
        <!-- <script src="lib/jquery/cookie/jquery.cookie.js"></script>        -->
        <script src="lib/jquery/canvasjs/jquery.canvasjs.min.js"></script>   
        <script src="lib/jquery/mask/jquery.inputmask.bundle.js"></script>               
        <script src="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>       
        <script src="lib/bootstrap/bootstrap-datepicker/bootstrap-datepicker.es.min.js"></script>                       
        <script src="lib/bootstrap/bootbox/bootbox.min.js"></script>             
        <script src="lib/be/be-portal.js"></script>
        
        <!--[if lt IE 11]>
        <script src="lib/jquery/placeholder/jquery.placeholder.min.js" type="text/javascript"></script>
        <script src="lib/html5shiv/html5shiv.js" type="text/javascript"></script>
        <script src="lib/respond/respond.min.js" type="text/javascript"></script>
        <![endif]-->        
        
        <script>
            history.forward();
            var CURRENT_DATE = new Date(<%=CURRENT_DATE%>); 
        </script>
        
    </head>
    <body class="<%=module_class%>">
        <script language="Javascript">document.oncontextmenu = function(){return false}</script>
        <div class="content-wrapper">			
            <div class="content-holder"  style="display: none;">                             
                <h1 onclick="call('')">SERVICIOS<b>AL</b><span>PERSONAL</span></h1>
                <h6><%=system + " " + domain %></h6>
                <table>
                    <tr>
                        <td valign="top" class="<%=hidden_class%>"><div id="header"><jsp:include page="mod/header/header.jsp" /></div></td>
                        <td valign="top" align="left"><div id="search" class="<%=home_class%>"><jsp:include page="<%=searchModule%>" /></div></td>
                    </tr>
                </table>
                <h2></h2>                
                <div id="message"></div>                
                <table>
                    <tr>
                        <td valign="top">
                            <div id="mainmenu"><jsp:include page="mod/mainmenu/mainmenu.jsp" /></div> 
                        </td>
                        <td valign="top">
                            <div id="content"><jsp:include page="<%= moduleContent %>" />
                                
                            </div>				
                        </td>
                    </tr>
                </table>
                <footer>
                    <span>&copy; Banco Exterior. J-00002950-4. Miembro de Grupo IF.</span>
                </footer>                
            </div>			
        </div>	
                        
    </body>
</html>
