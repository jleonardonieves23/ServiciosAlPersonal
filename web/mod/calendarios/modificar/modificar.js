var ENTITY = "Dia";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [ 
        {title: 'Fecha', name: 'fecha'},
        {title: 'Descripci&oacute;n', name: 'descripcion'},
        {title: 'Calendario', name: 'calendario'}
        ] } ;
    
$(function() {
    setTitle("Administraci&oacute;n > D&iacute;as de Calendarios");
    loadEntities({order:"fecha"});
    renderBatchLoad(ENTITY);
});
