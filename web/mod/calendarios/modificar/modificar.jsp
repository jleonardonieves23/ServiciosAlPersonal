<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/calendarios/modificar/modificar.css" rel="stylesheet" />        
<script src="mod/calendarios/modificar/modificar.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">

    <h4>Listado</h4>
    
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>
    
    <div class="listdata"></div>
    
    <div class="batchload"></div>
    
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>    
</div>     

<%
    } //if (currentUser != null)
%>    
