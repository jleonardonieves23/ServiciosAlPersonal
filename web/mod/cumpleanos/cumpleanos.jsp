<link href="mod/cumpleanos/cumpleanos.css" rel="stylesheet" />        
<script src="mod/cumpleanos/cumpleanos.js"></script>

<script>
    var ALL_BIRTHDAYS = true;
    $(function() { setTitle("Cumplea&ntilde;os"); });
</script>

<div class="submenu menu-content">
     
    <div id="birthdays" class='birthday-content'><img id='loading-bithdays' src="img/ajax-loader.gif" style="width: 150px;"></div>        
    <div id="birthdays-days"></div>
</div>    
