var ALL_BIRTHDAYS = false;
var LAST = null;
var LIST = [];

$(function() {         
    if (ALL_BIRTHDAYS) {
        execute("Calendario.calendarios", processAllBirthdays, {period: 'mes', birthday: ALL_BIRTHDAYS ? 'all' :  'only'});
    } else {
        execute("Calendario.calendarios", processBirthdays, {period: 'hoy', birthday: 'only'});
    }    
});

function processBirthdays(data) { 
    showBirthdays(data, "today-birthdays", "Cumplea&ntilde;os de hoy");    
}

function processAllBirthdays(data) { 
    showBirthdays(data, "birthdays", ALL_BIRTHDAYS ? "Cumplea&ntilde;os" : "Cumplea&ntilde;os del mes");
    if (!ALL_BIRTHDAYS) $("#birthdays").append("<a href='?a=birthday' class='birthday-all'>ver todos</a>");
}

function showBirthdays(data, element, title) {
    var info;
    element = $("#"+element);
    $("#loading-bithdays").hide();
    if (!ALL_BIRTHDAYS) element.append("<h4>" + title + "</h4>");
    var ultimo = null;    
    var texto = "";
    var dia = null;
    var j = 0;
    if (data && data.length > 0) {
        for (var i in data) {
            info = data[i];
            if (ALL_BIRTHDAYS) {                     
                var fecha = info.fecha;            
                fecha = fecha.split("/");            
                var mes = monthName(parseInt(fecha[1]));                
                if (mes != ultimo) {
                    if (ultimo) texto += "</div>";
                    ultimo = mes;
                    dia = null;
                    j=0;                    
                    texto += "<div><h4>" + mes + "</h4>";
                }                        
                
                if (dia != info.fecha.trim()) {            
                    texto += "<a href='#dias' class='birthday-date pointer' onclick='select(this)' id='"+info.fecha+"'>" + fecha[0] + "</a>";
                    if (j++%7==6){
                        texto += "<br/>";                        
                    }
                    dia = info.fecha.trim();
                }
                if (!LIST[info.fecha]) LIST[info.fecha] = "";
                LIST[info.fecha] += "<a href='?a=contactos&search="+info.descripcion+"'>" + info.descripcion.toLowerCase()  + "</a>";
            } else {
                element.append("<li><a href='?a=contactos&search="+info.descripcion+"'>" + info.descripcion.toLowerCase() + "</a></li>");
            }
        }
        texto += "</div>";
        element.append(texto);        
    } else {
        element.append("<div>No hay cumplea&ntilde;os para hoy</div>");
    }    
    
    if (!ALL_BIRTHDAYS) element.append("<a href='?a=cumpleanos' class='birthday-all'>Ver Todos <span class='glyphicon glyphicon-chevron-right'></span></a>");
        
}

function select(obj) {
    $(".selected").removeClass("selected");
    $(obj).addClass("selected");
    var id = $(obj).attr("id");
    var fecha = id;
    fecha = fecha.split("/");            
    var mes = monthName(parseInt(fecha[1]));     
    var dia = fecha[0];
    var titulo = "<h4>" + mes + " " + dia + "</h4>";
    var html = titulo + LIST[id] + "<a href='#' class='regresar' onclick='regresar()'><span class='glyphicon glyphicon-chevron-left'></span> Regresar</a>";   
    $("#birthdays-days").html(html);
    $("#birthdays-days").ScrollTo();    
}

function regresar() {
    $(".content-wrapper").ScrollTo();    
}
