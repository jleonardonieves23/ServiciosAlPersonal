var ENTITY = "VacacionesSolicitud";
var FIELD_ID = "id";
var START_DATE = null;
var END_DATE = null;
var DATES = []; 
var MARCAS = [];
var TOTAL_DIAS_DISFRUTE = 0;
var TOTAL_DIAS_DISFRUTE_SOLCITUDES = 0;
var TOTAL_DIAS = 0;
var DIAS = 0;
var BONO = false;
var RETURN_DATE = null;
var SOLICITUDES = [];

$(function() {    
    setTitle("Vacaciones > Solicitudes");        
    execute("Vacaciones.obtenerPlanificacion", processSolicitudesColaborador, { colaborador: currentUser.id });
});

function processSolicitudesColaborador(data) {
    var solicitudes = $(".vacaciones-solicitudes");   
    if (data.length > 0) {
        var item;
        var fecha_comienzo;
        var fecha_fin;
        var status = "";
        var content = "";
        
        content += "<table class='table table-bordered'>";
        content += "<tr>";
        content += "<th>Fecha comienzo</th>";
        content += "<th>Fecha fin</th>";
        content += "<th>Fecha reintegro</th>";
        content += "<th>D&iacute;as de disfrute</th>";
        content += "<th>Bono vacacional solicitado</th>";
        content += "<th>Estado</th>";
        content += "</tr>";
        for(var i in data) {
            item = data[i];
            fecha_comienzo = Date.parse(item.fecha_comienzo);
            fecha_fin = Date.parse(item.fecha_fin);
            TOTAL_DIAS_DISFRUTE_SOLCITUDES += parseInt(item.dias_disfrute);
            SOLICITUDES[SOLICITUDES.length] = { fecha_comienzo:fecha_comienzo, fecha_fin:fecha_fin };
            BONO |= item.bono_vacacional_solicitado == "1";
            status = item.status == 'Pendiente' ? "<a href='?a=vacaciones/solicitudes&id=" + item.id + "'>" + item.status + "</a>" : item.status;
            content += "<tr>";        
            content += "<td>" + dateFormat(item.fecha_comienzo) + "</td>";
            content += "<td>" + dateFormat(item.fecha_fin) + "</td>";
            content += "<td>" + dateFormat(item.fecha_reintegro) + "</td>";
            content += "<td>" + item.dias_disfrute + "</td>";
            content += "<td>" + (item.bono_vacacional_solicitado == 1 ? "S&iacute;" : "") + "</td>";
            content += "<td>" + status + "</td>";
            content += "</tr>";
        }
        
        content += "</table>";
                               
    } else {
        content = "<p>No hay solicitudes pendientes</p>";
    }
    content += "<div><b class='vacaciones-total'>Total d&iacute;as pendientes por disfrutar: <span id='vacaiones-dias-disfrutar-solicitudes' class='text-success'>"+TOTAL_DIAS_DISFRUTE_SOLCITUDES+"</span></b></div>";                
    solicitudes.html(content);
                
    execute("Vacaciones.periodos", processVacacionesPeriodos, {colaborador: currentUser.id});    
}

function processVacacionesPeriodos(data) {
    var periodos = $(".vacaciones-periodos");
    var solicitud = $(".vacaciones-solicitud");
    
    var content = "";
    content += "<table class='table table-bordered'>";
    
    content += "<tr>";
    content += "<th>Per&iacute;odo</th>";
    content += "<th>D&iacute;as a disfrutar</th>";
    content += "<th>D&iacute;as disfrutados</th>";
    content += "<th>D&iacute;as por disfrutar</th>";
    content += "<th>Bono pagado</th>";
    content += "</tr>";    

    var periodo = 0;
    var item;
    var total_dias_disfrute = 0;
    var total_dias_disfrutados = 0;
    var resto;
    var dias_disfrute;
    var dias_disfrutados;
    var restoClass;
    for(var i in data) {
        item = data[i];
        dias_disfrute = parseInt(item.dias_disfrute ? item.dias_disfrute : 0);
        dias_disfrutados = parseInt(item.dias_disfrutados ? item.dias_disfrutados : 0);        
        resto = dias_disfrute - dias_disfrutados;
        total_dias_disfrute += dias_disfrute;
        total_dias_disfrutados += dias_disfrutados;
        restoClass = resto ?  "text-danger" : "";
        if (periodo < parseInt(item.periodo)) {
            periodo = parseInt(item.periodo);
            BONO |= item.bono_pagado == "1";
        }
        content += "<tr>";
        content += "<td>" + item.periodo + "</td>";
        content += "<td>" + item.dias_disfrute + "</td>";
        content += "<td>" + item.dias_disfrutados + "</td>";
        content += "<td class='"+restoClass+"'>" + resto + "</td>";
        content += "<td>" + (item.bono_pagado == "1" ? "S&iacute;" : " ") + "</td>";        
        content += "</tr>";            
    }
    
    TOTAL_DIAS_DISFRUTE = total_dias_disfrute - total_dias_disfrutados;
    
    content += "</table>";    
    content += "<b class='vacaciones-total'>Total d&iacute;as para disfrutar: <span class='text-success'>" + TOTAL_DIAS_DISFRUTE + "</span></b>";    
                        
    periodos.html(content);
    
    content = "";
    
    TOTAL_DIAS = TOTAL_DIAS_DISFRUTE - TOTAL_DIAS_DISFRUTE_SOLCITUDES;
    
    if (TOTAL_DIAS_DISFRUTE > 0) {
        content += "<div><b class='vacaciones-total'>Total d&iacute;as para disfrutar disponibles: <span class='text-primary'>" + TOTAL_DIAS + "</span></b></div>";            
        content += "<div><label for='startDate'>Fecha comienzo <span>(primer d&iacute;a de disfrute)</span>: </label><input id='startDate' class='form-control' onchange='actualizarDiasSolicitud()'></div>";
        content += "<div><label for='endDate'>Fecha fin <span>(&uacute;ltimo d&iacute;a de disfrute)</span>: </label><input id='endDate' class='form-control' onchange='actualizarDiasSolicitud()' ></div>";
        content += "<div><label for='returnDate'>Fecha reintegro: </label><input id='returnDate' class='form-control' readonly='readonly' ></div>";
        content += "<div></label><input disabled='disabled' type='checkbox' value='1' id='bono' /> <label for='bono' id='lblBono' class='text-disabled'>Deseo recibir el bono vacacional</div>";
        content += "<div><b class='vacaciones-total'>Total d&iacute;as a disfrutar: <span id='vacaiones-dias-disfrutar' class='text-danger'>0</span></b></div>";            
        content += "<div><button disabled='disabled' id='btnAccept' class='btn btn-warning' onclick='guardarSolicitud()'>Aceptar</button></div>";
    } else {
        content += "Usted no posee d&ias pendientes para disfrute de vacaciones.";
    }
    
    solicitud.html(content);
    var today = CURRENT_DATE;    
    $("#startDate").datepicker({language: "es", startDate: today, autoclose: true});
    $("#endDate").datepicker({language: "es", startDate: today, autoclose: true});  
    execute("Calendario.calendarios", processCalendar, { calendario:'Feriados y Bancarios' });    
}

function processCalendar(data) {    
    var date = null;
    var fecha = null;
    var y, m, d;
    for (var i in data) {
        date = data[i];     
        var calendario = date.calendario;
        var descripcion = date.descripcion;
        var info = calendario + ": " + descripcion;        
        calendario = calendario.toLowerCase();
        fecha = dateFormat(date.fecha);
        if (typeof(DATES[fecha]) == "undefined") {            
            DATES[fecha] = info;
            MARCAS[fecha] = calendario.indexOf("cumple") == -1;
        } else {
            DATES[fecha] += "\n" + info;
            MARCAS[fecha] |= calendario.indexOf("cumple") == -1;
        }        
    }
    renderCalendar();      
}

function renderCalendar() {
    var today = CURRENT_DATE.getTime();    
    $('.vacaciones-calendario').calendar({
        language: "es",
        customDayRenderer: function(element, date) {
     
            var f = dateFormat(date);
            var info = "";     
            
            if(date.getTime() < today) {
                $(element).css('color', '#ccc');
            }             
            
            if(date.getDay() == 0 || date.getDay() == 6) {
                if(date.getTime() < today) {
                    $(element).css('color', '#ccc');
                }  else {
                    $(element).css('color', '#c09853');
                }
            }             
            
                          
            if(START_DATE && END_DATE && date.getTime() >= START_DATE && date.getTime() <= END_DATE && date.getDay() > 0 && date.getDay() < 6) {
                $(element).css('background-color', '#64adec');
                $(element).css('border-radius', '0px');
                $(element).css('color', 'white');
                info += "Vacaciones\n";             
            } 
                     
           if(date.getTime() == today) {
                $(element).css('background-color', '#FF7800');
                $(element).css('color', 'white');
                $(element).css('border-radius', '15px');
                info += "Hoy\n";
                
            }
             
           if(date.getTime() == RETURN_DATE) {
                $(element).css('background-color', '#ffcc00');
                $(element).css('border-radius', '0px');
                $(element).css('color', 'white');
                info += "Fecha Reintegro\n";
                
            }
            
            if ( typeof(DATES[f]) != "undefined") {                                
                if (MARCAS[f]) {
                    $(element).css('color', '#ff0000');    
                    $(element).css('background-color', '');
                    $(element).css('border-radius', '0px');
                    $(element).css('border', ''); 
                }
                info += DATES[f];
            }            
            $(element).attr("title", info); 
            $(element).attr("data-toggle", "tooltip")
            $(element).tooltip();                         
        }
    });      
}

function actualizarDiasSolicitud() {
    clear_message();
    if ($("#startDate").val() && $("#endDate").val()) {
        var date;
        var startDate = $("#startDate").datepicker('getDate');
        var endDate = $("#endDate").datepicker('getDate');

        START_DATE = startDate.getTime();
        END_DATE = endDate.getTime();
        
        var startDay = startDate.getDay();
        var endDay = endDate.getDay();
                
        var year;
        var month;
        var day;
        var today = getToday();

        var feriados = "|";        
        for (var i in DATES) {
            i = i.split("/");            
            year = parseInt(i[2]);
            month = parseInt(i[1])-1; 
            day = parseInt(i[0]);        
            date = new Date(year, month, day); 
            feriados += date.getTime() + "|";
        }
        
        var K = 24*60*60*1000;
        DIAS = 0;
               
        for(var i=START_DATE; i<=END_DATE; i+=K) {
            date = new Date(i);
            day = date.getDay();
            if (day > 0 && day < 6 && feriados.indexOf("|"+date.getTime()+"|")==-1) DIAS++;
        }
        
        RETURN_DATE = END_DATE + K;
        for(var i=RETURN_DATE; i<RETURN_DATE+(K*10); i+=K) {
            date = new Date(i);            
            day = date.getDay();
            if (day > 0 && day < 6 && feriados.indexOf("|"+date.getTime()+"|")==-1) {
               RETURN_DATE = i;
               break;
            }
        }     

        date = date.toLocaleDateString();
        
        $("#returnDate").val(date);
        $("#vacaiones-dias-disfrutar").html(DIAS);
        
        renderCalendar(); 
                
        if (START_DATE < today) {
            return lock("No puede comenzar las vacaciones antes del d&iacute;a actual");
        }

        if (END_DATE < today) {
            return lock("No puede finalizar las vacaciones antes del d&iacute;a actual");
        }
        
        if (START_DATE == END_DATE) {
            return lock("No puede comenzar y finalizar las vacaciones el mismo d&iacute;a");
        }

        if (START_DATE > END_DATE) {
            return lock("No puede finalizar las vacaciones antes de comenzarlas");
        }
        
        if (startDay == 0 || startDay == 6) {
            return lock("No puede comenzar las vacaciones en un d&iacute;a de fin de semana", {showInMessageBox: true});
        }
        
        if (endDay == 0 || endDay == 6) {
            return lock("No puede culminar las vacaciones en un d&iacute;a de fin de semana", {showInMessageBox: true});            
        }        
                     
        if (feriados.indexOf("|"+START_DATE+"|")>-1) {
            return lock("No puede comenzar las vacaciones en un d&iacute;a feriado", {showInMessageBox: true});            
        } 

        if (feriados.indexOf("|"+END_DATE+"|")>-1) {
            return lock("No puede culminar las vacaciones en un d&iacute;a feriado", {showInMessageBox: true});            
        } 
        
        for(var i in SOLICITUDES) {
            date =  SOLICITUDES[i];
                        
            if (START_DATE >= date.fecha_comienzo && START_DATE <= date.fecha_fin) {
                return lock("No puede comenzar sus vacaciones dentro de otro per&iacute;odo vacacional pendiente", {showInMessageBox: true});            
            }
            if (END_DATE >= date.fecha_comienzo && END_DATE <= date.fecha_fin) {
                return lock("No puede culminar sus vacaciones dentro de otro per&iacute;odo vacacional pendiente", {showInMessageBox: true});            
            }            
        }

        
        if (DIAS>=10 && !BONO)  {
            $("#bono").removeAttr("disabled");
            $("#lblBono").removeClass("text-disabled");            
        } else {
            $("#bono").prop("checked", false);
            $("#bono").attr("disabled", "disabled");            
            $("#lblBono").addClass("text-disabled");            
        }

        if (DIAS>0 && DIAS <= TOTAL_DIAS)  {
            $("#btnAccept").removeAttr("disabled");
        } else {
            $("#btnAccept").attr("disabled", "disabled"); 
            return lock("Debe seleccionar un periodo de dias de disfrute entre 1 y " + TOTAL_DIAS + " d&iacute;as");
        } 
        
    } 
    $("#vacaiones-dias-disfrutar").html(DIAS);        
}

function lock(msg) {
    $("#btnAccept").attr("disabled", "disabled");    
    $("#bono").attr("disabled", "disabled");
    $("#bono").prop("checked", false);
    $("#lblBono").addClass("text-disabled");       
    message(msg, {showInMessageBox: true});
    return;    
}

function guardarSolicitud() {    
    var fecha_comienzo = $("#startDate").val();
    var fecha_fin = $("#endDate").val();
    var fecha_reintegro = $("#returnDate").val();
    var bono = $("#bono").prop("checked") == true ? 1 : 0;    
    var dias_disfrute = DIAS;    
    execute("Vacaciones.guardarPlanificacion", processSaveVacacionesSolicitud, { colaborador: currentUser.id, fecha_comienzo:fecha_comienzo, fecha_fin:fecha_fin,  fecha_reintegro:fecha_reintegro, bono:bono, dias_disfrute:dias_disfrute} )
}

function processSaveVacacionesSolicitud(data) {
    if (data.status == "true") {
        message_success({ showInMessageBox:true, callback:function(){ call("vacaciones"); }});
        
    } else {
        message(data.message);
    }
}