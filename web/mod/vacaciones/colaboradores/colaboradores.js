$(function() {
    setTitle("Administraci&oacute;n > Consultar Vacaciones de Colaborador");    
    $("#colaborador").on("keypress", function(e) {       
        if ( e.which == 13 ) {
            buscarDatos();
        }       
    })   
    enmascararColaborador();
    $("#loading").hide();
});

function buscarDatos() {
    $(".colaborador").html("");
    $(".data").html("");    
    $("#loading").show();
    var colaborador = $("#colaborador").val();
    execute("Usuario.buscar", processConsultaColaborador, {search:colaborador});        
    execute("Vacaciones.consultarDiasPendientes", processConsultarDiasPendientes, {colaborador:colaborador});        
    execute("Vacaciones.consultarDiasPendientesDetallado", processConsultarDiasPendientesDetallado, {colaborador:colaborador});            
}

function processConsultaColaborador(data) {    
    $("#loading").hide();
    var content = $(".colaborador");
    if (data.status != "false") {
        content.html("<h5>Colaborador</h5><h2>" + data.usuario.nombre + " " + data.usuario.apellido + "</h2>");
    } 
}

function processConsultarDiasPendientes(data) {   
    $("#loading").hide();
    var content = $(".data");
    if (data.status != "false") {
        var rows = data.rows;
        if (!rows || rows.length == 0) {
            content.html("No tienes d&iacute;as pendientes por disfrutar");
        } else {
            var row;
            var totales     = 0;
            var disfrutados = 0;
            var pendientes  = 0;
            var acumulado   = 0;
            var total       = 0;
            var disfrutado  = 0;
            var pendiente   = 0;           
            var chuck       = "";
            var details     = "";            
            for (var i in rows) {
                row = rows[i];
                totales     += converToNumber(row.totales);
                disfrutados += converToNumber(row.disfrutados);
                pendientes  += converToNumber(row.pendientes);
            }
            acumulado = pendientes;
            var html = "<br /><h5>D&iacute;as pendientes</h5>";
                html += "<table class='table table-strip'>";
                html += "<tr>";
                html += "<th>";
                html += "PERIODO";
                html += "</th>";                
                html += "<th>";
                html += "D&Iacute;AS TOTALES";
                html += "</th>";
                html += "<th>";
                html += "D&Iacute;AS DISFRUTADOS";
                html += "</th>";                
                html += "<th>";
                html += "D&Iacute;AS PENDIENTES";
                html += "</td>";                       
                html += "</tr>";                 
            for(var i=rows.length-1; i>-1; i--) {                
                row = rows[i];
                total      = converToNumber(row.totales);
                disfrutado = converToNumber(row.disfrutados);
                pendiente  = converToNumber(row.pendientes);
                acumulado -= total;
                disfrutado = acumulado > 0 ? 0 : (total +  acumulado < 0 ? total : (-1*acumulado));
                pendiente  = total - disfrutado;
                if (pendiente!=0) {
                    chuck = "";
                    chuck += "<tr>";
                    chuck += "<td>";
                    chuck += row.periodo;
                    chuck += "</td>";                
                    chuck += "<td>";
                    chuck += total;
                    chuck += "</td>";
                    chuck += "<td>";
                    chuck += disfrutado;
                    chuck += "</td>";                
                    chuck += "<td>";
                    chuck += pendiente;
                    chuck += "</td>";                       
                    chuck += "</tr>";  
                    details = chuck + details;
                }
            }  
            html += details;
            html += "<tr>";
            html += "<td colspan='2' style='text-align: left;'>";
            html += "<h4>Total de d&iacute;as pendientes</h4>";
            html += "</td>";                
            html += "<td></td>";                
            html += "<td><h4>";
            html += pendientes;
            html += "</h4></td>";                       
            html += "</tr>";            
            html += "</table>";
            html = "<br /><span class='info'>Tienes <span class='dias'>" + pendientes + "</span> d&iacute;a(s) por disfrutar</span><br />" + html;
            content.html(html);
        }
    } else {
        content.html("No hay datos para mostrar");
    }
}


function processConsultarDiasPendientesDetallado(data) {    
    $("#loading").hide();
    var detalle = $(".details");
    if (data.status != "false") {
        var rows = data.rows;
        if (!rows || rows.length == 0) {
            detalle.html("");
        } else {
            var row;
            var dias        = 0;
            var acumulado   = 0;
            var total       = 0;
            var activo      = "";
            var html  = "<h5>Bit&aacute;cora de vacaciones </h5>";
                html += "<table class='table table-strip'>";
                html += "<tr>";
                html += "<th>";
                html += "FECHA <BR/>CONCEPTO";
                html += "</th>";                
                html += "<th>";
                html += "<BR/>CONCEPTO";
                html += "</th>";                
                html += "<th>";
                html += "FECHA <BR/>SALIDA";
                html += "</th>";
                html += "<th>";
                html += "FECHA <BR/>REGRESO";
                html += "</th>";                                
                html += "<th>";
                html += "D&Iacute;AS <BR/>APLICADOS";
                html += "</th>";                
                html += "<th>";
                html += "D&Iacute;AS <BR/>PENDIENTES";
                html += "</th>";                
                html += "</tr>";                 
            for(var i in rows) {                
                row       = rows[i];                
                dias       = converToNumber(row.dias);
                acumulado += row.concepto == "1" ? dias : -dias;
                total     += row.concepto == "1" ? dias : 0;
                activo     = row.concepto == "1" ? "activo" : "";                
                html += "<tr class='"+activo+"'>";
                html += "<td>";
                html += dateFormat(row.fecha);
                html += "</td>";        
                html += "<td class='concepto'>";
                html += row.concepto == "1" ? "Periodo vacacional vencido" : "Disfrute de vacaciones";
                html += "</td>";                
                html += "<td>";
                html += dateFormat(row.salida);
                html += "</td>"; 
                html += "<td>";
                html += dateFormat(row.regreso);
                html += "</td>";                        
                html += "<td>";
                html += dias;
                html += "</td>";                
                html += "<td>";
                html += acumulado;
                html += "</td>";                       
                html += "</tr>";                  
            }    
            html += "<tr>";
            html += "<td colspan='2' style='text-align: left;'>";
            html += "<span class='activo'>Total de d&iacute;as asignados</span>";
            html += "</td>";                            
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td><span class='activo'>";
            html += total;
            html += "</span></td>";                       
            html += "</tr>";             
            html += "<tr>";
            html += "<td colspan='2' style='text-align: left;'>";
            html += "<h4>Total de d&iacute;as pendientes</h4>";
            html += "</td>";                
            html += "<td></td>";
            html += "<td></td>";
            html += "<td></td>";
            html += "<td><h4>";
            html += acumulado;
            html += "</h4></td>";                       
            html += "</tr>";            
            html += "</table>"; 
            
            detalle.html(html);
        }
    } else {
        detalle.html("");
    }
}
