<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/vacaciones/planificacion/planificacion.css" rel="stylesheet" />        
<script src="mod/vacaciones/planificacion/planificacion.js"></script>

<!-- CALENDARIO -->
<link href="lib/bootstrap/calendar/bootstrap-year-calendar.min.css" rel="stylesheet" />        
<script src="lib/bootstrap/calendar/bootstrap-year-calendar.min.js"></script>
<script src="lib/bootstrap/calendar/bootstrap-year-calendar.es.js"></script> 
            
<script>
        var deco = '<%=deco%>';
        var currentUser = <%= usuario.serializar() %>;
</script>

<div class="submenu menu-content">

    <h4>Planificaci&oacute;n de vacaciones</h4>

    <h5>Planes</h5>
    <div class="vacaciones-planes"></div>
    
    <h5>Per&iacute;odos</h5>
    <div class="vacaciones-periodos"></div>
    
    <h5>Plan</h5>
    <div class="vacaciones-plan"></div>
    
    <h5>Calendario</h5>
    <div class="vacaciones-calendario"></div>
    
    <div class="btn-back"><a href="?a=vacaciones"><%=backDeco%> Regresar</a></div>    
</div>     

<%
    } //if (currentUser != null)
%>    
