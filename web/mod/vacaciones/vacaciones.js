$(function() {
    setTitle("Vacaciones");
    execute("Menu.obtener", processMenuVacaciones, {context: 'vacaciones'});        
});

function processMenuVacaciones(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
