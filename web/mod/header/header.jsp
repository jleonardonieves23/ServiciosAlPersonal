<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/header/header.css" rel="stylesheet" />        
<script src="mod/header/header.js"></script>

<div id="header">
    <h3><a href="?a=contactos/profile"><img src="<%=usuario.picture%>" alt="<%=usuario.obtenerNombreCompleto()%>"  /></a> Bienvenido, <b><a href="?a=contactos/profile"><%=usuario.obtenerNombreCompleto()%></a></b> | <a class='header-logout' href='?a=logout'><%=decoLogout%> Cerrar Sesi&oacute;n</a></h3>
    <div>
    <span class="currentDate"><%= currentDate %></span>
    | <span class='tiempo-sesion'>Tiempo de la sesi&oacute;n: <span id="reloj">&nbsp;&nbsp;&nbsp;</span> seg.</span>
    </div>
</div>

<%
    } //if (currentUser != null)
%>    
