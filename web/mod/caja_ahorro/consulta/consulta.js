$(function() {
    setTitle("Caja de Ahorro > Consulta");    
    execute("CajaAhorro.consultar", processConsultar);        
});

function processConsultar(data) {    
    var content = $(".data");
    if (data.status != "false") {
        var datos = data.data;
        var nombre = datos.nombre_apellido.toLowerCase();
        nombre = nombre.substr(nombre.indexOf(" "), nombre.length) + " " + nombre.substr(0, nombre.indexOf(" "));
        var header = "";
        header += "<h5>Colaborador</h5>";
        header += "<h2>" + nombre + "</h2>";
        header += "<h4>Contrato: <b>" + datos.nro_cuenta + "</b></h4>"; 
        header += "<h4>Actualizado hasta el <b>" + dateFormat(datos.fecha_ult_act) + "</b></h4>";        
        header += "<br />";       
        $(".colaborador").html(header);
        var fecha = dateFormat(datos.fecha_ult_act);
        var year = fecha ? fecha.split("/")[2] : "";
        var current_year = current_year ? current_year.split("/")[2] : "";
        var html = "";
        var disponible = (datos.saldo_neto / 100);// -  (datos.saldo_prest_corto/100) - (datos.saldo_prest_esp/100);
        var disponibleRetiro  = disponible * 0.7;
        var disponiblePrestamo = disponible * 0.8;
        if (year && year == current_year) {
            html += "<span class='info'><span class='saldo_disponible'>No</span> puedes realizar retiros parciales por este a&ntilde;o.</span><br />";
            html += "<br /><span class='info'>Tienes <span class='saldo_disponible'>Bs. " + numberFormat(disponibleRetiro) + "</span> disponibles para realizar &oacute;rdenes de compras.</span><br />";
        } else {
            html += "<span class='info'>Tienes <span class='saldo_disponible'>Bs. " + numberFormat(disponibleRetiro) + "</span> disponibles para realizar un retiro parcial o realizar &oacute;rdenes de compras.</span><br />";   
        }
        
        if (((datos.monto_ult_prest/100) - (datos.saldo_prest_corto/100)) + ((datos.monto_ult_prest_esp/100) - (datos.saldo_prest_esp/100)) > 0 ) {        
            html += "<br /><span class='info'>Tienes pr&eacute;stamo(s) vigentes actualmente. Comun&iacute;cate con el Departamento de Caja de Ahorro para consultar la posiblidad de un nuevo prestamo.</span><br />";
        } else {
            html += "<br /><span class='info'>Tienes <span class='saldo_disponible'>Bs. " + numberFormat(datos.disp_prestamo/100) + "</span> disponibles para realizar un pr&eacute;stamo.</span><br />";            
        }
        html += "<h6>Si eres un nuevo ingreso, recuerda que s&oacute;lo podr&aacute;s realizar retiros una vez que hayas alcanzado 6 aportes continuos en la Caja de Ahorro.</h6>";
        html += "<h6>Recuerda que aunque hayas hecho retiros parciales este a&ntilde;o puedes realizar &oacute;rdenes de compra con el monto disponible y participar en las jornadas de retiros especiales.</h6>";

        html += "<h5>Saldo Disponible</h5>";        
        html += "<table class='table table-strip'>";
        html += "<tr>";
        html += "<th class='center'>";
        html += "TOTAL AHORROS";
        html += "</th>";       
        html += "<th class='center'>";
        html += "SALDO NETO";
        html += "</th>";                
        html += "<th class='center'>";
        html += "DISPONIBLE PARA RETIRAR <span class='tip'>(70% saldo disponible)</span>";
        html += "</th>";
        html += "<th class='center'>";
        html += "PR&Eacute;STAMO DISPONIBLE <span class='tip'>(80% saldo disponible)</span>";
        html += "</th>"; 
        html += "</tr>";  

        html += "<tr>";
        html += "<td class='center'>";
        html += numberFormat((datos.saldo_neto/100)  +  (datos.saldo_prest_corto/100) + (datos.saldo_prest_esp/100));
        html += "</td>";       
        html += "<td class='center'>";
        html += numberFormat(disponible);
        html += "</td>";                
        html += "<td class='center'>";
        html += numberFormat(disponibleRetiro);
        html += "</td>";
        html += "<td class='center'>";
        html += numberFormat(disponiblePrestamo);
        html += "</td>"; 
        html += "</tr>";

        html += "</table>";
        
        html += "<h5>Aportes</h5>";   
        if (datos.fecha_ult_ret && datos.fecha_ult_ret != "00/00/0000") {              
            html += "<table class='table table-strip'>";
            html += "<tr>";
            html += "<th>";
            html += "DESCRIPCI&Oacute;N";
            html += "</th>";       
            html += "<th class='center'>";
            html += "FECHA";
            html += "</th>";                
            html += "<th class='center'>";
            html += "CUOTA";
            html += "</th>";        
            html += "</tr>";  

            html += "<tr>";
            html += "<td>";
            html += "Aportes del Socio";
            html += "</td>";       
            html += "<td class='center'>";
            html += dateFormat(datos.fecha_ult_aporte);
            html += "</td>";                
            html += "<td class='center'>";
            html += numberFormat(datos.cuota_aporte_emp/100);        
            html += "</td>";
            html += "</tr>";

            html += "<tr>";
            html += "<td>";
            html += "Aportes del Banco";
            html += "</td>";       
            html += "<td class='center'>";
            html += dateFormat(datos.fecha_ult_aporte);
            html += "</td>";                
            html += "<td class='center'>";
            html += numberFormat(datos.cuota_aporte_pat/100);        
            html += "</td>"; 
            html += "</tr>";

            html += "<tr>";
            html += "<td colspan='2'>";
            html += "<h4>Total</h4>";
            html += "</td>";       
            html += "<td class='center'>";
            html += "<h4>"+numberFormat((datos.cuota_aporte_emp/100) + (datos.cuota_aporte_pat/100))+"</h4>";        
            html += "</td>";
            html += "</tr>";

            html += "</table>";
        } else {
            html += "No has realizados aportes";
        }
        
        html += "<h5>Retiros</h5>";  
        if (datos.fecha_ult_ret && datos.fecha_ult_ret != "00/00/0000") {
            html += "<table class='table'>";
            html += "<tr>";
            html += "<th class='center'>";
            html += "Fecha del &Uacute;ltimo Retiro";
            html += "</th>"; 
            html += "<th class='center'>";
            html += "Monto del &Uacute;ltimo Retiro";
            html += "</th>";                            
            html += "</tr>";

            html += "<tr>";
            html += "<td class='center'>";
            html += dateFormat(datos.fecha_ult_ret);
            html += "</td>";        
            html += "<td class='center'>";
            html += numberFormat(datos.monto_ult_ret/100);
            html += "</td>";                 
            html += "</tr>";

            html += "</table>";
        } else {
            html += "No has realizado retiros";
        }
        
        html += "<h5>Pr&eacute;stamos</h5>";  
        if ((datos.fecha_ult_prest && datos.fecha_ult_prest != "00/00/0000") || (datos.fecha_ult_prest_esp && datos.fecha_ult_prest_esp != "00/00/0000")) {
            html += "<table class='table'>";
            html += "<tr>";
            html += "<th>DESCRIPCI&Oacute;N</th>";
            html += "<th>FECHA</th>";
            html += "<th>MONTO</th>";
            html += "<th>CUOTA MENSUAL</th>";
            html += "<th>DEUDA ACTUAL</th>";
            html += "</tr>";
            if (datos.fecha_ult_prest && datos.fecha_ult_prest != "00/00/0000") {
                html += "<tr>";
                html += "<td>";
                html += "Pr&eacute;stamo";
                html += "</td>";       
                html += "<td class='right'>";
                html += dateFormat(datos.fecha_ult_prest);
                html += "</td>";                       
                html += "<td class='right'>";
                html += numberFormat(datos.monto_ult_prest/100);
                html += "</td>";  
                html += "<td class='right'>";
                html += numberFormat(datos.cuota_prest_cor/100);
                html += "</td>";                
                html += "<td class='right'>";
                html += numberFormat(datos.saldo_prest_corto/100);
                html += "</td>";                                                  
                html += "</tr>";
            }
            if (datos.fecha_ult_prest_esp && datos.fecha_ult_prest_esp != "00/00/0000") {
                html += "<tr>";
                html += "<td>";
                html += "Pr&eacute;stamo Especial";
                html += "</td>";       
                html += "<td class='right'>";
                html += dateFormat(datos.fecha_ult_prest_esp);
                html += "</td>";                     
                html += "<td class='right'>";
                html += numberFormat(datos.monto_ult_prest_esp/100);
                html += "</td>"; 
                html += "<td class='right'>";
                html += numberFormat(datos.cuota_prest_esp/100);
                html += "</td>";                 
                html += "<td class='right'>";
                html += numberFormat(datos.saldo_prest_esp/100);
                html += "</td>";                                                          
                html += "</tr>";
               }
            html += "</table>";
        } else {
            html += "No has solicitado pr&eacute;stamos";
        }
                    
       var movimientos = data.rows;
       html += "<h5>Movimientos</h5>";  
        if (movimientos.length>0) {
            html += "<table class='table'>";
            html += "<tr>";
            html += "<th class='center'>";
            html += "FECHA";
            html += "</th>";            
            html += "<th>";
            html += "DESCRIPCI&Oacute;N";
            html += "</th>";                       
            html += "<th class='center'>";
            html += "RETIRO";
            html += "</th>";
            html += "<th class='center'>";
            html += "APORTE";
            html += "</th>";          
            html += "</tr>";             
            
            for(var i in movimientos) {
                var movimiento = movimientos[i];
                html += "<tr>";
                html += "<td class='center'>";
                html += dateFormat(movimiento.FECHA_MOV);
                html += "</td>";       
                html += "<td>";
                html += movimiento.DESCRIPCION.toUpperCase();
                html += "</td>";       
                html += "<td class='right'>";
                html += movimiento.DEBE == "000000000000" ? "" : numberFormat(movimiento.DEBE/100);
                html += "</td>";  
                html += "<td class='right'>";
                html += movimiento.HABER == "000000000000" ? "" :  numberFormat(movimiento.HABER/100);
                html += "</td>";                  
                html += "</tr>";
            }   


            html += "</table>";
        } else {
            html += "No has realizado movimientos";
        }
        
        content.html(html);        
    }  else {
        content.html("No hay datos para mostrar");        
    }     
   
}
