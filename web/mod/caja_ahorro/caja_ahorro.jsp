<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/caja_ahorro/caja_ahorro.css" rel="stylesheet" />        
<script src="mod/caja_ahorro/caja_ahorro.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">
    
    <h4>Men&uacute; de Caja de Ahorro</h4>
    
    <div id="submenu" class="menu-container"></div>
    
    <span id="loading"><img src="img/ajax-loader.gif"></span>
    
    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>    
    
</div>     
        
<%
    } //if (currentUser != null)
%>    
