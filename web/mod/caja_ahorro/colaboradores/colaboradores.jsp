<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/caja_ahorro/colaboradores/colaboradores.css" rel="stylesheet" />        
<script src="mod/caja_ahorro/colaboradores/colaboradores.js"></script>
<script>
        var deco = '<%=deco%>';
        var currentDate = '<%=currentDate%>';
</script>

<div class="submenu menu-content">

    <h4>Saldo en Caja de Ahorro</h4>    
    <label>Colaborador:</label>
    <table><tr><td><input type='text' class='form-control' id='colaborador' placeholder="colaborador"  /></td><td>&nbsp;<button class='btn btn-warning' onclick="buscarDatos()">Buscar</button></td></tr></table>
    <br /> 
    
    <div class="colaborador"></div>
    
    <span id="loading"><img src="img/ajax-loader.gif"></span>
    
    <div class="data"></div>
          
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>    
</div>     
    

<%
    } //if (currentUser != null)
%>    
