$(function() {
    setTitle("Caja de Ahorro");
    execute("Menu.obtener", processMenu, {context: 'caja_ahorro'});        
});

function processMenu(data) {
    $("#loading").hide();
    var menu = $("#submenu");
    buildMenu(menu, data);
}
