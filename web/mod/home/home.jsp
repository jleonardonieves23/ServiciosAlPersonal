<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>

<link href="mod/home/home.css" rel="stylesheet" />        
<script src="mod/home/home.js"></script>

<table> 
    <tr>
        <td valign="top">
            
            <!-- BANNER -->
            <link href="mod/banners/banners.css" rel="stylesheet" />
            <script src="mod/banners/banners.js"></script>       
            <div id="banner" class="carousel slide carousel-fade"></div>

            <!-- CALENDARIO -->
            <link href="lib/bootstrap/calendar/bootstrap-year-calendar.min.css" rel="stylesheet" />        
            <script src="lib/bootstrap/calendar/bootstrap-year-calendar.min.js"></script>
            <script src="lib/bootstrap/calendar/bootstrap-year-calendar.es.js"></script>       
            <div id="calendar"></div>
            
            <span id="loading" style='display:none;'><img src="img/ajax-loader.gif"></span>
            
        </td>
        
        <td valign="top">
            
            <!-- EVENTOS DE HOY
            <link href="mod/eventos/eventos.css" rel="stylesheet" />        
            <script src="mod/eventos/eventos.js"></script>
            <div id="events"></div> 
            
            <!-- CUMPLEAŅOS 
            <link href="mod/cumpleanos/cumpleanos.css" rel="stylesheet" />        
            <script src="mod/cumpleanos/cumpleanos.js"></script>
            <div id='birthday'>
                <div id="today-birthdays" class='birthday-content'></div>                
            </div>      
            -->     
            
            <%
                Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);                
                if (usuario != null && usuario.tieneAcceso("cumpleanos")) {         
            %>
            <link href="mod/cumpleanos/cumpleanos.css" rel="stylesheet" />        
            <script src="mod/cumpleanos/cumpleanos.js"></script>
            <div id='birthday'>
                <div id="today-birthdays" class='birthday-content'><img id='loading-bithdays' src="img/ajax-loader.gif" style="width: 150px;"></div>                
            </div>  
            <% } %>
            
        </td>      
        
        
    </tr>
