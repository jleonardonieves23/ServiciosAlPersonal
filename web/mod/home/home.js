$(function() {        
    setTitle("Inicio");   
    execute("Calendario.calendarios", processHome, { birthday: 'true'});
});

function processHome(data) {     
    var dates = [];    
    var marcas = [];
    var date = null;
    var fecha = null;
    if (data) {
        for (var i in data) {
            date = data[i];     
            var calendario = date.calendario;
            var descripcion = capitalize(date.descripcion);
            var info = calendario + ": " + descripcion;        
            calendario = calendario.toLowerCase();
            fecha = dateFormat(date.fecha);               
            if (typeof(dates[fecha]) == "undefined") {  
                dates[fecha] = info;
                marcas[fecha] = calendario.indexOf("cumple") == -1;
            } else {
                dates[fecha] += "\n" + info;
                marcas[fecha] |= calendario.indexOf("cumple") == -1;
            }        
        }
    }

    var today = CURRENT_DATE.getTime();  
    
    $("#loading").hide();
     
    $('#calendar').calendar({
        language: "es",
        customDayRenderer: function(element, date) {
            var f = dateFormat(date);                        
            var info = "";                            
            if(date.getTime() < today) {
                $(element).css('color', '#aaa');                
            }             
            
            if(date.getDay() == 0 || date.getDay() == 6) {
                if(date.getTime() < today) {
                    $(element).css('color', '#aaa');
                }  else {
                    $(element).css('color', '#FFbc44');
                }
            }             
            
            if(date.getTime() == today) {
                $(element).css('background-color', '#FF7800');
                $(element).css('color', 'white');
                $(element).css('border-radius', '15px');
                info += "Hoy\n";
                
            } 
                                            
            if ( typeof(dates[f]) != "undefined") {                                
                if (marcas[f]) {
                    $(element).css('color', '#FF7800');              
                }
                info += dates[f];
            }            
            $(element).attr("title", info); 
            $(element).attr("data-toggle", "tooltip")
            $(element).tooltip();             
        }         
    });
}