<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/fideicomiso/consulta/consulta.css" rel="stylesheet" />        
<script src="mod/fideicomiso/consulta/consulta.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">

    <h4>Saldo en Fideicomiso</h4>    
    
    <span id="loading"><img src="img/ajax-loader.gif"></span>
    
    <div class="colaborador"></div>
        
    <div class="data"></div>
          
    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>    
</div>     

<%
    } //if (currentUser != null)
%>    
