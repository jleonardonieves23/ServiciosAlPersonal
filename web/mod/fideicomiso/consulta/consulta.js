$(function() {
    setTitle("Fideicomiso > Consulta");    
    execute("Fideicomiso.consultar", processConsultar);        
});

function processConsultar(data) {       
    var content = $(".data");
    $("#loading").hide();
    if (data.status != "false") {
        var datos = data.data;
        var nombre = data.nombre.toLowerCase();        
 
        var header = "";
        header += "<h5>Colaborador</h5>";
        header += "<h2>" + nombre + "</h2>";
        header += "<h4>" + datos.cedula + "</h4>";
        header += "<h4>Cuenta: <b>" + datos.contrato + "</b></h4>"; 
        header += "<h4>Actualizado hasta el <b>" + dateFormat(datos.cierre) + "</b></h4>";        
        header += "<br />";       
        $(".colaborador").html(header);
        
        datos.rendanual = datos.rendanual.split("\.").join("");
        datos.rendanual = datos.rendanual.split("\,").join(".");
        datos.rendanual = numberFormat(parseFloat(datos.rendanual));

        datos.coefgasto = datos.coefgasto.split("\.").join("");
        datos.coefgasto = datos.coefgasto.split("\,").join(".");
        datos.coefgasto = numberFormat(parseFloat(datos.coefgasto));

        
        var html = "";
        html += "<span class='info'>Tienes <span class='saldo_disponible'>Bs. " + (datos.dispretiro) + "</span> disponibles para realizar un retiro parcial.</span><br />";   
        
        html += "<table class='table table-strip'>";
        
        html += "<tr>";
        html += "<td>Saldo Disponible</td>";
        html += "<td class='right'>"+(datos.saldodisp)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Intereses por Pagar</td>";
        html += "<td class='right'>"+(datos.intporpagar)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Intereses Cobrados</td>";
        html += "<td class='right'>"+(datos.intcobrados)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td><h4>Disponible para Retiro</h4><span class='tip'>(75% saldo disponible)</span></td>";
        html += "<td class='right'><h4>"+(datos.dispretiro)+"</h4></td>";
        html += "</tr>";
        
        html += "</table>";
        
        html += "<table class='table table-strip'>";
        
        html += "<tr>";
        html += "<td>Saldo del Fondo</td>";
        html += "<td class='right'>"+(datos.saldofondo)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Promedio diario de Saldos</td>";
        html += "<td class='right'>"+(datos.promdiario)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Ganancias Netas</td>";
        html += "<td class='right'>"+(datos.gananetas)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Intereses Acumulados </td>";
        html += "<td class='right'>"+(datos.intacumulados)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Coeficiente del Gasto (%)</td>";
        html += "<td class='right'>"+(datos.coefgasto)+"%</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Saldos Pr&eacute;stamos</td>";
        html += "<td class='right'>"+(datos.saldopresta)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Ganancias Brutas</td>";
        html += "<td class='right'>"+(datos.ganabrutas)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Gastos Aplicados</td>";
        html += "<td class='right'>"+(datos.gastos)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Embargo</td>";
        html += "<td class='right'>"+(datos.embargo)+"</td>";
        html += "</tr>";

        html += "<tr>";
        html += "<td>Rendimiento Anual (%)</td>";
        html += "<td class='right'>"+(datos.rendanual)+"%</td>";
        html += "</tr>";
        html += "</table>";
                
        content.html(html);        
    }  else {
        content.html("No hay datos para mostrar");        
    }     
   
}
