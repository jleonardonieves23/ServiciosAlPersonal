<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentDay = Util.getCurrentDay();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        
        //
        String nombre = usuario.obtenerNombreCompleto();
        String cedula = usuario.rif;
        String codigo = usuario.codigo;
        String fecha = Util.getCurrentDate();
%>
<link href="mod/impuestos/ari/ari.css" rel="stylesheet" />        
<script src="mod/impuestos/ari/ari.js"></script>

<script>var FECHA_ACTUAL = '<%=currentYear+currentMonth+currentDay%>'; </script>

<h4>Formulario ARI</h4>
<div class="menu-container">    
    <div class="btn-back"><a href="?a=impuestos"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>        
    
    <div class='ari'>    
        <input type='hidden' id='id' name='id' value ='' />
        <!-- PERIODO -->                
        <div>
            <h4>Periodo:</h4>
            <label>A&ntilde;o:</label>
            <select id="ano" class="form-control col-md-3" onchange='actualizarMeses()'></select>
        </div>
        
        <hr class='separador' />
        
        <div>
            <label>Mes:</label>
            <select id="mes" class="form-control col-md-3" onchange='cargar()'></select>            
        </div>
        
        <span id="loading"><br/><br/><img src="img/ajax-loader.gif"><br/><br/></span>
        
        <div id='DATOSARI' style='display: none'>
        <div>
            <label>Estado:</label>
            <input type="text" id="status" class="form-control col-md-3" readonly="readonly" />
        </div>
                        
        <hr class='separador' />
    
        <!-- HEADER -->
        <table class='table-header'>
            <tr>
                <td width="180" rowspan="2"><img class='logo' src='img/seniat.png' /></td>
                <th>Nombre:</th>
                <td colspan="5"><%=nombre%></td>
            </tr>              
            <tr>
                <th>C&eacute;dula:</th>
                <td><%=cedula%></td>
                <th>C&oacute;digo:</th>
                <td><%=codigo%></td>
                <th>Fecha:</th>
                <td ><%=fecha%></td>
            </tr>
        </table>
                             
        <!-- TITLE -->    
        <hr class='separador' />
        
        <!--- INSTRUCCIONES / LEYENDA -->
        <div class='section'>INSTRUCCIONES:</div>
        <ul class="info">
        <li>Los valores en color <span class='input'>&nbsp;AMARILLO&nbsp;</span> son los montos en <b>Bol&iacute;vares</b> que puedes cargar en el formulario.</li>
        <li>El valor en color <span class='inputfam'>&nbsp;MORADO&nbsp;</span> es la cantidad de <b>Carga Familiar</b> que puedes cargar en el formulario.</li>
        <li>Los valores en color <span class='disabled'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan montos en <b>Bol&iacute;vares</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='ut'>&nbsp;&nbsp;&nbsp;&nbsp;VERDE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan montos en <b>Unidades Tributarias</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='tarifa'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AZUL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan <b>Porcentajes o Tarifas</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='total-final'>&nbsp;&nbsp;&nbsp;ROJO&nbsp;&nbsp;&nbsp;</span> reflejan el <b>Porcentaje de Retenci&oacute;n Final</b> calculado por el sistema.</li>
        <li>Tilda el cuadro de la secci&oacute;n <span class='section'>(E)</span> para utilizar el <b>Desgravamen Unico</b> o d&eacute;jalo sin tildar para utilizar el cuadro de <b>Desgravamenes a Estimar</b> de la secci&oacute;n <span class='section'>(C)</span>.</li>
        <li>Pulsa el bot&oacute;n <span class='btn btn-primary'>Guardar</span> para guardar como <b>Borrador</b> tu formulario.</li>
        <li>Tilda el cuadro para <b>Aceptar la Declaraci&oacute;n</b> y pulsa el bot&oacute;n <span class='btn btn-warning'>Declarar</span> para guardar y <b>Declarar</b> tu formulario.</li>        
        <li>Haz click <a href="img/pdf/InfografiaARI.pdf" target="_blank" title="Infografia ARI">aqui</a> para m&aacute;s detalles.</li>
        </ul>
        
        
        
        <hr class='separador' />
        
        <!-- A) ESTIMACION DE LAS REMUNERACIONES A PERCIBIR -->
        <div class='section'><span>A)</span> ESTIMACION DE LAS REMUNERACIONES A PERCIBIR</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Empresa / Concepto</th>
                <th class='right' width='200'>Monto</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Banco Exterior</td>
                <td class='right'><span class='unit'>Bs.</span><input id='remuneracion_estimada' value='0,00' class='disabled right' readonly="readonly" onchange='actualizar()'/></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td>Banco Exterior</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones1' class='disabled right' readonly="readonly" onchange='actualizar()'/></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td><input id="remuneraciones2_texto" class='input large left'/></td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones2' class='input right remuneraciones' onchange='actualizar()' onkeypress="return soloNumero(event)"/></td>
            </tr>
            <tr>
                <td class='linenumber'>4</td>
                <td><input id="remuneraciones3_texto" class='input large left' /></td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones3' class='input right remuneraciones' onchange='actualizar()' onkeypress="return soloNumero(event)"/></td>
            </tr> 
            <tr>
                <th colspan="2">Total que estima a percibir</th>
                <th class='right'><span class='unit'>Bs.</span> <input id='total_remuneraciones' value='0,00' class='total right' readonly="readonly"/></th>                
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- B) CONVERSION DE LAS REMUNERACIONES A UNIDADES TRIBUTARIOS (U.T.) -->
        <div class='section'><span>B)</span> CONVERSION DE LAS REMUNERACIONES A UNIDADES TRIBUTARIOS (U.T.)</div>
        <table cellspacing='0'>
            <tr>

                <th class="center">Total Remuneraciones</th>
                <th></th>
                <th class="center">Valor U.T.</th>
                <th></th>                
                <th class="center" colspan="2">Total Remuneraciones en U.T.</th>                
            </tr>
            <tr>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id='total_remuneraciones_bs' value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id='ut' value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id='total_remuneraciones_b' value='0,00' class='disabled ut right' readonly="readonly"/></td>
            </tr>
        </table>
        
        <hr class='separador' />
        
        <!-- C) DESGRAVAMENES QUE ESTIMA PAGAR EN EL A�O GRAVABLE -->
        <div class='section'><span>C)</span> DESGRAVAMENES QUE ESTIMA PAGAR EN EL A�O GRAVABLE</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Empresa / Concepto</th>
                <th class='right' width='200'>Monto</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Institutos docentes del contribuyente y descendientes</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen1'  maxlength="15" class='input right desgravamenes' onkeypress="return soloNumero(event)"/></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td>Primas de seguro de hospitalizaci&oacute;n, cirugia y maternidad</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen2' maxlength="15" class='input right desgravamenes' onkeypress="return soloNumero(event)"/></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td>Servicios m&eacute;dicos, odontol&oacute;gicos y de hospitalizaci&oacute;n</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen3' maxlength="15" class='input right desgravamenes' onkeypress="return soloNumero(event)"/></td>
            </tr>
            <tr>
                <td class='linenumber'>4</td>
                <td>Intereses o alquiler para la adq. de la vivienda permanente</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='desgravamen4' onchange='actualizar()'  onblur='desgravamen()' maxlength="15" class='input right desgravamenes' onkeypress="desgravamen(); return soloNumero(event);"/></td>
            </tr> 
            <tr>
                <th colspan="2">Total Desgravamenes</th>
                <th class='right'><span class='unit'>Bs.</span> <input id='total_desgravamenes' value='0,00' class='total right' readonly="readonly"/></th>                
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- D) CONVERSION DE LOS DESGRAVAMENES A UNIDADES TRIBUTARIOS (U.T.) -->
        <div class='section'><span>D)</span> CONVERSION DE LOS DESGRAVAMENES A UNIDADES TRIBUTARIOS (U.T.)</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Total Desgravamenes en <span class='unit'>Bs.</span></th>                
                <th></th>
                <th class="center">Valor U.T.</th>
                <th></th>
                <th class="center">Total Desgravamenes <span class='section'>(D)</span></th>                
            </tr>
            <tr>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_desgravamenes_bs" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp;<span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="ut2" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>               
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_desgravamenes_d" value='0,00' class='disabled ut right' readonly="readonly"/></td>
            </tr>
        </table>
        
        <hr class='separador' />
        
        <!-- E) DESGRAVAMEN UNICO (NO CONSIDERE OTRO)  -->
        <table cellspacing='0'>
            <tr>                
                <td class="left"><span class="section"><span>E)</span> DESGRAVAMEN UNICO (NO CONSIDERE OTRO)</span></td>                                                
                <td class="left"> <input id="unico" name="unico" value="1" onchange='actualizar()' onclick="seleccion()" type='checkbox' class='checkbox' /></td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="desgravamen_unico" value='0,00' class='disabled ut right' readonly="readonly"/></td>                
            </tr>
        </table> 

        <hr class='separador' />
        
        <!-- F) DETERMINACION DE LA RENTA GRAVABLE -->
        <div class='section'><span>F)</span> DETERMINACION DE LA RENTA GRAVABLE</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Remuneraciones <span class='section'>(B)</span></th>
                <th></th>
                <th class="center">Desgravamenes <span class='section'>(D)</span> o <span class='section'>(E)</span></th>
                <th></th>
                <th class="center">Renta Gravable <span class='section'>(F)</span></th>                
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_remuneraciones_ut" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_desgravamenes_ut" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_renta_ut" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
        
        <!-- G) CALCULO DEL IMPUESTO (TARIFA No. 1) -->
        <div class='section'><span>G)</span> CALCULO DEL IMPUESTO (TARIFA No. 1)</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Renta Gravable <span class='section'>(F)</span></th>
                <th></th>                
                <th class="center">Tarifa</th>              
                <th></th> 
                <th class="center">Impuesto Base</th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_renta_g" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left tarifa'><input id="tarifa" value='0,00' class='tarifa right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_base" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <th class="center">Impuesto Base</th>
                <th></th>                
                <th class="center">Sustraendo</th>              
                <th></th>               
                <th class="center">Impuesto<span class='section'>(G)</span></th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_base2" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="sustraendo" value='0,00' class='ut right' readonly="readonly"/></td>             
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- H) REBAJAS AL IMPUESTO ESTIMADO -->
        <div class='section'><span>H)</span> REBAJAS AL IMPUESTO ESTIMADO</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Concepto</th>
                <th colspan="2" class='right'>Cantidad</th>                
                <th colspan="2" class='right'><span class='unit'>U.T.</span></th>                
                <th colspan="2" class='right'>Total</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td class='left'>Rebaja Personal</td>
                <td class='right'><span class=''>CANT.</span></td>
                <td class='right'><input id="rebaja1_cantidad" value='1' class='disabled input right' readonly='readonly' /></td>
                <td class='right'><span class=''><span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;U.T.</span></td>
                <td class='right'><input id="rebaja1_ut" value='0,00' class='ut input right' readonly='readonly' /></td>
                <td class='right'><span class=''><span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;U.T.</span></td>
                <td class='right'><input id="rebaja1_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td class='left'>Rebaja Familar (*)</td>
                <td class='right'><span class=''>CANT.</span></td>
                <td class='right'><input id="rebaja2_cantidad" onchange='actualizar()' value='0' onkeypress="return soloNumero(event)" class='inputfam right rebajas'  /></td>
                <td class='right'><span class=''><span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;U.T.</span></td>
                <td class='right'><input id="rebaja2_ut" value='0,00' class='ut input right' readonly='readonly' /></td>
                <td class='right'><span class=''><span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;U.T.</span></td>
                <td class='right'><input id="rebaja2_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td class='left'>Impuesto retenido de m&aacute;s en a&ntilde;os anteriores</td>
                <td class='right'><span class=''>Bs.</span></td>
                <td class='right'><input id="rebaja3_cantidad" onchange='actualizar()' value='0,0' class='input right rebajas' maxlength="15" onkeypress="return soloNumero(event)"/></td>
                <td class='right'><span class=''><span class="igual"><img src='img/division.png' /></span> &nbsp;&nbsp;Bs.</span></td>
                <td class='right'><input id="rebaja3_ut" value='0,00' class='rebajas input right' readonly='readonly' /></td>
                <td class='right'><span class=''><span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;U.T.</span></td>
                <td class='right'><input id="rebaja3_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>            
            <tr>
                <th colspan="2">Total Rebajas</th>
                <th class='ut right' colspan="5"><span>U.T.</span></th><th class='ut right'><input id="total_rebajas" value='0,00' class='ut total right' readonly="readonly"/></th>                
            </tr>            
        </table>
        <h6><b>(*)</b> La cantidad rebajas familiares debe coincidir con la declarada en SENIAT.</h6>
        <hr class='separador' />
        
        <!-- I) IMPUESTO (ESTIMADO) A RETENER -->
        <div class='section'><span>I)</span> IMPUESTO (ESTIMADO) A RETENER</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Impuesto <span class='section'>(G)</span></th>
                <th></th>   
                <th class="center">Rebajas <span class='section'>(H)</span></th>             
                <th></th> 
                <th class="center">Impuesto a Retener</th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_g" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="rebajas_h" value='0,00' class='ut right' readonly="readonly"/></td>                               
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto" value='0,00' class='ut right' readonly="readonly"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <th class="center">Impuesto a Retener <span class='section'>(I)</span></th>
                <th></th>                
                <th class="center">Valor U.T.</th>             
                <th></th>               
                <th class="center">Impuesto a Retener en <span class='unit'>Bs.</span></th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto_i" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="ut3" value='0,00' class='disabled right' readonly="readonly"/></td>                               
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_bs" value='0,00' class='disabled right' readonly="readonly"/></td>
            </tr>            
        </table>        
        
        <hr class='separador' />
        
        <!-- J) PORCENTAJE DE RETENCION INCIAL -->
        <div class='section'><span>J)</span> PORCENTAJE DE RETENCION INCIAL</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Impuesto Estimado a Retener <span class='section'>(I)</span></th>
                <th></th>
                <th class="center">Remuneraciones a Percibir <span class='section'>(B)</span></th>
                <th></th>
                <th class="center">Porcentaje de Retenci&oacute;n</th>                                             
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto_j" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='lef ut'><span class='unit'>U.T.</span> <input id="total_remuneraciones_j"  value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left tarifa'><input id="porcentaje_impuesto" value='0,00' class='tarifa right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
        
        <!-- K) EJECUCION HASTA LA FECHA -->
        <div class='section'><span>K)</span> EJECUCION HASTA LA FECHA</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Concepto</th>
                <th class='right' width='200'>Valor</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Total de remuneraciones percibidas hasta la fecha</td>
                <td class='right'><span class='unit'>Bs.</span> <input id="total_remuneraciones_percibidas" value='0,00' class='disabled right' readonly="readonly" /></td>
            </tr>                      
            <tr>
                <td class='linenumber'>2</td>
                <td>Total de impuesto retenido hasta la fecha</td>
                <td class='right'><span class='unit'>Bs.</span> <input id="total_impuesto_retenido" value='0,00' class='disabled right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
       
        <!-- L) PORCENTAJE DE VARIACION DE LOS DATOS -->
        <div class='section'><span>L)</span> PORCENTAJE DE VARIACION DE LOS DATOS</div>
        <table cellspacing='0' class='montos-percibir'>
            <tr>                
                <th class="center">Impuesto Estimado a Retener <span class='section'>(I)</span></th>
                <th></th>
                <th class="center">Impuesto Retenido <span class='section'>(K)</span></th>            
                <th></th>
                <td></td>                                                                
            </tr>
            
            <tr>                  
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_i2" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_k" value='0,00' class='disabled right' readonly="readonly"/></td>                
                <td></td>
                <th class="center">Porcentaje de Variaci&oacute;n</th>     
            </tr>   
            
            <tr>
                <td colspan="3" class='center'><div class='hr' /></td>
                <td class='left'> <span class="igual"><img src='img/igual.png' /></span> </td>
                <th class='center total-final'><input id="total_variacion" value='0,00 %' class='total-final center' readonly="readonly"/></th>                                                
            </tr>

            <tr>
                <th class="center">Remuneraciones a Percibir <span class='section'>(A)</span></th>
                <th></th>
                <th class="center">Remuneraciones Percibidas <span class='section'>(K)</span></th>                
                <th></th>
                <td class="center alert" rowspan="2">Este es el porcentaje final que <br />ser&aacute; retenido mensualmente</td>
            </tr>  
            
            <tr>  
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_remuneraciones_a" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'> &nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp; </td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_remuneraciones_k" value='0,00' class='disabled right' readonly="readonly"/></td>                
                <td></td>
            </tr>  
                      
        </table>
        
        <br />
    
        <hr class='separador' />
        
        <table width="100%">
            <tr>
                <td class="center">
                    <div>
                        <input style='float:none;' type="checkbox" id="aceptar" name="aceptar" onchange="aceptar()" /> 
                        <label for="aceptar" class="info left">Estoy conforme con los valores declarado en este formulario y deseo hacer la declaraci&oacute;n.</label>
                    </div>
                </td>
            </tr>
            <tr>                   
                <td class="center">
                    <button id="guardar"  class='btn btn-primary' onclick='guardar()'>Guardar</button>                                              
                    <button id="declarar" class="btn btn-warning" onclick='declarar()' disabled="disabled">Declarar</button>                    
                </td>
            </tr>
        </table>
                    
    </div>
    </div>
            
    <br />  
            
    <div class="btn-back"><a href="?a=impuestos"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>    
</div>  

<%
    } //if (currentUser != null)
%>    
