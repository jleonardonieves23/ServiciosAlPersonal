﻿$(function() {
    setTitle("Impuestos > Formulario ARI");
    execute("Impuestos.obtenerPeriodos", procesarInit);
});


///////////////////////////
//  VARIABLES GLOBALES   //
///////////////////////////

var IMPUESTO_RETENIDO        = 0;
var REMUNERACIONES_RECIBIDAS = 0;
var UT                       = 0;
var UT_REBAJAS               = 0;
var REMUNERACION_ESTIMADA    = 0;
var DESGRAVAMEN_UNICO        = 0;
var TARIFAS                  = 0;
/*var TARIFAS                  = [
                                { "desde": 0.01    , "hasta": 1000.00 , "tarifa": 0.06, "sustraendo":   0 },
                                { "desde": 1000.01 , "hasta": 1500.00 , "tarifa": 0.09, "sustraendo":  30 },
                                { "desde": 1500.01 , "hasta": 2000.00 , "tarifa": 0.12, "sustraendo":  75 },
                                { "desde": 2000.01 , "hasta": 2500.00 , "tarifa": 0.16, "sustraendo": 155 },
                                { "desde": 2500.01 , "hasta": 3000.00 , "tarifa": 0.20, "sustraendo": 255 },
                                { "desde": 3000.01 , "hasta": 4000.00 , "tarifa": 0.24, "sustraendo": 375 },
                                { "desde": 4000.01 , "hasta": 6000.00 , "tarifa": 0.29, "sustraendo": 575 },
                                { "desde": 6000.01 , "hasta": null    , "tarifa": 0.34, "sustraendo": 875 },                                                                
                               ];*/

var MESES                    = [];
var PERIODOS                 = [];
var DATA                     = [];

function procesarInit(data) {
   
    var periodo           = null;
    var ano               = null;    
    var ano2              = null;
    var mes               = null;
    var opciones_periodos = ""; 
    DATA = data.periodos;
    for(var i in data.periodos) {
        periodo = data.periodos[i];        
        ano = periodo.ano;
        mes = periodo.mes;
        if (ano2 != ano) {
            ano2 = ano;
            PERIODOS[ano+""] = []; 
            opciones_periodos += "<option>" + ano + "</option>";
        }
        PERIODOS[ano+""].push(mes);
    }
    $("#ano").html(opciones_periodos);

    actualizarMeses();
}

function actualizarMeses() {
    var opciones_meses = "";
    var ano            = $("#ano").val();
    var periodo        = PERIODOS[ano];
    var mes            = null;
    for(var i in periodo) {
        mes = periodo[i];
        opciones_meses += "<option>" + mes + "</option>";
    }   
    $("#mes").html(opciones_meses);
    
    cargar();
}

function cargar() {
    var ano = $("#ano").val();
    var mes = $("#mes").val();
    
    $("#id").val("");    
    $("#total_impuesto_retenido").val("");
    $("#total_remuneraciones_percibidas").val("");
    $("#remuneracion_estimada").val("");
    $("#desgravamen_unico").val("");
    $("#remuneracion_estimada").val("");
    $("#remuneraciones1").val("");
    $("#remuneraciones2").val("");
    $("#remuneraciones3").val("");
    $("#desgravamen1").val("");
    $("#desgravamen2").val("");
    $("#desgravamen3").val("");
    $("#desgravamen4").val("");
    $("#unico").prop("checked", false);
    $("#rebaja2_cantidad").val("");
    $("#rebaja3_cantidad").val("");
    $("#porcentaje_impuesto").val("");
    $("#total_variacion").val("");
    $("#remuneraciones1_texto").val("Banco Exterior");
    $("#remuneraciones2_texto").val("");
    $("#remuneraciones3_texto").val("");
    $("#status").val("");
    $("#loading").show();
    $("#DATOSARI").hide();
    execute("Impuestos.obtenerDatosARI", procesar, {ano:ano, mes:mes});
}

function procesar(data) {
    $("#loading").hide();
    $("#DATOSARI").show();
    MES                      = data.MES ? parseFloat(data.MES) : 0;
    MESING                   = data.MESING ? parseFloat(data.MESING) : 0;
    ANIOS                    = data.ANIOS ? parseFloat(data.ANIOS) : 0;
    DIARIO                   = data.DIARIO ? parseFloat(data.DIARIO)  : 0;
    TIPO_COLABORADOR         = data.TIPO_COLABORADOR ? parseFloat(data.TIPO_COLABORADOR) : 0;
    SALARIO                  = data.SALARIO ? parseFloat(data.SALARIO) : 0;
    PAQUETE                  = data.PAQUETE ? parseFloat(data.PAQUETE) : 0;
    IMPUESTO_RETENIDO        = data.TOTAL_RET_ACUM ? parseFloat(data.TOTAL_RET_ACUM) : 0;
    REMUNERACIONES_RECIBIDAS = data.TOTAL_REM_ACUM ? parseFloat(data.TOTAL_REM_ACUM) : 0;
    DESGRAVAMEN_UNICO        = data.DEGRAVAMEN_UNICO ? parseFloat(data.DEGRAVAMEN_UNICO) : 0;
    UT_REBAJAS               = data.REBAJA_PERSONAL ? parseFloat(data.REBAJA_PERSONAL) : 0;
    REBAJA_FAMILIAR          = data.REBAJA_FAMILIAR ? parseFloat(data.REBAJA_FAMILIAR) : 0;
    REBAJA_CONYUGE           = data.REBAJA_CONYUGE ? parseFloat(data.REBAJA_CONYUGE) : 0;
    UT                       = data.UNIDAD_TRIBUTARIA ? parseFloat(data.UNIDAD_TRIBUTARIA) : 0;
    REMUNERACION_ESTIMADA    = data.REMUNERACION_ESTIMADA ? parseFloat(data.REMUNERACION_ESTIMADA) : 0;
    REMUNERACIONES           = data.REMUNERACIONES ? parseFloat(data.REMUNERACIONES) : 0; 
    BONO_FUNCIONARIO         = data.BONO_FUNCIONARIO ? parseFloat(data.BONO_FUNCIONARIO) : 0;
    VACACIONES               = data.VACACIONES ? parseFloat(data.VACACIONES) : 0;
    BONO_ADICIONAL           = data.BONO_ADICIONAL ? parseFloat(data.BONO_ADICIONAL) : 0;
    TARIFAS                  = data.TARIFAS ? data.TARIFAS : TARIFAS;
       
    //CARGAR DATOS DEL ARI CONSULTADO:
    $("#guardar").prop("disabled", false);
    $("#aceptar").prop("disabled", false);
    
    if (data.ari && data.ari.id) {
        $("#id").val(data.ari.id);    
        $("#total_impuesto_retenido").val(data.ari.impuesto_retenido);
        $("#total_remuneraciones_percibidas").val(data.ari.remuneraciones_recibidas);
        $("#remuneracion_estimada").val(data.ari.remuneracion_estimada);
        $("#desgravamen_unico").val(data.ari.desgravamen_unico);
        $("#remuneracion_estimada").val(data.ari.remuneracion_estimada);
        $("#remuneraciones1").val(numberFormat(data.ari.remuneraciones1));
        $("#remuneraciones2").val(numberFormat(data.ari.remuneraciones2));
        $("#remuneraciones3").val(numberFormat(data.ari.remuneraciones3));
        $("#desgravamen1").val(numberFormat(data.ari.desgravamen1));
        $("#desgravamen2").val(numberFormat(data.ari.desgravamen2));
        $("#desgravamen3").val(numberFormat(data.ari.desgravamen3));
        $("#desgravamen4").val(numberFormat(data.ari.desgravamen4));
        $("#unico").prop("checked", data.ari.unico == 1);
        $("#rebaja1_cantidad").val(numberFormat(data.ari.rebaja1_cantidad));
        $("#rebaja2_cantidad").val(numberFormat(data.ari.rebaja2_cantidad));
        $("#rebaja3_cantidad").val(numberFormat(data.ari.rebaja3_cantidad));
        $("#porcentaje_impuesto").val(data.ari.porcentaje_impuesto);
        $("#total_variacion").val(data.ari.total_variacion);
        $("#remuneraciones1_texto").val("Banco Exterior");
        $("#remuneraciones2_texto").val(data.ari.remuneraciones2_texto);
        $("#remuneraciones3_texto").val(data.ari.remuneraciones3_texto);
        seleccion();
        var status = data.ari.status;
        $("#status").val(status == "1" ? "Borrado" : status == "2" ? "Declarado"  : "");
        if (status == "2") {
            $("#guardar").prop("disabled", true);
            $("#aceptar").prop("disabled", true);        
        }
    }
    actualizar();
}

function desgravamen() {
    var desgravamenmax = UT*1000;
    
    if (converToLocaleNumber($("#desgravamen4").val()) >= desgravamenmax) {
        message("El Monto m&aacute;ximo para el desgrav&aacute;men  \"4) Intereses o alquiler para la adq. de la vivienda permanente\"  no puede exceder las 1.000 U.T. ", {showInMessageBox: true});
        $("#desgravamen4").val("0");
        $("#desgravamen4").focus();
        actualizar();
    }
}
function soloNumero(e){
    tecla = (document.all) ? e.keyCode : e.which;
    //Tecla de retroceso para borrar, siempre la permite
    if (tecla==8){
        return true;
    }
    // Patron de entrada, en este caso solo acepta numeros
    patron =/[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}
function calcular_tarifa(impuesto) {
    var rango = null;
    var desde = null;
    var hasta = null;
    impuesto = parseFloat(impuesto);
    for (var i in TARIFAS) {
        rango = TARIFAS[i];
        desde = parseFloat(rango.desde);
        hasta = rango.hasta != null ? parseFloat(rango.hasta) : null;
        if (desde < impuesto && (hasta == null || hasta >= impuesto)) {
            return rango;
        }
    }
    return {"tarifa": 0, "sustraendo": 0 };
}

function seleccion() {
    
    if($("#unico").prop("checked")==1) {
        $("#unico").val(1);
        $("#desgravamen1").attr("disabled", "disabled");
        $("#desgravamen1").val("0,00");
        $("#desgravamen2").attr("disabled", "disabled");
        $("#desgravamen2").val("0,00");
        $("#desgravamen3").attr("disabled", "disabled");
        $("#desgravamen3").val("0,00");
        $("#desgravamen4").attr("disabled", "disabled");
        $("#desgravamen4").val("0,00");
        $("#total_desgravamenes").val("0,00");
    } else{
        $("#unico").val(0);
        $("#desgravamen1").removeAttr("disabled");
        $("#desgravamen2").removeAttr("disabled");
        $("#desgravamen3").removeAttr("disabled");
        $("#desgravamen4").removeAttr("disabled");
    }
}

function actualizar() {
    //
    $("#remuneracion_estimada").val(numberFormat(REMUNERACION_ESTIMADA));
    $("#ut").val(numberFormat(UT));    
    $("#ut2").val(numberFormat(UT));  
    $("#ut3").val(numberFormat(UT)); 
    $("#rebaja3_ut").val(numberFormat(UT));  
    $("#rebaja2_ut").val(numberFormat(UT_REBAJAS));  
    $("#rebaja1_ut").val(numberFormat(UT_REBAJAS));  
    $("#desgravamen_unico").val(numberFormat(DESGRAVAMEN_UNICO));  
        
    //Calculos nuevos
    var acumuladorem;
    acumuladorem = REMUNERACIONES ?  REMUNERACIONES : 0;
    var bonofun;
    bonofun      = BONO_FUNCIONARIO ? BONO_FUNCIONARIO : SALARIO / 2;   
    var bonovac;
    bonovac      = VACACIONES ? VACACIONES : 0;
    var bonoadi;
    bonoadi      = BONO_ADICIONAL ? BONO_ADICIONAL : 0;//Segunda linea
    var remuneraciones1;
    
    $("#BANCO").html(bonoadi!=0 ? "Banco Exterior" : ""); 
    
    remuneraciones1 = bonoadi;    
    
    var remuneraciones2 = converToLocaleNumber($("#remuneraciones2").val());
    var remuneraciones3 = converToLocaleNumber($("#remuneraciones3").val());            

    $("#remuneraciones1").val(numberFormat(remuneraciones1));    
    $("#remuneraciones2").val(numberFormat(remuneraciones2));
    $("#remuneraciones3").val(numberFormat(remuneraciones3));
            
    var mesanio = 13;//Se incluye el mes en curso para calcular el salario pendiente
    var mesfal = mesanio - MES;
    var tiempo = mesanio - MESING;
    var pend1 = SALARIO * mesfal;
    var totalGen = acumuladorem + pend1;
    
    //dias de vacaciones segun años de antiguedad
    var antiguedad;
    var dias;
    var diast;
    
    if(ANIOS > 20){
       dias= "n" 
    }else{
       dias= ANIOS 
    }
    
    antiguedad = dias;  
    var A=[];
    A["0"]=0;
    A["1"]=23;      
    A["2"]=25;
    A["3"]=26;
    A["4"]=27;
    A["5"]=28;
    A["6"]=29;
    A["7"]=30;
    A["8"]=31;
    A["9"]=32;
    A["10"]=33;
    A["11"]=35;
    A["12"]=36;
    A["13"]=38;
    A["14"]=40;
    A["15"]=44;
    A["16"]=47;
    A["17"]=50;
    A["18"]=53;
    A["19"]=56;
    A["20"]=59;
    A["n"]=62;
    
    diast = A[dias];
        
    var bonovacional = (diast / 30) * SALARIO;
    var utilidades   = (155 / 30) * SALARIO;
    var vacaciones;
    vacaciones = bonovac == 0 ? bonovacional : bonovac;
    //bono funcionario
    var bfuncionariototal;
    var bonofunc2 = SALARIO / 2;
    bfuncionariototal = bonofun + bonofunc2;
    //calculo de paquete
    var paquete;
    var paquetea;
    var paqueteb;
    //var total_paquete;
    paquetea = TIPO_COLABORADOR ==1 ? utilidades + vacaciones + bfuncionariototal : utilidades + vacaciones;
    paqueteb = TIPO_COLABORADOR ==1 ? (((tiempo * 155) / 12) * (SALARIO / 30)) + (bfuncionariototal) : ((tiempo * 155) / 12) * (SALARIO / 30);//REVISAR DIA ACTUAL CON LA CANTIDAD DE MESES (12)
    paquete  = ANIOS > 0 ? paquetea : paqueteb;
    var remuneracion_estimada= totalGen + paquete;//Linea 1
    //
    actualizarGrupo("total_remuneraciones", "remuneraciones");
    //
    var total_remuneraciones = $("#total_remuneraciones").val(); 
    total_remuneraciones = converToLocaleNumber(total_remuneraciones);
    //Total Ingresos
    remuneracion_estimada = $("#remuneracion_estimada").val();
    remuneracion_estimada = converToLocaleNumber(remuneracion_estimada);
    
    $("#remuneracion_estimada").val(numberFormat(remuneracion_estimada));    
    
    $("#remuneraciones1").val(numberFormat(remuneraciones1)); 
    
    total_remuneraciones += remuneracion_estimada + remuneraciones1;
    
    $("#total_remuneraciones").val(numberFormat(total_remuneraciones));     
    //
    $("#total_remuneraciones_bs").val(numberFormat(total_remuneraciones));             
    total_remuneraciones = total_remuneraciones / UT;    
    $("#total_remuneraciones_b").val(numberFormat(total_remuneraciones));
    $("#total_remuneraciones_ut").val(numberFormat(total_remuneraciones));    
    //
    actualizarGrupo("total_desgravamenes", "desgravamenes");
    //Desgravamenes     
    var total_desgravamenes = $("#total_desgravamenes").val();    
    $("#total_desgravamenes_bs").val(total_desgravamenes);             
    total_desgravamenes = converToLocaleNumber(total_desgravamenes);
    total_desgravamenes /= UT;
    $("#total_desgravamenes_d").val(numberFormat(total_desgravamenes));  
    //Validacion de Desgravamen unico
    var unico = $("#unico").prop("checked");
    
    var total_desgravamenes_ut =  (unico || total_desgravamenes < DESGRAVAMEN_UNICO) ? DESGRAVAMEN_UNICO : total_desgravamenes;
    $("#total_desgravamenes_ut").val(numberFormat(total_desgravamenes_ut));
    //Renta    
    var total_renta = total_remuneraciones - total_desgravamenes_ut;
    $("#total_renta_ut").val(numberFormat(total_renta));
    $("#total_renta_g").val(numberFormat(total_renta));
    //
    var rango      = calcular_tarifa(total_renta);
    var tarifa     = rango.tarifa;
    var sustraendo = rango.sustraendo;    
    //$("#tarifa").val(numberFormat(tarifa*100)+"%");
    $("#tarifa").val(numberFormat(tarifa)+"%");
    $("#sustraendo").val(numberFormat(sustraendo));
    //
    tarifa = parseFloat(tarifa)/100;    
    var impuesto_base = tarifa != 0 && total_renta != 0 ? (total_renta * tarifa) : 0;
    $("#impuesto_base").val(numberFormat(impuesto_base));  
    $("#impuesto_base2").val(numberFormat(impuesto_base));
    var impuesto = impuesto_base - sustraendo;
    $("#impuesto").val(numberFormat(impuesto));   
    //
    var rebaja1_total = UT_REBAJAS;
    $("#rebaja1_total").val(numberFormat(rebaja1_total));      
    //
    var rebaja2_cantidad = $("#rebaja2_cantidad").val();
    rebaja2_cantidad = converToLocaleNumber(rebaja2_cantidad);            
    $("#rebaja2_cantidad").val(numberFormat(rebaja2_cantidad));    
    var rebaja2_total = UT_REBAJAS * rebaja2_cantidad;    
    $("#rebaja2_total").val(numberFormat(rebaja2_total));       
    //
    var rebaja3_cantidad = $("#rebaja3_cantidad").val();
    rebaja3_cantidad = converToLocaleNumber(rebaja3_cantidad);
    $("#rebaja3_cantidad").val(numberFormat(rebaja3_cantidad));
    var rebaja3_total = rebaja3_cantidad / UT;
    $("#rebaja3_total").val(numberFormat(rebaja3_total));    
    //           
    var total_rebajas = rebaja1_total + rebaja2_total + rebaja3_total;
    $("#total_rebajas").val(numberFormat(total_rebajas)); 
    //
    $("#impuesto_g").val(numberFormat(impuesto));
    $("#rebajas_h").val(numberFormat(total_rebajas));
    var total_impuesto = impuesto - total_rebajas;
    $("#total_impuesto").val(numberFormat(total_impuesto)); 
    //
    $("#total_impuesto_i").val(numberFormat(total_impuesto));    
    $("#total_impuesto_bs").val(numberFormat(total_impuesto*UT));
    //        
    $("#total_impuesto_j").val(numberFormat(total_impuesto)); 
    $("#total_remuneraciones_j").val(numberFormat(total_remuneraciones));     
    var porcentaje_impuesto = total_remuneraciones == 0 ? 0 : (total_impuesto / total_remuneraciones);
    $("#porcentaje_impuesto").val(numberFormat(porcentaje_impuesto*100)+"%"); 
    //
    $("#total_impuesto_retenido").val(numberFormat(IMPUESTO_RETENIDO)); 
    $("#total_remuneraciones_percibidas").val(numberFormat(REMUNERACIONES_RECIBIDAS)); 
    //
    $("#total_impuesto_i2").val(numberFormat(total_impuesto*UT));
    $("#total_impuesto_k").val(numberFormat(IMPUESTO_RETENIDO));
    $("#total_remuneraciones_a").val(numberFormat(total_remuneraciones*UT));
    $("#total_remuneraciones_k").val(numberFormat(REMUNERACIONES_RECIBIDAS));
    //                
    var variacion_remueracion = (total_remuneraciones*UT) - REMUNERACIONES_RECIBIDAS;
    var variacion_impuesto =  (total_impuesto*UT) - IMPUESTO_RETENIDO;
    var total_variacion = variacion_remueracion == 0 ? 0 : (variacion_impuesto/variacion_remueracion)*100;    
            
    total_variacion = total_variacion < 0 ? 0 : total_variacion;
    $("#total_variacion").val(numberFormat(total_variacion)+"%"); 

    $("#loading").hide();
}

function actualizarGrupo(id, grupo) {
    var total  = 0;
    $("." + grupo).each(function() {    
            try {
                this.value = this.value == "" ? 0 : this.value;
                var valor = converToLocaleNumber(this.value);
                valor = $.isNumeric(valor) ? valor : 0;
                total += valor;
                this.value = numberFormat(valor);
            } catch(e) {
                this.value = numberFormat(0);
            }
    });
    $("#" + id).val(numberFormat(total));    
}

function aceptar() {
    var status = $("#aceptar").prop("checked");
    if (status) {
        message("Una vez declarado no se podr&aacute;n realizar cambios.", {showInMessageBox: true});
        $("#declarar").removeProp("disabled");
    } else {
        $("#declarar").prop("disabled", true);
    }   
}

function guardar() {
    revisar("Impuestos.guardarDatosARI");
}

function declarar() {
    revisar("Impuestos.declararARI");           
}

function revisar(accion) {
    var periodo;
    var fecha;    
    var parts;
    var fecha_actual = FECHA_ACTUAL;        
    var valido       = true;
    var ano          = $("#ano").val();
    var mes          = $("#mes").val();    
    
    for (var i in DATA) {
        periodo = DATA[i];
        if (periodo.ano == ano && periodo.mes == mes) {
            fecha  = periodo.fecha_cierre;
            fecha  = fecha.split("-").join("");
            valido = parseFloat(fecha_actual) < parseFloat(fecha);
            parts = periodo.fecha_cierre.split("-");
            fecha  = parts[2] + "/" + parts[1] + "/" + parts[0];
            break;
        }
    }
    if (valido) {
        enviar(accion);        
    } else {
        message("<span class='warning'>Advertencia!</span> La fecha de cierre de este periodo de declaraci&oacute;n fue el <b>" + fecha + "</b>. Te encuentras fuera de la fecha de declaraci&oacute;n.", {showInMessageBox: true, callback: function() { enviar(accion)}  });        
    }    
}

function enviar(accion) {
    $("#guardar").attr("disabled", true);
    $("#declarar").attr("disabled", true);
    var data = {};
    data.id                       = $("#id").val().trim();   
    data.impuesto_retenido        = numberToString($("#total_impuesto_retenido").val()).trim();
    data.remuneraciones_recibidas = numberToString($("#total_remuneraciones_percibidas").val()).trim();
    data.ut                       = numberToString($("#ut").val()).trim();   
    data.ut_rebajas               = UT_REBAJAS;
    data.remuneracion_estimada    = numberToString($("#remuneracion_estimada").val()).trim();
    data.desgravamen_unico        = numberToString($("#desgravamen_unico").val()).trim();
    data.remuneracion_estimada    = numberToString($("#remuneracion_estimada").val()).trim();
    data.remuneraciones1          = numberToString($("#remuneraciones1").val()).trim();
    data.remuneraciones2          = numberToString($("#remuneraciones2").val()).trim();
    data.remuneraciones3          = numberToString($("#remuneraciones3").val()).trim();
    data.desgravamen1             = numberToString($("#desgravamen1").val()).trim();
    data.desgravamen2             = numberToString($("#desgravamen2").val()).trim();
    data.desgravamen3             = numberToString($("#desgravamen3").val()).trim();
    data.desgravamen4             = numberToString($("#desgravamen4").val()).trim();
    data.unico                    = $("#unico").prop("checked") ? "1" : "0";
    data.rebaja1_cantidad         = numberToString($("#rebaja1_cantidad").val()).trim();
    data.rebaja2_cantidad         = numberToString($("#rebaja2_cantidad").val()).trim();
    data.rebaja3_cantidad         = numberToString($("#rebaja3_cantidad").val()).trim();
    data.porcentaje_impuesto      = numberToString($("#porcentaje_impuesto").val().split("%").join("")).trim();
    data.total_variacion          = numberToString($("#total_variacion").val().split("%").join("")).trim();
    //data.remuneraciones1_texto    = $("#remuneraciones1_texto").val().trim();
    data.remuneraciones2_texto    = $("#remuneraciones2_texto").val().trim();
    data.remuneraciones3_texto    = $("#remuneraciones3_texto").val().trim();
    data.mes                      = $("#mes").val().trim();
    data.ano                      = $("#ano").val().trim();
    
    execute(accion, procesarGuardar, data);
}

function procesarGuardar(data) {
    $("#guardar").removeProp("disabled");
    var status = $("#aceptar").prop("checked");
    if (status) {
        $("#declarar").removeProp("disabled");
    } else {
        $("#declarar").prop("disabled", true);
    }
    message(data.message, {showInMessageBox:true, callback:recargar});
}

function recargar() {
    call("impuestos/ari");
}