<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentDay = Util.getCurrentDay();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        
%>
<link href="mod/impuestos/ari/colaboradores/colaboradores.css" rel="stylesheet" />        
<script src="mod/impuestos/ari/colaboradores/colaboradores.js"></script>

<script>var FECHA_ACTUAL = '<%=currentYear+currentMonth+currentDay%>'; </script>
<script>
        var deco = '<%=deco%>';
        var currentDate = '<%=currentDate%>';
</script>
<div class="submenu menu-content">

    <h4>Consultar Formulario ARI</h4>    
    <label>Colaborador:</label>
    <table><tr><td><input type='text' class='form-control' id='colaborador' placeholder="colaborador"  /></td><td>&nbsp;<button class='btn btn-warning' onclick="buscarDatos()">Buscar</button></td></tr></table>
    <br /> 
     
    <input type='hidden' id='id' name='id' value ='' />
     
    <!-- PERIODO -->                
    <div id='anio-div' style="display: none">
        <h4>Periodo:</h4>
        <label>A&ntilde;o:</label>
        <select id="ano" class="form-control col-md-3" onchange='actualizarMeses()'></select>
    </div>

    

    <div id='mes-div' style="display: none">
        <label>Mes:</label>
        <select id="mes" class="form-control col-md-3" onchange='cargar()'></select>            
    </div>

    <span id="loading" style="display: none"><br/><br/><img src="img/ajax-loader.gif"><br/><br/></span>

    <div id='DATOSARI' style='display: none'>    
    
    <div id='status-div' style="display: none">
        <label>Estado:</label>
        <input type="text" id="status" class="form-control col-md-3" readonly="readonly" />
    </div>

    <hr class='separador' />

    <div class="colaborador"></div>
    <div class="mensj" style="display: none">No hay datos para mostrar</div>
    <div class="data" style="display: none">
   
    
    
    <div class='ari'>    
       <!-- HEADER -->
        <table class='table-header'>
            <tr>
                <td width="180" rowspan="2"><img class='logo' src='img/seniat.png'/></td>
                <th>Nombre:</th>
                <td colspan="5"><span id='APENOM'></span></td>
            </tr>              
            <tr>
                <th>C&eacute;dula:</th>
                <td><span id='CEDULA'></span></td>
                <th>C&oacute;digo:</th>
                <td><span id='CODIGO'></span></td>
                <th>Fecha:</th>
                <td ><span id='fecha'></span></td>
            </tr>
        </table>
                             
        <!-- TITLE -->    
        <hr class='separador' />
        
        <!--- INSTRUCCIONES / LEYENDA -->
        <div class='section'>INFORMACI&Oacute;N:</div>
        <ul class="info">
        <li>Los valores en color <span class='input'>&nbsp;AMARILLO&nbsp;</span> son los montos en <b>Bol&iacute;vares</b> que el colaborador cargo en el formulario.</li>
        <li>Los valores en color <span class='inputfam'>&nbsp;MORADO&nbsp;</span> es la cantidad de <b>Carga Familiar</b> que el colaborador cargo en el formulario.</li>
        <li>Los valores en color <span class='disabled'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GRIS&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan montos en <b>Bol&iacute;vares</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='ut'>&nbsp;&nbsp;&nbsp;&nbsp;VERDE&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan montos en <b>Unidades Tributarias</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='tarifa'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AZUL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> reflejan <b>Porcentajes o Tarifas</b> calculados por el sistema.</li>
        <li>Los valores en color <span class='total-final'>&nbsp;&nbsp;&nbsp;ROJO&nbsp;&nbsp;&nbsp;</span> reflejan el <b>Porcentaje de Retenci&oacute;n Final</b> calculado por el sistema.</li>
        </ul>
        
        <hr class='separador' />
        
        <!-- A) ESTIMACION DE LAS REMUNERACIONES A PERCIBIR -->
        <div class='section'><span>A)</span> ESTIMACION DE LAS REMUNERACIONES A PERCIBIR</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Empresa / Concepto</th>
                <th class='right' width='200'>Monto</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Banco Exterior</td>
                <td class='right'><span class='unit'>Bs.</span><input id='remuneracion_estimada' value='0,00' class='disabled right' readonly="readonly" onchange='actualizar()'/></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td>Banco Exterior</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones1' class='disabled right' readonly="readonly" onchange='actualizar()'/></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td><input id="remuneraciones2_texto" readonly="readonly" class='input large left'/></td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones2' class='input right remuneraciones' readonly="readonly" onchange='actualizar()'/></td>
            </tr>
            <tr>
                <td class='linenumber'>4</td>
                <td><input id="remuneraciones3_texto" readonly="readonly" class='input large left' /></td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' id='remuneraciones3' class='input right remuneraciones' readonly="readonly" onchange='actualizar()' /></td>
            </tr> 
            <tr>
                <th colspan="2">Total que estima a percibir</th>
                <th class='right'><span class='unit'>Bs.</span> <input id='total_remuneraciones' value='0,00' class='total right' readonly="readonly"/></th>                
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- B) CONVERSION DE LAS REMUNERACIONES A UNIDADES TRIBUTARIOS (U.T.) -->
        <div class='section'><span>B)</span> CONVERSION DE LAS REMUNERACIONES A UNIDADES TRIBUTARIOS (U.T.)</div>
        <table cellspacing='0'>
            <tr>

                <th class="center">Total Remuneraciones</th>
                <th></th>
                <th class="center">Valor U.T.</th>
                <th></th>                
                <th class="center" colspan="2">Total Remuneraciones en U.T.</th>                
            </tr>
            <tr>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id='total_remuneraciones_bs' value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id='ut' value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id='total_remuneraciones_b' value='0,00' class='disabled ut right' readonly="readonly"/></td>
            </tr>
        </table>
        
        <hr class='separador' />
        
        <!-- C) DESGRAVAMENES QUE ESTIMA PAGAR EN EL A�O GRAVABLE -->
        <div class='section'><span>C)</span> DESGRAVAMENES QUE ESTIMA PAGAR EN EL A�O GRAVABLE</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Empresa / Concepto</th>
                <th class='right' width='200'>Monto</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Institutos docentes del contribuyente y descendientes</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen1' readonly="readonly" class='input right desgravamenes' /></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td>Primas de seguro de hospitalizaci&oacute;n, cirugia y maternidad</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen2' readonly="readonly" class='input right desgravamenes'/></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td>Servicios m&eacute,dicos, odontol&oacute;gicos y de hospitalizaci&oacute;n</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen3' readonly="readonly" class='input right desgravamenes' /></td>
            </tr>
            <tr>
                <td class='linenumber'>4</td>
                <td>Intereses o alquiler para la adq. de la vivienda permanente</td>
                <td class='right'><span class='unit'>Bs.</span> <input value='0,00' onchange='actualizar()' id='desgravamen4' readonly="readonly" class='input right desgravamenes' /></td>
            </tr> 
            <tr>
                <th colspan="2">Total Desgravamenes</th>
                <th class='right'><span class='unit'>Bs.</span> <input id='total_desgravamenes' value='0,00' class='total right' readonly="readonly"/></th>                
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- D) CONVERSION DE LOS DESGRAVAMENES A UNIDADES TRIBUTARIOS (U.T.) -->
        <div class='section'><span>D)</span> CONVERSION DE LOS DESGRAVAMENES A UNIDADES TRIBUTARIOS (U.T.)</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Total Desgravamenes en <span class='unit'>Bs.</span></th>                
                <th></th>
                <th class="center">Valor U.T.</th>
                <th></th>
                <th class="center">Total Desgravamenes <span class='section'>(D)</span></th>                
            </tr>
            <tr>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_desgravamenes_bs" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp;<span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="ut2" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>               
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_desgravamenes_d" value='0,00' class='disabled ut right' readonly="readonly"/></td>
            </tr>
        </table>
        
        <hr class='separador' />
        
        <!-- E) DESGRAVAMEN UNICO (NO CONSIDERE OTRO)  -->
        <table cellspacing='0'>
            <tr>                
                <td class="left"><span class="section"><span>E)</span> DESGRAVAMEN UNICO (NO CONSIDERE OTRO)</span></td>                                                
                <td class="left"> <input id="unico" value="1" onchange='actualizar()' type='checkbox' class='checkbox' /></td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="desgravamen_unico" value='0,00' class='disabled ut right' readonly="readonly"/></td>                
            </tr>
        </table> 

        <hr class='separador' />
        
        <!-- F) DETERMINACION DE LA RENTA GRAVABLE -->
        <div class='section'><span>F)</span> DETERMINACION DE LA RENTA GRAVABLE</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Remuneraciones <span class='section'>(B)</span></th>
                <th></th>
                <th class="center">Desgravamenes <span class='section'>(D)</span> o <span class='section'>(E)</span></th>
                <th></th>
                <th class="center">Renta Gravable <span class='section'>(F)</span></th>                
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_remuneraciones_ut" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_desgravamenes_ut" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_renta_ut" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
        
        <!-- G) CALCULO DEL IMPUESTO (TARIFA No. 1) -->
        <div class='section'><span>G)</span> CALCULO DEL IMPUESTO (TARIFA No. 1)</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Renta Gravable <span class='section'>(F)</span></th>
                <th></th>                
                <th class="center">Tarifa</th>              
                <th></th> 
                <th class="center">Impuesto Base</th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_renta_g" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left tarifa'><input id="tarifa" value='0,00' class='tarifa right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_base" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <th class="center">Impuesto Base</th>
                <th></th>                
                <th class="center">Sustraendo</th>              
                <th></th>               
                <th class="center">Impuesto<span class='section'>(G)</span></th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_base2" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="sustraendo" value='0,00' class='ut right' readonly="readonly"/></td>             
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto" value='0,00' class='ut ut right' readonly="readonly"/></td>
            </tr>            
        </table>
        
        <hr class='separador' />
        
        <!-- H) REBAJAS AL IMPUESTO ESTIMADO -->
        <div class='section'><span>H)</span> REBAJAS AL IMPUESTO ESTIMADO</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Concepto</th>
                <th class='right'>Cantidad</th>                
                <th class='right'><span class='unit'>U.T.</span></th>                
                <th class='right'>Total</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td class='left'>Rebaja Personal</td>
                <td class='right'><input id="rebaja1_cantidad" value='1' class='disabled input right' readonly='readonly' /></td>
                <td class='right'><input id="rebaja1_ut" value='0,00' class='ut input right' readonly='readonly' /></td>
                <td class='right'><input id="rebaja1_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>
            <tr>
                <td class='linenumber'>2</td>
                <td class='left'>Rebaja Familar</td>
                <td class='right'><input id="rebaja2_cantidad" onchange='actualizar()' value='0' class='inputfam right rebajas' readonly="readonly" /></td>
                <td class='right'><input id="rebaja2_ut" value='0,00' class='ut input right' readonly='readonly' /></td>
                <td class='right'><input id="rebaja2_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>
            <tr>
                <td class='linenumber'>3</td>
                <td class='left'>Impuesto retenido de m&aacute;s en a&ntilde;os anteriores Bs.</td>
                <td class='right'><input id="rebaja3_cantidad" onchange='actualizar()' value='0' class='input right rebajas' readonly="readonly" /></td>
                <td class='right'><input id="rebaja3_ut" value='0,00' class='ut input right' readonly='readonly' /></td>
                <td class='right'><input id="rebaja3_total" value='0,00' class='ut input right' readonly='readonly' /></td>
            </tr>            
            <tr>
                <th colspan="2">Total Rebajas</th>
                <th class='right' colspan="3"><span class='unit'>U.T.</span> <input id="total_rebajas" value='0,00' class='ut total right' readonly="readonly"/></th>                
            </tr>            
        </table>

        <hr class='separador' />
        
        <!-- I) IMPUESTO (ESTIMADO) A RETENER -->
        <div class='section'><span>I)</span> IMPUESTO (ESTIMADO) A RETENER</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Impuesto <span class='section'>(G)</span></th>
                <th></th>   
                <th class="center">Rebajas <span class='section'>(H)</span></th>             
                <th></th> 
                <th class="center">Impuesto a Retener</th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="impuesto_g" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>                
                <td class='left ut'><span class='unit'>U.T.</span> <input id="rebajas_h" value='0,00' class='ut right' readonly="readonly"/></td>                               
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto" value='0,00' class='ut right' readonly="readonly"/></td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr>
                <th class="center">Impuesto a Retener <span class='section'>(I)</span></th>
                <th></th>                
                <th class="center">Valor U.T.</th>             
                <th></th>               
                <th class="center">Impuesto a Retener en <span class='unit'>Bs.</span></th>
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto_i" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="multiplicacion"><img src='img/multiplicacion.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="ut3" value='0,00' class='disabled right' readonly="readonly"/></td>                               
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_bs" value='0,00' class='disabled right' readonly="readonly"/></td>
            </tr>            
        </table>        
        
        <hr class='separador' />
        
        <!-- J) PORCENTAJE DE RETENCION INCIAL -->
        <div class='section'><span>J)</span> PORCENTAJE DE RETENCION INCIAL</div>
        <table cellspacing='0'>
            <tr>
                <th class="center">Impuesto Estimado a Retener <span class='section'>(I)</span></th>
                <th></th>
                <th class="center">Remuneraciones a Percibir <span class='section'>(B)</span></th>
                <th></th>
                <th class="center">Porcentaje de Retenci&oacute;n</th>                                             
            </tr>
            <tr>
                <td class='left ut'><span class='unit'>U.T.</span> <input id="total_impuesto_j" value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="division"><img src='img/division.png' /></span> &nbsp;&nbsp;</td>
                <td class='lef ut'><span class='unit'>U.T.</span> <input id="total_remuneraciones_j"  value='0,00' class='ut right' readonly="readonly"/></td>
                <td class="center">&nbsp;&nbsp; <span class="igual"><img src='img/igual.png' /></span> &nbsp;&nbsp;</td>                
                <td class='left tarifa'><input id="porcentaje_impuesto" value='0,00' class='tarifa right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
        
        <!-- K) EJECUCION HASTA LA FECHA -->
        <div class='section'><span>K)</span> EJECUCION HASTA LA FECHA</div>
        <table cellspacing='0' class='table table-striped montos-percibir'>
            <tr>
                <th colspan="2" class='left'>Concepto</th>
                <th class='right' width='200'>Valor</th>                
            </tr>
            <tr>
                <td class='linenumber'>1</td>
                <td>Total de remuneraciones percibidas hasta la fecha</td>
                <td class='right'><span class='unit'>Bs.</span> <input id="total_remuneraciones_percibidas" value='0,00' class='disabled right' readonly="readonly" /></td>
            </tr>                      
            <tr>
                <td class='linenumber'>2</td>
                <td>Total de impuesto retenido hasta la fecha</td>
                <td class='right'><span class='unit'>Bs.</span> <input id="total_impuesto_retenido" value='0,00' class='disabled right' readonly="readonly"/></td>
            </tr>
        </table>

        <hr class='separador' />
       
        <!-- L) PORCENTAJE DE VARIACION DE LOS DATOS -->
        <div class='section'><span>L)</span> PORCENTAJE DE VARIACION DE LOS DATOS</div>
        <table cellspacing='0' class='montos-percibir'>
            <tr>                
                <th class="center">Impuesto Estimado a Retener <span class='section'>(I)</span></th>
                <th></th>
                <th class="center">Impuesto Retenido <span class='section'>(K)</span></th>            
                <th></th>
                <td></td>                                                                
            </tr>
            
            <tr>                  
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_i2" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'>&nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp;</td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_impuesto_k" value='0,00' class='disabled right' readonly="readonly"/></td>                
                <td></td>
                <th class="center">Porcentaje de Variaci&oacute;n</th>     
            </tr>   
            
            <tr>
                <td colspan="3" class='center'><div class='hr' /></td>
                <td class='left'> <span class="igual"><img src='img/igual.png' /></span> </td>
                <th class='center total-final'><input id="total_variacion" value='0,00 %' class='total-final center' readonly="readonly"/></th>                                                
            </tr>

            <tr>
                <th class="center">Remuneraciones a Percibir <span class='section'>(A)</span></th>
                <th></th>
                <th class="center">Remuneraciones Percibidas <span class='section'>(K)</span></th>                
                <th></th>
                <td class="center alert" rowspan="2">Este es el porcentaje final que <br />ser&aacute; retenido mensualmente</td>
            </tr>  
            
            <tr>  
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_remuneraciones_a" value='0,00' class='disabled right' readonly="readonly"/></td>
                <td class='center'> &nbsp;&nbsp; <span class="resta"><img src='img/resta.png' /></span>&nbsp;&nbsp; </td>
                <td class='left disabled'><span class='unit'>Bs.</span> <input id="total_remuneraciones_k" value='0,00' class='disabled right' readonly="readonly"/></td>                
                <td></td>
            </tr>  
                      
        </table>

        <hr class='separador' />
                   
    </div>
    </div>
  
</div>         
    <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>    

</div>

<%
    } //if (currentUser != null)
%>    
