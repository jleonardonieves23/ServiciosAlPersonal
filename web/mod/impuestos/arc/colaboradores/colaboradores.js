$(function() {
    setTitle("Administraci&oacute;n > Consultar ARC de Colaborador");
    $("#colaborador").on("keypress", function(e) {       
        if ( e.which == 13 ) {
            buscarDatos();
        }       
    });
    $('#anio').inputmask('Regex', { regex: "[0-9]{4,4}" });
    enmascararColaborador();
    $("#loading").hide();
});

function arcpdf() {
        var pdf = new jsPDF('l', 'pt', 'letter');
        pdf.addHTML($('.arc'), function() {          
             pdf.save('ARC.pdf');
        });
   } 
   
function buscarDatos() {
    var colaborador = $("#colaborador").val();
    var anio = $("#anio").val();
    $("#loading").show();
    execute("Impuestos.obtenerDatosARC", procesar, {colaborador:colaborador, anio:anio});   
    $(".data").hide();
}

function procesar(data) {
    $("#loading").hide();
    var content = $(".data");
    if (data.status=="false") {  
        var msg = data.message ? data.message : ""        
        message(msg);
        $(".mensj").show();
        $(".data").hide();
        $("#imprimir").hide();
    } else {    
        $(".data").show(); 
        $("#imprimir").show();
        $("#ANIOD").html(data.ANIO);
        $("#ANIOH").html(data.ANIO);
        $("#APENOM").html(data.APENOM);
        $("#CODIGO").html(data.CODIGO);
        $("#NACIONALIDAD").html(nacionalidad(data.NACIONALIDAD));
        $("#CEDULA").html(data.CEDULA);
        $("#RIF").html(data.RIF);
        $("#NOMBRE_BANCO").html(data.NOMBRE_BANCO);
        $("#RIF_BANCO").html(data.RIF_BANCO);
        $("#NRO_CONTRIBUYENTE").html(data.NRO_CONTRIBUYENTE);
        $("#NOMBRE_FUNCIONARIO").html(data.NOMBRE_FUNCIONARIO);
        $("#CEDULA_FUNCIONARIO").html(data.CEDULA_FUNCIONARIO);
        $("#DIRECCION_AGENTE").html(data.DIRECCION_AGENTE);
        $("#CIUDAD").html(data.CIUDAD);
        $("#CODIGO_POSTAL").html(data.CODIGO_POSTAL);
        $("#ESTADO").html(data.ESTADO);
        $("#TELEFONO").html(data.TELEFONO);
        /*REMUNERACIONES PAGADAS ABONADAS EN CUENTA*/
        $("#REM_ENE").html(numberFormat(data.REM_ENE));
        $("#REM_FEB").html(numberFormat(data.REM_FEB));
        $("#REM_MAR").html(numberFormat(data.REM_MAR));
        $("#REM_ABR").html(numberFormat(data.REM_ABR));
        $("#REM_MAY").html(numberFormat(data.REM_MAY));
        $("#REM_JUN").html(numberFormat(data.REM_JUN));
        $("#REM_JUL").html(numberFormat(data.REM_JUL));
        $("#REM_AGO").html(numberFormat(data.REM_AGO));
        $("#REM_SEP").html(numberFormat(data.REM_SEP));
        $("#REM_OCT").html(numberFormat(data.REM_OCT));
        $("#REM_NOV").html(numberFormat(data.REM_NOV));
        $("#REM_DIC").html(numberFormat(data.REM_DIC));
        
        /*PORCENTAJE DE RETENCION*/
        var REM_ENE = data.REM_ENE;
        var RET_ENE = data.RET_ENE;
        var IMP_RET_ENE = RET_ENE*100 / REM_ENE;
        $("#IMP_RET_ENE").html(numberFormat(numeroNegativo(IMP_RET_ENE)) ? numberFormat(numeroNegativo(IMP_RET_ENE)):"");
        var REM_FEB = data.REM_FEB;
        var RET_FEB = data.RET_FEB;        
        var IMP_RET_FEB = parseFloat(RET_FEB)*100 / parseFloat(REM_FEB);
        $("#IMP_RET_FEB").html(numberFormat(numeroNegativo(IMP_RET_FEB)) ? numberFormat(numeroNegativo(IMP_RET_FEB)):"");
        var REM_MAR = data.REM_MAR;
        var RET_MAR = data.RET_MAR;        
        var IMP_RET_MAR = parseFloat(RET_MAR)*100 / parseFloat(REM_MAR);
        $("#IMP_RET_MAR").html(numberFormat(numeroNegativo(IMP_RET_MAR)) ? numberFormat(numeroNegativo(IMP_RET_MAR)):"");
        var REM_ABR = data.REM_ABR;
        var RET_ABR = data.RET_ABR;        
        var IMP_RET_ABR = parseFloat(RET_ABR)*100 / parseFloat(REM_ABR);
        $("#IMP_RET_ABR").html(numberFormat(numeroNegativo(IMP_RET_ABR)) ? numberFormat(numeroNegativo(IMP_RET_ABR)):"");
        var REM_MAY = data.REM_MAY;
        var RET_MAY = data.RET_MAY;        
        var IMP_RET_MAY = parseFloat(RET_MAY)*100 / parseFloat(REM_MAY);
	$("#IMP_RET_MAY").html(numberFormat(numeroNegativo(IMP_RET_MAY)) ? numberFormat(numeroNegativo(IMP_RET_MAY)):"");
        var REM_JUN = data.REM_JUN;
        var RET_JUN = data.RET_JUN;        
        var IMP_RET_JUN = parseFloat(RET_JUN)*100 / parseFloat(REM_JUN);
	$("#IMP_RET_JUN").html(numberFormat(numeroNegativo(IMP_RET_JUN)) ? numberFormat(numeroNegativo(IMP_RET_JUN)):"");
        var REM_JUL = data.REM_JUL;
        var RET_JUL = data.RET_JUL;        
        var IMP_RET_JUL = parseFloat(RET_JUL)*100 / parseFloat(REM_JUL);
	$("#IMP_RET_JUL").html(numberFormat(numeroNegativo(IMP_RET_JUL)) ? numberFormat(numeroNegativo(IMP_RET_JUL)):"");
        var REM_AGO = data.REM_AGO;
        var RET_AGO = data.RET_AGO;        
        var IMP_RET_AGO = parseFloat(RET_AGO)*100 / parseFloat(REM_AGO);
	$("#IMP_RET_AGO").html(numberFormat(numeroNegativo(IMP_RET_AGO)) ? numberFormat(numeroNegativo(IMP_RET_AGO)):"");
        var REM_SEP = data.REM_SEP;
        var RET_SEP = data.RET_SEP;        
        var IMP_RET_SEP = parseFloat(RET_SEP)*100 / parseFloat(REM_SEP);
	$("#IMP_RET_SEP").html(numberFormat(numeroNegativo(IMP_RET_SEP)) ? numberFormat(numeroNegativo(IMP_RET_SEP)):"");
        var REM_OCT = data.REM_OCT;
        var RET_OCT = data.RET_OCT;        
        var IMP_RET_OCT = parseFloat(RET_OCT)*100 / parseFloat(REM_OCT);
	$("#IMP_RET_OCT").html(numberFormat(numeroNegativo(IMP_RET_OCT)) ? numberFormat(numeroNegativo(IMP_RET_OCT)):"");
        var REM_NOV = data.REM_NOV;
        var RET_NOV = data.RET_NOV;        
        var IMP_RET_NOV = parseFloat(RET_NOV)*100 / parseFloat(REM_NOV);
	$("#IMP_RET_NOV").html(numberFormat(numeroNegativo(IMP_RET_NOV)) ? numberFormat(numeroNegativo(IMP_RET_NOV)):"");
        var REM_DIC = data.REM_DIC;
        var RET_DIC = data.RET_DIC;        
        var IMP_RET_DIC = parseFloat(RET_DIC)*100 / parseFloat(REM_DIC);
	$("#IMP_RET_DIC").html(numberFormat(numeroNegativo(IMP_RET_DIC)) ? numberFormat(numeroNegativo(IMP_RET_DIC)):"");
        
        /*IMPUESTO RETENIDO RETENCION 2x3*/
        $("#RET_ENE").html(numberFormat(data.RET_ENE) ? numberFormat(numeroNegativo(data.RET_ENE)):""); 
        $("#RET_FEB").html(numberFormat(data.RET_FEB) ? numberFormat(numeroNegativo(data.RET_FEB)):"");
        $("#RET_MAR").html(numberFormat(data.RET_MAR) ? numberFormat(numeroNegativo(data.RET_MAR)):"");
        $("#RET_ABR").html(numberFormat(data.RET_ABR) ? numberFormat(numeroNegativo(data.RET_ABR)):"");
        $("#RET_MAY").html(numberFormat(data.RET_MAY) ? numberFormat(numeroNegativo(data.RET_MAY)):"");
        $("#RET_JUN").html(numberFormat(data.RET_JUN) ? numberFormat(numeroNegativo(data.RET_JUN)):"");
        $("#RET_JUL").html(numberFormat(data.RET_JUL) ? numberFormat(numeroNegativo(data.RET_JUL)):"");
        $("#RET_AGO").html(numberFormat(data.RET_AGO) ? numberFormat(numeroNegativo(data.RET_AGO)):"");
        $("#RET_SEP").html(numberFormat(data.RET_SEP) ? numberFormat(numeroNegativo(data.RET_SEP)):"");
        $("#RET_OCT").html(numberFormat(data.RET_OCT) ? numberFormat(numeroNegativo(data.RET_OCT)):"");
        $("#RET_NOV").html(numberFormat(data.RET_NOV) ? numberFormat(numeroNegativo(data.RET_NOV)):"");
        $("#RET_DIC").html(numberFormat(data.RET_DIC) ? numberFormat(numeroNegativo(data.RET_DIC)):"");
        
        /*REMUNERACIONES PAGADAS / ABONADAS*/
        var acumuladoe = parseFloat(REM_ENE);
        $("#ACU_ENE").html(acumuladoe ? numberFormat(acumuladoe):"");
        var acumuladof = acumuladoe + parseFloat(REM_FEB);
        $("#ACU_FEB").html(acumuladof ? numberFormat(acumuladof):"");
        var acumuladom = acumuladof + parseFloat(REM_MAR);
        $("#ACU_MAR").html(acumuladom ? numberFormat(acumuladom):"");
        var acumuladoa = acumuladom + parseFloat(REM_ABR);
        $("#ACU_ABR").html(acumuladoa ? numberFormat(acumuladoa):"");
        var acumuladoma = acumuladoa + parseFloat(REM_MAY);
        $("#ACU_MAY").html(acumuladoma ? numberFormat(acumuladoma):"");
        var acumuladoj = acumuladoma + parseFloat(REM_JUN);
        $("#ACU_JUN").html(acumuladoj ? numberFormat(acumuladoj):"");
        var acumuladoju = acumuladoj + parseFloat(REM_JUL);
        $("#ACU_JUL").html(acumuladoju ? numberFormat(acumuladoju):"");
        var acumuladoag = acumuladoju + parseFloat(REM_AGO);
        $("#ACU_AGO").html(acumuladoag ? numberFormat(acumuladoag):"");
        var acumuladose = acumuladoag + parseFloat(REM_SEP);
        $("#ACU_SEP").html(acumuladose ? numberFormat(acumuladose):"");
        var acumuladoo = acumuladose + parseFloat(REM_OCT);
        $("#ACU_OCT").html(acumuladoo ? numberFormat(acumuladoo):"");
        var acumuladon = acumuladoo + parseFloat(REM_NOV);
        $("#ACU_NOV").html(acumuladon ? numberFormat(acumuladon):"");
        var acumuladod = acumuladon + parseFloat(REM_DIC);
        $("#ACU_DIC").html(acumuladod ? numberFormat(acumuladod):"");
        /*IMPUESTO RETENIDO ACUMULADO*/
        var iacumuladoe = parseFloat(RET_ENE);
        $("#IACU_ENE").html(numberFormat(iacumuladoe) ? numberFormat(numeroNegativo(iacumuladoe)):"");
        var iacumuladof = iacumuladoe + parseFloat(RET_FEB);
        $("#IACU_FEB").html(numberFormat(iacumuladof) ? numberFormat(numeroNegativo(iacumuladof)):"");
        var iacumuladom = iacumuladof + parseFloat(RET_MAR);
        $("#IACU_MAR").html(numberFormat(iacumuladom) ? numberFormat(numeroNegativo(iacumuladom)):"");
        var iacumuladoa = iacumuladom + parseFloat(RET_ABR);
        $("#IACU_ABR").html(numberFormat(iacumuladoa) ? numberFormat(numeroNegativo(iacumuladoa)):"");
        var iacumuladoma = iacumuladoa + parseFloat(RET_MAY);
        $("#IACU_MAY").html(numberFormat(iacumuladoma) ? numberFormat(numeroNegativo(iacumuladoma)):"");
        var iacumuladoj = iacumuladoma + parseFloat(RET_JUN);
        $("#IACU_JUN").html(numberFormat(iacumuladoj) ? numberFormat(numeroNegativo(iacumuladoj)):"");
        var iacumuladoju = iacumuladoj + parseFloat(RET_JUL);
        $("#IACU_JUL").html(numberFormat(iacumuladoju) ? numberFormat(numeroNegativo(iacumuladoju)):"");
        var iacumuladoag = iacumuladoju + parseFloat(RET_AGO);
        $("#IACU_AGO").html(numberFormat(iacumuladoag) ? numberFormat(numeroNegativo(iacumuladoag)):"");
        var iacumuladose = iacumuladoag + parseFloat(RET_SEP);
        $("#IACU_SEP").html(numberFormat(iacumuladose) ? numberFormat(numeroNegativo(iacumuladose)):"");
        var iacumuladoo = iacumuladose + parseFloat(RET_OCT);
        $("#IACU_OCT").html(numberFormat(iacumuladoo) ? numberFormat(numeroNegativo(iacumuladoo)):"");
        var iacumuladon = iacumuladoo + parseFloat(RET_NOV);
        $("#IACU_NOV").html(numberFormat(iacumuladon) ? numberFormat(numeroNegativo(iacumuladon)):"");
        var iacumuladod = iacumuladon + parseFloat(RET_DIC);
        $("#IACU_DIC").html(numberFormat(iacumuladod) ? numberFormat(numeroNegativo(iacumuladod)):"")
        
        /*RELACIÓN ANUAL*/
        
        /*SERIE 1*/
        $("#MESVAR1").html(data.MESVAR1 ? monthName(data.MESVAR1):"");//1RA RELACION
        $("#REL_ENE").html(data.REL_ENE ? numberFormat(data.REL_ENE):"");//1RA
        $("#REL_ENED").html(data.REL_ENED ? numberFormat(data.REL_ENED):"");//1RA DESGRAVAMEN
        //$("#REL_ENEC").html(data.REL_ENEC ? data.REL_ENEC:"");//1RA CONYUGE
        if(data.REL_ENEC==1){
           $("#REL_ENEC").html("Si");
        }else if(data.REL_ENEC==0 && data.REL_ENEC!=""){
           $("#REL_ENEC").html("No"); 
        } 
        $("#REL_ENEF").html(data.REL_ENEF ? numberFormat(data.REL_ENEF):"");//1RA CARGA FAMILIAR
        $("#REL_ENEP").html(data.REL_ENEP ? numberFormat(data.REL_ENEP):"");//1RA PORCENTAJE
        /*SERIE 2*/
        $("#MESVAR2").html(data.MESVAR2 ? monthName(data.MESVAR2):"");//1RA VARIACION
        $("#REL_VAR2").html(data.REL_VAR2 ? numberFormat(data.REL_VAR2):"");//2DA
        $("#REL_VAR2D").html(data.REL_VAR2D ? numberFormat(data.REL_VAR2D):"");//2DA DESGRAVAMEN
        //$("#REL_VAR2C").html(data.REL_VAR2C ? data.REL_VAR2C:"");//2DA CONYUGE
        if(data.REL_VAR2C==1){
           $("#REL_VAR2C").html("Si");
        }else if(data.REL_VAR2C==0 && data.REL_VAR2C!=""){
           $("#REL_VAR2C").html("No"); 
        } 
        $("#REL_VAR2F").html(data.REL_VAR2F ? numberFormat(data.REL_VAR2F):"");//2DA CARGA FAMILIAR
        $("#REL_VAR2P").html(data.REL_VAR2P ? numberFormat(data.REL_VAR2P):"");//2DA PORCENTAJE
        /*SERIE 3*/
        $("#MESVAR3").html(data.MESVAR3 ? monthName(data.MESVAR3):"");//2DA VARIACION
        $("#REL_JUN").html(data.REL_JUN ? numberFormat(data.REL_JUN):"");//3RA
        $("#REL_JUND").html(data.REL_JUND ? numberFormat(data.REL_JUND):"");//3RA DESGRAVAMEN
        //$("#REL_JUNC").html(data.REL_JUNC ? data.REL_JUNC:"");//3RA CONYUGE
        if(data.REL_JUNC==1){
           $("#REL_JUNC").html("Si");
        }else if(data.REL_JUNC==0 && data.REL_JUNC!=""){
           $("#REL_JUNC").html("No"); 
        } 
        $("#REL_JUNF").html(data.REL_JUNF ? numberFormat(data.REL_JUNF):"");//3RA CARGA FAMILIAR
        $("#REL_JUNP").html(data.REL_JUNP ? numberFormat(data.REL_JUNP):"");//3RA PORCENTAJE
        /*SERIE 4*/
        $("#MESVAR4").html(data.MESVAR4 ? monthName(data.MESVAR4):"");//3RA VARIACION
        $("#REL_VAR4").html(data.REL_VAR4 ? numberFormat(data.REL_VAR4):"");//4TA
        $("#REL_VAR4D").html(data.REL_VAR4D ? numberFormat(data.REL_VAR4D):"");//4TA DESGRAVAMEN
        //$("#REL_VAR4C").html(data.REL_VAR4C ? data.REL_VAR4C:"");//4TA CONYUGE
        if(data.REL_VAR4C==1){
           $("#REL_VAR4C").html("Si");
        }else if(data.REL_VAR4C==0 && data.REL_VAR4C!=""){
           $("#REL_VAR4C").html("No"); 
        }
        $("#REL_VAR4F").html(data.REL_VAR4F ? numberFormat(data.REL_VAR4F):"");//4TA CARGA FAMILIAR
        $("#REL_VAR4P").html(data.REL_VAR4P ? numberFormat(data.REL_VAR4P):"");//4TA PORCENTAJE
        /*SERIE 5*/
        $("#MESVAR5").html(data.MESVAR5 ? monthName(data.MESVAR5):"");//4TA VARIACION
        $("#REL_VAR5").html(data.REL_VAR5 ? numberFormat(data.REL_VAR5):"");//5TA
        $("#REL_VAR5D").html(data.REL_VAR5D ? numberFormat(data.REL_VAR5D):"");//5TO DESGRAVAMEN
        //$("#REL_VAR5C").html(data.REL_VAR5C ? data.REL_VAR5C:"");//5TO CONYUGE
        if(data.REL_VAR5C==1){
           $("#REL_VAR5C").html("Si");
        }else if(data.REL_VAR5C==0 && data.REL_VAR5C!=""){
           $("#REL_VAR5C").html("No"); 
        }
        $("#REL_VAR5F").html(data.REL_VAR5F ? numberFormat(data.REL_VAR5F):"");//5TA CARGA FAMILIAR
        $("#REL_VAR5P").html(data.REL_VAR5P ? numberFormat(data.REL_VAR5P):"");//5TO PORCENTAJE    
        
        content;  
    }   
}
