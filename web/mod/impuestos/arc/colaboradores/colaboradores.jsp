<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) {
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        //
        String fecha = Util.getCurrentDate();
        String codigo = usuario.codigo;
%>
<link href="mod/impuestos/arc/colaboradores/colaboradores.css" rel="stylesheet" />        
<script src="mod/impuestos/arc/colaboradores/colaboradores.js"></script>
<script src="lib/jquery/jspdf/html2canvas.js"></script>
<script src="lib/jquery/jspdf/jspdf.debug.js"></script>

<table width='100%'>
    <tr>
        <td>
            <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span>Regresar</a></div>
        </td> 
</table>
<script>
        var deco = '<%=deco%>';
        var currentDate = '<%=currentDate%>';
</script>

<div class="colaboradores submenu menu-content">

    <h4>Consultar Formulario ARC</h4>    
    <label>Colaborador:</label>
    <table style="width:500px;">
        <tr>
            <td><input type='text' class='form-control' id='anio' placeholder="a&ntilde;o"  /></td>
            <td><input type='text' class='form-control' id='colaborador' placeholder="colaborador"  /></td>
            <td><button class='btn btn-warning' onclick="buscarDatos()">Buscar</button></td>
            <td><button class='btn btn-primary' id='imprimir' style='display: none;' onclick="arcpdf();"><span class='glyphicon glyphicon-print'></span> Imprimir</button></td>
        </tr>
    </table>
    <br /> 
    <br />
    <span id="loading" style="display: none"><img src="img/ajax-loader.gif"></span>
    <div class="colaborador"></div>
    
    <div class="mensj" style="display: none">No hay datos para mostrar</div>
   
    <div class="arc data" style="display: none">
    <table id='comprobantearc'>
        <thead>   
        <div class='separador1'></div>

        <div id='titulo' class='title'>COMPROBANTE DE RETENCI&Oacute;N ARC
        </div>

        <div class='title2'>COMPROBANTE DE RETENCI&Oacute;N DE IMPUESTO SOBRE LA RENTA 
            ANUAL O DE CESE DE ACTIVIDADES PARA PERSONAS RESIDENTES PERCEPTORAS 
            DE SUELDOS, SALARIOS Y DEMAS REMUNERACIONES SIMILARES
        </div>

        <div class='periodo'>PER&Iacute;ODO DESDE 01/01/<span id='ANIOD'></span> HASTA 31/12/<span id='ANIOH'></span>  </div>
        <hr class='separador' />
        </thead>
    </table>
    <!-- BENEFICIARIO DE LAS REMUNERACIONES -->
    <div class='section'>BENEFICIARIO DE LAS REMUNERACIONES</div>
    <table border="0" class='table-header' cellspacing='0' cellpadding='0'>
        <tr>
            <td><h6>Apellidos y Nombres</h6>
                <span id='APENOM'></span>
            </td>
            <td class='sep'></td>
            <td><h6>C&oacute;digo</h6>
                <span id='CODIGO'></span>
            </td>
            <td class='sep'></td>
            <td><h6>Nro. RIF</h6>
                <span class='codigo' id='RIF'></span></td>
            <td class='sep'></td>
            <td><h6>Nacionalidad</h6>
                <span id='NACIONALIDAD'></span>
            </td>
            <td class='sep'></td>
            <td><h6>C&eacute;dula</h6>
                <span id='CEDULA'></span>
            </td>
            <td class='sep'></td>
            <td><h6>Fecha</h6>
                <span class='fecha'><%=fecha%></span>
            </td>
        </tr>
    </table>
    <div class='section'>TIPO DE AGENTE DE RETENCI&Oacute;N</div> 
    <table border="0" class='table-header'> 
        <tr>
            <td><h6>Persona Jur&iacute;dica</h6>
                <span class='nombre' id='NOMBRE_BANCO'></span></td>
            <td class='sep'></td>
            <td><h6>Nro. RIF</h6>
                <span class='codigo' id='RIF_BANCO'></span></td>
            <td class='sep'></td>
            <td><h6>Contribuyente Nro.</h6>
                <span class='codigo' id='NRO_CONTRIBUYENTE'></span></td>
        </tr>
        <tr>
            <td><h6>Funcionario autorizado hacer la retenci&oacute;n</h6>
                <span class='nombre' id='NOMBRE_FUNCIONARIO'></span></td>
            <td class='sep'></td>
            <td><h6>C&eacute;dula</h6>
                <span class='cedula' id='CEDULA_FUNCIONARIO'></span></td>
            <td class='sep'></td>
            <td><h6></h6></td>
        </tr> 
    </table> 
    <div class='section'>DIRECCI&Oacute;N DEL AGENTE DE RETENCI&Oacute;N <br> </div>
    <table border="0" class='table-header'>
        <tr>
            <td colspan="4"><h6>De la sociedad,comunidad, dependencia oficial, organismo internacional y otros</h6></td>
        </tr>
        <tr>
            <td colspan="4"><span class='nombre' id='DIRECCION_AGENTE'></span></td>
        </tr> 
        <tr>
            <td><h6>Ciudad o Lugar</h6>
                <span class='nombre' id='CIUDAD'></span></td>
            <td class='sep'></td>
            <td><h6>Zona Postal</h6>
                <span class='nombre' id='CODIGO_POSTAL'></span></td>
            <td class='sep'></td>
            <td><h6>Estado o Entidad Federal</h6>
                <span class='nombre' id='ESTADO'></span></td>
            <td class='sep'></td>
            <td><h6>Tel&eacute;fono</h6>
                <span class='telefono' id='TELEFONO'></span></td>
        </tr>
    </table>
    <table class='table'>
        <tr>
            <th class='center'>Mes</th>
            <th class='center'>Remuneraciones <br>Pagadas/Abonadas <br> en Cuenta</th>
            <th class='center'>Porcentaje <br> de Retenc.</th>
            <th class='center'>Impuesto Retenido <br> (2 x 3)</th>
            <th class='center'>Remuneraciones <br>Pagadas/Abonad.</th>
            <th class='center'>Impuesto Retenido <br> Acumulado</th>
            <th class='center' colspan="7">Datos obtenidos de la informaci&oacute;n suministrada por el contribuyente</th>
        </tr>              
        <tr>
            <td>ENE</td>
            <td class='right' id='REM_ENE'></td>
            <td class='right' id='IMP_RET_ENE'></td>
            <td class='right' id='RET_ENE'></td>
            <td class='right' id='ACU_ENE'</td>
            <td class='right' id='IACU_ENE'</td>
            <th class='center' rowspan="2" colspan="2">Informaci&oacute;n <br> seg&uacute;n AR-I mes</th>
            <th class='center' rowspan="2">Remuneraci&oacute;n <br> Anual Bs. F.</th>
            <th class='center' rowspan="2">Desgravamenes <br>Bs. F.</th>
            <th class='center' valign="center" rowspan="2">Conyuge <!-- <br>(1 = Si)--></th>
            <th class='center' rowspan="2">Cargas <br> Fam.</th>
            <th class='center' rowspan="2">&nbsp; % &nbsp; </th>
        </tr>
        <tr>
            <td>FEB</td>
            <td class='right' id='REM_FEB'></td>
            <td class='right' id='IMP_RET_FEB'></td>
            <td class='right' id='RET_FEB'></td>
            <td class='right' id='ACU_FEB'></td>
            <td class='right' id='IACU_FEB'></td>
        </tr>
        <tr>
            <td>MAR</td>
            <td class='right' id='REM_MAR'></td>
            <td class='right' id='IMP_RET_MAR'></td>
            <td class='right' id='RET_MAR'></td>
            <td class='right' id='ACU_MAR'></td>
            <td class='right' id='IACU_MAR'></td>
            <th>1ra.Rel.</th>
            <td id='MESVAR1'></td>
            <td id='REL_ENE'></td>
            <td id='REL_ENED'></td>
            <td id='REL_ENEC'></td>
            <td id='REL_ENEF'></td>
            <td id='REL_ENEP'></td>
        </tr>
        <tr>
            <td>ABR</td>
            <td class='right' id='REM_ABR'></td>
            <td class='right' id='IMP_RET_ABR'></td>
            <td class='right' id='RET_ABR'></td>
            <td class='right' id='ACU_ABR'></td>
            <td class='right' id='IACU_ABR'></td>
            <th>1ra.Var.</th>
            <td id='MESVAR2'></td>
            <td id='REL_VAR2'></td>
            <td id='REL_VAR2D'></td>
            <td id='REL_VAR2C'></td>
            <td id='REL_VAR2F'></td>
            <td id='REL_VAR2P'></td>
        </tr>
        <tr>
            <td>MAY</td>
            <td class='right' id='REM_MAY'></td>
            <td class='right' id='IMP_RET_MAY'></td>
            <td class='right' id='RET_MAY'></td>
            <td class='right' id='ACU_MAY'></td>
            <td class='right' id='IACU_MAY'></td>
            <th>2da.Var.</th>
            <td id='MESVAR3'></td>
            <td id='REL_JUN'></td>
            <td id='REL_JUND'></td>
            <td id='REL_JUNC'></td>
            <td id='REL_JUNF'></td>
            <td id='REL_JUNP'></td>
        </tr>
        <tr>
            <td>JUN</td>
            <td class='right' id='REM_JUN'></td>
            <td class='right' id='IMP_RET_JUN'></td>
            <td class='right' id='RET_JUN'></td>
            <td class='right' id='ACU_JUN'></td>
            <td class='right' id='IACU_JUN'></td>
            <th>3ra.Var.</th>
            <td id='MESVAR4'></td>
            <td id='REL_VAR4'></td>
            <td id='REL_VAR4D'></td>
            <td id='REL_VAR4C'></td>
            <td id='REL_VAR4F'></td>
            <td id='REL_VAR4P'></td>
        </tr>
        <tr>
            <td>JUL</td>
            <td class='right' id='REM_JUL'></td>
            <td class='right' id='IMP_RET_JUL'></td>
            <td class='right' id='RET_JUL'></td>
            <td class='right' id='ACU_JUL'></td>
            <td class='right' id='IACU_JUL'></td>
            <th>4ta.Var.</th>
            <td id='MESVAR5'></td>
            <td id='REL_VAR5'></td>
            <td id='REL_VAR5D'></td>
            <td id='REL_VAR5C'></td>
            <td id='REL_VAR5F'></td>
            <td id='REL_VAR5P'></td>
        </tr>
        <tr>
            <td>AGO</td>
            <td class='right' id='REM_AGO'></td>
            <td class='right' id='IMP_RET_AGO'></td>
            <td class='right' id='RET_AGO'></td>
            <td class='right' id='ACU_AGO'></td>
            <td class='right' id='IACU_AGO'></td>
            <th colspan="7" rowspan="5" class='blanco'>Firma del agente de retenci&oacute;n</br>
                <img class='firma' src='img/firma/FirmaAprobador.png'/>
                <img class='sello' src='img/firma/LogoA.png' /></th>
        </tr>
        <tr>
            <td>SEP</td>
            <td class='right' id='REM_SEP'></td>
            <td class='right' id='IMP_RET_SEP'></td>
            <td class='right' id='RET_SEP'></td>
            <td class='right' id='ACU_SEP'></td>
            <td class='right' id='IACU_SEP'></td>
        </tr>
        <tr>
            <td>OCT</td>
            <td class='right' id='REM_OCT'></td>
            <td class='right' id='IMP_RET_OCT'></td>
            <td class='right' id='RET_OCT'></td>
            <td class='right' id='ACU_OCT'></td>
            <td class='right' id='IACU_OCT'></td>
        </tr>
        <tr>
            <td>NOV</td>
            <td class='right' id='REM_NOV'></td>
            <td class='right' id='IMP_RET_NOV'></td>
            <td class='right' id='RET_NOV'></td>
            <td class='right' id='ACU_NOV'></td>
            <td class='right' id='IACU_NOV'></td>
        </tr>
        <tr>
            <td>DIC</td>
            <td class='right' id='REM_DIC'></td>
            <td class='right' id='IMP_RET_DIC'></td>
            <td class='right' id='RET_DIC'></td>
            <td class='right' id='ACU_DIC'></td>
            <td class='right' id='IACU_DIC'></td>
        </tr>
    </table>  
</div>  
</div> 
<div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span>Regresar</a></div> 
<!-- <div align='right'><a href="#" onclick="arcpdf();"><span class='glyphicon glyphicon-print'></span> Imprimir</a></div>-->

</div>
<%
    } //if (currentUser != null)
%>    