var ENTITY = "ARIPeriodo";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [         
        {title: 'A&ntilde;o', name: 'ano'},
        {title: 'Mes', name: 'mes'},
        {title: 'Fecha de Cierre', name: 'fecha_cierre'},        
        {title: 'Activo', name: 'status', select: 'Listado.bivalente' }					        
        ] } ;
$(function() {
    setTitle("Administraci&oacute;n de Periodos para ARI");
    loadEntities({order: 'fecha_cierre'});
});
