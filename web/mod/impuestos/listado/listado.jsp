<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        
        //
        String nombre = usuario.obtenerNombreCompleto();
        String cedula = usuario.rif;
        String codigo = usuario.codigo;
        String fecha = Util.getCurrentDate();
%>

<link href="mod/impuestos/listado/listado.css" rel="stylesheet" />        
<script src="mod/impuestos/listado/listado.js"></script>
    
<script type="text/javascript" src="lib/ext/shieldui-all.min.js"></script>
<script type="text/javascript" src="lib/ext/jszip.min.js"></script>

<div class="submenu menu-content">    
<h4>Listado de Formularios ARI</h4> 
    
    <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>        
    
        <div class='listadoari'> 
    <table class="params">
        <tr>
            <th class="center" colspan="3">Datos</th>
                <th></th>
            </tr>
        <tr>
           <td>
                Mes: 
                <select id="mes" class="form-control col-md-3" onchange='cargar()'></select>  
            </td>
            <td>
                A&ntilde;o:
               <select id="ano" class="form-control col-md-3" onchange='actualizarMeses()'></select>
                              
            </td>
             <td>
                Estatus:
                <select class="form-control" id="status">
                    <option value="1">Guardado</option>
                    <option value="2">Declarado</option>
                </select>  
            </td>
            <td valign="bottom">
                <button class="btn btn-default" type="submit" onclick="searchAri()"><i class="glyphicon glyphicon-search"></i></button>
            </td>
        </tr>
    </table>
   </div> 
    
    <div class='listadoari'>     
     <div class='consultarlistadoari' />
                      
    </div>
   
    <br />  
    <div class="input-btn">
      <button id="exportButtonxls" class='btn btn-primary'>Exportar a Excel</button>
    </div>
    
    <br />  
    <table width='100%'>
        <tr><td><div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div></td>
    <td><!--<div align='right'><a id='exportxls' onclick="exportButtonxls();"><span class='glyphicon glyphicon-book'></span> Imprimir</a></div>--></td></tr>
    </table>
</div>  
</div>

<%
    } //if (currentUser != null)
%>    
