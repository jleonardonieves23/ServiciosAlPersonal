var BE_NO_DATA_BCV       = "No existen datos para mostrar";
var BE_NO_GRUPO          = "Debe seleccionar los datos a consultar";
var PERIODOS             = [];
$(function() {            
    setTitle("Administraci&oacute;n > Listado de Formularios ARI");    
    $("#result-message").fadeOut();    
    $("#loading").fadeOut(); 
    execute("Impuestos.obtenerPeriodos", procesarInit);
    $("#exportButtonxls").click(function () {
        // Analizar el elemento de tabla HTML que tiene un id = tabla de exportación
        var i = 0;
        var dataSource = shield.DataSource.create({
            data: "#reporte",
            schema: {
                type: "table",
                fields: {
                    Categoria: { type: String },
                    Codigo: { type: String },
                    Nombre: { type: String },
                    Remuneraciones: { type: String },
                    Desgravamen_Unico: { type: String },
                    Desgravamen: { type: String },
                    Familiares: { type: String },
                    Rebajas_Adicionales: { type: String },
                    Rebajas: { type: String },
                    Retencion: { type: String },
                    Variacion: { type: String },
                    Mes: { type: String },
                    Ano: { type: String }
                }
            }
        });
        
        // Cuando se realiza el análisis, exporte los datos a Excel
        dataSource.read().then(function (data) {
            new shield.exp.OOXMLWorkbook({
                author: "Servicios al Personal - Banco Exterior",
                worksheets: [
                    {
                        name: "table",
                        rows: [
                            {
                                cells: [
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Categor\u00eda"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "C\u00f3digo"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Nombre y Apellido"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Total Remuneraciones",
                                        width: "20px"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Desgravamen \u00danico"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Total Desgravamen"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Cantidad Familiares"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Otra Rebajas"
                                    },                                    
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Rebajas de Impuesto",
                                        width: "300px"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "% Retenci\u00f3n"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "% Variaci\u00f3n"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "Mes"
                                    },
                                    {
                                        style: {
                                            bold: true
                                        },
                                        type: String,
                                        value: "A\u00f1o"
                                    }
                                ]
                            }
                            ].concat($.map(data, function(item) {                                  
                            return {
                                cells: [
                                    { type: String, value: item.Categoria },
                                    { type: String, value: item.Codigo },
                                    { type: String, value: item.Nombre },
                                    { type: String, value: item.Remuneraciones },
                                    { type: String, value: item.Desgravamen_Unico },
                                    { type: String, value: item.Desgravamen },
                                    { type: String, value: item.Familiares },
                                    { type: String, value: item.Rebajas_Adicionales },
                                    { type: String, value: item.Rebajas },
                                    { type: String, value: item.Retencion },
                                    { type: String, value: item.Variacion },
                                    { type: String, value: item.Mes },
                                    { type: String, value: item.Anio }
                                ]
                            };
                        }))
                    }
                ]

            }).saveAs({
                fileName: "FormulariosARI"
            });
        });
    });
});
    
function searchAri() {
    
    $("#result-message").fadeOut();    

    if ($("#grupo").val() === ''){

        $("#result-message").fadeIn();   
        $("#result-message").html(BE_NO_GRUPO);    
    }
    else
    {
        var mes = $( "#mes" ).val();
        var anio = $( "#ano" ).val();
        var status = $( "#status" ).val();
        var data = { mes:mes,anio:anio,status:status };   
        
        $("#registro").fadeOut();
        $("#result-message").fadeOut();    
        $("#loading").fadeOut();
                             
        execute("Impuestos.listadoConsolidadoARI", processResultsAri, data);
    }
}

function procesarInit(data) {
   
    var periodo           = null;
    var ano               = null;    
    var ano2              = null;
    var mes               = null;
    var opciones_periodos = ""; 
    DATA = data.periodos;
    for(var i in data.periodos) {
        periodo = data.periodos[i];        
        ano = periodo.ano;
        mes = periodo.mes;
        if (ano2 != ano) {
            ano2 = ano;
            PERIODOS[ano+""] = []; 
            opciones_periodos += "<option>" + ano + "</option>";
        }
        PERIODOS[ano+""].push(mes);
    }
    $("#ano").html(opciones_periodos);

    actualizarMeses();
}

function actualizarMeses() {
    var opciones_meses = "";
    var ano            = $("#ano").val();
    var periodo        = PERIODOS[ano];
    var mes            = null;
    for(var i in periodo) {
        mes = periodo[i];
        opciones_meses += "<option>" + mes + "</option>";
    }   
    $("#mes").html(opciones_meses);

}

function processResultsAri(data) {    
    var clientes = $(".consultarlistadoari"); 
    if (data.length > 0) {
        var item = "";
        var content = "";
        content += "<table id='reporte' class='table table-striped'>";
        content += "<thead>";
        content += "<tr>";
        content += "<th class='center'>Categoria</th>";
        content += "<th class='center'>Codigo</th>";
        content += "<th class='center'>Nombre</th>";
        content += "<th class='center'>Remuneraciones</th>";
        content += "<th class='center'>Desgravamen_Unico</th>";
        content += "<th class='center'>Desgravamen</th>";
        content += "<th class='center'>Familiares</th>";
        content += "<th class='center'>Rebajas_Adicionales</th>";
        content += "<th class='center'>Rebajas</th>";
        content += "<th class='center'>Retencion</th>";
        content += "<th class='center'>Variacion</th>";
        content += "<th class='center'>Mes</th>";
        content += "<th class='center'>Anio</th>";
        content += "</tr>";
        content += "</thead>";
        
        for(var i in data) {
            var totaldesgravamen;
            var rebajaimp;
            var remuneraciones;
            var desgravamenunico;
            item = data[i];
            var categoria = item.codigo;
            var unico = item.unico;
            categoria = categoria.charAt(0);
            
            if(categoria=="E" || categoria=="e"){
                categoria ="Empleado";
            }
            else if (categoria=="F" || categoria=="f"){
                categoria ="Funcionario";
            }
            else if (categoria=="A" || categoria=="a"){
                categoria ="Aprendiz";
            }            
            else if (categoria=="C" || categoria=="c"){
                categoria ="Contratado";
            }            
            else if (categoria=="P" || categoria=="p"){
                categoria ="Pasante";
            }
            
            if(unico==1){
                desgravamenunico = item.desgravamen_unico * item.ut;  
            }
            else if(unico==0){
                desgravamenunico = 0;    
            }

            remuneraciones   =  parseFloat(item.remuneraciones1)+parseFloat(item.remuneraciones2)+parseFloat(item.remuneraciones3)+parseFloat(item.remuneracion_estimada);
            totaldesgravamen =  parseFloat(item.desgravamen1)+parseFloat(item.desgravamen2)+parseFloat(item.desgravamen3)+parseFloat(item.desgravamen4)+parseFloat(desgravamenunico);
            rebajaimp        = (parseFloat(item.rebaja1_cantidad)*10*item.ut)+(parseFloat(item.rebaja2_cantidad)*10*item.ut)+parseFloat(item.rebaja3_cantidad);
            
            content += "<tr class='data' id = '"+item.id+"'>"; 
            content += "<td class='center' >";
            content += categoria;
            content += "</td>";
            content += "<td class='center'>";
            content += item.colaborador;
            content += "</td>";       
            content += "<td class='center'>";
            content += item.nombre+" "+item.apellido;
            content += "</td>";       
            content += "<td class='center' >";
            content += parseFloat(remuneraciones);
            content += "</td>";
            content += "<td class='center' >";
            content += parseFloat(desgravamenunico);
            content += "</td>";
            content += "<td class='center' >";
            content += parseFloat(totaldesgravamen);
            content += "</td>";   
            content += "<td class='center' >";
            content += parseFloat(item.rebaja2_cantidad);
            content += "</td>";
            content += "<td class='center' >";
            content += parseFloat(item.rebaja3_cantidad);
            content += "</td>";            
            content += "<td class='center' >";
            content += parseFloat(rebajaimp);
            content += "</td>";
            content += "<td class='center' >";
            content += item.porcentaje_impuesto+" %";
            content += "</td>"; 
            content += "<td class='center' >";
            content += item.total_variacion+" %";
            content += "</td>"; 
            content += "<td class='center' >";
            content += item.mes;
            content += "</td>"; 
            content += "<td class='center' >";
            content += item.ano;
            content += "</td>";
            content += "</tr>";
           }
        
        $("#loading").fadeOut();
        content += "</table>";
    } else {
        content = "<p>" + BE_NO_DATA_BCV + "</p>";
        $("#loading").fadeOut();
    }
     clientes.html(content);
     
}


function processResultsCat(data) { 
    var a="a";    
    if (data.length > 0) {
        var item = "";    
        for(var i in data) {            
          item = data[i];                      
        }
    }    
    return a;
}