var ENTITY = "Auditoria";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [ 
        {title: 'Fecha', name: 'fecha'},
        {title: 'Usuario', name: 'usuario'},
        {title: 'Acci&oacute;n', name: 'accion'},
        {title: 'Resultado', name: 'resultado'},
        {title: 'IP', name: 'direccion_ip'},
        {title: 'Objeto', name: 'objeto'},
        {title: 'Traza', name: 'traza'}					        
        ],
        editable: false
    } ;
$(function() {
    setTitle("Administraci&oacute;n > Log de Auditoria");
    loadEntities({order: 'fecha desc'});
});