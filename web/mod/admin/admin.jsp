<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/admin/admin.css" rel="stylesheet" />        
<script src="mod/admin/admin.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">
    
    <h4>Men&uacute; de Administraci&oacute;n</h4>
    
    <div id="submenu" class="menu-container"></div>
    
    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>    
    
</div>     
        
<%
    } //if (currentUser != null)
%>    
