$(function() {
    setTitle("Administraci&oacute;n");
    execute("Menu.obtener", processMenuAdmin, {context: 'admin'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
