    
var BE_NO_DATA_BCV       = "No existe matriz registrada para el perfil";
var BE_NO_GRUPO          = "Debe seleccionar un grupo";

$(function() {    
        
    setTitle("Administraci&oacute;n > Matriz");    
    
    $("#result-message").fadeOut();    
    $("#loading").fadeOut();
    var action = 'Listado.gruposDirectorioActivo';
    var field = 'grupo';
    prepareSelect(action, field);   
});



function searchMatriz() {
    
    $("#result-message").fadeOut();    

    if ($("#grupo").val() === ''){

        $("#result-message").fadeIn();   
        $("#result-message").html(BE_NO_GRUPO);    
    }
    else
    {
        var grupo   = $("#grupo").val(); 
        var data = { grupo:grupo };   
        $("#registro").fadeOut();
        $("#result-message").fadeOut();    
        $("#loading").fadeOut();
        execute("Menu.obtenerLista", processResultsMatriz, data);
    }
}



function processResultsMatriz(data) {    
   
    var clientes = $(".consultamatriz"); 
    
    if (data.length > 0) {
        var item = "";
        var content = "";
        


        content += "<table id='reporte' class='table table-striped'>";
        content += "<thead>";
        content += "<tr>";
        content += "<th class='center'>Titulo</th>";
        content += "<th class='center'>Acci&oacute;n</th>";
        content += "<th class='center'>Contexto</th>";
        content += "<th class='center'>Posici&oacute;n</th>";
        content += "<th class='center'>Permiso</th>";
        content += "<th class='center'>Activo</th>";
        content += "</tr>";
        content += "</thead>";
        
        for(var i in data) {
            
            item = data[i];
       

            content += "<tr class='data' id = '"+item.titulo+"'>"; 
            content += "<td class='center'>";
            content += item.titulo;
            content += "</td>";       
            content += "<td class='center'>";
            content += item.accion;
            content += "</td>";       
            content += "<td class='center'>";
            content += item.contexto;
            content += "</td>";  
            content += "<td class='center' >";
            content += item.posicion;
            content += "</td>";   
            content += "<td class='center' >";
            content += item.permiso;
            content += "</td>";   
            content += "<td class='center' >";
            content += item.activo;
            content += "</td>";                      
            content += "</tr>";

        }
        
        $("#loading").fadeOut();
        content += "</table>";
                           
    } else {
        content = "<p>" + BE_NO_DATA_BCV + "</p>";
        $("#loading").fadeOut();
    }
        
    clientes.html(content);

}