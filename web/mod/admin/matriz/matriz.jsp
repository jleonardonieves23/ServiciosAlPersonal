<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        
        //
        String nombre = usuario.obtenerNombreCompleto();
        String cedula = usuario.rif;
        String codigo = usuario.codigo;
        String fecha = Util.getCurrentDate();
%>

<link href="mod/admin/matriz/matriz.css" rel="stylesheet" />        
<script src="mod/admin/matriz/matriz.js"></script>
    
<script type="text/javascript" src="lib/ext/shieldui-all.min.js"></script>
<script type="text/javascript" src="lib/ext/jszip.min.js"></script>

<script type="text/javascript">


    
    

   jQuery(function ($) {
        $("#exportButtonxls").click(function () {
            // parse the HTML table element having an id=exportTable
            var i = 0;
            var dataSource = shield.DataSource.create({
                data: "#reporte",
                schema: {
                    type: "table",
                    fields: {
                    Titulo:  { type: String },
                    Acci�n: { type: String },
                    Contexto: { type: String },
                    Posici�n: { type: String },
                    Permiso: { type: String },
                    Activo: { type: String }
                    }
                }
            });

            // when parsing is done, export the data to Excel
            dataSource.read().then(function (data) {
                new shield.exp.OOXMLWorkbook({
                    author: "SITCAM - Banco Exterior",
                    worksheets: [
                        {
                            name: "Table",
                            rows: [
                                {
                                    cells: [
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Titulo"
                                        },
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Acci�n"
                                        },
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Contexto"
                                        },
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Posici�n"
                                        },
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Permiso"
                                        },
                                        {
                                            style: {
                                                bold: true
                                            },
                                            type: String,
                                            value: "Activo"
                                        }                                        
                                        
                                    ]
                                }
                           
                           
                                
                            ].concat($.map(data, function(item) {
                               if (i>0){
                                    
                                return {
                                    cells: [
                                        { type: String, value: item.Titulo },
                                        { type: String, value: item.Acci�n },
                                        { type: String, value: item.Contexto },
                                        { type: String, value: item.Posici�n },
                                        { type: String, value: item.Permiso },
                                        { type: String, value: item.Activo }
                                    ]
                                };
                            }
                            i=i+1;
                                
                            }))
                        }
                    ]
                }).saveAs({
                    fileName: "MatrizPerfiles"
                });
            });
        });
        
        $("#exportButton").click(function () {
            // parse the HTML table element having an id=exportTable
            var dataSource = shield.DataSource.create({
                data: "#reporte",
                schema: {
                    type: "table",
                    fields: {
                        Titulo: { type: String },
                        Acci�n: { type: String },
                        Contexto: { type: String },
                        Posici�n: { type: String },
                        Permiso: { type: String },
                        Activo: { type: String }
                    }
                }
            });

            // when parsing is done, export the data to PDF
            dataSource.read().then(function (data) {
                var pdf = new shield.exp.PDFDocument({
                    author: "SITCAM-Banco Exterior",
                    created: new Date()
                });

                pdf.addPage("a2", "portrait");

                pdf.table(
                    50,
                    50,
                    data,
                    [
                        { field: "Titulo", title: "Titulo", width: 200 },
                        { field: "Acci�n", title: "Acci�n", width: 200 },
                        { field: "Contexto", title: "Contexto", width: 200 },
                        { field: "Posici�n", title: "Posici�n", width: 100 },
                        { field: "Permiso", title: "Permiso", width: 200 },
                        { field: "Activo", title: "Activo", width: 100 }
                    ],
                    {
                        margins: {
                            top: 50,
                            left: 50
                        }
                    }
                );

                pdf.saveAs({
                    fileName: "MatrizPerfiles"
                });
            });
        });
    });
</script>

<div class="submenu menu-content">    
<h4>Reporte de Matriz de Perfiles</h4> 
    
    <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>        
    
    <div class='registro'>              
       
    
    <table cellspacing='0' > 
            <tr>
                <th class="center" style="width: 20%">Grupo</th>
                <th></th>
            </tr>
            <tr>
                 <td class=''>                
                    <select id='grupo'  class='select-grupo form-control' style="width:160px">
                        <option value="">Seleccione</option>
                    </select>
                </td>
                <td><button class="btn btn-default" type="submit" onclick="searchMatriz()"><i class="glyphicon glyphicon-search"></i></button></td>
                <td>
                    <div id="result-message"></div>
                    <center id="loading"><img src="img/ajax-loader.gif"></center>
                </td>
            </tr>
     </table>
     </div>  
    <div class='registro'>     
     <div class='consultamatriz' />
            
                    
    </div>
   
    <br />  
    <div class="input-group-btn">
       <button id="exportButton"  class='btn btn-primary'>Exportar a PDF</button> 
       &nbsp;
       <button id="exportButtonxls" class='btn btn-primary'>Exportar a Excel</button>
    </div>
    
    <br />  
  
    <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>    
</div>  
</div>

<%
    } //if (currentUser != null)
%>    
