var ENTITY = "Menu";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [ 
        {title: 'T&iacute;tulo', name: 'titulo'},
        {title: 'Acci&oacute;n', name: 'accion'},
        {title: 'Contexto', name: 'contexto'},
        {title: 'Posici&oacute;n', name: 'posicion'},
        {title: 'Permiso', name: 'permiso', select: 'Listado.gruposDirectorioActivo'},
        {title: 'Activo', name: 'activo', select: 'Listado.bivalente' }					        
        ] } ;
$(function() {
    setTitle("Administraci&oacute;n del Men&uacute;");
    loadEntities({order: 'accion'});
});
