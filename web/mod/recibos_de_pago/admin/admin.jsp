<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/recibos_de_pago/admin/admin.css" rel="stylesheet" />        
<script src="mod/recibos_de_pago/admin/admin.js"></script>
<script>
        var deco = '<%=deco%>';
        var backDeco = '<%=backDeco%>';
</script>

<div class="submenu menu-content">

    <h4>Recibos</h4>
    <label>Colaborador:</label>
    <table><tr><td><input type='text' class='form-control' id='colaborador' placeholder="colaborador"  /></td><td>&nbsp;<button class='btn btn-warning' onclick="buscarRecibos()">Buscar</button></td></tr></table>
    <br />
    <span id="loading"><img src="img/ajax-loader.gif"></span>
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>
    <div class="recibos"></div>    
</div>    

<div id="print-section" style="display: none;">
    <form id="print-form" method="post" action="pdf" target="_blank">
        <input id="print-input" type="hidden" name="data" />
        <input id="print-filename" type="hidden" name="filename" />
    </form>
</div>

<%
    } //if (currentUser != null)
%>    
