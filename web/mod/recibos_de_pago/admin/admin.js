var optionsMap        = [];
var ENCABEZADOS       = null;
var DETALLES          = null;
var TOTAL_DEDUCCIONES = 0;
var TOTAL_PAGOS       = 0;
var TOTAL_NETO        = 0;
var UBICACION         = null;
var CARGO             = null;
var CEDULA            = null;
var NOMBRE            = null;    
var FECHA             = null;    
var CODIGO            = null;    
var PROCESO           = null;

$(function() {
    setTitle("Administraci&oacute;n > Consultar Recibos de Pagos de Colaborador");  
    $("#colaborador").on("keypress", function(e) {       
        if ( e.which == 13 ) {
            buscarRecibos();
        }       
    })    
    enmascararColaborador();
    $("#loading").hide();
});

function buscarRecibos() {
    var colaborador = $("#colaborador").val();
    $("#loading").show();
    execute("ReciboPago.obtenerRecibos", procesarRecibosPago, {colaborador:colaborador});   
}

function procesarRecibosPago(data) {    
    $("#loading").hide();
    if (data.length > 0) {
        var html        = '';
        var year        = null;
        var obj         = null;
        var fecha       = null;
        var y           = null;
        var m           = null;
        var descripcion = null;
        var defaultYear = null;
        var yearOptions = '';
        var options     = ''; 

        ENCABEZADOS = data;
        for(var d in ENCABEZADOS) {
            obj         = ENCABEZADOS[d];
            descripcion = obj.DESCRIPCION_RECIBO;
            fecha       = obj.FECMOV.split(" ")[0].split("-");        
            y           = fecha[0];
            m           = fecha[1];
            if (y != year) {
                if (defaultYear == null) defaultYear = y;
                optionsMap[year] = options;
                options          = '';
                year             = y;
                yearOptions     += '<option>'+y+'</option>';            
            }
            options += '<option value=' + d + '>' + descripcion + '</option>'
        }   
        optionsMap[year] = options;
        options          = optionsMap[defaultYear];
        html            += "<label>A&ntilde;o:</label><select class='form-control' id='years' onchange='actualizarRecibos()'>" + yearOptions + "</select>";
        html            += "<br />";
        html            += "<label>Recibo:</label><select class='form-control' id='bill' onchange='actualizarDetalles()'>" + options + "</select>";
        html            += "<br />";
        html            += "<div id='detalles'></div>";
        $('.recibos').html(html);    
        data = ENCABEZADOS[0];
        actualizarDatos(data);   
        var colaborador = $("#colaborador").val();
        data.colaborador = colaborador;
        execute("ReciboPago.obtenerDetallesRecibos", procesarDetallesRecibosPago, data);  
    } else {
        $(".recibos").html("No hay datos para mostrar."); 
        $("#detalles").html(""); 
    }
}

function actualizarRecibos() {
    var year    = $("#years").val();
    var options = optionsMap[year];
    $("#bill").html(options);
    actualizarDetalles();
}

function actualizarDetalles() {
    var recibo = $("#bill").val();
    var data = ENCABEZADOS[recibo];
    actualizarDatos(data);
    var colaborador = $("#colaborador").val();
    data.colaborador = colaborador;    
    execute("ReciboPago.obtenerDetallesRecibos", procesarDetallesRecibosPago, data);
}

function actualizarDatos(data) {
    CODIGO            = data.TRAB_FICTRA;
    UBICACION         = data.DESDEP;
    CARGO             = data.DESCAR;
    CEDULA            = data.CEDULA;
    NOMBRE            = data.APELLNOMB;
    TOTAL_PAGOS       = data.TOTAL_ASIG;
    TOTAL_DEDUCCIONES = data.TOTAL_DEDUC;
    TOTAL_NETO        = data.TOTAL_NETO;
    FECHA             = data.FECMOV;   
    TEXTORECIBO       = data.TEXTORECIBO;
    PROCESO           = data.PROC_TIPPRO;
}

function procesarDetallesRecibosPago(data) {
    var credito    = null;
    var debito     = null;
    var decripcion = null;
    var unidades   = null;
    var row        = null;
    var html       = "";    
    var detalles   = data.detalles;    
    var sueldo     = data.sueldo;
    var ano        = FECHA.substring(0, 4);
    var mes        = monthName(FECHA.substring(5, 7)).toUpperCase();  
    html += "<br />";   
    
    html += "<table width='100%' cellspacing='0'>";
    html += "<tr>";
    html += "<td>";
    html += "<img src='img/LogoExterior.png' />";
    html += "</td>";
    html += "<td>";
    html += "<img src='img/grupo_if.gif' />";
    html += "</td>";
    html += "</tr>";
    html += "</table>";
    html += "<div id='print-data'>";   
    html += "<div class='separador1'></div>";
    html += "<h2>Vicepresidencia de Conexi&oacute;n Humana</h2>"; 
    html += "<br />";  
    
    html += "<div class='separador2'></div>";
    
    html += "<table cellspacing='0' width='100%' class='table-header'>";
    
    
    html += "<tr>";
    html += "<td>";
    html += "<h6>nombre</h6>";
    html += "<h3>" + NOMBRE + "</h3>";
    html += "</td>";
    html += "<td class='sep'></td>";
    html += "<td>";
    html += "<h6>c&oacute;digo</h6>";
    html += "<h4>" + CODIGO + "</h4>";
    html += "</td>";
    html += "<td class='sep'></td>";
    html += "<td>";
    html += "<h6>c&eacute;dula</h6>";
    html += CEDULA;
    html += "</td>";
    html += "</tr>";
    
    html += "<tr><td>&nbsp;</td></tr>";
    
    html += "<tr>";
    html += "<td>";
    html += "<h6>unidad</h6>";
    html += UBICACION;
    html += "</td>";
    html += "<td class='sep'></td>";
    html += "<td>";
    html += "<h6>cargo</h6>";
    html += CARGO;
    html += "</td>";
    html += "<td class='sep'></td>";
    html += "<td>";
    html += "<h6>fecha recibo</h6>";
    html += dateFormat(FECHA);
    html += "</td>";
    html += "</tr>";        
 
    html += "</table>";
    html += "<br />";  
    html += "<br />";  
    
    html += "<div class='separador3'></div>";
    
    html += "<table width='100%' class='recibo'><tr><td align='center'>";
    html += "<b class='seccion'>RECIBO DE PAGO " +  mes + "  " + ano + "</b>";
    html += "</td></tr></table>";
    
    if (PROCESO != 53) {
        html += "<table width='100%' cellspacing='0'><tr><td><h4>Sueldo:</h4></td><td align='right'><h4>"+numberFormat(sueldo)+"</h4></td></tr></table>";
    }
    
    html += "<table class='table table-striped' width='100%' style='min-width:600px; margin-top: 0;' cellspacing='0'>";   
    html += "<tr><th>DESCRIPCI&Oacute;N</th><th align='right'>CANTIDAD</th><th align='right'>ASIGNACI&Oacute;N</th><th align='right'>DEDUCCI&Oacute;N</th></tr>";
    for (var i in detalles) {
        row        = detalles[i];
        credito    = row.ASIGNACION == 0 ? "" : numberFormat(row.ASIGNACION);
        debito     = row.DEDUCCION  == 0 ? "" : numberFormat(row.DEDUCCION);
        decripcion = row.CONCEPTO.toLowerCase().toUpperCase();
        unidades   = row.UNIDADES.toLowerCase().toUpperCase();
        if (unidades.substring(0,1)==',') unidades = '0' + unidades;
        //alert(unidades);
        html += "<tr>" +  "<td class='cell'>" + decripcion + "</td>" +  "<td align='right' class='cell'>" + unidades + "</td>" +  "<td align='right' class='cell'>" + credito + "</td>" + "<td align='right' class='cell'>" + debito + "</td>"  + "</tr>";
    } 

    html += "<tr><td colspan='2'><h4>Totales: </h4></td><td align='right'><h4>" + numberFormat(TOTAL_PAGOS) + "</h4></td><td align='right'><h4>" + numberFormat(TOTAL_DEDUCCIONES) + "</h4></td></tr>";
    html += "<tr><td colspan='3'><h2>Total Neto: </h2></td><td align='right'><h2>" + numberFormat(TOTAL_NETO) + "</h2></td></tr>";
    html += "</table>";
    
    if(TEXTORECIBO) {
      html += "<h4>" + TEXTORECIBO + "</h4>";    
    }
    
    html += "</div>";
    html += "<div class='btn-back' style='float: left;'><a href='/'>" + backDeco +" Regresar</a></div>  <div align='right'><a href='#print' onclick='print()' name='print'><span class='glyphicon glyphicon-print'></span> Imprimir</a></div>";
    $("#detalles").html(html);   
}

function print() {
    var data =   $("#print-data").html(); 
    var filename = "BancoExterior-ReciboPago-"+CODIGO.trim()+"-"+$("#bill").text();
    $("#print-input").val(data);
    $("#print-filename").val(filename);
    $("#print-form").submit();
}
