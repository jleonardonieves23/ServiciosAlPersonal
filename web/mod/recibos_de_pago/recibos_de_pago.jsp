<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/recibos_de_pago/recibos_de_pago.css" rel="stylesheet" />        
<script src="mod/recibos_de_pago/recibos_de_pago.js"></script>
<script>
        var deco = '<%=deco%>';
        var backDeco = '<%=backDeco%>';
</script>

<div class="submenu menu-content">

    <h4>Recibos</h4>
    <br />  
    <div class="recibos"><img src='img/ajax-loader.gif' /></div>
    
</div>    

<div id="print-section" style="display: none;">
    <form id="print-form" method="post" action="pdf" target="_blank">
        <input id="print-input" type="hidden" name="data" />
        <input id="print-filename" type="hidden" name="filename" />
        <input id="print-qr" type="hidden" name="qr" />
    </form>
</div>

<%
    } //if (currentUser != null)
%>    
