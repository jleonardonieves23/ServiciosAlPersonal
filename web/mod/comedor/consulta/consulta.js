$(function() {
    setTitle("Comedor > Estado de Cuenta");
});

function searchComedor() {
    var m = $("#month").find(":selected").val();
    var y = $("#year").find(":selected").val();  
    var data = { month:m, year:y };    
    $("#results").fadeOut();
    $("#result-message").fadeOut();    
    $("#loading").fadeIn();
    execute("Comedor.buscar", processResultsComedor, data);
}

function processResultsComedor(data) {    
    var hasData = data && data.status == "true";  
    $("#loading").fadeOut();
    if (hasData && data.rows.length > 0) {
        var total = numberFormat(data.total);
        $(".total").html(total);
        $("tr.data").remove();
        var row           = null;
        var tags          = "";
        var cantidad      = 0;
        var precio        = 0;
        var monto         = 0;
        var fecha         = "";
        var hora         = "";
        var articulo      = "";
        var factura       = "";
        var turno         = "";
        var daily         = [];
        var articles      = [];
        var turns         = [];
        var item          = null;
        var points        = null;
        var porcentaje    = 0;
        var totalCantidad = 0;
        var totalMonto    = 0;
        var totals        = [];
        var name          = "";
        var last          = "";      
        var cantidadTurno = 0;
        
        totals["general"] = 0;
        for(var i in data.rows) {
            row = data.rows[i];
            cantidad = converToNumber(row.cantidad);
            precio   = converToNumber(row.monto_articulo);
            monto    = precio * cantidad;
            fecha    = dateFormat(row.fecha);
            hora     = row.hora;
            articulo = row.articulo;
            factura  = row.factura;
            turno    = row.turno;
            cantidadTurno  = 0;
            
            totals["general"] += monto;
            
            if (last != factura) {
                last = factura;
                cantidadTurno  = 1;
            }
            
            //daily
            item = daily[fecha];
            item = typeof(item) == "object" ? item : { name: "",  cantidad: 0, monto: 0, cantidadDesayunos: 0, montoDesayunos: 0, cantidadAlmuerzos: 0, montoAlmuerzos: 0};
            item.name = fecha;
            item.monto += monto;            
            if (turno.toUpperCase().trim() == "DESAYUNO") {
                item.cantidadDesayunos += cantidadTurno;
                item.montoDesayunos += monto;
            } else {
                item.cantidadAlmuerzos += cantidadTurno;
                item.montoAlmuerzos += monto;                
            }
            daily[fecha] = item;

            //articles
            item = articles[articulo];
            item = typeof(item) == "object" ? item : { name: "", cantidad: 0, monto: 0};
            item.name = articulo;
            item.monto += monto;
            item.cantidad += cantidad;
            articles[articulo] = item;
            
            //turns
            item = turns[turno];
            item = typeof(item) == "object" ? item : { name: "", cantidad: 0, monto: 0};
            item.name = turno;
            item.monto += monto;
            item.cantidad += cantidadTurno;
            turns[turno] = item;
            
            tags = "";
            tags += "<tr class='data'>";
            tags += "<td>";
            tags += fecha;
            tags += "</td>";  
            tags += "<td>";
            tags += hora;
            tags += "</td>";            
            tags += "<td>";
            tags += factura;
            tags += "</td>";
            tags += "<td>";
            tags += turno;
            tags += "</td>";            
            tags += "<td>";
            tags += articulo;
            tags += "</td>";            
            tags += "<td class='numero'>";
            tags += numberFormat(cantidad);
            tags += "</td>";    
            tags += "<td class='numero'>";
            tags += numberFormat(precio);
            tags += "</td>";                
            tags += "<td class='numero'>";
            tags += numberFormat(monto);
            tags += "</td>";                      
            tags += "</tr>";
            $("#details").append(tags);                       
        }
        
        var days = 0;
        //daily listt
        totals["diario-monto"] = 0;
        for(var i in daily) {
            days++;
            item = daily[i];
            fecha = i;
            monto = item.monto;
            totals["diario-monto"] += monto;            
            tags = "";
            tags += "<tr class='data'>";
            tags += "<td rowspan='3'>";
            tags += fecha;
            tags += "</td>";            
            tags += "<td>";
            tags += "</td>";            
            tags += "<td>";            
            tags += "</td>"; 
            tags += "<td class='numero'><b>"
            tags += numberFormat(monto);
            tags += "</b></td>";                      
            tags += "</tr>";
            
            monto    = item.montoDesayunos;
            cantidad = item.cantidadDesayunos;            
            tags += "<tr class='data desayuno'>";          
            tags += "<td>";
            tags += "DESAYUNO";
            tags += "</td>";                
            tags += "<td class='numero'>";
            tags += cantidad;
            tags += "</td>";                
            tags += "<td class='numero'>"
            tags += numberFormat(monto);
            tags += "</td>";                      
            tags += "</tr>";
            
            monto    = item.montoAlmuerzos;
            cantidad = item.cantidadAlmuerzos;            
            tags += "<tr class='data almuerzo'>";         
            tags += "<td>";
            tags += "ALMUERZOS";
            tags += "</td>";                
            tags += "<td class='numero'>";
            tags += cantidad;
            tags += "</td>";                
            tags += "<td class='numero'>"
            tags += numberFormat(monto);
            tags += "</td>";                      
            tags += "</tr>";
            
            $("#daily").append(tags);            
        }
        /*
        naranja: #F58233 
        azul:    #006CB7
        */
        $("#daily tr").css("backgroundColor", "#ffffff");
        $("#daily tr").css("borderBottom", "1px dashed #cccccc");
        
        $(".desayuno").css("backgroundColor", "#FEF0E7");
        $(".almuerzo").css("backgroundColor", "#E6F4FF");

        /*
        $(".desayuno").css("backgroundColor", "#cceeff");
        $(".almuerzo").css("backgroundColor", "#ffeecc");
        */
       
        //articles list
        totals["articulos-cantidad"] = 0;
        totals["articulos-monto"]    = 0;
        for(var i in articles) {
            item = articles[i];
            articulo = i;
            monto = item.monto;
            cantidad = item.cantidad;
            totals["articulos-cantidad"] +=  cantidad;
            totals["articulos-monto"]    += monto;            
            tags = "";
            tags += "<tr class='data'>";
            tags += "<td>";
            tags += articulo;
            tags += "</td>";            
            tags += "<td class='numero'>";
            tags += cantidad;
            tags += "</td>";             
            tags += "<td class='numero'>"
            tags += numberFormat(monto);
            tags += "</td>";                      
            tags += "</tr>";
            $("#articles").append(tags);            
        }
        
        //turns list
        totals["turnos-cantidad"] = 0;
        totals["turnos-monto"]    = 0;        
        for(var i in turns) {
            item = turns[i];
            turno = i;
            monto = item.monto;
            cantidad = item.cantidad;
            totals["turnos-cantidad"] += cantidad;
            totals["turnos-monto"]    += monto;                        
            tags = "";
            tags += "<tr class='data'>";
            tags += "<td>";
            tags += turno;
            tags += "</td>";            
            tags += "<td class='numero'>";
            tags += cantidad;
            tags += "</td>";             
            tags += "<td class='numero'>"
            tags += numberFormat(monto);
            tags += "</td>";                      
            tags += "</tr>";
            $("#turns").append(tags);            
        }
        
        ///////////////////////////////////////////////////////////////////////
        //daily        
        totalMonto    = totals["diario-monto"];        
        points = [];                    
        for(var i in daily) {           
            item = daily[i];
            name = item.name;
            name = name.split("/");
            name = name[0] + "/" + name[1];
            monto = item.monto;            
            points[points.length]  = { label: name, y: monto, legendText: name + ": " + monto};
        }
        
        $("#daily-chart-1").CanvasJSChart({ 
		title: { 
			text: "Consumo por fecha",
			fontSize: 24
		}, 
		axisX: {
			interval: 1,
                        title: "Fecha",
                        includeZero: false,
                        maximum: days,
                        labelFontSize: 10
		},                
		axisY: { 
			title: "Monto",
                        includeZero: false,
                        interval: 500,                        
		}, 
		legend: { 
			verticalAlign: "center", 
			horizontalAlign: "right"
		}, 
		data: [ 
		{ 
			type: "line", 
			showInLegend: false, 
			toolTipContent: "{name}: {y}", 
			indexLabel: "Bs. {y}", 
			dataPoints: points
		} 
		] 
	});        
        
        
        ///////////////////////////////////////////////////////////////////////
        //articles        
        totalMonto    = totals["articulos-monto"];      
        totalCantidad = totals["articulos-cantidad"];                
        points = [];            
        for(var i in articles) {           
            item = articles[i];
            name = i.trim();
            monto = item.monto;
            cantidad = item.cantidad;
            porcentaje  = Math.round(cantidad / totalCantidad * 100);
            
            points[points.length]  = { label: name, y: porcentaje, legendText: name + ": " + cantidad};
        }
        
        $("#articles-chart-1").CanvasJSChart({                 
		title: { 
			text: "Consumo por articulo (Cantidad)",
			fontSize: 24
		}, 
		axisY: { 
			title: "Artículo" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right",
                        fontFamily: "Century Gothic",
                        fontSize: 11                       
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{name}: {y}", 
			indexLabel: "{y} %", 
			dataPoints: points
		} 
		] 
	});        
        
        
        points = [];  
        for(var i in articles) {           
            item = articles[i];
            name = i.trim();
            monto = item.monto;
            cantidad = item.cantidad;
            porcentaje  = Math.round(cantidad / totalCantidad * 100);
            
            points[points.length]  = { label: name, y: porcentaje, legendText: name + ": " + numberFormat(monto)};
        }
        
        $("#articles-chart-2").CanvasJSChart({ 
		title: { 
			text: "Consumo por articulo (Monto)",
			fontSize: 24
		}, 
		axisY: { 
			title: "Artículo" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right",
                        fontFamily: "Century Gothic",
                        fontSize: 11
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{name}: {y}", 
			indexLabel: "{y} %", 
			dataPoints: points
		} 
		] 
	});   
                                    
        ///////////////////////////////////////////////////////////////////////
        //turns        
        totalCantidad = totals["turnos-cantidad"];
        totalMonto    = totals["turnos-monto"];
        
        points = [];            
        for(var i in turns) {           
            item = turns[i];
            turno = i.trim();
            monto = item.monto;
            cantidad = item.cantidad;
            porcentaje  = Math.round(cantidad / totalCantidad * 100);
            
            points[points.length]  = { label: turno, y: porcentaje, legendText: turno + ": " + cantidad};
        }
        
        $("#turns-chart-1").CanvasJSChart({ 
		title: { 
			text: "Consumo por turnos (Cantidad)",
			fontSize: 24
		}, 
		axisY: { 
			title: "Tuno" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right",
                        fontFamily: "Century Gothic",
                        fontSize: 11 
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{name}: {y}", 
			indexLabel: "{y} %", 
			dataPoints: points
		} 
		] 
	});        
        
        
        points = [];            
        for(var i in turns) {           
            item = turns[i];
            turno = i.trim();
            monto = item.monto;
            cantidad = item.cantidad;
            porcentaje  = Math.round(monto / totalMonto * 100);
            
            points[points.length]  = { label: turno, y: porcentaje, legendText: turno + ": " + numberFormat(monto)};
        }
        
        $("#turns-chart-2").CanvasJSChart({ 
		title: { 
			text: "Consumo por turnos (Monto)",
			fontSize: 24
		}, 
		axisY: { 
			title: "Tuno" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right",
                        fontFamily: "Century Gothic",
                        fontSize: 11 
		}, 
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{name}: {y}", 
			indexLabel: "{y} %", 
			dataPoints: points
		} 
		] 
	});  
        
        
        $("#results").fadeIn();    
        
    }  else {
        $("#result-message").html(BE_NO_DATA_FOUND);
        $("#result-message").fadeIn();
    }    
}