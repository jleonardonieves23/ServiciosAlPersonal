$(function() {
    setTitle("Comedor");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
