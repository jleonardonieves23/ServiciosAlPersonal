<%@page import="be.utils.Etiqueta"%>
<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();        
%>
<link href="mod/comedor/colaborador/colaborador.css" rel="stylesheet" />        
<script src="mod/comedor/colaborador/colaborador.js"></script>
<script>    
    $(function(){                
        $("#month option").filter(function() {
            return $(this).val() == <%=currentMonth%>; 
        }).prop('selected', true);
        
        
        $("#year option").filter(function() {
            return $(this).val() == <%=currentYear%>; 
        }).prop('selected', true);    
                     
    });
</script>

<h4>Estado de cuenta del servicio del comedor</h4>
<div class="menu-container">    

    <table class="params">
        <tr>
            <td>
                Colaborador:
                <input type='text' class='form-control' id='colaborador' placeholder="colaborador"  />
            </td>
            <td>
                Mes: 
                <select class="form-control" id="month">
                    <option value="1">Enero</option>
                    <option value="2">Febrero</option>
                    <option value="3">Marzo</option>
                    <option value="4">Abril</option>
                    <option value="5">Mayo</option>
                    <option value="6">Junio</option>
                    <option value="7">Julio</option>
                    <option value="8">Agosto</option>
                    <option value="9">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
                </select>
            </td>
            <td>
                A&ntilde;o:
                <select class="form-control" id="year">
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014">2014</option>
                    <option value="2015">2015</option>
                    <option value="2016">2016</option>
                    <option value="2017">2017</option>
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                </select>                
            </td>
            <td valign="bottom">
                <button class="btn btn-primary" onclick="searchComedor()">Buscar</button>
            </td>
        </tr>
    </table>
    <div class='info'><%=Etiqueta.$("BE_COMEDOR")%></div>
    <div id="results" style='display: none'>
        <h5>Colaborador</h5>        
        <h2 id='nombre'></h2>
        
        <h5>Consumo detallado</h5>        
        <div class="total-label">Este mes consumiste un monto total de Bs. <span class="total"> en el comedor</span></div> 
        <table class="table table-striped" id="details">
            <tr>
                <th>fecha</th>
                <th>hora</th>
                <th>factura</th>                                
                <th>turno</th>                 
                <th>art&iacute;culo</th>
                <th class="numero">cantidad</th>
                <th class="numero">precio unitario</th>
                <th class="numero">total</th>
            </tr>
        </table>
        <div id="details-chart"></div>
        
        <h5>Distribuci&oacute;n del consumo por d�a</h5>
        
        <table class="table table-striped" id="daily">
            <tr>
                <th>fecha</th>
                <th>turno</th>
                <th class="numero">cantidad</th>
                <th class="numero">total</th>
            </tr>
        </table>
        
        <table class="chart">
            <tr>
                <td><div id="daily-chart-1" ></div></td>
            </tr>
        </table>
        
        <h5>Distribuci&oacute;n del consumo por art�culo</h5>
        <table class="table table-striped" id="articles">
            <tr>
                <th>art&iacute;culo</th>
                <th class="numero">cantidad</th>
                <th class="numero">total</th>
            </tr>
        </table>
        <table class="chart">
            <tr>
                <td><div id="articles-chart-1" ></div></td>
            </tr>
            <tr>
                <td><div id="articles-chart-2" ></div></td>
            </tr>
        </table>
        
        <h5>Distribuci&oacute;n del consumo por turnos</h5>
        <table class="table table-striped" id="turns">
            <tr>
                <th>turno</th>
                <th class="numero">cantidad</th>
                <th class="numero">total</th>
            </tr>
        </table>
        <table class="chart">
            <tr>
                <td><div id="turns-chart-1" ></div></td>
            </tr>
            <tr>
                <td><div id="turns-chart-2" ></div></td>
            </tr>
        </table>
                
    </div>
    <div id="result-message"></div>
    
    <span id="loading" style='display:none;'><img src="img/ajax-loader.gif"></span>
    
    <div class="btn-back"><a href="?a=admin"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>    
</div>  

<%
    } //if (currentUser != null)
%>    
