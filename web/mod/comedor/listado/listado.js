$(function() {
    setTitle("Comedor > Listado Consolidado de Consumos");
    $('#colaborador').inputmask('Regex', { regex: "[0-9]{1,5}" });
});

function searchComedor() {
    var m = $("#month").find(":selected").val();
    var y = $("#year").find(":selected").val();  
    var colaborador = $("#colaborador").val();
    var report = "colaboradores";
    var data = { month:m, year:y, colaborador:colaborador, report:report };    
    $("#results").fadeOut();
    $("#result-message").fadeOut();    
    $("#loading").fadeIn();
    execute("Comedor.listar", processResultsComedor, data);
}

function processResultsComedor(data) {    
    var hasData = data.status == "true";  
    $("#loading").fadeOut();
    if (hasData) {
        var rows = data.rows;
        for(var i in rows) {
            rows[i].monto_neto        = numberFormat(rows[i].monto_neto);
            rows[i].monto_colaborador = numberFormat(rows[i].monto_colaborador);
            rows[i].monto_banco       = numberFormat(rows[i].monto_banco);
            rows[i].cantidad          = numberFormat(rows[i].cantidad);
            
            rows[i].monto_neto        = rows[i].monto_neto        != "NaN" ? rows[i].monto_neto        : "";
            rows[i].monto_colaborador = rows[i].monto_colaborador != "NaN" ? rows[i].monto_colaborador : "";
            rows[i].monto_banco       = rows[i].monto_banco       != "NaN" ? rows[i].monto_banco       : "";          
            rows[i].cantidad          = rows[i].cantidad          != "NaN" ? rows[i].cantidad          : "";          
        }                 
        var options = {
            editable :  false, 
            fieldId  :  "id", 
            columns  :  data.columns,
            export :  true
        };
        var table = createTable(rows, options);         
        var filter = table.indexOf("Colaborador") > -1  ? "<h2 style='float: left; margin-top: 20px; margin-bottom: 20px;'>Listado</h2>" : "";
        $(".listdata").html(table);                          
        $(".table-filter-wrapper").html(filter);
        $(".table-export").css("float", "left");
        $(".table-export").css("margin-top", "10px");
        $("#results").fadeIn();            
    }  else {
        
        $(".table-filter-wrapper").html("");
        $("#result-message").html(BE_NO_DATA_FOUND);
        $("#result-message").fadeIn();
    }    
}