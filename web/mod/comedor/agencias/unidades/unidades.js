var ENTITY = "Unidad";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [ 
        {title: 'Nombre', name: 'nombre'},
        {title: 'Centro de Costo', name: 'centro_costo'},
        {title: 'Tel&eacute;fono Local', name: 'telefono_local'},
        {title: 'Responsable', name: 'responsable'},
        {title: 'Correo Electr&oacute;nico', name: 'correo_electronico'},
        {title: 'Direcci&oacute;n', name: 'direccion'},
        {title: 'Unidad Superior', name: 'unidad_superior'}				        
        ]} ;
$(function() {
    setTitle("Comedor > Agencias > Administraci&oacute;n del Unidades");
    loadEntities({order: 'nombre'});
    
    execute("Entidad.grupos", processGrupos);    
});

function processGrupos(response) {
    fillOptions(".remote-select", response.data.items);
}


