$(function() {
    setTitle("Comedor > Agencias > Lotes");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor/agencias/lotes'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
