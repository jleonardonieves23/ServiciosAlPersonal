<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/comedor/agencias/pedidos/pedidos.css" rel="stylesheet" />        
<script src="mod/comedor/agencias/pedidos/pedidos.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">
    
    <h4>Men&uacute; de Pedidos</h4>
    
    <div id="submenu" class="menu-container"></div>
    
    <div class="btn-back"><a href="?a=comedor/agencias"><%=backDeco%> Regresar</a></div>    
    
</div>     
        
<%
    } //if (currentUser != null)
%>    
