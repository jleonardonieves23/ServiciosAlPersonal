$(function() {
    setTitle("Comedor > Agencias > Pedidos");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor/agencias/pedidos'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
