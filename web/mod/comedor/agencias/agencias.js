$(function() {
    setTitle("Comedor > Agencias");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor/agencias'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
