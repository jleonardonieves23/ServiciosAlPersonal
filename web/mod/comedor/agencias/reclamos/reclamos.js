$(function() {
    setTitle("Comedor > Agencias > Reclamos");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor/agencias/reclamos'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
