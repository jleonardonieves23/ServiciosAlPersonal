$(function() {
    setTitle("Comedor > Agencias > Reportes");
    execute("Menu.obtener", processMenuAdmin, {context: 'comedor/agencias/reportes'});        
});

function processMenuAdmin(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
