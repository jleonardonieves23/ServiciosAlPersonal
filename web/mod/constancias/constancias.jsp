<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String colaborador = usuario.id;
%>
<link href="mod/constancias/constancias.css" rel="stylesheet" />        
<script src="mod/constancias/constancias.js"></script>
<script>
        var deco = '<%=deco%>';
        var colaborador = '<%=colaborador%>';
</script>

<div class="submenu menu-content">
       
    <h4>Seleccione el tipo de constancia</h4>
    
    <table class="table table-striped params">
        <tr>
            <th></th>
            <th>Descripci&oacute;n</th>
            <th></th>
        </tr>

        <tr>
            <td align="center">
                <input name="tipo" type="radio" class='type' id="CkeckEmbAmer" value="4" onclick='updateCheck(this)' />
            </td>
            <td> 
                Embajada Americana
            </td>
            <td></td>
        </tr>

        <tr>
            <td align="center">
                <input name="tipo" type="radio" class='type' id="CkeckPoliticaHab" value="5"  onclick='updateCheck(this)' />
            </td>
            <td> 
                Pol&iacute;tica Habitacional
            </td>
            <td></td>
        </tr>

        <tr>
            <td align="center">
                <input name="tipo" type="radio" class='type' id="CkeckEspecifica"  value="6" onclick='updateCheck(this)' />
            </td>
            <td>
                Espec&iacute;fica
            </td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td>
                <label>A:</label>
                <input name="txtdetalleEsp" type="text" id="txtdetalleEsp" class="form-control"  maxlength="50" disabled="disabled"  onkeyup="checkSend()" />
            </td>
            <td></td>
        </tr>

        <tr>
            <td></td>
            <td >
                <label>Sueldo:</label>
                &nbsp;&nbsp;&nbsp;
                <input value="2" name="sueldo" type="radio" checked="checked" /> 
                Sin Sueldo                
                &nbsp;&nbsp;&nbsp;
                <input value="3" name="sueldo" type="radio"  /> 
                Mensual
                &nbsp;&nbsp;&nbsp;
                <input value="1" name="sueldo" type="radio" /> 
                Anual
            </td>
            <td></td>
        </tr>

    </table>
    
    <button onclick="solicitar()" class="btn btn-primary" id="solicitar" disabled="disabled">Solicitar</button>
    
    <hr >
    
    <h4>IMPORTANTE:</h4>
    <ul>
        <li>No se procesar&aacute;n constancias  con notas gen&eacute;ricas como &ldquo;A qui&eacute;n pueda interesar&quot;.</li>
        <li>Las Solicitudes de Constancias realizadas entre los d�as jueves y viernes, ser�n entregadas los d�as viernes de la semana siguiente a la que se realiza dicha solicitud.</li>
        <li>Todas las Solicitudes de Constancias que se realicen entre los d&iacute;as lunes y mi&eacute;rcoles, ser&aacute;n entregadas los d&iacute;as viernes de la misma semana en que se realiza dicha solicitud. </li>
    </ul>

    <hr >
    
    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>       

<%
    } //if (currentUser != null)
%>    
