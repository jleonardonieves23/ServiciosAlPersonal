$(function() {
    setTitle("Constancias de Trabajo > Solicitud");
});

function updateCheck(check) {
    if ($("#CkeckEspecifica").prop("checked")) {
        $("#txtdetalleEsp").attr("disabled", false);
    } else {
        $("#txtdetalleEsp").attr("disabled", true);
        $("#txtdetalleEsp").val("");
    }
    checkSend();       
}

function checkSend() {            
    var valid = false;
    valid |= $("#CkeckEmbAmer").prop("checked");
    valid |= $("#CkeckPoliticaHab").prop("checked");
    valid |= $("#CkeckEspecifica").prop("checked") && $("#txtdetalleEsp").val().trim() != "";    
    $("#solicitar").attr("disabled", !valid);    
}

function solicitar() {
    var tipo    = $("input[name=tipo]:checked").val();
    var detalle = $("#txtdetalleEsp").val().trim().toUpperCase();
    var sueldo  = $("input[name=sueldo]:checked").val();
    var data    = {tipo:tipo, sueldo:sueldo, detalle:detalle};
    execute("ConstanciaTrabajo.solicitar", processResults, data);
}

function processResults(response) {
    message(response.message, {showInMessageBox: true});    
}
