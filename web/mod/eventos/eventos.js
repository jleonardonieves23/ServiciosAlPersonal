$(function() {        
    execute("Calendario.calendarios", processEvents, {period: 'hoy'});
});

function processEvents(data) {
    var info;
    var element = $("#events");
    element.append("<h4>Eventos de hoy</h4>");
    
    if (data && data.length > 0) {
        for (var i in data) {
            info = data[i];     
            element.append("<div><b class='glyphicon glyphicon-info-sign'></b> " + info.descripcion + "<span>" + info.calendario + "</span></div>");
        }
    } else {
        element.append("<div>No hay eventos para hoy</div>");
    }        
}