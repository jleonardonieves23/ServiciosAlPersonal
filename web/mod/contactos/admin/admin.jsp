<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
    
<h4>Datos del colaborador</h4>
    
    <link href="mod/contactos/admin/admin.css" rel="stylesheet" />        
    <script src="mod/contactos/admin/admin.js"></script>
    <form id="user-form" enctype="multipart/form-data" onsubmit="return false;">
        <input type="hidden" name="id" />
        <input type="hidden" name="a" />
        <table>
            <tr><td></td><td width="300" valign="bottom"><img src="pic/avatar.jpg" align="bottom" id="picture"></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-picture" data-toggle="tooltip" title="Foto"></b></td><td><input type="file" class="hidden-control" id="file" name="file"><button class="btn btn-warning" onclick="changeImage()" id="btnChange">Cambiar</button></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-user" data-toggle="tooltip" title="C�digo"></b></td><td><input class="form-control" name="codigo" placeholder="c�digo" ></td><td><button class="btn btn-warning" onclick="searchContact()" id="btnSearchContact" data-loading-text="<i class='fa fa-circle-o-notch fa-spin'></i> Buscando...">Buscar</button></td></tr>
            <tr><td><b class="glyphicon glyphicon-user" data-toggle="tooltip" title="Nombre"></b></td><td><input class="form-control" name="nombre" placeholder="nombre" maxlength='30' ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-user" data-toggle="tooltip" title="Apellido"></b></td><td><input class="form-control" name="apellido" placeholder="apellido" maxlength='30' ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-star" data-toggle="tooltip" title="Cargo"></b></td><td><input class="form-control" name="cargo" placeholder="cargo" maxlength='30' ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-home" data-toggle="tooltip" title="Unidad"></b></td><td><input class="form-control" name="unidad" placeholder="unidad" maxlength='100'></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-file" data-toggle="tooltip" title="C�dula/RIF"></b></td><td><input class="form-control" name="rif" placeholder="c�dula/rif" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-envelope" data-toggle="tooltip" title="Correo Corporativo"></b></td><td><input class="form-control" name="correo_corporativo" placeholder="correo corporativo" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-envelope" data-toggle="tooltip" title="Correo Personal"></b></td><td><input class="form-control" name="correo_personal" placeholder="correo personal" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-earphone" data-toggle="tooltip" title="Tel�fono Oficina"></b></td><td><input class="form-control" name="telefono_oficina" placeholder="tel�fono oficina" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-earphone" data-toggle="tooltip" title="Tel�fono Personal"></b></td><td><input class="form-control" name="telefono_personal" placeholder="tel�fono personal" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-phone" data-toggle="tooltip" title="Celular Corporativo"></b></td><td><input class="form-control" name="celular_corporativo" placeholder="celular corporativo" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-phone" data-toggle="tooltip" title="Celular Personal"></b></td><td><input class="form-control" name="celular_personal" placeholder="celular personal" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-road" data-toggle="tooltip" title="Direcci�n"></b></td><td><input class="form-control" name="direccion" placeholder="direcci�n" maxlength='250' ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-certificate" data-toggle="tooltip" title="Fecha Cumplea�os"></b></td><td><input class="form-control" name="fecha_nacimiento" placeholder="fecha nacimiento" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-certificate" data-toggle="tooltip" title="Fecha Ingreso"></b></td><td><input class="form-control" name="fecha_ingreso" placeholder="fecha ingreso" ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-star" data-toggle="tooltip" title="C�digo Supervisor"></b></td><td><input class="form-control" name="codigo_supervisor" placeholder="c�digo supervisor"  ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-star" data-toggle="tooltip" title="Supervisor"></b></td><td><input class="form-control" name="nombre_supervisor" placeholder="nombre supervisor"  ></td><td></td></tr>
            <tr><td><b class="glyphicon glyphicon-star" data-toggle="tooltip" title="Status"></b></td><td><input class="form-control" name="status" placeholder="status" readonly ></td><td></td></tr>            
            <tr><td></td><td><button id="btnSave" class="btn btn-primary" onclick="save()">Aceptar</button></td><td></td></tr>
        </table>  
    </form>
                
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>
    
<%
    } //if (currentUser != null)
%>      