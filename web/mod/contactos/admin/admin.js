$(function() {
    setTitle("Administraci&oacute;n > Perfil Colaborador"); 
    $("#file").change(function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
               $('#picture').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }   
    });
    
   
    $('input[name="correo_corporativo"]').inputmask('Regex', { regex: "[a-zA-Z0-9._%-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,4}" });   
    /*
    $('input[name="codigo"]').inputmask('Regex', { regex: "[A-Za-z]{1,1}[0-9]{1,5}" });
    //$('input[name="nombre"]').mask(); 
    //$('input[name="apellido"]').mask(); 
    //$('input[name="cargo"]').mask(); 
    //$('input[name="unidad"]').mask(); 
    $('input[name="rif"]').mask("A000000000"); 
    //$('input[name="correo_corporativo"]').mask();
    //$('input[name="correo_personal"]').mask(); 
    $('input[name="telefono_oficina"]').mask("0000"); 
    $('input[name="telefono_personal"]').mask("(000)000.00.00"); 
    $('input[name="celular_corporativo"]').mask("(000)000.00.00"); 
    $('input[name="celular_personal"]').mask("(000)000.00.00"); 
    //$('input[name="direccion"]').mask(); 
    $('input[name="fecha_nacimiento"]').mask("00/00/0000"); 
    $('input[name="fecha_ingreso"]').mask("00/00/0000"); 
    $('input[name="codigo_supervisor"]').mask("A00000"); 
    //$('input[name="nombre_supervisor"]').mask(); 
    //$('input[name="status"]').mask(); 
    */
    
});

function searchContact() {
    clear_message();
    var codigo = $("input[name=codigo]").val().trim();          
    if (codigo == "" ) {
        message("Debe ingresar un c&oacute;digo para buscar");
    } else {
        var data = {
            search: codigo
        } 
       
        $("#user-form input").val("");
        $("input[name=codigo]").val(codigo);
        $("#picture").attr("src", "pic/avatar.jpg");
        $("#btnSearchContact").addClass("disabled");
        $("#btnSearchContact").prop("disabled", "disabled");
        $("#btnSearchContact").html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Buscando...");
        execute("Usuario.buscar", processSearchContact, data);
    }
}

function processSearchContact(data)  {       
    $("#btnSearchContact").removeClass("disabled");
    $("#btnSearchContact").removeProp("disabled");  
    $("#btnSearchContact").html("Buscar");    
    var status = clearNull(data.status);
    if (status == "true") {
        var user = data.usuario;                
        var form = $("#user-form");
        populate(form, user);
        if (user.picture) {
            $('#picture').attr('src', user.picture);
        }
    } else {
        message_not_found();
    }    
}

function changeImage() {
    $("#file").click();
}

function save() {
    $("#btnSave").addClass("disabled");
    $("#btnSave").prop("disabled", "disabled");
    $("#btnSave").html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Guardando...");              
    $("input[name=a]").val("Usuario.guardarPerfil");
    var form = $("#user-form");        
    executeWithFile(processSave, form);
}

function processSave(data) {
    $("#btnSave").removeClass("disabled");
    $("#btnSave").removeProp("disabled", "disabled");
    $("#btnSave").html("Aceptar");
    
    if (data != null && data.status == "true") {
        message_success({showInMessageBox: true});
    } else 
        message_error();
}