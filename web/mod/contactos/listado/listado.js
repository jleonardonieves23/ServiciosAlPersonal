$(function() {    
    setTitle("Colaboradores > Listado");    
    execute("Usuario.listar", processUsers);           
})

function processUsers(data) {    
    $(".usuarios").html("");
    for(var i in data) {
        var item = data[i];
        var html = "";
        var pic = item.codigo.substring(1);
        html += "<div>";
        html += "<img src='/pic/" + pic + ".png' />";
        html += "<span>" + item.codigo + "</span>";
        html += "<span>" + item.nombre + "<br>" + item.apellido + "</span>";
        //html += "<div>" + item.unidad + "</div>";
        //html += "<div>" + item.cargo + "</div>";        
        ///html += "<div>" + item.correo_corporativo + "</div>";
        //html += "<div>" + item.telefono_oficina + "</div>";
        html += "</div>";        
        $(".usuarios").append(html);        
    }
    $(".total").append("<p>Total registros: " + data.length + "</p>");
    $(".usuarios img").error(function () {
        $(this).unbind("error").attr("src", "img/avatar2.jpg");
    });    
}
