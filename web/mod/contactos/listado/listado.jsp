<%@page import="be.seguridad.Seguridad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
        String currentMonth = Util.getCurrentMonth();
        String currentYear = Util.getCurrentYear();
        
        //
        String nombre = usuario.obtenerNombreCompleto();
        String cedula = usuario.rif;
        String codigo = usuario.codigo;
        String fecha = Util.getCurrentDate();
%>
<link href="mod/contactos/listado/listado.css" rel="stylesheet" />        
<script src="mod/contactos/listado/listado.js"></script>

<h4>Listado</h4>
<div class="menu-container">    
    <div class="btn-back"><a href="/"><span class="glyphicon glyphicon-chevron-left"></span> Regresar</a></div>        
    <br>
    <div class='total'>           
    </div> 
    
    <div class='usuarios'>   
        <img src="img/ajax-loader.gif" /> 
    </div>
   
</div>  

<%
    } //if (currentUser != null)
%>    
