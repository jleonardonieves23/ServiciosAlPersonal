$(function() {    
    setTitle("Colaboradores");
    if (contactsNumber > 1) {
        $(".contact-grid").hide();  
    } else {
        $(".contact-list").hide();  
    }
})


function showContacts(list) {
    if (list) {
        $(".contact-grid").hide();
        $(".contact-list").show();
    } else {
        $(".contact-grid").show();
        $(".contact-list").hide();        
    }
}

function showBlock(obj) {
    var contact = $(obj).parents("div.usuario-list");
    var block = contact.find("div.usuario-block");    
    $.featherlight(block.html(), {type: "html"});    
}