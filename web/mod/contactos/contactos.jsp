<%@page import="be.seguridad.Seguridad"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.io.File"%>
<%@page import="javax.naming.directory.DirContext"%>
<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.ActiveDirectory"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%    
    String search                = Util.checkParam(request.getParameter("search"));
    ArrayList<Usuario> usuarios  = null;
    Usuario usuario              = null;
    String nombre                = "";
    String apellido              = "";
    String codigo                = "";
    String rif                   = "";
    String cargo                 = "";
    String unidad                = "";
    String correo_corporativo    = "";
    String telefono_oficina      = "";
    String direccion             = "";
    String status                = "";
    String avatar                = "pic/avatar.jpg";     
    String picture               = avatar;
    String path                  = "";
    String block                 = null;
    File   file                  = null;    
    String results               = null;
    String list                  = null;
    int    total                 = 0;
    String backDeco              = GUI.getButtonBackDeco();  
    search = search.replaceAll("&#x20;", " ").trim();
    if (search != null && !search.equals("")) {       
        search = search.replaceAll(" ", "*");        
        DirContext activeDirectoryContext = Seguridad.getContext();
        if (activeDirectoryContext != null) {
            ActiveDirectory ad = new ActiveDirectory(activeDirectoryContext);
            if (ad != null) {
                usuarios = ad.getUsers(search);    
                if (usuarios != null) {
                    int i   = 0;
                    results = "";
                    list    = "";
                    total   = usuarios.size();
                    while (usuarios!=null && i<total) {
                        usuario = usuarios.get(i++);
                        if (usuario != null) {
                            if (usuario.codigo != null && usuario.codigo.trim().length()>1) {
                                picture = "pic/" + usuario.codigo.trim().substring(1) + ".png";
                                path    = request.getSession().getServletContext().getRealPath(picture);
                                file    = new File(path);                
                                picture = file.isFile() ? picture : avatar;
                            }

                            nombre             = usuario.nombre != null && !usuario.nombre.equals("") ?  usuario.nombre : "<label>nombre</label>";        
                            apellido           = usuario.apellido != null && !usuario.apellido.equals("") ?  usuario.apellido : "<label>apellido</label>";        
                            codigo             = usuario.codigo != null && !usuario.codigo.equals("") ?  usuario.codigo : "<label>c�digo</label>";
                            rif                = usuario.rif != null && !usuario.rif.equals("") ?  usuario.rif : "<label>c�dula/rif</label>";
                            cargo              = usuario.cargo != null && !usuario.cargo.equals("") ?  usuario.cargo : "<label>cargo</label>";
                            unidad             = usuario.unidad != null && !usuario.unidad.equals("") ?  usuario.unidad : "<label>unidad</label>";
                            correo_corporativo = usuario.correo_corporativo != null && !usuario.correo_corporativo.equals("") ?  usuario.correo_corporativo : "<label>correo coportativo</label>";
                            telefono_oficina   = usuario.telefono_oficina != null && !usuario.telefono_oficina.equals("") ?  usuario.telefono_oficina : "<label>tel�fono oficina</label>";
                            direccion          = usuario.direccion != null && !usuario.direccion.equals("") ?  usuario.direccion : "<label>direcci�n</label>";
                            status             = usuario.status != null && !usuario.status.equals("") ?  usuario.status : "<label>estado de la cuenta</label>";
                            block = "";
                            block += "<div class=\"usuario\">";
                            block += "    <div class=\"usuario-picture\"><img src=\"" + picture + "\"></div>";
                            block += "    <div class=\"usuario-usuarioname\"><span>" + nombre + " " + apellido + "&nbsp;</span></div>";        
                            block += "    <div class=\"usuario-code\"><b class=\"glyphicon glyphicon-usuario\"></b> <span>" + codigo + "&nbsp;</span></div>";
                            //block += "    <div class=\"usuario-rif\"><b class=\"glyphicon glyphicon-usuario\"></b> <span>" + rif + "&nbsp;</span></div>";
                            block += "    <div class=\"usuario-title\"><b class=\"glyphicon glyphicon-star\"></b> <span>" + cargo + "&nbsp;</span></div>";    
                            block += "    <div class=\"usuario-department\"><b class=\"glyphicon glyphicon-home\"></b> <span> " + unidad + "&nbsp;</span></div>";
                            block += "    <div class=\"usuario-email\"><b class=\"glyphicon glyphicon-envelope\"></b> <span><a href=\"mailto:" + correo_corporativo + "\">" + correo_corporativo+ "</a>&nbsp;</span></div>";
                            block += "    <div class=\"usuario-phone\"><b class=\"glyphicon glyphicon-earphone\"></b> <span>" + telefono_oficina + "&nbsp;</span></div>";
                            block += "    <div class=\"usuario-location\"><b class=\"glyphicon glyphicon-road\"></b> <span>" + direccion + "&nbsp;</span></div>";
                            block += "    <div class=\"usuario-status\"><b class=\"glyphicon glyphicon-exclamation-sign\"></b> <span>" + status + "&nbsp;</span></div>";
                            block += "</div>";   
                            results += block;

                            list += "<div class=\"usuario-list\">";
                            list += "    <div class=\"usuario-usuarioname\"><a href='#' onclick='showBlock(this)'><span>" + nombre + " " + apellido + "&nbsp;</span></a></div>";        
                            list += "    <div class=\"usuario-code\"><span>" + codigo + "&nbsp;</span></div>";
                            list += "    <div class=\"usuario-title\"><span>" + cargo + "&nbsp;</span></div>";    
                            list += "    <div class=\"usuario-department\"><span> " + unidad + "&nbsp;</span></div>";
                            list += "    <div class='usuario-block' style='display: none'>" + block + "</div>";
                            list += "</div>";
                        }
                    }
                    activeDirectoryContext.close();
                } 
            } 
        } 
   
%>
    <h4>Datos del colaborador</h4>
    
    <link href="mod/contactos/contactos.css" rel="stylesheet" />        
    <script src="mod/contactos/contactos.js"></script>
    <script>
        var contactsNumber = <%=total%>;
    </script>
    <% if (results != null && !results.equals("")) { %>
        <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div> 
        <div class="contact-content">
            <div class="buttons">
                <a href="#" onclick="showContacts(false)"><span class="glyphicon glyphicon-align-justify"></span> Extendido</a>
                <a href="#" onclick="showContacts(true)"><span class="glyphicon glyphicon-th"></span> Resumido</a>
            </div>

            <div class="contact-grid">
                <%=results%>
            </div>

            <div class="contact-list">
                <%=list%>
            </div>  
        </div>
    <% } //if (resuls != null ) 
    else { %>
        <div class="no-data">No hay datos para mostrar</div>
    <% } //if (resuls != null )  %> 
    <br />
    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>
    <% } //session new %> 