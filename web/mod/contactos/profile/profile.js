$(function() {
    setTitle("Mi Perfil");        
    $("#file").change(function(){
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
               $('#picture').attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }   
    });    
    populate($("#user-form"), currentUser);  
    $('#picture').attr('src', currentUser.picture);
});

function changeImage() {
    $("#file").click();
}

function save() {
    $("#btnSave").addClass("disabled");
    $("#btnSave").prop("disabled", "disabled");
    $("#btnSave").html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Guardando...");              
    $("input[name=a]").val("Usuario.guardarPerfil");
    var form = $("#user-form");        
    executeWithFile(processSave, form);
}

function processSave(data) {
    $("#btnSave").removeClass("disabled");
    $("#btnSave").removeProp("disabled", "disabled");
    $("#btnSave").html("Aceptar");
    
    if (data != null && data.status == "true") {
        message_success({showInMessageBox: true});
        //call("contactos/profile");
    } else 
        message_error();
}