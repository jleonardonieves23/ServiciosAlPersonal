$(function() {
    setTitle("Administraci&oacute;n > Consultar Seguros de Colaborador");        
    $("#colaborador").on("keypress", function(e) {       
        if ( e.which == 13 ) {
            buscarPoliza();
        }       
    });
    enmascararColaborador();
    $("#loading").hide();
});

function buscarPoliza() {
    $(".colaborador").html("");
    $(".data").html("");
    var colaborador = $("#colaborador").val();
    $("#loadig").show();
    execute("Usuario.buscar", processConsultaColaborador, {search:colaborador});
    execute("Seguros.consultarInfoPoliza", processConsultarInfoPoliza, {colaborador:colaborador});        
}

function processConsultaColaborador(data) { 
    $("#loading").hide();
    var content = $(".colaborador");
    if (data.status != "false") {
        content.html("<h5>Colaborador</h5><h2>" + data.usuario.nombre + " " + data.usuario.apellido + "</h2>");
    } 
}

function processConsultarInfoPoliza(data) {   
    $("#loading").hide();
    var content = $(".data");
    if (data.status != "false") {
        var rows = data.rows;
        if (!rows || rows.length == 0) {
            content.html("No hay informaci&oacute;n para mostrar");
        } else {
            var row;
            var check = "<span class='glyphicon glyphicon-ok'></span>";
            var html   = "";
            var tipos  = [false, false, false, false];
            html += "<table class='table table-strip'>";
            html += "<tr>";
            html += "<th>Nombre</th>";
            html += "<th>Nombramiento</th>";
            html += "<th>Parentesco</th>";
            html += "<th>C&eacute;dula</th>";
            html += "<th>G&eacute;nero</th>";
            html += "<th>Nacimiento</th>";                       
            html += "<th>B&aacute;sico</th>"; 
            html += "<th>Exceso I</th>"; 
            html += "<th>Exceso II</th>"; 
            html += "<th>Exceso Extra.</th>"; 
            html += "</tr>";            
            //var cedula = "";
            var nombre = "";
            var tr = "";           
            for(var i=0; i<rows.length; i++) {                
                row = rows[i];
                //if (cedula != row.cedula) {
                //    cedula = row.cedula;                    
                if (nombre != row.nombre) {
                    nombre = row.nombre;                      
                    if (tr) {                        
                        html += "<td class='concepto'>" + (tipos[0] ? check :  "") + "</td>"; 
                        html += "<td class='concepto'>" + (tipos[1] ? check :  "") + "</td>"; 
                        html += "<td class='concepto'>" + (tipos[2] ? check :  "") + "</td>"; 
                        html += "<td class='concepto'>" + (tipos[3] ? check :  "") + "</td>"; 
                        tipos = [false, false, false, false];                     
                    }
                    html += tr + "<tr>";
                    html += "<td class='nombre'>" + row.nombre.toLowerCase() + "</td>";
                    html += "<td class='tipo'>" + (row.tipo==0 ? "Titular" : "Pariente") + "</td>";
                    html += "<td class='parentesco'>" + row.parentezco.toLowerCase() + "</td>";
                    html += "<td class='cedula'>" + row.cedula + "</td>";
                    html += "<td class='genero'>" + row.genero.toLowerCase() + "</td>";
                    html += "<td class='fecha'>" + dateFormat(row.fecha_nacimiento) + "</td>";
                    tr = "</tr>";                    
                }                
                tipos[0] = tipos[0] || row.cod_concepto.toUpperCase() == "HCM";
                tipos[1] = tipos[1] || row.cod_concepto.toUpperCase() == "EX1";
                tipos[2] = tipos[2] || row.cod_concepto.toUpperCase() == "EX2";
                tipos[3] = tipos[3] || row.cod_concepto.toUpperCase() == "EXC3" || row.cod_concepto.toUpperCase() == "EX3";                              
            }    
            html += "<td class='concepto'>" + (tipos[0] ? check :  "") + "</td>"; 
            html += "<td class='concepto'>" + (tipos[1] ? check :  "") + "</td>"; 
            html += "<td class='concepto'>" + (tipos[2] ? check :  "") + "</td>"; 
            html += "<td class='concepto'>" + (tipos[3] ? check :  "") + "</td>"; 
            html += "</tr></table>";
                    
            html += "<hr >";
            html += data.description ? data.description : "";
            content.html(html);        
        }
        
    }
}

