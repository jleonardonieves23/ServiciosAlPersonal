$(function() {
    setTitle("Preferencias");    
    $(".fondos option").css("padding-left", "40px");
    $(".fondos option").css("background-repeat", "no-repeat");
    $(".fondos option").css("background-size", "40px auto");
    $("#fondo1").css("background-image", "url('/img/wallpapers/wallpaper1.jpg')");
});

function actualizarFuente() {
    var fuente = $("#fuente").val();
    cambiarFuente(fuente);
}

function actualizarFondo() {
    var fondo = $("#fondo").val();
    cambiarFondo(fondo);
}

function zoomin() {
    zoom(1.2);
}

function zoomout() {
   zoom(0.8);
}

function zoomnormal() {
   zoom(1);
}

function restoreSettings() {
    zoomnormal();
    cookie("fuente", BE_FUENTE);
    cookie("fondo", BE_FONDO);
    cookie("zoom", null);
}