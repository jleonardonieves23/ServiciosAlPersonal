<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/preferencias/preferencias.css" rel="stylesheet" />        
<script src="mod/preferencias/preferencias.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class='field'>
    <label>Fuente:</label>
    <select id='fuente' class='form-control' onchange='actualizarFuente()'>
        <option style="font-family: 'Arial Narrow'">Arial Narrow</option>
        <option style="font-family: 'Arial'">Arial</option>
        <option style="font-family: 'Calibri'">Calibri</option>
        <option style="font-family: 'Century Gothic'">Century Gothic</option>
        <option style="font-family: 'Courier New'">Courier New</option>
        <option style="font-family: 'Georgi'">Georgi</option>
        <option style="font-family: 'Lucida Sans'">Lucida Sans</option>
        <option style="font-family: 'Microsoft Sans Serif '"> Microsoft Sans Serif</option>
        <option style="font-family: 'Tahoma'">Tahoma</option>
        <option style="font-family: 'Times News Roman'">Times News Roman</option>
        <option style="font-family: 'Trebuchet MS'">Trebuchet MS</option>
        <option style="font-family: 'Verdana'">Verdana</option>
    </select>
</div> 
<br/>
<div class='field fondos'>
    <label>Fondo:</label>
    <select id='fondo' class='form-control' onchange='actualizarFondo()'>
        <option value='1' id='fondo1'>Fondo 1</option>
        <option value='2' id='fondo3'>Fondo 2</option>
        <option value='3' id='fondo3'>Fondo 3</option>
        <option value='4' id='fondo4'>Fondo 4</option>
        <option value='5' id='fondo5'>Fondo 5</option>
        <option value='6' id='fondo6'>Fondo 6</option>
        <option value='7' id='fondo7'>Fondo 7</option>
        <option value='8' id='fondo8'>Fondo 8</option>
        <option value='9' id='fondo9'>Fondo 9</option>
        <option value='10' id='fondo10'>Fondo 10</option>
        <option value='11' id='fondo11'>Fondo 11</option>
        <option value='12' id='fondo12'>Fondo 12</option>
        <option value='13' id='fondo13'>Fondo 13</option>
        <option value='14' id='fondo14'>Fondo 14</option>
        <option value='15' id='fondo15'>Fondo 15</option>
        <option value='16' id='fondo16'>Fondo 16</option>      
    </select>
</div>
<br/>
<div><button class='btn btn-primary' onclick='zoomin()'>Ampliar</button> <button class='btn btn-primary' onclick='zoomnormal()'>Ajustar</button> <button class='btn btn-primary' onclick='zoomout()'>Reducir</button></div>
<br/>
<div><a href='#restablecer' name='restablecer' onclick='restoreSettings()'>Restablecer la configuraci&oacute;n por defecto</a></div>
<br/>
<div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div> 

<%
    } //if (currentUser != null)
%>    
