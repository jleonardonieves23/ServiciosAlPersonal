<%@page import="be.seguridad.Seguridad"%>
<%@page import="be.utils.Util"%>
<%
    String[] keys          = Util.obtenerParLlaves(request);
    String   publicKeyMod  = keys[0];
    String   publicKeyExp  = keys[1];        
%>

<link   href = "mod/login/login.css" rel="stylesheet" />        
<script src  = "mod/login/login.js"></script>
<script src  = "lib/encrypt/aes.js"></script>
<script src  = "lib/encrypt/api.js"></script>
<script src  = "lib/encrypt/hash.js"></script>
<script src  = "lib/encrypt/jsbn.js"></script>
<script src  = "lib/encrypt/random.js"></script>
<script src  = "lib/encrypt/rsa.js"></script>  
        
<form id="login-form" onsubmit="return false;">
    <input type="hidden" id="login-key" value="<%=publicKeyMod%>">
    <input type="hidden" id="login-exp" value="<%=publicKeyExp%>">
    <div>
        <input id="login" type="text" class="form-control" placeholder="usuario" />
    </div>
    <div>
        <input id="password" type="password" maxlength="20" class="form-control" placeholder="contraseņa" />
    </div>
    <div>
        <button id="btnLogin" class="btn btn-primary">Aceptar</button>
    </div>
</form>
