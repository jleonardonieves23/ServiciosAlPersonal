$(function() {
    $("#login").focus();
    $("#btnLogin").on("click", login);     
    $("#login").inputmask('Regex', { regex: "[A-Za-z]{1,1}[0-9]{1,5}" });
});

var LOGIN_BLANK = "Debe introducir el usuario para iniciar sesi&oacute;n"
var PASS_BLANK = "Debe introducir la contrase\u00f1a para iniciar sesi&oacute;n"

//FUNCTIONS
function login() { 
    
    if($("#login").val() === ""){
        
        message(LOGIN_BLANK, {showInMessageBox:true});  
        $("#login").focus();  
    }
    else if($("#password").val() === ""){
        
        message(PASS_BLANK, {showInMessageBox:true});    
        $("#password").focus();  
    }
    else
    {
        $("#btnLogin").addClass("disabled");
        $("#btnLogin").prop("disabled", "disabled");
        showLoading();
        var login      = $("#login").val();
        var password   = encrypt($("#password").val());
        var data       = { login: login, password: password };    
        execute("Login.procesar", processLogin, data);   
    }
}

function processLogin(data) {
    $("#btnLogin").removeClass("disabled");
    $("#btnLogin").removeProp("disabled");
    hideLoading();
    if (data.status == "true") {
        call("home");
    }  else {                
        message(data.message, {showInMessageBox:true, callback: function(){ setTimeout( function() { $("#password").focus();}, 100);}});            
        $("#password").val("");
       
    }
}

function encrypt(value) {       
    return encode64(value);
}


function encode64(input) {
 var keyStr = "ABCDEFGHIJKLMNOP" +
               "QRSTUVWXYZabcdef" +
               "ghijklmnopqrstuv" +
               "wxyz0123456789+/" +
               "=";
       
   //input = escape(input);
   var output = "";
   var chr1, chr2, chr3 = "";
   var enc1, enc2, enc3, enc4 = "";
   var i = 0;

   do {
      chr1 = input.charCodeAt(i++);
      chr2 = input.charCodeAt(i++);
      chr3 = input.charCodeAt(i++);

      enc1 = chr1 >> 2;
      enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
      enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
      enc4 = chr3 & 63;

      if (isNaN(chr2)) {
         enc3 = enc4 = 64;
      } else if (isNaN(chr3)) {
         enc4 = 64;
      }

      output = output +
         keyStr.charAt(enc1) +
         keyStr.charAt(enc2) +
         keyStr.charAt(enc3) +
         keyStr.charAt(enc4);
      chr1 = chr2 = chr3 = "";
      enc1 = enc2 = enc3 = enc4 = "";
   } while (i < input.length);

   return output;
}
