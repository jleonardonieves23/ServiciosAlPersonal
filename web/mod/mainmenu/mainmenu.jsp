<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/mainmenu/mainmenu.css" rel="stylesheet" />        
<script src="mod/mainmenu/mainmenu.js"></script>
<script>
    var deco = '<%=GUI.getMenuDeco()%>';
    var decoLogout = '<%=GUI.getMenuLogoutDeco()%>';
</script>

<div class="menu">
    <h4>Men&uacute; Principal</h4>
    <div class="menu-container">
    </div>
</div>
    
<%
    } //if (currentUser != null)
%>    