<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>
<link href="mod/arbol_organizacional/arbol_organizacional.css" rel="stylesheet" />        
<script src="mod/arbol_organizacional/arbol_organizacional.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">
    
    <h4>Men&uacute; &Aacute;rbol Organizacional</h4>
    
    <div id="submenu" class="menu-container"></div>

    <div class="btn-back"><a href="?a=home"><%=backDeco%> Regresar</a></div>
    
</div>     

<%
    } //if (currentUser != null)
%>    
