$(function() {
    setTitle("Árbol Organizacional (Admin)");
    execute("Menu.get", processMenuArbolOrganizacional, {context: 'arbol_organizacional'});    
});

function processMenuArbolOrganizacional(data) {
    var menu = $("#submenu");
    buildMenu(menu, data);
}
