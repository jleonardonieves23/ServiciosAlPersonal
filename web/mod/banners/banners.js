var BANNERS = [];
var BANNER_TIME = 5000;
var BANNERS_ACTIVE_CANVAS = 0;

$(function() {
    execute("Banner.obtener", processBanners);
});

function getBanner() {
    return "mod/banners/img/" + BANNERS[ Math.floor(BANNERS.length * Math.random())];
}

function processBanners(data) {
    BANNERS = data.images; 
    $("#banner").hide();
    var banner1 = getBanner();
    var banner2 = getBanner();    
    $("#banner").append("<img id='banner1' src='"+banner1+"'>");
    $("#banner").append("<img id='banner2' src='"+banner2+"'>");    
    $("#banner2").hide();   
    $("#banner").show();
    slideShow();    
}

function slideShow() {
    var banner = getBanner();
    var banner1 = $(BANNERS_ACTIVE_CANVAS == 0 ? "#banner1" : "#banner2");
    var banner2 = $(BANNERS_ACTIVE_CANVAS == 0 ? "#banner2" : "#banner1");
    BANNERS_ACTIVE_CANVAS = BANNERS_ACTIVE_CANVAS == 0 ? 1 : 0;        
    var oldBanner = banner1.attr("src");
    if (oldBanner == banner) {
        banner = getBanner();
    }
    banner2.attr("src", banner);
    banner1.fadeOut("slow");                
    banner2.fadeIn("slow", function() {        
        setTimeout(slideShow,  BANNER_TIME);
    });
}
