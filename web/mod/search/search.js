$(function() {
   $("#search-input").on("keypress", function(e) {       
    if ( e.which == 13 ) {
        checkSearch();
    }       
   })  
   $('#search-input').inputmask('Regex', { regex: "[ a-zA-Z0-9._%-]{1,50}" });   
   
});
function checkSearch() {
    var search = $("#search-input").val().trim();
    if (search == "") {
        message("Debe ingresar un valor para buscar", {showInMessageBox: true});        
    } else {  
        showLoading();
        $("#search-button").addClass("disabled");
        $("#search-button").prop("disabled", "disabled");
        $("#search-button").html("<span class='glyphicon glyphicon-refresh glyphicon-refresh-animate'></span> Buscando...");        
        $("#search-form").removeAttr("onsubmit");
        $("#search-form").removeProp("onsubmit");
        $("#search-form").submit();
    }
}