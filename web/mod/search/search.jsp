<%@page import="be.utils.Util"%>
<%
        String search = Util.checkParam(request.getParameter("search"));
%>
<link href="mod/search/search.css" rel="stylesheet" />        
<script src="mod/search/search.js"></script>

<form id="search-form" action="/ServiciosAlPersonal/" onsubmit="return false;">
    <input type="hidden" name="a" value="contactos">
    <table align="left" width="100%">
        <tr>
            <td class="" align="left">
                <input type="text" id="search-input" name="search" class="form-control" title="Buscar" placeholder="Buscar colaborador" value="<%=search%>" />   
            </td>
            <td align="left">
                <input type="button" id="search-button" class="btn btn-primary" onclick="checkSearch()" value="Buscar" />
            </td>
        </tr>
    </table>
</form>