var ENTITY = "Info";
var FIELD_ID = "id";
var TABLE_OPTIONS = { columns: [         			        
        {title: 'T&iacute;tulo', name: 'titulo'},
        {title: 'Texto', name: 'texto'},
        ]} ;
$(function() {
    setTitle("Administraci&oacute;n > Contenido Informativo");
    loadEntities({order: 'titulo', export: false, editable: false});            
});