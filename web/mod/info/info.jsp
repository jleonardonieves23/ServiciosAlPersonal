<%@page import="be.gui.GUI"%>
<%@page import="be.entidades.Usuario"%>
<%@page import="be.utils.Util"%>
<%
    Usuario usuario = (Usuario) Util.obtenerUsuarioActual(request);
    String action = Util.obtenerAccion(request);
    if (usuario != null) { 
        String currentDate = Util.getLongCurrentDate();
        String deco = GUI.getMenuDeco();
        String decoLogout = GUI.getMenuLogoutDeco();
        String backDeco = GUI.getButtonBackDeco();
%>

<link href="mod/info/info.css" rel="stylesheet" />        
<script src="mod/info/info.js"></script>
<script>
        var deco = '<%=deco%>';
</script>

<div class="submenu menu-content">

    <h4>Listado de contenido</h4>
    
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>
   <!-- <button class="btn btn-primary" onclick="add()">Agregar</button>  -->              
    <div class="listdata"></div>
    
    <div class="btn-back"><a href="?a=admin"><%=backDeco%> Regresar</a></div>    
</div>     

<%
    } //if (currentUser != null)
%>    
